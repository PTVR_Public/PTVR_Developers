# PTVR

## Description
[Perception Toolbox for Virtual Reality](https://ptvr.inria.fr/) 

## Getting started

Matériel pour utiliser PTVR:
- Casque HTC Vive Pro Eyes
- Base de Stations
- PC (préferer toujours un ordinateur VR Ready)

La configuration minimum recommandé pour le pc est :
Système d'exploitation : Windows 7 SP1, Windows 8.1 ou plus récent, Windows 10
Processeur : Intel Core i5-4590/AMD FX 8350 equivalent ou mieux
Mémoire vive : 4 GB de mémoire
Graphiques : NVIDIA GeForce GTX 1060, AMD Radeon RX 480 equivalent ou mieux

Prévoir un câble mini display port vers mini display port (car la plupart des ordinateurs portables
possèdent uniquement un mini display port et pas de display port).

## Branchement Casque HTC Vive Pro Eyes
![Boitier liaison Casque](https://notes.inria.fr/uploads/upload_40c71929308126f4d5990c5ad8ee9a53.png)

Brancher le câble du HTC VIVE Pro Eye dans le **port 1**.

![Branchement Casque](https://notes.inria.fr/uploads/upload_06b1a6f0e7b8bf907e8154cb13ffd36a.png)

Brancher le chargeur et le brancher sur le **port 3**

Brancher au boîtier le câble mini display port sur le **port 4** et relier au port mini display de votre ordinateur ( noter que si votre ordinateur posséde directement un port display utiliser  directement le câble fourni avec le casque)

Brancher au botier le câble usb sur le **port 5** et le relier vers un port usb 3.0 de votre pc.

Une fois les câbles branchés la dernière étape est d’appuyer sur le **bouton 2,** un voyant vert devrait s’allumer sur le dessus.

Brancher les bases de stations.

![base station](https://notes.inria.fr/uploads/upload_3e718fbb6e98ccbc7d9e43daca2e90f5.png)


## Installation Casque HTC Vive Pro Eyes
Si vous ne possédé pas déjà Steam, allez à l’adresse suivante : [https://store.steampowered.com/about/](https://store.steampowered.com/about/) pour le télécharger.

Si vous n’avez pas de Compte Steam, créez-en un, il sera utile pour installer SteamVR.

Une fois télécharger installer Steam.

Connecter vous à Steam puis chercher [SteamVR]([https://store.steampowered.com/app/250820/SteamVR/?l=french](https://store.steampowered.com/app/250820/SteamVR/?l=french)) et cliqué sur installer.

Une fois installé, lancer SteamVR , la fenêtre suivante doit s’ouvrir :

![SteamVR](https://notes.inria.fr/uploads/upload_8703904a70d78db5a4c0fdd9a505a626.png)

En appuyant sur les 3 barres, un menu s’ouvre, il faut séléctionner Configuration de la Pièce

![Configuration](https://notes.inria.fr/uploads/upload_41ee48a1c40c3f64674bd17a6a87d77b.png)

Il faut choisir ce qui correspond le mieux à la calibration souhaitée pour le projet.

![mode htc.PNG](HTC%20VIVE%20PRO%20EYE%20a55fc093a0d140d9946cc81d58102625/mode_htc.png)

A noté que chaque fois qu’un programme aura besoin de Steam VR pour fonctionner, le programme lancera SteamVR si celui-ci n’est pas déjà lancé.

### Messages d’erreurs de connexion du Casque : pistes de solutions proposées par HTC VIVE

[Solutions proposées](https://www.vive.com/fr/support/cosmos/category_howto/what-do-these-error-codes-and-messages-mean.html)

## Installation système d'eyestracking HTC Vive Pro Eyes

Allez télécharger [SRanipal]([https://developer-express.vive.com/resources/vive-sense/eye-and-facial-tracking-sdk/download/latest/](https://developer-express.vive.com/resources/vive-sense/eye-and-facial-tracking-sdk/download/latest/)) et installer le.

## Calibration des Yeux

Une fois SteamVR installé et lancé, il est possible d’enfiler le casque et d’appuyer avec une des manettes sur le **bouton 3** et de séléctionner le bouton en forme d’oeil, pour calibrer les yeux, puis laisser vous guider par le tutoriel en selectionnant le bouton calibrate.
![Calibrate](https://notes.inria.fr/uploads/upload_26b4e36d4bd8a85e2b61da941eb2beaf.png)


## Installation de PTVR

Pour créer une experience il faut un ide pour python, PTVR a été conçu et testé grâce à [Spyder](https://www.spyder-ide.org)
Une fois Spyder installé, il faut ensuite relier le chemin à la library PTVR


faire le chemin pour Spyder relier à PTVR

Installer Template PTVR
How to integrate the spyder's module template?

### Template to create PTVR experiment

A default template has been created to help to create the PTVR's demos and experiments more quickly using spyder.
An easy way to use this template is described below

#### Code template
```
# -*- coding: utf-8 -*-
import PTVR.Experiment # Used to create the experiment
import PTVR.SystemUtils # Used to launch the experiment
import PTVR.Stimuli.World # Used to create the scene , visual object , ui object
#import PTVR.Stimuli.Color as color # Used for color in PTVR's objects
#import PTVR.Data.Input as input # Used for PTVR's input 
#import PTVR.Data.Event as event # Used for PTVR's event 

#import PTVR.PointingCursor.PointingCursor # Used to create PointingCursors
#import PTVR.PointingCursor.CursorGenerator as CG # Used to create a custom Reticle

#import numpy as np # np.array is internally used in PTVR.(eg position)

# =============================================================================
#                               PARAMETERS                                    #
# =============================================================================
username = "Doe"
   
text_description ="""
Goal: 

How To Use:  

Predictions:   

Doc: 
    
Created on: %(date)s
@author: %(username)s
"""

dist_xp_m = 500 # 500 m remove parallax effect
# =============================================================================
#                              END PARAMETERS                                 #
# =============================================================================

def main():
    exp = PTVR.Experiment.Experiment(detailsExperiment=text_description)
    myScene = PTVR.Stimuli.World.VisualScene ()
    exp.add_scene(myScene)
    exp.write() # Write the Experiment to .json file
    
    
#   __main__ if statement allows to import this demo
if __name__ == "__main__":
            main()
            PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
```

#### How to integrate it in spyder
Tools->Advanced settings->Edit template for new files

![](https://notes.inria.fr/uploads/upload_b043136efa90d5d61c0e236778b8652f.png)
![](https://notes.inria.fr/uploads/upload_ab0260893da95562f950c618d509e39a.png)
![](https://notes.inria.fr/uploads/upload_39038a8b52ac7752f368215b26295e8d.png)
![](https://notes.inria.fr/uploads/upload_ecf8aa573bede09e6c3491fb87240670.PNG)

Copy/paste the code up there in the window, then save.
When you'll create a new file this base code will be automatically set. Then you'll just have to complete it with the full description.


## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
