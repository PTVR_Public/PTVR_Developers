using UnityEngine;
using System.Collections;
using System.IO;

public class ImportObj : MonoBehaviour
{
    public Shader shader;

    public GameObject go;
    void Update()
    {
        LoadOBJ();
    }

    //LoadTexture
    public Texture2D LoadTexture(string _path)
    {
        Texture2D _texture = new Texture2D(1, 1);
        byte[] _bytes = File.ReadAllBytes(_path);
        _texture.LoadImage(_bytes);
        return _texture;
    }

    //Create Material
    public Material CreateMaterial()
    {
        Material _material = new Material(shader);
        _material.mainTexture = LoadTexture("C:/Users/jtermozm/Desktop/TestTexture.jpg");
        return _material;
    }

    //Load OBJ
    public void LoadOBJ()
    {
        go.GetComponent<MeshRenderer>().material = CreateMaterial();
    }

}
