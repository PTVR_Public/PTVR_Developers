using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    public float speed;
    public float x;
    public float y;
    public float z;
    public float tempZ;
    public bool isGlobal;
    public GameObject current_CS;
    void Start()
    {
        tempZ = z;
    }

    // Update is called once per frame
    void Update()
    {
        if (isGlobal)
        {
            Quaternion rotationsX = Quaternion.AngleAxis(x * Time.deltaTime * speed, Vector3.right);
            Quaternion rotationsY = Quaternion.AngleAxis(y * Time.deltaTime * speed, Vector3.up);
            Quaternion rotationsZ = Quaternion.AngleAxis(z * Time.deltaTime * speed, Vector3.forward);
            x += x * Time.deltaTime;
            y += y * Time.deltaTime;
            z += tempZ * Time.deltaTime;
            if (z > 360)
            {
                z = tempZ;
            }
            if (transform.parent != null)
            {


                Debug.Log(transform.rotation.eulerAngles);
                //transform.Rotate(new Vector3(x * Time.deltaTime * speed, y * Time.deltaTime * speed, z * Time.deltaTime * speed), Space.World);
                //transform.RotateAround(transform.position, Vector3.forward, z);

                transform.localRotation = Quaternion.Euler(-transform.parent.rotation.eulerAngles + new Vector3(x * speed, y * speed, z * speed));
                /* transform.rotation *= rotationsX;
                 transform.rotation *= rotationsY;
                 transform.rotation *= rotationsZ;*/


            }
            else
            {
                transform.rotation *= rotationsX;
                transform.rotation *= rotationsY;
                transform.rotation *= rotationsZ;
            }

        }
        else
        {
            Quaternion rotationsX = Quaternion.AngleAxis(x * Time.deltaTime * speed, current_CS.transform.right);
            Quaternion rotationsY = Quaternion.AngleAxis(y * Time.deltaTime * speed, current_CS.transform.up);
            Quaternion rotationsZ = Quaternion.AngleAxis(z * Time.deltaTime * speed, current_CS.transform.forward);
            transform.rotation *= rotationsX;
            transform.rotation *= rotationsY;
            transform.rotation *= rotationsZ;
        }
    }
}
