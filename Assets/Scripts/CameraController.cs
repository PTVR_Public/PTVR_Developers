using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    #region Inspector
    public Material matRenderer;
    public Material matPhoto;
    public float fov = 10;
    #endregion Inspector

    #region Privates
    [SerializeField]
    Material baseMat;
    float defaultFov;
    Camera cam;
    bool captureEnabled = false;

    #endregion Privates

    public void Setup()
    {
        RenderTexture render = new RenderTexture(2048, 2048, 32, RenderTextureFormat.ARGB32);
        render.Create();
        cam = GetComponent<Camera>();
        cam.targetTexture = render;
        cam.fieldOfView = fov;
        defaultFov = fov;
        matRenderer = new Material(baseMat);
        matRenderer.SetTexture("_MainTex", render);
    }

    public void SetupShot()
    {/*
        matPhoto = new Material(baseMat);

        Graphics.Blit(source, destination, matPhoto);
        SetVisualSettings.instance.GameObjectCol[id].transform.GetChild(0).GetChild(0).GetComponent<Renderer>().sharedMaterial = matPhoto;
        SetVisualSettings.instance.GameObjectCol[id].layer = 10;
        SetVisualSettings.instance.GameObjectCol[id].transform.GetChild(0).gameObject.layer = 10;
        SetVisualSettings.instance.GameObjectCol[id].transform.GetChild(0).GetChild(0).gameObject.layer = 10;

        /*
           Camera camOV = cam;
           RenderTexture currentRT = RenderTexture.active;
           RenderTexture.active = camOV.targetTexture;
           camOV.Render();
           Texture2D imageOverview = new Texture2D(camOV.targetTexture.width, camOV.targetTexture.height, TextureFormat.RGB24, false);

           RenderTexture.active = currentRT;
           imageOverview.ReadPixels(new Rect(0, 0, camOV.targetTexture.width, camOV.targetTexture.height), 0, 0);
           imageOverview.Apply();
           matPhoto = new Material(baseMat);
           matPhoto.SetTexture("_MainTex", imageOverview);
           /*
           // Encode texture into PNG
           byte[] bytes = imageOverview.EncodeToPNG();


           // Capturez la vue actuelle de la cam�ra et appliquez-la � la texture
           cam.targetTexture = RenderTexture.GetTemporary(cam.pixelWidth, cam.pixelWidth, 32);
           cam.Render();
           RenderTexture.active = cam.targetTexture;
           screenshot.ReadPixels(new Rect(0, 0, cam.pixelWidth,  cam.pixelHeight), 0, 0);
           screenshot.Apply();
           matPhoto = new Material(baseMat);
           matPhoto.SetTexture("_MainTex", screenshot);
           */
    }
    public IEnumerator TakeScreenShot(long id)
    {
        yield return new WaitForEndOfFrame();

        Camera camOV = cam;
        RenderTexture currentRT = RenderTexture.active;
        RenderTexture rt = RenderTexture.active;
       /* RenderTexture.active = camOV.targetTexture;
        camOV.Render();
        Texture2D imageOverview = new Texture2D(camOV.targetTexture.width, camOV.targetTexture.height, TextureFormat.RGB24, false);
        imageOverview.ReadPixels(new Rect(0, 0, camOV.targetTexture.width, camOV.targetTexture.height), 0, 0);
        imageOverview.Apply();
        RenderTexture.active = currentRT;
        byte[] bytes = imageOverview.EncodeToPNG();
        matPhoto = new Material(baseMat);
        matPhoto.SetTexture("_MainTex", imageOverview);*/

        
    }

    private void Update()
    {
        if(cam.fieldOfView != fov)
        {
            cam.fieldOfView = fov;
        }
    }


    public void Reset()
    {
        cam.fieldOfView = defaultFov;
    }
    public void SetMaterialToObject(long id)
    {
        SetVisualSettings.instance.GameObjectCol[id].transform.GetChild(0).GetChild(0).GetComponent<Renderer>().sharedMaterial = matRenderer;
        SetVisualSettings.instance.GameObjectCol[id].layer = 10;
        SetVisualSettings.instance.GameObjectCol[id].transform.GetChild(0).gameObject.layer = 10;
        SetVisualSettings.instance.GameObjectCol[id].transform.GetChild(0).GetChild(0).gameObject.layer = 10;
  
        //  SetVisualSettings.instance.GameObjectCol[id].transform.GetChild(0).GetChild(1).gameObject.layer = 10;
    }
    public void SetPhotoToObject(long id)
    {
        StartCoroutine(TakeScreenShot(id));
     
    }
}
