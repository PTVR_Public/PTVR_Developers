using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellPage : MonoBehaviour
{
    public GameObject textDetails;
    public GameObject textName;
    public GameObject buttonUse;
    public GameObject background;
    public GameObject buttonBack;
}
