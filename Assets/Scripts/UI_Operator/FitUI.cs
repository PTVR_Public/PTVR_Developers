using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FitUI : MonoBehaviour
{
    public GameObject parent;
    public Rect tempSize;
    public float dif;
    public float adding;
    void Start()
    {
        Rect actualSize = parent.GetComponent<RectTransform>().rect;
        tempSize = actualSize;
    }

    // Update is called once per frame
    void Update()
    {
        Rect actualSize = parent.GetComponent<RectTransform>().rect;
        if (tempSize.width!= actualSize.width)
        {
            dif = actualSize.width - tempSize.width;
            adding = dif / 2;
            transform.localPosition -= new Vector3(adding, 0, 0);

            tempSize = actualSize;
        }
    }
}
