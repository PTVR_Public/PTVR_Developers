using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[CreateAssetMenu(fileName = "ItemButton", menuName = "ScriptableObjects/ItemButton", order = 1)]
public abstract class ItemButton : ScriptableObject
{
    public string nameButton;

    public Category category;

    public string detailsButton;

    public IconButton icon;

    public Rect sizeButton;
}
public enum Category
{   DEFAULT,
    EXPERIMENTS,
    TESTS,
    DEBUGS,
    DEMOS,
    EXTERNALS,
    DOCUMENTATIONS
}

[Serializable]
public class IconButton
{
    public Texture2D icon;
    public Rect size;
}
