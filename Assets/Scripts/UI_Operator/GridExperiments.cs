using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GridExperiments : MonoBehaviour
{
    #region Inspector
    public static GridExperiments instance;
    public Rect sizeButton;
    public float spacing;
    public List<GameObject> experiments;
    public float valueTemp;
    public float selectZoneMax;
    public float selectZoneMin;

    #endregion Inspector

    #region Privates
    public GameObject currExperiments;
    bool isSelect;
    bool hasButtonPress;
    int actualExperimentNumber;
    int experimentChose;
    float maxTemp;
    float minTemp;
    //clamp
    public bool hasAllItemOut;

    #endregion Privates

    #region UnityCall
    void Awake()
    {
        if(instance == null)
        { instance = this; }
        Debug.Log("Screen.width " + Screen.width);
        Debug.Log("Screen.height " + Screen.height);
    }

    void Start()
    {
        for (int count = 0; count < experiments.Count; count++)
        {
            
            experiments[count].transform.localPosition = new Vector3(-count*35, (spacing * count), 0);
            experiments[count].GetComponent<CanvasGroup>().alpha = 1.0f / (count + 0.15f); 
        }
        minTemp = transform.position.y;
    }

    void Update()
    {
        if(isSelect)
            isSelect = false;
        for (int count = 0; count < experiments.Count; count++)
        {
            if(experiments[count].transform.localPosition.x <= selectZoneMax && experiments[count].transform.localPosition.x >= -selectZoneMin)
            {
                isSelect = true;
                if (count == experiments.Count - 1)
                {   
                    if(maxTemp< transform.position.y)
                      maxTemp = transform.position.y;
                    hasAllItemOut = true;
                }
                else if (count == 0)
                {
                    if(minTemp< transform.position.y)
                    
                    hasAllItemOut = false;
                }
                else
                {
                    hasAllItemOut = false;
                }
               
                if (currExperiments != experiments[count])
                { currExperiments = experiments[count];
                    UIManager.instance.SetActualItem(currExperiments.GetComponentInChildren<TextMeshProUGUI>().text,currExperiments);
                    actualExperimentNumber = count;
                }
            }
            else if (!isSelect)
            {
                currExperiments = null;
            }
           
        }
        if(currExperiments == null)
        {
            UIManager.instance.SetActualItem("", currExperiments);
        }
        if (maxTemp < transform.position.y&& hasAllItemOut)
        {
            transform.position = new Vector3(transform.position.x, maxTemp, transform.position.z);
        }
        else if (minTemp > transform.position.y && !hasAllItemOut)
        {
            transform.position = new Vector3(transform.position.x, minTemp, transform.position.z);
        }
    }
        #endregion UnityCall
        public void Initialize()
    {
        for (int count = 0; count < experiments.Count; count++)
        {
            experiments[count].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,sizeButton.width);
            experiments[count].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, sizeButton.height);
            experiments[count].transform.localPosition = new Vector3((-count )* 35, (spacing * count), 0);
         //   experiments[count].GetComponent<CanvasGroup>().alpha = 1.0f / (count + 0.15f);
        }
      
    }

    //TO DO Button Move
    public void MoveUI(int numberExperiment)
    {

        experimentChose = numberExperiment;
        hasButtonPress = true;
    }

    //TODO Protype for infinite Loop
    public void InfiniteLoopGameObject(GameObject _go)
    {
        List<GameObject> go = new List<GameObject>();
        GameObject tempExp;
        for (int count = 0; count < experiments.Count; count++)
        {
            if(_go == experiments[count])
            {
                tempExp = _go;
            }
            else
            {
                go.Add(experiments[count]);
            }
        }
        Destroy(_go);
        go.Add(Instantiate(_go, transform));
        experiments.Clear();
        experiments = go;
    }

    public void ResetPosition()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, 0, 0);
    }
}
