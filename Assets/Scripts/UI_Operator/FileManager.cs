using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SFB;

public class FileManager : MonoBehaviour
{
    [SerializeField]
    private GameObject gui;

    private LoaderManager loaderManager;

    private void Awake()
    {
        var dt = System.DateTime.Now;
        loaderManager = FindObjectOfType<LoaderManager>();
    }

    private void Start()
    {
        var args = System.Environment.GetCommandLineArgs();
        if (args.Length == 2) // exactly 2, as Unity also passes multiple parameters when run from the editor
        {
            LoadJsonFile(args[1]);
        }
    }


    public void OpenFile()
    {
        var extensions = new [] {
            new ExtensionFilter("JSON Files", "json"),
        };
        var paths = StandaloneFileBrowser.OpenFilePanel("Open File", "", extensions, false);
        foreach (var path in paths)
        {
            print(path);
            LoadJsonFile(path);
        }
    }

    private void LoadJsonFile(string path)
    {
        gui.SetActive(false);
        loaderManager.LoadJson(path);
    }

}
