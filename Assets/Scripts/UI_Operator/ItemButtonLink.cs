using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemButtonLink", menuName = "ScriptableObjects/ItemButtonLink", order = 2)]
public class ItemButtonLink : ItemButton
{
    public string url;
}
