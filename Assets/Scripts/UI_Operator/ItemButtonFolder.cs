
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ItemButtonFolder", menuName = "ScriptableObjects/ItemButtonFolder", order = 1)]
public class ItemButtonFolder : ItemButton
{
    public string pathFolder;
}
