using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Linq;
using TMPro;
using Newtonsoft.Json;

public class UIManager : MonoBehaviour
{
    #region Inspector
    public static UIManager instance;
    [SerializeField]
    UISettings settings;
    [SerializeField]
    List<ItemButton> button;
    [SerializeField]
    List<Button> buttonUI;

    [SerializeField]
    List<ItemData> item;
    public Transform canvas;
    [SerializeField]
    Transform panelExp;
    [SerializeField]
    Transform panelPage;
    #endregion Inspector

    #region Privates
    Category actualCategory;
    bool isEditor;
    bool isExtend;
    string folderName;
    string folderPath;
    List<string> previousFolderPath;
    ItemData actualItem;
    GameObject experimentItem;
    GameObject background;
    GameObject buttonExpand;
    GameObject page;
    float tempvalue = 0;
    #endregion Privates

    #region UnityCall
    void Awake()
    {
       #if UNITY_EDITOR
        isEditor = true;
       #endif
        instance = this;
        previousFolderPath = new List<string>();
    }
 
    void Start()
    {
            for (int count = 0; count < buttonUI.Count; count++)
            {
                if (count < button.Count)
                {
                    if (button[count] is ItemButtonFolder)
                    {
                        ItemButtonFolder itemFolder = button[count] as ItemButtonFolder;
                        string checkpath;
                        if (isEditor)
                        {
                            checkpath = "PTVR_Researchers/PTVR_Operators/JSON_Files/" + itemFolder.pathFolder;
                           
                        }
                        else
                        {
                            checkpath = "JSON_Files/" + itemFolder.pathFolder;
                        }
                        if (Directory.Exists(checkpath))
                        {
                            if (Directory.GetFiles(checkpath).Length == 0)
                            {
                                GameObject go = buttonUI[count].gameObject;
                                go.SetActive(false);
                            }
                            else
                            {
                                GameObject go = buttonUI[count].gameObject; //Instantiate(settings.button, h.transform);
                                SetupButton(go, button[count]);
                                if (button[count] is ItemButtonFolder)
                                {
                                    go.GetComponent<Button>().onClick.AddListener(delegate { CheckFolder(itemFolder.pathFolder, itemFolder.category); });
                                }
                            }
                        }

                    }

                    else if (button[count] is ItemButtonLink)
                    {
                        GameObject go = buttonUI[count].gameObject;
                        SetupButton(go, button[count]);
                        if (count == 0)
                        {
                            go.GetComponent<ButtonController>().textDetails.GetComponent<TextMeshProUGUI>().color = settings.fontTitle.colorFont;
                        }
                        ItemButtonLink itemLink = button[count] as ItemButtonLink;
                        go.GetComponent<Button>().onClick.AddListener(delegate { PressWebsite(itemLink.url); });
                    }
                    else
                    {
                        GameObject go = buttonUI[count].gameObject;
                        go.SetActive(false);
                    }
                }
                else
                {
                    GameObject go = buttonUI[count].gameObject;
                    go.SetActive(false);
                }
            }

    }

    void Update()
    {
     //   UpdatePanel();
    //   UpdateExpandButton();
    //if(isEditor)
        UpdatePage();
    }
    #endregion UnityCall

    public void SetupButton(GameObject _button, ItemButton _item)
    {
        ButtonController btnController = _button.GetComponent<ButtonController>();
        RectTransform btnRectTransform = btnController.icon.GetComponent<RectTransform>();
        TextMeshProUGUI btnTextMeshProUGUI = btnController.textDetails.GetComponent<TextMeshProUGUI>();

        btnController.icon.GetComponent<RawImage>().texture = _item.icon.icon;
        btnRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _item.icon.size.width);
        btnRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _item.icon.size.height);
        btnTextMeshProUGUI.text = _item.detailsButton;
        btnTextMeshProUGUI.color = settings.fontButton.colorFont;
        btnTextMeshProUGUI.fontSize = settings.fontButton.sizeFont;
        _button.GetComponent<Button>().colors = settings.colorButton;
    }
    
    public void SetActualItem(string _nameItem, GameObject _go)
    {
        if (_go != null)
        {
            for (int count = 0; count < item.Count; count++)
            {
                if (item[count].Name == _nameItem)
                {
                    if (experimentItem != null)
                        experimentItem.GetComponent<CellController>().text.GetComponent<TextMeshProUGUI>().color = settings.fontText.colorFont;
                    experimentItem = _go;
                    experimentItem.GetComponent<CellController>().text.GetComponent<TextMeshProUGUI>().color = settings.fontTitle.colorFont;
                    actualItem = item[count];
                    page.GetComponent<CellPage>().buttonUse.GetComponent<Button>().onClick.RemoveAllListeners();
                  
                    if (actualItem.TypeItem == Type.FOLDER)
                    {
                        page.GetComponent<CellPage>().buttonUse.GetComponent<Button>().onClick.AddListener(delegate { OpenningFolder(folderPath + "/" + actualItem.Name + "/"); });
                    }
                    else
                    {
                        //TODO Add Open Experiments
                        string filename;
                        if (isEditor)
                        {
                            filename = "PTVR_Researchers/PTVR_Operators/JSON_Files/" + folderPath+"/" + actualItem.Name+ ".json";
                        }
                        else
                        {
                            filename = "JSON_Files\\"+ folderPath + "\\" + actualItem.Name + ".json";
                        }

                        page.GetComponent<CellPage>().buttonUse.GetComponent<Button>().onClick.AddListener(delegate { LoaderManager.instance.LoadJson(filename); });
                            
                    }
                }
            }
        }
        else
        {
            actualItem = null;
        }
    }
    /*
    void UpdatePanel()
    {
        if (isExtend)
        {
            if (tempvalue < 1)
            {
                tempvalue += Time.deltaTime * 2.5f;
            }
        }
        else
        {
            if (tempvalue > 0)
            {
                tempvalue -= Time.deltaTime * 2.5f;
            }
        }
        RectTransform m_RectTransform = background.GetComponent<RectTransform>();
        if ((m_RectTransform.rect != settings.sizeBackgroundOpen && isExtend) || (m_RectTransform.rect != settings.sizeBackgroundClose && !isExtend))
        {
            m_RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Mathf.Lerp(settings.sizeBackgroundClose.height, settings.sizeBackgroundOpen.height, tempvalue));
            m_RectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Lerp(settings.sizeBackgroundClose.width, settings.sizeBackgroundOpen.width, tempvalue));
        }
    }
    */
    void UpdatePage()
    {
        if(page!= null)
        {
            if (actualItem != null)
            {
                if (!page.active)
                    page.SetActive(true);
                page.GetComponent<CellPage>().textDetails.GetComponent<TextMeshProUGUI>().text = actualItem.Detail;
                page.GetComponent<CellPage>().textName.GetComponent<TextMeshProUGUI>().text = actualItem.Name;
            }
            else
            {
                page.SetActive(false);
            }
        }
    }
    /*
    //Expand Menu
    public void PressMenuExpand()
    {
        isExtend = !isExtend;
    }

    void UpdateExpandButton()
    {
        RectTransform m_RectTransform = buttonExpand.GetComponent<RectTransform>();
      
        m_RectTransform.transform.localPosition = Vector3.Lerp(new Vector3(settings.buttonExpandClose.x, settings.buttonExpandClose.y, 0), new Vector3(settings.buttonExpandOpen.x, settings.buttonExpandOpen.y, 0), tempvalue);
        if (isExtend)
            buttonExpand.GetComponentInChildren<RawImage>().gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -90));
        else
            buttonExpand.GetComponentInChildren<RawImage>().gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 90));
    }

    */
    //Function for ButtonLink
    public void PressWebsite(string _url)
    {
        Application.OpenURL(_url);
    }
    
    //Function for ButtonFolder
    public void CheckFolder(string _folderName, Category _category)
    {
        CloseFolder();
        AddPath(_folderName);
        OpenFolder(folderPath, _category);
    }

    public void AddPath(string _path)
    {
        if(folderPath != string.Empty)
        previousFolderPath.Add(folderPath);
        else
        { folderName = _path; }
        folderPath = _path;
    }

    public void RemovePath()
    {
        if (previousFolderPath.Count > 0)
        {
            folderPath = previousFolderPath[previousFolderPath.Count - 1];
            previousFolderPath.RemoveAt(previousFolderPath.Count - 1);
        }
        else
        {
            folderPath = folderName;
        }
    }

    public void OpenFolder(string _folderName, Category _category)
    {
        if (actualCategory != _category)
        {
            actualCategory = _category;
            CheckFolder(_folderName);
        }
        else
        {
            actualCategory = Category.DEFAULT;
        }
    }

    public void CloseFolder()
    {
        previousFolderPath.Clear();
        for(int count = 0; count< GridExperiments.instance.experiments.Count; count++)
        {
            Destroy(GridExperiments.instance.experiments[count]);
        }
        GridExperiments.instance.experiments.Clear();
        Destroy(page);
        page = null;
        item.Clear(); 
    }

    public void OpenningFolder(string _path)
    {
        AddPath(_path);
        item.Clear();
        page.GetComponent<CellPage>().buttonBack.SetActive(true);
        page.GetComponent<CellPage>().buttonBack.GetComponent<Button>().onClick.RemoveAllListeners();
        if (previousFolderPath.Count >= 1)
            page.GetComponent<CellPage>().buttonBack.GetComponent<Button>().onClick.AddListener(delegate { ReturnFolder(previousFolderPath[previousFolderPath.Count - 1]); });
        else
        {
            page.GetComponent<CellPage>().buttonBack.GetComponent<Button>().onClick.AddListener(delegate { ReturnFolder(folderPath); }); }
        foreach ( GameObject game in panelExp.GetComponent<GridExperiments>().experiments)
        {
            Destroy(game);
        }
      
        GridExperiments.instance.experiments.Clear();
        CheckFolder(folderPath);
    }

    public void ReturnFolder(string _path)
    {
        Debug.Log(_path);
        page.GetComponent<CellPage>().buttonBack.GetComponent<Button>().onClick.RemoveAllListeners();
        actualItem = null;
        page.GetComponent<CellPage>().buttonBack.GetComponent<Button>().onClick.AddListener(delegate { ReturnFolder(_path); });
        for(int count = 0; count < panelExp.GetComponent<GridExperiments>().experiments.Count; count++)
        {
            Destroy(panelExp.GetComponent<GridExperiments>().experiments[count]);
        }
        panelExp.GetComponent<GridExperiments>().experiments.Clear();
        item.Clear();
        RemovePath();
        if (previousFolderPath[previousFolderPath.Count-1] == null)
        {
           
            page.GetComponent<CellPage>().buttonBack.SetActive(false);
        }
        CheckFolder(folderPath);
        GridExperiments.instance.ResetPosition();
    }
    
    public void CheckFolder(string _path)
    {
        string path;
        if (isEditor)
        {
            path = "PTVR_Researchers/PTVR_Operators/JSON_Files/" + _path;
        }
        else
        {
            path = "JSON_Files/" + _path;
        }
        GridExperiments.instance.ResetPosition();
        CheckFolderFile(path);
    }


    void CheckFolderFile(string _path)
    {
        string[] releaseFile;
        releaseFile = Directory.GetFiles(_path).Where(name => !name.EndsWith(".txt") && !name.EndsWith(".swp")).ToArray();

        for (int count = 0; count < releaseFile.Length; count++)
        {
            string[] textSplit = releaseFile[count].Split('/', '\\');
            releaseFile[count].Replace('\\', '/');
            string nameExp = textSplit[textSplit.Length - 1];
            string expDescription = "";
            
            StreamReader reader; //Reads the experiment description
            reader = new StreamReader(releaseFile[count]);
            
            bool startDetails =false;
        /*
            foreach (string line in System.IO.File.ReadLines(releaseFile[count]))
            {
                if (line.Contains("detailsExperiment"))
                {
                    startDetails = true;
               
                }
                if (line.Contains("outputHead"))
                {
                    startDetails = false;
                    break;
                }
                if (startDetails)
                {
                    expDescription = expDescription + " " + line;
                }

            }
        */
            
            foreach (string line in System.IO.File.ReadLines(releaseFile[count]))
            {
                if (line.Contains("detailsExperiment"))
                {
                    startDetails = true;
                }
                if (line.Contains("outputHead"))
                {
                    startDetails = false;
                    break;
                }
                if (startDetails)
                {
                    expDescription = expDescription + " " + line;
                }
          
            }
            
            /*
            for (int i = 1; i < 10; i++)
            {
                string line = reader.ReadLine();
                if (line.StartsWith("\"detailsExperiment\""))
                {
                    int position = line.IndexOf(":");
                    line = line.Substring(position + 1);
                    position = line.IndexOf("\"");
                    line = line.Substring(position + 1);
                    position = line.IndexOf("\"");
                    line = line.Substring(0, position);
                    //Debug.LogError(line);
                    expDescription = line;
                }
            }
            */
            

            nameExp = nameExp.Replace(".json", "");
            item.Add(new ItemData(nameExp, expDescription, Type.FILE));
        }
        string[] folderFile;
        folderFile = Directory.GetDirectories(_path);
        string[] folderInformation;

        folderInformation = Directory.GetFiles(_path).Where(name => !name.EndsWith(".json")).ToArray();
        if (folderInformation.Length == folderFile.Length)
            for (int count = 0; count < folderFile.Length; count++)
            {
                string[] textSplit = folderFile[count].Split('/', '\\');
                string nameExp = textSplit[textSplit.Length - 1];

                StreamReader reader; //Reads the experiment description

                reader = new StreamReader(folderInformation[count]);

                string readData = reader.ReadToEnd(); //CAUTION: MAY BE A CAUSE OF WORRY FOR VERY LARGE FILE SIZES
                if (folderInformation[count].Contains(nameExp))
                {
                    item.Add(new ItemData(nameExp, readData, Type.FOLDER));
                }
                else
                {
                    item.Add(new ItemData(nameExp, "", Type.FOLDER));
                }

            }
        for (int count = 0; count < item.Count; count++)
        {
            GameObject go = Instantiate(settings.buttonCell, panelExp);
            panelExp.GetComponent<GridExperiments>().experiments.Add(go);
            go.GetComponent<Button>().onClick.AddListener(delegate { GridExperiments.instance.MoveUI(count); });
            go.GetComponent<CellController>().text.GetComponent<TextMeshProUGUI>().color = settings.fontText.colorFont;
            go.GetComponent<CellController>().text.GetComponent<TextMeshProUGUI>().text = item[count].Name;
            go.GetComponent<Button>().colors = settings.colorButton;
            if (item[count].TypeItem == Type.FILE)
            {
                go.GetComponent<CellController>().icon.GetComponent<RawImage>().texture = settings.iconFile;
            }
            else
            {
                go.GetComponent<CellController>().icon.GetComponent<RawImage>().texture = settings.iconFolder;
            }

        }
        panelExp.GetComponent<GridExperiments>().Initialize();
        if (page == null)
        {
            page = Instantiate(settings.pageCell, panelPage);
            page.GetComponent<CellPage>().background.GetComponent<RawImage>().color = settings.colorBackground;
            page.GetComponent<CellPage>().textDetails.GetComponent<TextMeshProUGUI>().fontSize = settings.fontText.sizeFont;
            page.GetComponent<CellPage>().textDetails.GetComponent<TextMeshProUGUI>().color = settings.fontText.colorFont;
            page.GetComponent<CellPage>().textName.GetComponent<TextMeshProUGUI>().fontSize = settings.fontTitle.sizeFont;
            page.GetComponent<CellPage>().textName.GetComponent<TextMeshProUGUI>().color = settings.fontTitle.colorFont;
            page.GetComponent<CellPage>().buttonUse.GetComponent<Button>().colors = settings.colorButton;
            page.GetComponent<CellPage>().buttonUse.GetComponentInChildren<TextMeshProUGUI>().color = settings.fontButton.colorFont;

            page.GetComponent<CellPage>().buttonBack.GetComponent<Button>().colors = settings.colorButton;
        }
    }
}
