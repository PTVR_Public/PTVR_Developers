
[System.Serializable]
public class ItemData 
{
    public string Name;
    public string Detail;
    public Type TypeItem;

    public ItemData(string name,string detail, Type type)
    {
        Name = name;
        Detail = detail;
        TypeItem = type;
    }
}

    public enum Type
    {
    FOLDER,
    FILE
    }