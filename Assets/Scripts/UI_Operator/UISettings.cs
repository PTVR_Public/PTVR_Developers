using UnityEngine;
using UnityEngine.UI;
using System;

[CreateAssetMenu(fileName = "UISettings", menuName = "ScriptableObjects/UISettings", order = 1)]
public class UISettings : ScriptableObject
{
    public GameObject buttonCell;
    public GameObject pageCell;

    public GameObject button;

    public GameObject background;
    public Rect sizeBackgroundClose;
    public Rect sizeBackgroundOpen;

    public Rect buttonExpandClose;
    public Rect buttonExpandOpen;
    public IconButton expandIcon;
    public GameObject buttonPanel;

    public Texture iconFolder;
    public Texture iconFile;
    public Color colorBackground;

    public Color colorUI;

    public Color colorUISelect;

    public Color colorUIActive;

    public Color colorIcon;

    public ColorBlock colorButton;

    public FontText fontTitle;

    public FontText fontText;

    public FontText fontButton;
}

[Serializable]
public class FontText
{
    public UnityEngine.Font font;
    public string fontName;
    public Color colorFont;
    public float sizeFont;

   public FontText (UnityEngine.Font _font)
    {
        font = _font;

    }
}

