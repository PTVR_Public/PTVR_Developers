using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// 1 / Choisir si ROI
/// 2 / Setup Target
/// 3 / Setup Audio
/// 4 / Augmented

public class Target : MonoBehaviour
{
    #region Inspector
    [SerializeField]
    float speed = 2f;

    public AudioSource audioSource;
    public AudioClip clipSelectStart;
    public AudioClip clipSelectStop;

    public AudioClip clipAugmentedStart;
    public AudioClip clipAugmentedStop;

    public Color feedbackColorIsTarget = Color.red;
    public Color feedbackColorIsAugmented = Color.green;
    #endregion Inspector

    #region Privates
    bool isTarget;
    bool isAugmented;
    MeshRenderer rend;
    float outlineWidth;
    float valueLerp;
    Material mat;
    [HideInInspector]
    public Shader shader;
    #endregion Privates

    #region UnityCall
    void Start()
    {
        mat = new Material(shader);

        rend = GetComponent<MeshRenderer>();
        mat.mainTexture = rend.material.mainTexture;
        mat.color = rend.material.color;
        rend.material = mat;
    }

    void Update()
    {
            if(isTarget)
            {
               if (valueLerp < 1)
                   valueLerp += Time.deltaTime*speed;
               if(isAugmented)
                {
                    rend.material.SetColor("_HighlightOutlineColor", feedbackColorIsAugmented);
                }
               else
                {
                    rend.material.SetColor("_HighlightOutlineColor", feedbackColorIsTarget);
                }
            }
            else
            {
               if (valueLerp>0)
                   valueLerp -= Time.deltaTime*speed;
                rend.material.SetColor("_HighlightOutlineColor", feedbackColorIsTarget);
            }
            outlineWidth = Mathf.Lerp(0, 0.15f, valueLerp);
            rend.material.SetFloat("_HighlightOutlineWidth", outlineWidth);
    }
    #endregion UnityCall

    public void SelectTarget ()
    {
      if(!isTarget)
        if (audioSource != null)
        {
            if (clipSelectStart != null)
            {
                audioSource.clip = clipSelectStart;
                audioSource.Play();
            }
        }
        isTarget = true;
    }

    public void DeSelectTarget()
    {
        if (isTarget)
            if (audioSource != null)
            {
                if (clipSelectStop != null)
                {
                    audioSource.clip = clipSelectStop;
                    audioSource.Play();
                }
            }
        isTarget = false;
    }
}
