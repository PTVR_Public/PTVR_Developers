using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastManager : MonoBehaviour
{
    GameObject tempTarget;
    void Start()
    {
        tempTarget = null;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
        {
            if (hit.transform.gameObject.GetComponent<Target>())
            {
                if(tempTarget == null)
                {
                    tempTarget = hit.transform.gameObject;
                    hit.transform.gameObject.GetComponent<Target>().SelectTarget();
                }
            }
         
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
        }
        else
        {
            if (tempTarget != null)
            {
                tempTarget.GetComponent<Target>().DeSelectTarget();
                tempTarget = null;
            }
        }
    }
}
