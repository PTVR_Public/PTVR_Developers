using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour
{
    [SerializeField]
    private WaypointPath _waypointPath;

    public float speed;

    private int _targetWaypointIndex;

    private Transform _previousWaypoint;

    private Transform _targetWaypoint;

    private float _timeToWaypoint;

    private float _elapsedTime;


    // Start is called before the first frame update
    void Start()
    {
        TargetnextWaypoint();
    }

    // Update is called once per frame
    void Update()
    {
        _elapsedTime += Time.deltaTime;

        float elapsedPercentage = _elapsedTime / _timeToWaypoint;

        transform.position = Vector3.Lerp(_previousWaypoint.position, _targetWaypoint.position, elapsedPercentage);

        if (elapsedPercentage >= 1)
        {
            TargetnextWaypoint();
        }
    }

    private void TargetnextWaypoint()
    {
        _previousWaypoint = _waypointPath.GetWaypoint(_targetWaypointIndex);
        _targetWaypointIndex = _waypointPath.GetNextWaypointIndex(_targetWaypointIndex);
        _targetWaypoint = _waypointPath.GetWaypoint(_targetWaypointIndex);

        _elapsedTime = 0;

        float distanceToWaypoint = Vector3.Distance(_previousWaypoint.position, _targetWaypoint.position);

        _timeToWaypoint = distanceToWaypoint / speed;
    }
}
