using UnityEngine;
using UnityEngine.XR;
using Valve.VR;

// simple library and examples for XR haptics
// note: controllers must be tracking to use haptics

// other examples: https://vrtoolkit.readme.io/docs/vrtk_interacthaptics

public static class HapticPulse
{

    public static void PulseAll() { }
    public static void PulseLeft() { }
    public static void PulseRight() { }


    // https://valvesoftware.github.io/steamvr_unity_plugin/tutorials/SteamVR-Input.html
    public static void HapticPulseSteam(SteamVR_Input_Sources _sources,float _frequency,float _amplitude)
    {
        Debug.Log("Haptic SteamVR");

        SteamVR_Actions.default_Haptic[_sources].Execute(0, 0, _frequency, _amplitude);
    }

    // https://docs.unity3d.com/2019.1/Documentation/ScriptReference/XR.InputDevice.SendHapticImpulse.html
    // https://docs.unity3d.com/2018.3/Documentation/Manual/xr_input.html
    // todo: ifdef for unity xr
    static void HapticPulseUnity()
    {
        Debug.Log("Haptic Unity");

        InputDevice device = InputDevices.GetDeviceAtXRNode(XRNode.RightHand);
        HapticCapabilities capabilities;
        if (device.TryGetHapticCapabilities(out capabilities))
            if (capabilities.supportsImpulse)
                device.SendHapticImpulse(0, 0.5f, 1.0f);

        device = InputDevices.GetDeviceAtXRNode(XRNode.LeftHand);
        if (device.TryGetHapticCapabilities(out capabilities))
            if (capabilities.supportsImpulse)
                device.SendHapticImpulse(0, 0.5f, 1.0f);
    }


#if PLATFORM_OCULUS
    // https://developer.oculus.com/documentation/unity/unity-haptics/
    static void HapticPulseOculus() 
    {
        Debug.Log("Haptic Oculus");
        OVRInput.SetControllerVibration(1, 1, OVRInput.Controller.RTouch);
        OVRInput.SetControllerVibration(1, 1, OVRInput.Controller.LTouch);
    }
#endif
}
