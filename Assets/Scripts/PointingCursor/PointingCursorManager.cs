// Disable cause we got problem 
//#define ExpliciteFiltering
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using SpriteGlow;
using ViveSR;
using ViveSR.anipal.Eye;
using System.Linq;

public class PointingCursorManager : MonoBehaviour
{
    #region Inspector
    public float eccentricity = 0f;
    public float halfMeridian = 0f;
    public ContingentType contingentType;
    public MaskedEye maskedEye;
    public float maxDistance = 500f;
    public float distance_if_empty_pointing_cone = 0.5f;
    public Vector3 initialSize;
    public bool isProjected = false;
    public string pointing_method = "cone";
    public bool display_pointing_method = false;
    public bool is_distant_constant = false;
    public Material mat_top_object;
    public Material mat_basic_object;
    public float widthCollision;
    SpriteRenderer sprite;

    public float spriteAngularRadiusX=2.0f;
    public float spriteAngularRadiusY=2.0f;

    public bool isRenderedOverObjects = false;
    public GazeIndex selectType;
    //Nombre de Valeur pour le Lissage Temporel
    public int timeSamplesInThePast = 50;

    public PTVR.Stimuli.Color defaultColor;
    public Texture defaultCursorModel = null;
    public GameObject defaultCursor = null;

    public PTVR.Stimuli.Color actualColor;
    public Texture actualCursorModel = null;

    public LayerMask ignoreLayer;
    public bool isShowDistanceInM = false;
    #endregion Inspector

    #region Privates
    [Tooltip("VR Headset")]
    Camera headCamera;
    public bool isAngularSizeConstant = true;
    bool isAutoRotate = true;
    static GameObject baseDirectionTool;

    //Lissage Temporel Position
    float [] pastRadialDistances;

    [Tooltip ("Position Get from raycast hit")]
    Vector3 destinationPosition = new Vector3();

    [Tooltip("Size Get from raycast distance or maxDistance")]
    float destinationSize;
    TextMeshPro debugText;
    LineRenderer line;
    public float laserWidth;
    public PTVR.Stimuli.Color laserColor;
    public GameObject cone;
    public GameObject headset;
    public KevinCastejon.ConeMesh.Cone Conescript;
    public Collider ConeCollider;
    #endregion Privates

    #region UnityCall
    void Start()
    {

        initialSize = transform.localScale;

        headset = GameObject.Find("Headset");

        if (pointing_method.Equals("cone"))
        {
            var pref_cone = Resources.Load("Cone_go") as GameObject;
            cone = Instantiate(pref_cone,
                               new Vector3(headset.transform.position.x, headset.transform.position.y, headset.transform.position.z),
                               Quaternion.identity);

            Conescript = cone.transform.GetChild(0).GetComponent<KevinCastejon.ConeMesh.Cone>();

            ConeCollider = cone.transform.GetChild(0).GetComponent<MeshCollider>();

            if (display_pointing_method)
            {

            }
            else
            {
                cone.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            }

            Conescript.ConeHeight = 20.0f;

            float size_cone_meters = 0;

            if (spriteAngularRadiusX <= spriteAngularRadiusY)
            {
                 size_cone_meters = 0.5f * Conescript.ConeHeight * Mathf.Atan((Mathf.PI / 180) * spriteAngularRadiusX);
            } else
            {
                 size_cone_meters = 0.5f * Conescript.ConeHeight * Mathf.Atan((Mathf.PI / 180) * spriteAngularRadiusY);
            }

            
            Conescript.ConeRadius = size_cone_meters;

            var mat_cone = Resources.Load<Material>("Material/ColliderMaterial");

            cone.transform.GetChild(0).GetComponent<MeshRenderer>().material = mat_cone;

        }
        sprite = defaultCursor.GetComponent<PrefabController>().renderers[0].gameObject.GetComponent<SpriteRenderer>();

        sprite.sortingOrder = 1;

        if (baseDirectionTool == null)
         baseDirectionTool = new GameObject();
        headCamera = Camera.main;
        transform.LookAt(headCamera.transform);
        pastRadialDistances = new float[timeSamplesInThePast];
        // Debug Test
        GameObject go = new GameObject();
        go.transform.parent = transform; 
        go.transform.localRotation = Quaternion.Euler(0,180,0);
        go.transform.localPosition = new Vector3(0.0f, -0.15f,0.0f);
        debugText= go.AddComponent<TextMeshPro>();
        debugText.fontSize = 1;
        debugText.alignment = TextAlignmentOptions.CenterGeoAligned;
        ignoreLayer = LayerMask.GetMask("PointedAt");
        if (GetComponentInChildren<SpriteGlowEffect>()!=null)
             GetComponentInChildren<SpriteGlowEffect>().OutlineWidth = 0;
        Update();
    }

    void Update()
    {
        MaskEye();
        ProjectPointer();
        ActualizeCursor();
        if (timeSamplesInThePast > 0)
        {
           TemporalSmoothing();
        }
        else
            DirectPointing();
        LaserProjection();
        //Debug
        if (isShowDistanceInM)
            DebugText();
        RotateReticule();
        RenderMaterial();

        if (pointing_method.Equals("cone"))
            update_cone();
    }
    #endregion UnityCall

    void update_cone()
    {
        Vector3 pointingDirection = GeneratePointerDirection();
        Vector3 originPointing = GenerateOriginPointer();

        cone.transform.position = new Vector3(originPointing.x,
                                              originPointing.y,
                                              originPointing.z);


        cone.transform.forward = -pointingDirection;
    }

    void LaserProjection()
    {
        if (isProjected && (contingentType == ContingentType.RIGHT_HAND || contingentType == ContingentType.LEFT_HAND))
        {
            if (line == null)
            {
                if (contingentType == ContingentType.RIGHT_HAND)
                {
                    line = SetVisualSettings.instance.CreateObject("line", -2).GetComponent<LineRenderer>();
                    line.SetWidth(laserWidth,laserWidth);
                    line.startColor = laserColor.ToUnityColor();
                    line.endColor = laserColor.ToUnityColor();
                }
                if (contingentType == ContingentType.LEFT_HAND)
                {
                    line = SetVisualSettings.instance.CreateObject("line", -1).GetComponent<LineRenderer>();
                    line.SetWidth(laserWidth,laserWidth);
                    line.startColor = laserColor.ToUnityColor();
                    line.endColor = laserColor.ToUnityColor();
                }
            }
            else
            {
                if (contingentType == ContingentType.RIGHT_HAND)
                {
                    line.GetComponent<LineRenderer>().SetPosition(0, Vector3.zero);
                    line.GetComponent<LineRenderer>().SetPosition(1,  new Vector3(0,0, destinationSize));
                }
                if (contingentType == ContingentType.LEFT_HAND)
                {
                    line.GetComponent<LineRenderer>().SetPosition(0, Vector3.zero);
                    line.GetComponent<LineRenderer>().SetPosition(1, new Vector3(0, 0, destinationSize));
                }
            }
        }
    }

    void RenderMaterial()
    {
        if(isRenderedOverObjects && sprite.material != mat_top_object)
        {
            sprite.material = mat_top_object;
            sprite.material.mainTexture = actualCursorModel;

        }
        else if(!isRenderedOverObjects && sprite.material != mat_basic_object)
        {
            sprite.material = mat_basic_object;
            sprite.material.mainTexture = actualCursorModel;

        }
    }
    void MaskEye()
    {
        switch(maskedEye)
        {
            case MaskedEye.NONE:
                if (gameObject.layer == 11 || gameObject.layer == 12)
                {
                    gameObject.layer = 9;
                    sprite.gameObject.layer = 9;
                }
                break;
            case MaskedEye.LEFT_EYE:
                if(gameObject.layer != 11)
                {
                    gameObject.layer = 11;
                    sprite.gameObject.layer = 11;
                }
                break;
            case MaskedEye.RIGHT_EYE:
                if (gameObject.layer != 12)
                {
                    gameObject.layer = 12;
                    sprite.gameObject.layer = 12;
                }
                break;
            default:
                if (gameObject.layer == 11 || gameObject.layer == 12)
                {
                    gameObject.layer = 9;
                    sprite.gameObject.layer = 9;
                }
                break;
        }
    }
    void RotateReticule()
    {
        Transform trans = headCamera.transform;
        switch (contingentType)
        {
            case ContingentType.HEADSET:
                trans = headCamera.transform;
                break;
            case ContingentType.GAZE:
                trans = headCamera.transform;
                break;
            case ContingentType.RIGHT_EYE:
                trans = EyesTrackingManager.instance.eyeOrbiteRight.transform;
                break;
            case ContingentType.LEFT_EYE:
                trans = EyesTrackingManager.instance.eyeOrbiteLeft.transform;
                break;
            case ContingentType.RIGHT_HAND:
                trans = CallbackManager.instance.rightController.transform;
                break;
            case ContingentType.LEFT_HAND:
                trans = CallbackManager.instance.leftController.transform;
                break;
            default:
                trans = headCamera.transform;
                break;
        }
        if (isAutoRotate)
            transform.LookAt(trans, trans.transform.up);
        else
            transform.LookAt(trans);
    }

    void ActualizeCursor()
    {
        if (defaultCursor.GetComponent<PrefabController>().renderers[0].gameObject.GetComponent<SpriteRenderer>().sprite != actualCursorModel)
        {
        }
        if (defaultCursor.GetComponent<PrefabController>().actualColor != actualColor )
        {
            defaultCursor.GetComponent<PrefabController>().actualColor = actualColor;
            defaultCursor.GetComponent<PrefabController>().renderers[0].GetComponent<SpriteRenderer>().color = actualColor.ToUnityColor();
        }
    }
    //Indique la direction du Raycast TODO See if is relevant to keep
    
    Vector3 GeneratePointerDirection()
    {
        switch (contingentType)
        {
            case ContingentType.HEADSET:
                baseDirectionTool.transform.rotation = headCamera.transform.rotation;
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, headCamera.transform.up, eccentricity / Mathf.PI * 180);
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, headCamera.transform.forward, halfMeridian / Mathf.PI * 180);
                return baseDirectionTool.transform.forward;
            case ContingentType.GAZE:
                baseDirectionTool.transform.localRotation = EyesTrackingManager.instance.eyeOrbiteCombine.rotation;
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, EyesTrackingManager.instance.eyeOrbiteCombine.transform.up, eccentricity / Mathf.PI * 180);
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, EyesTrackingManager.instance.eyeOrbiteCombine.transform.forward, halfMeridian / Mathf.PI * 180);
                return baseDirectionTool.transform.forward;
            case ContingentType.RIGHT_EYE:
                baseDirectionTool.transform.localRotation = EyesTrackingManager.instance.eyeOrbiteRight.rotation;
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, EyesTrackingManager.instance.eyeOrbiteRight.transform.up, eccentricity / Mathf.PI * 180);
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, EyesTrackingManager.instance.eyeOrbiteRight.transform.forward, halfMeridian / Mathf.PI * 180);
                return baseDirectionTool.transform.forward;
            case ContingentType.LEFT_EYE:
                baseDirectionTool.transform.localRotation = EyesTrackingManager.instance.eyeOrbiteLeft.rotation;
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, EyesTrackingManager.instance.eyeOrbiteLeft.transform.up, eccentricity / Mathf.PI * 180);
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, EyesTrackingManager.instance.eyeOrbiteLeft.transform.forward, halfMeridian / Mathf.PI * 180);
                return baseDirectionTool.transform.forward;
            case ContingentType.RIGHT_HAND:
                baseDirectionTool.transform.localRotation = CallbackManager.instance.rightController.transform.rotation;
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, CallbackManager.instance.rightController.transform.up, eccentricity / Mathf.PI * 180);
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, CallbackManager.instance.rightController.transform.forward, halfMeridian / Mathf.PI * 180);
                return baseDirectionTool.transform.forward;
            case ContingentType.LEFT_HAND:
                baseDirectionTool.transform.localRotation = CallbackManager.instance.leftController.transform.rotation;
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, CallbackManager.instance.leftController.transform.up, eccentricity / Mathf.PI * 180);
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, CallbackManager.instance.leftController.transform.forward, halfMeridian / Mathf.PI * 180);
                return baseDirectionTool.transform.forward;
            default:
                baseDirectionTool.transform.rotation = headCamera.transform.rotation;
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, headCamera.transform.up, eccentricity / Mathf.PI * 180);
                baseDirectionTool.transform.RotateAround(baseDirectionTool.transform.position, headCamera.transform.forward, halfMeridian / Mathf.PI * 180);
                return baseDirectionTool.transform.forward;
        }
    }

    Vector3 GenerateOriginPointer()
    {
        switch (contingentType)
        {
            case ContingentType.HEADSET:
                return headCamera.transform.position;
            case ContingentType.GAZE:
                return headCamera.transform.position;
            case ContingentType.RIGHT_EYE:
                return Utils.instance.RightCoordinatesToLeftCoordinates(EyesTrackingManager.instance.eyeDataLastValidityRight.verboseEye.gaze_origin_mm * 0.001f) + headCamera.transform.position;
            case ContingentType.LEFT_EYE:
                return Utils.instance.RightCoordinatesToLeftCoordinates(EyesTrackingManager.instance.eyeDataLastValidityLeft.verboseEye.gaze_origin_mm * 0.001f) + headCamera.transform.position;
            case ContingentType.RIGHT_HAND:
                return CallbackManager.instance.rightController.transform.position;
            case ContingentType.LEFT_HAND:
                return CallbackManager.instance.leftController.transform.position;
            default:
                return headCamera.transform.position;
        }
    }

    //Modifie la position du gameObject Cursor (qui correspond au Reticule)
    void ProjectPointer()
    {
        Vector3 pointingDirection = GeneratePointerDirection();
        Vector3 originPointing = GenerateOriginPointer();
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer

        // in order to project further away from the reticle (or scotoma) position
        var maxDistanceForProjection = 500;


        if (is_distant_constant)

        {
            destinationPosition = originPointing + pointingDirection * maxDistance;
            destinationSize = distance_if_empty_pointing_cone;
        }


        else

        {

            if (pointing_method == "cone")
            {

                

                Dictionary<GameObject, float> dic_distances = new Dictionary<GameObject, float>();
                foreach (GameObject gObject in Conescript.currentCollisions.Keys)
                {
                    //var distance = Vector3.Distance(gObject.transform.position, headset.transform.position);
                    var distance = Vector3.Distance(Conescript.currentCollisions[gObject], headset.transform.position);
                    dic_distances.Add(gObject, distance);
                }

                //Debug.Log(dic_distances.Count);

                if (dic_distances.Count > 0)
                {
                    var keyAndValue = dic_distances.OrderBy(kvp => kvp.Value).First();

                    if (isProjected)
                    {
                        //Debug.Log(Vector3.Distance(Conescript.currentCollisions[keyAndValue.Key], headset.transform.position));
                        if (Vector3.Distance(Conescript.currentCollisions[keyAndValue.Key], headset.transform.position) > 0.5f)
                        {

                            //destinationPosition = originPointing + pointingDirection * Vector3.Distance(Conescript.currentCollisions[keyAndValue.Key].point, headset.transform.position);//Conescript.currentCollisions[keyAndValue.Key].point;
                            destinationPosition = originPointing + pointingDirection * Vector3.Distance(Conescript.currentCollisions[keyAndValue.Key], headset.transform.position);
                            destinationSize = Vector3.Distance(Conescript.currentCollisions[keyAndValue.Key], headset.transform.position);
                        }

                    }
                }
                else
                {

                    destinationPosition = originPointing + pointingDirection * distance_if_empty_pointing_cone;
                    destinationSize = distance_if_empty_pointing_cone;
                }
            }

            else
            {
                if (isProjected && Physics.Raycast(originPointing, pointingDirection, out hit, maxDistanceForProjection, ~ignoreLayer))
                {

                    if (!hit.transform.name.Equals("Cone"))
                    {
                        destinationPosition = hit.point;
                        destinationSize = hit.distance;

                    }

                }
                // The ray don't intersect
                else
                {
                    destinationPosition = originPointing + pointingDirection * maxDistance;
                    destinationSize = maxDistance;
                }
            }
        }
    }

    //Debug Test
    void DebugText()
    {
        debugText.fontSize = TextHeightManager.instance.CalculateXPhysicalHeightPt(destinationSize, 2, debugText);
        
        debugText.transform.position = destinationPosition - new Vector3(0, transform.GetChild(0).localScale[0]*4 , 0); ;
        
        debugText.text = System.Math.Round((Camera.main.transform.position - transform.position).magnitude,2).ToString() + "m";
    }

    int compteur = 0;

    void DirectPointing()
    {
        float distance = (headCamera.transform.position - destinationPosition).magnitude;
        // if (isAngularSizeConstant)
        //  transform.localScale = new Vector3(1, 1, 1);
        transform.position = destinationPosition;
        //Debug.Log("Before isAngularSizeConstant");
        //transform.localScale = new Vector3(distance, distance, distance);
        var ratio_distance_size = new Vector3(initialSize[0] * distance / maxDistance,
                                              initialSize[1] * distance / maxDistance,
                                              initialSize[2] * distance / maxDistance);

        transform.localScale = ratio_distance_size;
        //if (isAngularSizeConstant)
        //{
        //    transform.localScale = new Vector3(distance, distance, distance);
        //    Debug.Log("isAngularSizeConstant");
        //}
    }


    float smoothRadialDistance = 0.0f;//Mis � 0 par securit� mais aucun impact dans le calcul
    public float alpha = 0.0f;
    void TemporalSmoothing()
    {
#if ExpliciteFiltering
          float smoothRadialDistance = 0.0f;
                pastRadialDistances[compteur % timeSamplesInThePast] = (float)(System.Math.Round((headCamera.transform.position - destinationPosition).magnitude,2));
                // pour chaque timeSamplesInThePast ajoute la distance radial � smoothRadialDistance
                for (int count = 0; count < Mathf.Min(pastRadialDistances.Length,compteur+1); count++)
                {
                    //Calcul coordonn�e du timeSamplesInThePast par rapport au HeadSet
                    smoothRadialDistance += pastRadialDistances[count];
                }
                //Divise les timeSamplesInThePast radialDistance par leur nombres
                smoothRadialDistance /= Mathf.Min(pastRadialDistances.Length, compteur + 1);
          Debug.Log(" compteur : " + compteur + " indice du tableau : "+ (compteur % timeSamplesInThePast).ToString()  + " lastRadialDistance " + pastRadialDistances[compteur % timeSamplesInThePast] + " smoothRadialDistance " + smoothRadialDistance );

#else
        //Get new position Lissage
        int temporalIntervalForComputation = Mathf.Min(timeSamplesInThePast, compteur + 1);
        float currentCursorDistance = (headCamera.transform.position - destinationPosition).magnitude;

        //  smoothRadialDistance = (currentCursorDistance + (temporalIntervalForComputation - 1) * smoothRadialDistance) / temporalIntervalForComputation;
        if (currentCursorDistance <= smoothRadialDistance)
            smoothRadialDistance = alpha * currentCursorDistance + (1 - alpha) * smoothRadialDistance;
        else
        {
            smoothRadialDistance = smoothRadialDistance + alpha * smoothRadialDistance;
            if (currentCursorDistance <= smoothRadialDistance || smoothRadialDistance < 1)
                smoothRadialDistance = currentCursorDistance;
        }
        /*smoothRadialDistance = smoothRadialDistance + alpha * smoothRadialDistance;
        if (smoothRadialDistance > currentCursorDistance)
            smoothRadialDistance = currentCursorDistance;
    }*/
#endif


        //Calcul les coordonn�e de la derniere position par rapport au HeadSet
        Vector3 coordinatesFromHeadSet = destinationPosition - headCamera.transform.position;

        // Transforme les donn�es  Cartesienne en donn�e perimetric de coordinatesFromHeadSet
        Vector3 smoothPosition = Utils.instance.XYZtoPerimetricRD(coordinatesFromHeadSet.x, coordinatesFromHeadSet.y, coordinatesFromHeadSet.z);

        //RadialDistance
        smoothPosition[2] = smoothRadialDistance;
        smoothPosition = Utils.instance.PerimetricRDToXYZ(smoothPosition[0], smoothPosition[1], smoothPosition[2]);
        smoothPosition += headCamera.transform.position;

        transform.position = smoothPosition;

        //Calcul de la distance avec la smoothPosition pour scale le pointing cursor en fonction de la distance
        float distance = (headCamera.transform.position - smoothPosition).magnitude;
        if (isAngularSizeConstant)
        { 
            transform.localScale = new Vector3(distance, distance, distance);
            Debug.Log("isAngularSizeConstant");
        }
        compteur += 1;
    }
}


[System.Serializable]
public class PointingLast
{
    public Vector3 position;
    public float probability =1;

}

