using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PTVR;
using ViveSR.anipal.Eye;
public class PointingCursorGenerator : MonoBehaviour
{
    #region Inspector
    public static PointingCursorGenerator instance;
    public GameObject defaultSpriteContainer;
    #endregion Inspector

    #region Privates
    private Dictionary<long,PointingCursor> pointingCursorData;	

    private string prefixForWWWQuery = "file:///";     //!< This prefix is used for Windows for local drive retrieval

    private Dictionary<string, Texture2D> SpriteImagesDict = new Dictionary<string, Texture2D>();

    bool isEditor;
    #endregion Privates

    #region UnityCall
    void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            Debug.LogWarning("Double PointingCursorGenerator destroy all exept one");
        }
        else
        {
            instance = this;
        }
     #if UNITY_EDITOR
        isEditor = true;
     #endif
    }
    #endregion UnityCall

    private void SetActiveAllChildren(Transform transform, bool value)
    {
        transform.gameObject.SetActive(value);
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(value);

            SetActiveAllChildren(child, value);
        }
    }

    public GameObject InitNewPointer(PointingCursor aPtr)
    {
        if (aPtr == null)
        {
            Debug.LogError("Trying to initiate a null pointer.");
            return null;
        }

        GameObject currentReticleCtn;
        if (The3DWorldManager.instance.GameObjectCol.ContainsKey(aPtr.id))
        {
            currentReticleCtn = The3DWorldManager.instance.GameObjectCol[aPtr.id];
            currentReticleCtn.SetActive(true);
            SetActiveAllChildren(currentReticleCtn.transform, true);
        }
        else
        {
            currentReticleCtn = Instantiate(defaultSpriteContainer, SceneManager.instance.visualTemplate);
            The3DWorldManager.instance.GameObjectCol[aPtr.id] = currentReticleCtn;
        }

        if (aPtr.contingency_type == ContingentType.GAZE)
                currentReticleCtn.GetComponent<PointingCursorManager>().selectType = GazeIndex.COMBINE;
        if (aPtr.contingency_type == ContingentType.LEFT_EYE)
                currentReticleCtn.GetComponent<PointingCursorManager>().selectType = GazeIndex.LEFT;
        if (aPtr.contingency_type == ContingentType.RIGHT_EYE)
                currentReticleCtn.GetComponent<PointingCursorManager>().selectType = GazeIndex.RIGHT;

        currentReticleCtn.GetComponent<PointingCursorManager>().laserColor = aPtr.laserColor;
        currentReticleCtn.GetComponent<PointingCursorManager>().laserWidth = aPtr.laserWidth;
        currentReticleCtn.GetComponent<PointingCursorManager>().widthCollision = aPtr.widthCollision;
        currentReticleCtn.GetComponent<PointingCursorManager>().isRenderedOverObjects = aPtr.isRenderedOverObjects;
        currentReticleCtn.GetComponent<PointingCursorManager>().contingentType = aPtr.contingency_type;
        currentReticleCtn.GetComponent<PointingCursorManager>().maskedEye = aPtr.maskedEye;
        currentReticleCtn.GetComponent<PointingCursorManager>().defaultColor = aPtr.color;
        currentReticleCtn.GetComponent<PointingCursorManager>().actualColor = aPtr.color;
        currentReticleCtn.GetComponent<PointingCursorManager>().defaultCursor.GetComponent<PrefabController>().coloratedObject = new PTVR.Stimuli.Objects.ColoratedObject();
        currentReticleCtn.GetComponent<PointingCursorManager>().defaultCursor.GetComponent<PrefabController>().coloratedObject.type = "sprite";
        currentReticleCtn.GetComponent<PointingCursorManager>().defaultCursor.GetComponent<PrefabController>().coloratedObject.color = aPtr.color;
        currentReticleCtn.GetComponent<PointingCursorManager>().eccentricity = aPtr.eccentricity;
        currentReticleCtn.GetComponent<PointingCursorManager>().halfMeridian = aPtr.half_meridian;
        currentReticleCtn.GetComponent<PointingCursorManager>().maxDistance = aPtr.maxDistance;
        currentReticleCtn.GetComponent<PointingCursorManager>().distance_if_empty_pointing_cone = aPtr.distance_if_empty_pointing_cone;
        currentReticleCtn.GetComponent<PointingCursorManager>().isProjected = aPtr.isProjected;
        currentReticleCtn.GetComponent<PointingCursorManager>().timeSamplesInThePast = aPtr.smoothingStep;
        currentReticleCtn.GetComponent<PointingCursorManager>().isShowDistanceInM = aPtr.isShowDistanceInM;
        currentReticleCtn.GetComponent<PointingCursorManager>().alpha = aPtr.alphaSmooth;
        currentReticleCtn.GetComponent<PointingCursorManager>().isAngularSizeConstant = aPtr.distanceRescale;
        currentReticleCtn.GetComponent<PointingCursorManager>().spriteAngularRadiusX = aPtr.spriteAngularRadiusX;
        currentReticleCtn.GetComponent<PointingCursorManager>().spriteAngularRadiusY = aPtr.spriteAngularRadiusY;
        currentReticleCtn.GetComponent<PointingCursorManager>().pointing_method = aPtr.pointing_method;
        currentReticleCtn.GetComponent<PointingCursorManager>().display_pointing_method = aPtr.display_pointing_method;
        currentReticleCtn.GetComponent<PointingCursorManager>().is_distant_constant = aPtr.is_distant_constant;
        //currentReticleCtn.GetComponent<PointingCursorManager>().spritePath = aPtr.spritePath;


        //if (aPtr.GetType() == typeof(HeadPovContingent))
        //    ;
        return currentReticleCtn;

    }

    public void StartNewPointer (PointingCursor aPtr)
    {
        GameObject go = InitNewPointer(aPtr);
        SetReticleSprite(go, aPtr);
    }

    void SetReticleSprite(GameObject currentReticleCtn, PointingCursor aPtr)
    {
        string spr = aPtr.sprite;
        //float reticleHeigh = (float)Math.Tan(aPtr.spriteAngularRadiusY/180*Math.PI)*2*aPtr.maxDistance;
        //float reticleWidth = (float)Math.Tan(aPtr.spriteAngularRadiusX/180*Math.PI)*2 * aPtr.maxDistance;

        float reticleHeigh = (float)Math.Tan(aPtr.spriteAngularRadiusY / 180 * Math.PI) * aPtr.maxDistance;
        float reticleWidth = (float)Math.Tan(aPtr.spriteAngularRadiusX / 180 * Math.PI) * aPtr.maxDistance;

        Vector2 scale = new Vector2(reticleWidth, reticleHeigh) * aPtr.widthCollision;
        GameObject reticleGo = currentReticleCtn.transform.GetChild(0).gameObject;


        if (aPtr.widthCollision != 1.0f)
        {

            reticleGo.SetActive(false);
        }
        else
        {
            Texture2D tex;
            if (!SpriteImagesDict.TryGetValue(spr, out tex))
            {
                tex = new Texture2D(1, 1);
                string url = prefixForWWWQuery + spr;

                String applicationPath = Application.dataPath;
                char[] c = applicationPath.ToCharArray();
                string path = string.Empty;

                if (isEditor)
                {

                    //path = aPtr.spritePath + spr;
                    path = aPtr.spritePath;

                }
                else
                {
                    //path = applicationPath.Remove(c.Length - 9);

                    //if (aPtr.name == "scotoma")
                    //{
                    //    path = path + "/resources/Images/Scotomas/" + spr;
                    //}
                    //else
                    //{
                    //    path = path + "/resources/Images/" + spr;
                    //}

                    path = aPtr.spritePath;

                }
                WWW www = new WWW(path);
                //Debug.Log("url :" + path);
                www.LoadImageIntoTexture(tex);
                SpriteImagesDict.Add(spr, tex);
            }

            Vector2 pivot = new Vector2(0.5f, 0.5f);

            reticleGo.GetComponent<PrefabController>().renderers[0].GetComponent<SpriteRenderer>().sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), pivot);


            Texture Tex = reticleGo.GetComponent<PrefabController>().renderers[0].GetComponent<SpriteRenderer>().sprite.texture;
            float pixPerUnit = reticleGo.GetComponent<PrefabController>().renderers[0].GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
            reticleGo.GetComponent<PrefabController>().renderers[0].GetComponent<SpriteRenderer>().material.mainTexture = Tex;
            currentReticleCtn.GetComponent<PointingCursorManager>().defaultCursorModel = Tex;
            currentReticleCtn.GetComponent<PointingCursorManager>().actualCursorModel = Tex;

            reticleGo.transform.localScale = new Vector3((pixPerUnit / Tex.width) * scale[0], (pixPerUnit / Tex.height) * scale[1], 0.1f);
        }
    }
   
}
