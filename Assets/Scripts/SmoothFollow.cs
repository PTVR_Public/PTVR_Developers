using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SmoothFollow : MonoBehaviour
{

    public Transform target;

    [Range(0,1)]
    public float positionDamping;

    [Range(0, 1)]
    public float rotationDamping;

    private float scale = 0.001f;

    public PlayerInput playerInput;

    private Vector2 mouse_pos;

    private bool first_mouse_pos = true;

    //public Input input;
    //public  InputS playerInput;

    // Start is called before the first frame update
    void OnEnable()
    {
        Debug.Log("SmoothFollow");
        var mouse_position = playerInput.actions.actionMaps[1].actions[3].ReadValue<Vector2>();

    }


    // Update is called once per frame
    void Update()
    {

        var mouse_position = playerInput.actions.actionMaps[1].actions[3].ReadValue<Vector2>();

        if ((first_mouse_pos) & (mouse_position[0] != 0))
        {
            mouse_pos = playerInput.actions.actionMaps[1].actions[3].ReadValue<Vector2>();
            first_mouse_pos = false;
        }

        


        float mouse_diff = 0;
        if (mouse_pos[0] != mouse_position[0])
        {
            if (mouse_pos[0] >= mouse_position[0])
            {
                mouse_diff = - Mathf.Abs(mouse_pos[0] - mouse_position[0]);
            } else
            {
                mouse_diff =  Mathf.Abs(mouse_pos[0] - mouse_position[0]);
            }

            mouse_pos = mouse_position;

            transform.Rotate(new Vector3(0, 0.25f*mouse_diff, 0));
        }

        



        // slow down when close
        //transform.position = Vector3.Lerp(transform.position, target.position, positionDamping);
        //playerInput.GetComponent<PlayerInp>
        var mouse_scroll = playerInput.actions.actionMaps[1].actions[5].ReadValue<Vector2>()[1];

        //var mouse_click_left = playerInput.actions.actionMaps[1].actions[4].ReadValueAsObject();

        //var mouse_click_right = playerInput.actions.actionMaps[1].actions[7].ReadValueAsObject();

        //int mouse_click_left_int = 0;

        //int mouse_click_right_int = 0;

        

        //Debug.Log(mouse_position);

        //if (mouse_click_left != null)
        //{
        //    mouse_click_left_int = Convert.ToInt32(mouse_click_left);
        //}
        //if (mouse_click_right != null)
        //{
        //    mouse_click_right_int = Convert.ToInt32(mouse_click_right);
        //}

        if (mouse_scroll != 0)
        {
            //transform.position = new Vector3(transform.position.x + mouse_scroll * scale, transform.position.y,
            //    transform.position.z );

            transform.Translate(new Vector3(0, 0, mouse_scroll * scale)); 
        }

        //if (mouse_click_left_int == 1)
        //{
        //    transform.position = new Vector3(transform.position.x , transform.position.y,
        //        transform.position.z + 0.1f);
        //}
        //if (mouse_click_right_int == 1)
        //{
        //    transform.position = new Vector3(transform.position.x, transform.position.y,
        //        transform.position.z - 0.1f);
        //}
        //transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, rotationDamping);
        //transform.position = new Vector3(transform.position.x, transform.position.y,
        //    transform.position.z + Input.mouseScrollDelta.y * scale);


    }
}
