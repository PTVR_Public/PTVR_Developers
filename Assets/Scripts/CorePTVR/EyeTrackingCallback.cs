using UnityEngine;
using ViveSR.anipal.Eye;
using System.Runtime.InteropServices;

using System.IO;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

/// <summary>
/// Example usage for eye tracking callback
/// Note: Callback runs on a separate thread to report at ~120hz.
/// Unity is not threadsafe and cannot call any UnityEngine api from within callback thread.
/// </summary>
public class EyeTrackingCallback : MonoBehaviour 
{
    #region Inspector
    public static EyeTrackingCallback instance;
    public Vector3 leftDirection;
    public Vector3 rightDirection;
    public Vector3 leftDirectionWorldSpace;
    public Vector3 combinedDirection;
    public UnityEngine.Vector3 leftEyeGaze;
    public UnityEngine.Vector3 rightEyeGaze;
    public Vector3 combined_eye_origin;
    public Vector3 left_eye_origin;
    public Vector3 right_eye_origin;
    #endregion Inspector

    #region Privates
    static Transform baseDirectionTool=null;
    static Transform basePositionTool=null;

    private static EyeData_v2 eyeData = new EyeData_v2();
    private static ConcurrentQueue<EyeData_v2> eyeDataColl = new ConcurrentQueue<EyeData_v2>();
    private static List<EyeData_v2> updateSyncEyeDataColl = new List<EyeData_v2>();
    private static List<Vector3> updateSyncLeftEyeGaze = new List<Vector3>();
    private static List<Vector3> updateSyncRightEyeGaze = new List<Vector3>();
    private static List<Vector3> updateSyncCombinedEyeGaze = new List<Vector3>();
    private static bool hasEyeCallbackRegistered = false;
    private static System.DateTime startTime;
    private static bool hasStartTime = false;
    Camera headCamera;
    Vector3 camForward;
    #endregion Privates

    #region UnityCall

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            Debug.LogWarning("Double Eyetracking Callback destroy all exept one");
        }
        else
        {
            baseDirectionTool = new GameObject().transform;
            basePositionTool = new GameObject().transform;
            instance = this;
        }
    }
    
    void Start ()
    {
        headCamera = Camera.main;
    }

    void Update()
    {
        if (SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.WORKING) return;

        if (SRanipal_Eye_Framework.Instance.EnableEyeDataCallback == true && hasEyeCallbackRegistered == false)
        {
            SRanipal_Eye_v2.WrapperRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye_v2.CallbackBasic)EyeCallback));
            hasEyeCallbackRegistered = true;
        }
        else if (SRanipal_Eye_Framework.Instance.EnableEyeDataCallback == false && hasEyeCallbackRegistered == true)
        {
            SRanipal_Eye_v2.WrapperUnRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye_v2.CallbackBasic)EyeCallback));
            hasEyeCallbackRegistered = false;
        }
        string date = System.DateTime.Now.ToString("yyyy-M-dd--HH-mm-ss.fff");

        if (The3DWorldManager.instance.curr3DWorld != null)
        {
            while (eyeDataColl.TryDequeue(out eyeData))
            {
                updateSyncEyeDataColl.Add(eyeData);
                CalculateAbsoluteGazeUnityCall();

                if (The3DWorldManager.instance.curr3DWorld.outputGaze)
                {
                    //ExporterManager.instance.WriteEyeToFileAsync(eyeData);
                }
            }
        }
    }

    void LateUpdate()
    {
        updateSyncEyeDataColl.Clear();
        updateSyncLeftEyeGaze.Clear();
        updateSyncRightEyeGaze.Clear();
    }

    private void OnDisable()
    {
        Release();
    }

    void OnApplicationQuit()
    {
        Release();
    }
    #endregion UnityCall


    void CalculateAbsoluteGazeUnityCall()
    {

        if (eyeData.verbose_data.left.pupil_position_in_sensor_area != new Vector2(-1f, -1f))
            leftDirection = new Vector3(eyeData.verbose_data.left.gaze_direction_normalized[0],
                eyeData.verbose_data.left.gaze_direction_normalized[1],
                eyeData.verbose_data.left.gaze_direction_normalized[2]);
        else
            leftDirection = new Vector3(0, 0, 1);

        if (eyeData.verbose_data.right.pupil_position_in_sensor_area != new Vector2(-1f, -1f))
            rightDirection = new Vector3(eyeData.verbose_data.right.gaze_direction_normalized[0],
            eyeData.verbose_data.right.gaze_direction_normalized[1],
            eyeData.verbose_data.right.gaze_direction_normalized[2]);
    
        else
            rightDirection = new Vector3(0, 0, 1);

        left_eye_origin = Utils.instance.RightCoordinatesToLeftCoordinates(eyeData.verbose_data.left.gaze_origin_mm / 1000);
        right_eye_origin = Utils.instance.RightCoordinatesToLeftCoordinates(eyeData.verbose_data.right.gaze_origin_mm / 1000); 
        combined_eye_origin = Utils.instance.RightCoordinatesToLeftCoordinates(eyeData.verbose_data.combined.eye_data.gaze_origin_mm / 1000);
        //the eyetracker gives -x instead of x for the vive
        leftDirection = Utils.instance.RightCoordinatesToLeftCoordinates(leftDirection);
        rightDirection = Utils.instance.RightCoordinatesToLeftCoordinates(rightDirection);

        
        Vector2 camperim = Utils.instance.XYZtoPerimetric(leftDirection[0], leftDirection[1], leftDirection[2]);
        basePositionTool.transform.rotation = headCamera.transform.rotation;
        // 3D position HeadSet 
        
        leftDirectionWorldSpace = headCamera.transform.TransformDirection(leftDirection);


        if (!float.IsNaN(camperim[0]) && !float.IsNaN(camperim[1]))
        {

            basePositionTool.transform.RotateAround(basePositionTool.transform.position, headCamera.transform.up, camperim[0]);
            basePositionTool.transform.RotateAround(basePositionTool.transform.position, headCamera.transform.forward, camperim[1]);
        }
        leftEyeGaze = basePositionTool.forward;

        camperim = Utils.instance.XYZtoPerimetric(rightDirection[0], rightDirection[1], rightDirection[2]);
        basePositionTool.transform.rotation = headCamera.transform.rotation;
        if (!float.IsNaN(camperim[0]) && !float.IsNaN(camperim[1]))
        {
            basePositionTool.transform.RotateAround(basePositionTool.transform.position, headCamera.transform.up, camperim[0]);
            basePositionTool.transform.RotateAround(basePositionTool.transform.position, headCamera.transform.forward, camperim[1]);
        }
        rightEyeGaze = basePositionTool.forward;

        updateSyncLeftEyeGaze.Add(leftDirection);
        updateSyncRightEyeGaze.Add(rightDirection);

    }

    Vector3 ConvertLeftHandToRightHand(Vector3 v) //unity uses y and z inversion
    {
        float tmp = v[1];
        v[2] = v[1];
        v[1] = tmp;

        return v;
    }

    Vector3 CalculateCombineRotation(Vector3 Vcam, Vector3 Veye) //direct system
    {
        float angle = Vector3.Angle(Vcam, Veye) * (float)Math.PI / 180;
        //right hand vector preparation
        Vector3 k = ConvertLeftHandToRightHand(- Vector3.Cross(Vcam, Veye)); //Vector3 cross is left hand
        Vector3 VcamCrossK = ConvertLeftHandToRightHand(-Vector3.Cross(Vcam, k));

        Vector3 rVcam = ConvertLeftHandToRightHand(Vcam);
        Vector3 rVeye = ConvertLeftHandToRightHand(Veye);

        Vector3 v1 = rVcam * (float)Math.Cos(angle);
        Vector3 v2 = VcamCrossK * (float)Math.Sin(angle);
        Vector3 v3 = k * (Vector3.Dot(k, rVcam) * (1 - (float)Math.Cos(angle)));

        return v1 + v2 + v3;

        /*Vector3 v1 = Vcam * (float)Math.Cos(angle);
        Vector3 v2 = Vector3.Cross(Vcam, k) * (float)Math.Sin(angle);
        Vector3 v3 = k * (Vector3.Dot(k, Vcam) * (1 - (float)Math.Cos(angle)));

        return v1 + v2 + v3;

        /*

        //temporarty vector preparation
        Vector3 v1 = Vcam;
        Vector3 v2 = new Vector3(Veye[0] * Vcam[2],
                                 Veye[1] * Vcam[2],
                                 -Veye[0] * Vcam[0] - Veye[1] * Vcam[1]);
        Vector3 v3 = new Vector3(-Veye[0] * Veye[0] * Vcam[0] - Veye[0] * Veye[1] * Vcam[1],
                                 -Veye[0] * Veye[1] * Vcam[0] - Veye[1] * Veye[1] * Vcam[1],
                                 (-Veye[0] * Veye[0] - Veye[1] * Veye[1]) * Vcam[2]);

        Vector3 toReturn = (float)Math.Cos(angle) * v1; // param 1
        toReturn += (float)Math.Sin(Math.Acos(Veye[2])) * v2;
        toReturn += (1 - Veye[2]) * v3;
        return toReturn;*/
    }

    void CalculateAbsoluteGazeAsync()
    {
        //to finish: the Rodrigues' matrix formula
        Vector3 leftEyeDirection = eyeData.verbose_data.left.gaze_direction_normalized; //u
        Vector3 rightEyeDirection = eyeData.verbose_data.right.gaze_direction_normalized;

        Vector3 CamDirection = headCamera.transform.forward; //v
        /*/CamDirection[1] = -CamDirection[1];
        leftEyeDirection[1] = -leftEyeDirection[1];
        rightEyeDirection[1] = -rightEyeDirection[1];*/

        leftEyeGaze = CalculateCombineRotation(CamDirection, leftEyeDirection);
        rightEyeGaze = CalculateCombineRotation(CamDirection, rightEyeDirection);
    }
    public EyeData_v2 GetEyeData ()
    {
        return eyeData;
    }

    public System.DateTime GetStartTime ()
    {
        return startTime;
    }

    public int GetBufferSize ()
    {
        return updateSyncEyeDataColl.Count;
    }

    public void GetEyeBufferedEyeVectors
        (int index, out Vector3 left, out Vector3 right)
    {
        left = updateSyncLeftEyeGaze[index];
        right = updateSyncRightEyeGaze[index];
    }

    public System.DateTime GetEyeDateTime(int index)
    {
        return startTime.AddMilliseconds(updateSyncEyeDataColl[index].timestamp);
    }
        

    /// <summary>
    /// Release callback thread when disabled or quit
    /// </summary>
    private static void Release()
    {
        if (hasEyeCallbackRegistered == true)
        {
            SRanipal_Eye.WrapperUnRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye_v2.CallbackBasic)EyeCallback));
            hasEyeCallbackRegistered = false;
        }
    }

    /// <summary>
    /// Required class for IL2CPP scripting backend support
    /// </summary>
    internal class MonoPInvokeCallbackAttribute : System.Attribute
    {
        public MonoPInvokeCallbackAttribute() { }
    }

    /// <summary>
    /// Eye tracking data callback thread.
    /// Reports data at ~120hz
    /// MonoPInvokeCallback attribute required for IL2CPP scripting backend
    /// </summary>
    /// <param name="eye_data">Reference to latest eye_data</param>
    [MonoPInvokeCallback]
    private static void EyeCallback(ref EyeData_v2 eye_data)
    {
        if (hasStartTime == false)
        {
            startTime = System.DateTime.Now;
            startTime=startTime.AddMilliseconds(-eye_data.timestamp);
            hasStartTime = true;
        }
        eyeDataColl.Enqueue(eye_data);
        //ResultManager.instance.WriteEyeToFileAsync(System.DateTime.Now.ToString("yyyy-M-dd--HH-mm-ss.fff"));
        // do stuff with eyeData...
    }

}
