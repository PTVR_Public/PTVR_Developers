﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using ViveSR.anipal.Eye;
using System.Threading.Tasks;
using System;
using System.Globalization;
using System.Text;
public class ExporterManager : MonoBehaviour {

    const string DateTimeFormatMilliseconds = "yyyy-MM-dd-HH-mm-ss.fff";

    #region Inspector
    public static ExporterManager instance; //singleton
    public Dictionary<string, WriterData> writers;
    #endregion Inspector

    #region Privates
    static StreamWriter gazeDirectionWriter = null;
    static WriterData gazeWriter = null;
    StreamWriter resultWriter;

    public List<string> metaHeaders;
    StreamWriter camPosRotWriter;

    bool isEditor = false;
    public string startDate;
    long currentSceneNb = 0;
    #endregion Privates

    #region UnityCall
    void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            Debug.LogWarning("Double ExporterManager destroy all exept one");
        }
        else
        {
            instance = this;
        }
    #if UNITY_EDITOR
        isEditor = true;
    #endif
        startDate = System.DateTime.Now.ToString(DateTimeFormatMilliseconds);
        writers = new Dictionary<string, WriterData>();
    }

    // Close this file once the application quits
    void OnApplicationQuit()
    {
        if (resultWriter != null)
            resultWriter.Close();
        if (gazeDirectionWriter != null)
            gazeDirectionWriter.Close();
        if (camPosRotWriter != null)
            camPosRotWriter.Close();
        foreach (var item in writers)
        {
            if (item.Value != null)
            {
                item.Value.writer.Close();
            }
        }
    }
    #endregion UnityCall

    public void SetupDefaultData(DateTime _timeStamp)
    {
        DataBank.Instance.SetData("name_of_subject", The3DWorldManager.instance.userId);
        DataBank.Instance.SetData("trial", "");
        DataBank.Instance.SetData("scene_id", "-1");
        
        DataBank.Instance.SetData("date_of_exp_run", "-1");

        DataBank.Instance.SetData("event_name", "NoEvent");
        DataBank.Instance.SetData("event", "NoEvent");
        DataBank.Instance.SetData("event_mode", "NoMode");
        DataBank.Instance.SetData("callback_name", "NoCallback");
        DataBank.Instance.SetData("callback", "NoCallback");
        DataBank.Instance.SetData("callback_effect", "NoEffect");
        DataBank.Instance.SetData("callback_timestamp", _timeStamp.ToString(DateTimeFormatMilliseconds));
        DataBank.Instance.SetData("callback_timestamp_ms", format_date_time_to_milliseconds(_timeStamp).ToString());
        //DataBank.Instance.SetData("timestamp_diff_ms", _timeStamp.ToString());
        //DataBank.Instance.SetData("elapsed_frames", "");
        DataBank.Instance.SetData("scene_start_timestamp", "-1");
        DataBank.Instance.SetData("scene_end_timestamp", "-1");
        DataBank.Instance.SetData("ptvr_version", Application.version);

    }

    public int format_date_time_to_milliseconds(DateTime date)
    {
        return date.Hour * 3600000 + date.Minute * 60000 + date.Second * 1000 + date.Millisecond;
    }

    public void SetDefaultData(DateTime _timestamp ,string _trial = "", string _id = "" , string _button = "", string _mode = "",
                 string _callback = "", string _effect = "",string _timestamp_diff_ms ="", string _eventName = "")
    {
        int actualid = 0;
        for (int count = 0; count < The3DWorldManager.instance.curr3DWorld.id_scene.Count;count++)
        {
            if (The3DWorldManager.instance.curr3DWorld.id_scene[count] == The3DWorldManager.instance.currScene.id)
            {
                actualid = count;
            }
        }
        DataBank.Instance.SetData("trial", _trial);
        DataBank.Instance.SetData("scene_id", (actualid+1).ToString());
        DataBank.Instance.SetData("date_of_exp_run", startDate);
        DataBank.Instance.SetData("callback", _callback);
        DataBank.Instance.SetData("callback_effect", _effect);
        DataBank.Instance.SetData("callback_name", _eventName);
        DataBank.Instance.SetData("callback_timestamp", _timestamp.ToString(DateTimeFormatMilliseconds));
        DataBank.Instance.SetData("callback_timestamp_ms", format_date_time_to_milliseconds(_timestamp).ToString());
        DataBank.Instance.SetData("ptvr_version", Application.version);
        //DataBank.Instance.SetData("timestamp_diff_ms", _timestamp_diff_ms);
        //DataBank.Instance.SetData("elapsed_frames", Time.frameCount.ToString());
    }

    #region Gaze
    public void SetupGazeFile(string _userId,string _header)
    {

        string gazeresultFileName = The3DWorldManager.instance.curr3DWorld.name + "_gaze_data_" + _userId + "_" + startDate;

        if (The3DWorldManager.instance.curr3DWorld.pathResult != string.Empty)
        {
            gazeresultFileName = The3DWorldManager.instance.curr3DWorld.pathResult + The3DWorldManager.instance.curr3DWorld.name + "/" + gazeresultFileName  + ".csv";
        } else
        {
            gazeresultFileName = Application.dataPath + "/../Results/" + The3DWorldManager.instance.curr3DWorld.name + "/" + gazeresultFileName + ".csv";
        }

        gazeDirectionWriter = new StreamWriter(gazeresultFileName);
        gazeDirectionWriter.AutoFlush = true;

        //Update Name Gaze
        StringBuilder header = new StringBuilder();
        header.Append("trial;")
              .Append("elapsed_frames;")
              .Append("date_of_exp_run;")
              .Append("time_stamp_computer;")
              .Append("time_stamp_ms;")
              .Append("left_eye_gaze_normalized_vector_validity;")
              .Append("left_eye_gaze_normalized_vector;")
              .Append("left_eye_gaze_origin_validity;")
              .Append("left_eye_gaze_origin_in_mm;")
              .Append("left_eye_pupil_position_in_sensor_area_validity;")
              .Append("left_eye_position_in_sensor;")
              .Append("left_eye_pupil_diameter_validity;")
              .Append("left_eye_pupil_diameter_in_mm;")
              .Append("left_eye_openness_validity;")
              .Append("left_eye_openness;")
              .Append("left_eye_wide;")
              .Append("left_eye_squeeze;")
              .Append("left_eye_frown;")

              .Append("right_eye_gaze_normalized_vector_validity;")
              .Append("right_eye_gaze_normalized_vector")
              .Append("right_eye_gaze_origin_validity;")
              .Append("right_eye_gaze_origin_in_mm;")
              .Append("right_eye_pupil_position_in_sensor_area_validity;")
              .Append("right_eye_position_in_sensor;")
              .Append("right_eye_pupil_diameter_validity;")
              .Append("right_eye_pupil_diameter_in_mm;")
              .Append("right_eye_openness_validity;")
              .Append("right_eye_openness;")
              .Append("right_eye_wide;")
              .Append("right_eye_squeeze;")
              .Append("right_eye_frown");

             // .Append("combined_eye_gaze_normalized_vector");

        gazeDirectionWriter.WriteLine(header.ToString()); //Add a header
    }

    public void WriteEyeToFileAsync(EyeDataPTVR eyeData)
    {
     //trial;elapsed_frames;time_stamp_gaze;Left_gaze_x;Left_gaze_y;left_eye_perimetric_ecc;left_eye_perimetric_HM;Left_gaze_z;left_eye_position_in_sensor;right_gaze_x;right_gaze_y;right_gaze_z;right_eye_perimetric_ecc;right_eye_perimetric_HM;right_eye_position_in_sensor
        //Diviser ligne
        if (gazeDirectionWriter != null)
        {
            StringBuilder line = new StringBuilder();
            line.Append(The3DWorldManager.instance.currScene.trial.ToString())
                .Append(";")
                .Append(Time.frameCount.ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(startDate)
                .Append(";")
                .Append(format_date_time(System.DateTime.Now).ToString())
                .Append(";")
                .Append(eyeData.eyes.timestamp.ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyeLeftGazeDirectionValidity.ToString())
                .Append(";")
                .Append(Utils.instance.RightCoordinatesToLeftCoordinates(eyeData.eyes.verbose_data.left.gaze_direction_normalized).ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyeLeftGazeOriginValidity.ToString())
                .Append(";")
                .Append(Utils.instance.RightCoordinatesToLeftCoordinates(Utils.instance.mmInM(eyeData.eyes.verbose_data.left.gaze_origin_mm)).ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyeLeftPupilPositionInSensorValidity.ToString().Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyes.verbose_data.left.pupil_position_in_sensor_area.ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyeLeftPupilDiameterValidity.ToString().Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyes.verbose_data.left.pupil_diameter_mm.ToString().Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyeLeftOpenessValidity.ToString())
                .Append(";")
                .Append(eyeData.eyes.verbose_data.left.eye_openness.ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyes.expression_data.left.eye_wide.ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyes.expression_data.left.eye_squeeze.ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyes.expression_data.left.eye_frown.ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyeRightGazeDirectionValidity.ToString())
                .Append(";")
                .Append(Utils.instance.RightCoordinatesToLeftCoordinates(eyeData.eyes.verbose_data.right.gaze_direction_normalized).ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyeRightGazeOriginValidity.ToString())
                .Append(";")
                .Append(Utils.instance.RightCoordinatesToLeftCoordinates(Utils.instance.mmInM(eyeData.eyes.verbose_data.right.gaze_origin_mm)).ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyeRightPupilPositionInSensorValidity.ToString().Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyes.verbose_data.right.pupil_position_in_sensor_area.ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyeRightPupilDiameterValidity.ToString().Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyes.verbose_data.right.pupil_diameter_mm.ToString().Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyeRightOpenessValidity.ToString())
                .Append(";")
                .Append(eyeData.eyes.verbose_data.right.eye_openness.ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyes.expression_data.right.eye_wide.ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyes.expression_data.right.eye_squeeze.ToString("F12").Replace(",", "."))
                .Append(";")
                .Append(eyeData.eyes.expression_data.right.eye_frown.ToString("F12").Replace(",", "."));
                //.Append(";")
               // .Append(Utils.instance.RightCoordinatesToLeftCoordinates((eyeData.eyes.verbose_data.right.gaze_direction_normalized * eyeData.eyes.verbose_data.right.eye_openness + eyeData.eyes.verbose_data.left.gaze_direction_normalized * eyeData.eyes.verbose_data.left.eye_openness) / (eyeData.eyes.verbose_data.right.eye_openness + eyeData.eyes.verbose_data.left.eye_openness)).ToString("F12").Replace(",", "."));
            
            gazeDirectionWriter.WriteLine(line.ToString());
  
        }
    }
    #endregion Gaze

    public void Setup(string _name, string header)
    {
        if (writers.ContainsKey(_name))
            return;

        string userId = The3DWorldManager.instance.userId;
        string fileName = The3DWorldManager.instance.curr3DWorld.name + "_" + _name + "_" + userId + "_" + startDate;


        if (The3DWorldManager.instance.curr3DWorld.pathResult != string.Empty)
        {
            fileName = The3DWorldManager.instance.curr3DWorld.pathResult + The3DWorldManager.instance.curr3DWorld.name + "/" + fileName + ".csv";
            if (!Directory.Exists(The3DWorldManager.instance.curr3DWorld.pathResult + The3DWorldManager.instance.curr3DWorld.name + "/"))
            {
                var subfolder = Directory.CreateDirectory(The3DWorldManager.instance.curr3DWorld.pathResult + The3DWorldManager.instance.curr3DWorld.name + "/");
            }
        } else
        {
            fileName = Application.dataPath + "/../Results/" + The3DWorldManager.instance.curr3DWorld.name + "/" + fileName + ".csv";
            if (!Directory.Exists(Application.dataPath + "/../Results/"))
            {
                Debug.Log("Warning folder not Exist");
                var folder = Directory.CreateDirectory(Application.dataPath + "/../Results/");

            }
            if (!Directory.Exists(Application.dataPath + "/../Results/" + The3DWorldManager.instance.curr3DWorld.name + "/"))
            {
                var subfolder = Directory.CreateDirectory(Application.dataPath + "/../Results/" + The3DWorldManager.instance.curr3DWorld.name + "/");
            }
        }

        //Debug.Log("writer : " + fileName);
        writers[_name] = new WriterData();
        writers[_name].writer = new StreamWriter(fileName);
        // Debug.Log(_name);
        //Split header
        //Debug.Log(header);
        string[] splitHeader = header.Split(';');
        writers[_name].appendString = new List<string>();
        for (int count = 0; count< splitHeader.Length-1;count++)
        {
            writers[_name].appendString.Add(splitHeader[count]);
        }
        writers[_name].writer.AutoFlush = true;
        writers[_name].writer.WriteLine(header);
    }

    public int format_date_time(System.DateTime date)
    {
        return date.Hour * 3600000 + date.Minute * 60000 + date.Second * 1000 + date.Millisecond;
    }

    CultureInfo provider = CultureInfo.InvariantCulture;
    public void WriteNewLineValidate(string _name, InputUnityEvent _event, string _mode, string _callback, string _effect,DateTime _timeStamp,string _eventName)
    {
        DateTime startTime = DateTime.ParseExact(DataBank.Instance.GetData("callback_timestamp"), DateTimeFormatMilliseconds, provider);
        DateTime now = DateTime.ParseExact(_timeStamp.ToString(DateTimeFormatMilliseconds), DateTimeFormatMilliseconds, provider);
        TimeSpan timestamp_diff_ms = now - startTime;

        DataBank.Instance.SetData("scene_end_timestamp", format_date_time(System.DateTime.Now).ToString());

        // For now, we will provide backward compatibility as we will keep the InputUnityEvent _input parameter but this is going to change at some point.
        string button = (_event == null)? "no_button" : _event.button;

        SetDefaultData(_timeStamp, The3DWorldManager.instance.currScene.trial .ToString(), The3DWorldManager.instance.currScene.id.ToString(), button, _mode, _callback, _effect, timestamp_diff_ms.TotalMilliseconds.ToString().Replace(",","."),_eventName);
        if (writers.ContainsKey(_name))
        {
            StreamWriter theWriter = writers[_name].writer;
            var index_event_name = writers[_name].appendString.IndexOf("event_name");

            if (DataBank.Instance.GetData(writers[_name].appendString[index_event_name]) != "start of scene")

            {

                for (int count = 0; count < writers[_name].appendString.Count; count++)
                {

                    theWriter.Write(DataBank.Instance.GetData(writers[_name].appendString[count]) + ";");
                }
                theWriter.Write("\n");

            }
        }
        //DataBank.Instance.SetData("timestamp_diff_ms", DateTime.Now.ToString()); 
            //DataBank.Instance.SetData("timestamp_diff_ms", timestamp_diff_ms.Milliseconds.ToString());
    }

    public void WriteFile(string _name, List<string> _data = null)
    {
        currentSceneNb = The3DWorldManager.instance.currScene.id;
        //"Trial;SceneId;Timestamp;ElapsedFrames;x_pos;y_pos;z_pos;x_rot;y_rot;z_rot;Eccentricity;Half_Meridian;xdeg;ydeg"
        if (writers.ContainsKey(_name))
        {
            StreamWriter theWriter = writers[_name].writer;
            if (_data != null)
            {
                Debug.Log("DATA NOT NULL");
                for (int count = 0; count < _data.Count; count++)
                {
                    theWriter.Write(_data[count].Replace(",", ".") + ";");
                    Debug.Log(_data[count].Replace(",", "."));
                }
                theWriter.Write("\n");
            }
            else
            {
                for (int count = 0; count < writers[_name].appendString.Count; count++)
                {
                    theWriter.Write(DataBank.Instance.GetData(writers[_name].appendString[count]).Replace(",", ".") + ";");
                }
                theWriter.Write("\n");
            }
        }
        else
            Debug.LogError("no file with header " + _name + " initialized. can't write.");
    }
}
[System.Serializable]
public class WriterData
{
    public StreamWriter writer;
    public List <string>appendString;
}
