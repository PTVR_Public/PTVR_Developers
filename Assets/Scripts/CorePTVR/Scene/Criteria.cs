using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Criteria : MonoBehaviour
{

    #region Privates
    protected long id = -1; //Identifier of the scene in experiment structure--> Set in Set Trial Configuration
    protected float timeForTrial = 0f;
    protected float reactionTime = 0f;
    protected float timeAtStart = 0f; //Set at the beginning, see StartTimer()
    protected float timeCurrent = 0f; //Updated each frame, see LateUpdate()
    #endregion Privates

    #region UnityCall
    private void OnEnable()
    {
        GenericFulfilmentCriteria.instance.startTimer.AddListener(StartTimer);
        GenericFulfilmentCriteria.instance.setTimeForTrial.AddListener(SetTimeForTrial) ;
        GenericFulfilmentCriteria.instance.Fulfiled.AddListener(Fulfilled);
    }

    private void OnDisable()
    {
        timeForTrial = 0f;
        GenericFulfilmentCriteria.instance.startTimer.RemoveListener(StartTimer);
        GenericFulfilmentCriteria.instance.setTimeForTrial.RemoveListener(SetTimeForTrial);
        GenericFulfilmentCriteria.instance.Fulfiled.RemoveListener(Fulfilled);
    }

    void LateUpdate()
    {
       // TimerFunction();
    }
    #endregion UnityCall
    public virtual void TimerFunction()
    {

            timeCurrent = Time.realtimeSinceStartup; //Re-set current time

            reactionTime = timeCurrent - timeAtStart; //Get the reaction time (so far)

            if (reactionTime >= timeForTrial && timeForTrial > 0f)
            {
                Fulfilled("NoControllerEvent");
                // timeForTrial = 0f;

            }
    }

    //Prepares the list of things to write in the result
    public virtual List<string> GetResult(string _button)
    {
        List<string> result = new List<string>();
        //Add things to the result list
        result.Add(id.ToString()); // Add the identifier to the scene
        result.Add(reactionTime.ToString()); // Reaction time
        result.Add(_button); // Button pressed
        return result;
    }
    public virtual void SetId(long _id)
    {
        id = _id;
    }

    public virtual void SetTimeForTrial(float _timeInSeconds) //Set the terminationtime for the scene
    {
        timeForTrial = _timeInSeconds;
    }

    public virtual void StartTimer() //Called via SendMessage call from GenericFulfilmentCriteria
    {
        timeAtStart = Time.realtimeSinceStartup; //Set Reaction Time at the beginning of a trial
        //Reset variables
        timeCurrent = 0f;
        reactionTime = 0f;
    }

    //Fulfilment on controller button press
    public virtual void Fulfilled(string _button)
    {
        if (_button != "NoControllerEvent")
            Debug.Log("Trial ending as " + _button + " was pressed");
        else
            Debug.Log("Trial ending because of a timeout");
           // ExporterManager.instance.SetDefaultData(id.ToString(), reactionTime.ToString(), _button);
        //to do Remove
         //   ResultManager.instance.WriteToFile(); //Write result
           // ExporterManager.instance.WriteFile("_Main_");
            id = -1; //Reset the id
    }
}
