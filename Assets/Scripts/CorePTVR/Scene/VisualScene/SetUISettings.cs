using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class SetUISettings : MonoBehaviour
{
    public static SetUISettings instance;
    [SerializeField]
    BasicUIObjectsData basicUIObjectsData;
    [SerializeField]
    Transform canvasUI;
    int tempWidthScreen;
    int tempHeightScreen;
    bool isEditor;
    static string prefixForWWWQuery = "file:///";

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
#if UNITY_EDITOR
        isEditor = true;
#endif
        tempWidthScreen = Screen.width;
        tempHeightScreen = Screen.height;
    }

    public GameObject ObjectsUI(string _typeObject)
    {
        for (int count = 0; count < basicUIObjectsData.basicUIObjects.Count; count++)
        {
            if (basicUIObjectsData.basicUIObjects[count].typeObject == _typeObject)
            {
                return basicUIObjectsData.basicUIObjects[count].prefabObject;
            }
        }
        return null;
    }

    public GameObject CreateUIObject(PTVR.Stimuli.Objects.UIObject _uiObj, Transform _parent = null)
    {
        GameObject go;
        if(_parent == null)
        {
            _parent = canvasUI;
        }
        switch (_uiObj.type)
        {
            case "textUI":
                PTVR.Stimuli.Objects.TextUI textUI = (PTVR.Stimuli.Objects.TextUI)_uiObj;
                go = Instantiate(ObjectsUI(_uiObj.type), _parent);
                go.GetComponent<TextMeshProUGUI>().text = textUI.text;

                go.GetComponent<PrefabController>().coloratedObject = _uiObj;
                go.GetComponent<PrefabController>().posUI = _uiObj;
                TextMeshProUGUI tmpUGUI = go.GetComponent<TextMeshProUGUI>();
                RectTransform textRect = go.GetComponent<RectTransform>();
                textRect.localPosition = CheckPositionUI(textUI);

                String applicationPath = Application.dataPath;
                char[] c = applicationPath.ToCharArray();
                string path = string.Empty;
                if (isEditor)
                {
                    path = applicationPath.Remove(c.Length - 6);
                    path = path + "PTVR_Researchers/PTVR_Operator/resources/Fonts/" + textUI.fontName + ".ttf";
                }
                else
                {
                    path = applicationPath.Remove(c.Length - 9);
                    path = path + "/resources/Fonts/" + textUI.fontName + ".ttf";
                }
                Font fontLoad = new Font(path);

                TMP_FontAsset font = TMP_FontAsset.CreateFontAsset(fontLoad);
                tmpUGUI.font = font;
                tmpUGUI.fontSize = textUI.fontSize;
                go.GetComponent<TextMeshProUGUI>().color = textUI.color.ToUnityColor();
                textRect.sizeDelta = new Vector2(textUI.textboxWidth, textUI.textboxHeight);


                if (textUI.isBold && textUI.isItalic)
                    tmpUGUI.fontStyle = FontStyles.Bold & FontStyles.Italic;
                else if (textUI.isBold)
                    tmpUGUI.fontStyle = FontStyles.Bold;
                else if (textUI.isItalic)
                    tmpUGUI.fontStyle = FontStyles.Italic;


                // Set horizontal alignment
                if (textUI.alignHorizontal == "Left")
                    tmpUGUI.horizontalAlignment = HorizontalAlignmentOptions.Left;
                if (textUI.alignHorizontal == "Right")
                    tmpUGUI.horizontalAlignment = HorizontalAlignmentOptions.Right;
                if (textUI.alignHorizontal == "Center")
                    tmpUGUI.horizontalAlignment = HorizontalAlignmentOptions.Center;
                if (textUI.alignHorizontal == "Justified")
                    tmpUGUI.horizontalAlignment = HorizontalAlignmentOptions.Justified;


                if (textUI.alignVertical == "Middle")
                    // Special treatment is needed for this one, because unlike the other parameter, VerticalAlignmentOptions.Middle and TextAlignmentOptions.Midline
                    // are differents : the first one doesn't align the text on the center of the x. 
                    if (textUI.alignHorizontal == "Left")
                        tmpUGUI.alignment = TextAlignmentOptions.MidlineLeft;
                if (textUI.alignHorizontal == "Right")
                    tmpUGUI.alignment = TextAlignmentOptions.MidlineRight;
                if (textUI.alignHorizontal == "Center")
                    tmpUGUI.alignment = TextAlignmentOptions.Midline;
                if (textUI.alignHorizontal == "Justified")
                    tmpUGUI.alignment = TextAlignmentOptions.MidlineJustified;
                if (textUI.alignVertical == "Capline")
                    tmpUGUI.verticalAlignment = VerticalAlignmentOptions.Capline;
                if (textUI.alignVertical == "Baseline")
                    tmpUGUI.verticalAlignment = VerticalAlignmentOptions.Baseline;
                if (textUI.alignVertical == "Bottom")
                    tmpUGUI.verticalAlignment = VerticalAlignmentOptions.Bottom;
                if (textUI.alignVertical == "Top")
                    tmpUGUI.verticalAlignment = VerticalAlignmentOptions.Top;
                if (textUI.alignVertical == "midline")
                    tmpUGUI.alignment = TextAlignmentOptions.Midline;
                if (textUI.alignVertical == "capline")
                    tmpUGUI.alignment = TextAlignmentOptions.Capline;
                if (textUI.alignVertical == "baseline")
                    tmpUGUI.alignment = TextAlignmentOptions.Baseline;
                go.GetComponent<PrefabController>().coloratedObject = textUI;
                go.GetComponent<PrefabController>().Setup();
                return go;

            case "imageUI":
                PTVR.Stimuli.Objects.ImageUI imageUI = (PTVR.Stimuli.Objects.ImageUI)_uiObj;
                go = Instantiate(ObjectsUI(_uiObj.type), _parent);
                go.GetComponent<RawImage>().color = imageUI.color.ToUnityColor();
                go.GetComponent<PrefabController>().coloratedObject = imageUI;
                go.GetComponent<RectTransform>().sizeDelta = new Vector2(imageUI.width, imageUI.height);
                go.GetComponent<RectTransform>().localPosition = CheckPositionUI(imageUI);
                go.GetComponent<PrefabController>().defaultColor = imageUI.color;
                if (imageUI.imageType == "background")
                {
                    go.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
                    go.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
                    go.GetComponent<RectTransform>().offsetMin= new Vector2(0, 0);
                    go.GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);

                }
                //Add Texture
                if (imageUI.imageFile != "default")
                {
                    Texture2D tex;
                    tex = new Texture2D(4, 4, TextureFormat.DXT1, false);
                    string url = prefixForWWWQuery + imageUI.imageFile;
                    applicationPath = Application.dataPath;
                    c = applicationPath.ToCharArray();
                    path = string.Empty;
                    if (isEditor)
                    {
                        path = applicationPath.Remove(c.Length - 6);
                        path = path + "PTVR_Researchers/PTVR_Operators/resources/Images/" + imageUI.imageFile;
                    }
                    else
                    {
                        path = applicationPath.Remove(c.Length - 9);
                        path = path + "/resources/Images/" + imageUI.imageFile;
                    }
                    WWW www = new WWW(path);//osolete
                    www.LoadImageIntoTexture(tex);
                    go.GetComponent<PrefabController>().Setup();
                    go.GetComponent<RawImage>().texture = tex; //obsolete   
                }
                return go;
            case "graphUI":
                PTVR.Stimuli.Objects.GraphUI graphUI = (PTVR.Stimuli.Objects.GraphUI)_uiObj; // This is the main window graph 
                go = Instantiate(ObjectsUI(_uiObj.type), _parent);
                RectTransform Rect = go.GetComponent<RectTransform>();
                Rect.sizeDelta = new Vector2(graphUI.size[0], graphUI.size[1]);
                //go.GetComponent<RectTransform>().localPosition = CheckPositionUI(graphUI);
                //go.GetComponent<RectTransform>().localPosition = new Vector3(graphUI.position_current_CS[0],graphUI.position_current_CS[1],graphUI.position_current_CS[2]);
               
                Rect.localPosition = CheckPositionUI(graphUI);
                GraphUIManager.instance.CreateGraph( go, graphUI);
                // go.GetComponent<PrefabController>().coloratedObject = graphUI;
                // go.GetComponent<PrefabController>().Setup();
                return go;
            case "buttonUI":
                PTVR.Stimuli.Objects.ButtonUI buttonUI = (PTVR.Stimuli.Objects.ButtonUI)_uiObj;
                if (The3DWorldManager.instance.GameObjectCol.ContainsKey(buttonUI.id))
                {
                    go = The3DWorldManager.instance.GameObjectCol[buttonUI.id];
                }
                else
                {
                    go = Instantiate(ObjectsUI(_uiObj.type), _parent);
                }
                go.GetComponent<Image>().color = buttonUI.color.ToUnityColor();

                RectTransform buttonRect = go.GetComponent<RectTransform>();
                buttonRect.localPosition = CheckPositionUI(buttonUI);
                Debug.Log(" buttonRect.localPosition" + buttonRect.localPosition.ToString());
                foreach (PTVR.Stimuli.Objects.UIObject uiObj in buttonUI.UIObjects)
                {
                    GameObject childGo = CreateUIObject(uiObj, go.transform);
                }
                go.GetComponent<ButtonUI>().button = buttonUI.nameButton;
                go.GetComponent<RectTransform>().sizeDelta = new Vector2(buttonUI.width, buttonUI.height);
                Debug.Log("Here");
                if (go.GetComponent<PrefabController>() != null)
                {
                    go.GetComponent<PrefabController>().coloratedObject = buttonUI;
                    go.GetComponent<PrefabController>().Setup();
                }
                return go;
            default:
                return null;
        }
    }

    public void CleanUI()
    {
        foreach (Transform child in canvasUI)
        {
           child.gameObject.SetActive(false);
        }
    }

    public Vector3 CheckPositionUI(PTVR.Stimuli.Objects.VirtualPoint _virtualPoint)
    {
        if (tempHeightScreen != Screen.height)
        {
            tempHeightScreen = Screen.height;
        }
        if (tempWidthScreen != Screen.width)
        { 
            tempWidthScreen = Screen.width;
        }
        Vector3 temp = _virtualPoint.ToUnityUI();
        float y = temp.y * (tempHeightScreen/2);
        float x = temp.x * (tempWidthScreen/2);

        Vector3 pos = new Vector3(x,y);
        return pos;
    }
}
