
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Globalization;
using UnityEngine.UI;
/**
* \brief Internal class that manages the Visual scenes. It create the objects extracted from the JSON files and place them in the world.
*/
public class SetVisualSettings : MonoBehaviour
{
    #region Inspector
    public static SetVisualSettings instance;

    public Dictionary<long, GameObject> GameObjectCol; //!< List of created GameObject based on uniq IDs to avoid collision. Ref to the ExperimentManager's one
    public Material materialTransparency;
    public List<float> infoTextmeshpro = new List<float>(); //!< Info used in the DemoEnvironments.cs script for the Typography Demo.

    [SerializeField]
    BasicObjectsData basicObjectsData;

    [SerializeField]
    Transform canvasUI;
    
    public GameObject[] controllerModels; //!< The controler 3D model put in the Unity interface. To possibly put them invisible or other later.
    public System.DateTime t_scene;
    //Shaders are set in the inspector
    [SerializeField]
    Shader diffuse;
    [SerializeField]
    Shader unlit;
    [SerializeField]
    Material transparencyMat;
    [SerializeField]
    Material opaqueMat;

    [SerializeField]
    Shader outline;
    public Transform current_cs;
    public Pose current_coordinate_system_transform = new Pose();

    public Camera camRight; //!< A reference to the camera Right Eye

    public Camera camLeft; //!< A reference to the camera Left Eye
    #endregion Inspector
    public GameObject side_view_camera;
    public GameObject headset_sphere;
    #region Privates


    string prefixForWWWQuery = "file:///";     //!< This prefix is used for Windows for local drive retrieval

    Dictionary<string, Texture2D> SpriteImagesDict = new Dictionary<string, Texture2D>();  //!< Deprecated

    public bool isEditor;

    //The origin reference by default is (0,0,0) if the json had .SetOrigin is the origin setup
    public Vector3 origin = Vector3.zero;
    public Vector3 originRotation = Vector3.zero;
    List<PTVR.Stimuli.Objects.VisualObject> objectsTMP1, objectsTMP2;


    #endregion Privates

    #region UnityCall
    public void Awake()
    {

        if (instance != null)
        {
            Destroy(this);
            Debug.LogWarning("Double SetVisualSettings.cs ");
        }
        else
        {
            instance = this;
        }
    
        GameObjectCol = The3DWorldManager.instance.GameObjectCol;
        GameObjectCol[-1] = The3DWorldManager.instance.controller[1].transform.gameObject;//Right
        GameObjectCol[-2] = The3DWorldManager.instance.controller[0].transform.gameObject;//Left
        GameObjectCol[-3] = camLeft.transform.parent.gameObject;
        GameObjectCol[-4] = transform.gameObject;//Current_CS
        GameObjectCol[-5] = transform.gameObject;//Global_CS


#if UNITY_EDITOR
        isEditor = true;
        Debug.Log("is Editor");
#endif

    }

    /** Unity update method. It was used for facing billboard sprite. Deprecated. */
    void Update()
    {
        if (!GameObjectCol.ContainsKey(-6))//Correspond to one orbite
        {
            AddEyetrackingOrbiteTransform();
        }
    }
    #endregion UnityCall

    void AddEyetrackingOrbiteTransform()
    {
      
            GameObjectCol[-6] = EyesTrackingManager.instance.eyeOrbiteLeft.gameObject;//Left Eye
            GameObjectCol[-7] = EyesTrackingManager.instance.eyeOrbiteRight.gameObject;//Right Eye
            GameObjectCol[-8] = EyesTrackingManager.instance.eyeOrbiteCombine.gameObject;//Combined Eye
        
    }
    /** Method that manages audio file opening
     * @param filepath is the audio file absolute or relative path to the execution location. ogg to be prefered.
     */
    private AudioClip LoadAudioFile(string _filepath)
    {
        if (System.IO.File.Exists(_filepath))
        {
            string url = prefixForWWWQuery + _filepath;
            WWW www = new WWW(url); // osolete
            /*  UnityWebRequest uwr = UnityWebRequestMultimedia.GetAudioClip(url,AudioType.OGGVORBIS);
              AudioClip myClip = DownloadHandlerAudioClip.GetContent(uwr);
  */
            //     Debug.Log(url);
            return www.GetAudioClip(true, true); //obsolete

        }
        return null;
    }

    /** private method to configure the controller visibility for the current scene. Does not desactivate them.
     * @param vis true/false for visible/invisible.
     */
    void ControllerVisibility(bool _isVisible)
    {
        controllerModels[0].SetActive(_isVisible);
        controllerModels[1].SetActive(_isVisible);
    }
    void ControllerVisibilityLeft(bool _isVisible)
    {
        controllerModels[2].SetActive(_isVisible);
    }

    void ControllerVisibilityRight(bool _isVisible)
    {
        controllerModels[3].SetActive(_isVisible);
    }

    void HandVisibilityLeft(bool _isVisible)
    {
        controllerModels[0].SetActive(_isVisible);
    }

    void HandVisibilityRight(bool _isVisible)
    {
        controllerModels[1].SetActive(_isVisible);
    }

    void activate_children(Transform Obj)
    {
        foreach (Transform Child in Obj)
        {
            Child.gameObject.SetActive(true);

            if (Child.transform.childCount > 0)
            {
                activate_children(Child);
            }
        }
    }

    /** The main method to create the scene. It creates the main objects, the camera events etc.
     * @param ts is the ptvr scene constructed from the JSON
     */
    public void SetScene(PTVR.Stimuli.Objects.VisualScene _visualScene)
    {
        //GetComponent<VisualFulfilmentCriteria>().SetId(_visualScene.id); //Set the id. It is used during fulfilment to store results with ids.
        PositionManager.instance.SetId(_visualScene.id); //Set the id. It is used during fulfilment to store results with ids.
        if (The3DWorldManager.instance.GameObjectCol.ContainsKey(_visualScene.current_coordinate_system_id))
        {
           // current_coordinate_system_transform = ExperimentManager.instance.GameObjectCol[_visualScene.current_coordinate_system_id].transform;
        }
            // Debug.Log(_visualScene.cameraOperators.Count);
        /*
        foreach(string ex in _examine.type)
        {
            Debug.Log("Find Ex");
        }
        */

        DataBank.Instance.SetData("trial", _visualScene.trial.ToString());
        DataBank.Instance.SetData("id_scene", _visualScene.id.ToString());
        DataBank.Instance.SetData("elasped_frames", Time.frameCount.ToString());
        if (current_coordinate_system_transform != null)
        {
            origin = current_coordinate_system_transform.position;

            originRotation = current_coordinate_system_transform.rotation.eulerAngles;
        }

        objectsTMP2 = _visualScene.objects;
        int acounter = 0;
        //TODO: When No Parent while be usefull
        while(objectsTMP2.Count != 0)
        {
            if (acounter > 5* _visualScene.objects.Count)
            {
                Debug.LogError(objectsTMP2.Count);
                break;
            }
            acounter++;
            objectsTMP1 = objectsTMP2;
            objectsTMP2 = new List<PTVR.Stimuli.Objects.VisualObject>();
            foreach (PTVR.Stimuli.Objects.VisualObject visualObject in objectsTMP1)
            {
                GameObject go;
                if (GameObjectCol.ContainsKey(visualObject.id))
                {
                    //go = GameObjectCol[visualObject.id];
                    go = SetupObject(visualObject);
                    if (visualObject.isActive)
                    {
                        go.SetActive(true);
                        // activate_children(go.transform);
                        // Carlos : Re-activate son, grandson but not more.
                        if (go.transform.childCount != 0 && go.transform.GetChild(0).childCount != 0)
                        {
                            go.transform.GetChild(0).gameObject.SetActive(true);
                            go.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                        }
                    }
                    else
                        go.SetActive(false);
                    SetCoordinatedProperties(go, visualObject);

                   // SetSizeableProperties(go, visualObject);
                   if(go.GetComponent<PrefabController>()!=null)
                    go.GetComponent<PrefabController>().Start();
                }
                else
                {
                    go = SetupObject(visualObject);
          
                    GameObjectCol[visualObject.id] = go;
                
                    if (visualObject.isActive)
                        go.SetActive(true);
                    else
                        go.SetActive(false);
                }

                if (visualObject.type == "audio_source")
                {
                    while (go.GetComponent<AudioSource>().clip.loadState == AudioDataLoadState.Unloaded || go.GetComponent<AudioSource>().clip.loadState == AudioDataLoadState.Failed)
                        // while (!go.GetComponent<AudioSource>().clip.isReadyToPlay)//obsolete
                        //What's happen here?    
                        ;
                    go.GetComponent<AudioSource>().Play();
                }

                if (visualObject.type == "tangent_screen")
                {
                    PTVR.Stimuli.Objects.FlatScreen tangentScreen = (PTVR.Stimuli.Objects.FlatScreen)visualObject;
                    PrepareTangentScreen(go, tangentScreen);
                    go.GetComponent<PrefabController>().Start();
                }
                else if (visualObject.type == "flat_screen")
                {
                    PTVR.Stimuli.Objects.FlatScreen flatscreen = (PTVR.Stimuli.Objects.FlatScreen)visualObject;
                    PrepareFlatScreen(go, flatscreen);
                }
                else if (visualObject.type == "lineTool")
                {
                   /* PTVR.Stimuli.Objects.LineTool line = (PTVR.Stimuli.Objects.LineTool)visualObject;
                    SetLine(go, line.parentId, line.StartPointToUnityPos(), line.EndPointToUnityPos(),line.scale[0]);
                    go.transform.Rotate(go.GetComponent<PrefabController>().currentCS.rot.x, go.GetComponent<PrefabController>().currentCS.rot.y, go.GetComponent<PrefabController>().currentCS.rot.z,Space.World);
   */
                }
                else if (visualObject.type == "sprite")
                {
                    /* PTVR.Stimuli.Objects.LineTool line = (PTVR.Stimuli.Objects.LineTool)visualObject;
                     SetLine(go, line.parentId, line.StartPointToUnityPos(), line.EndPointToUnityPos(),line.scale[0]);
                     go.transform.Rotate(go.GetComponent<PrefabController>().currentCS.rot.x, go.GetComponent<PrefabController>().currentCS.rot.y, go.GetComponent<PrefabController>().currentCS.rot.z,Space.World);
    */
                }
                else 
                {
                    SetSizeableProperties(go, visualObject);
                }
            }
        }
        
        foreach(PTVR.Stimuli.Objects.UIObject uIObject in _visualScene.uiObjects)
        {
            GameObject go;
            if (GameObjectCol.ContainsKey(uIObject.id))
            {
                go = GameObjectCol[uIObject.id];
                if (uIObject.isActive)
                    go.SetActive(true);
                else
                    go.SetActive(false);
            }
            else
            {
                go = SetUISettings.instance.CreateUIObject(uIObject);
                GameObjectCol[uIObject.id] = go;
                if (uIObject.isActive)
                    go.SetActive(true);
                else
                    go.SetActive(false);
            }
        }
        
        camRight.backgroundColor = _visualScene.rightBackgroundColor.ToUnityColor(); //Set the background color
        camLeft.backgroundColor = _visualScene.leftBackgroundColor.ToUnityColor();//Set the background color

        if (The3DWorldManager.instance.curr3DWorld.leftBlind == true)
        { 
            camLeft.cullingMask = 0;
        }
        if (The3DWorldManager.instance.curr3DWorld.rightBlind == true)
        {
            camRight.cullingMask = 0;
        }


        // side view camera and sphere headset
        headset_sphere = GameObject.Find("SphereHeadset");
        side_view_camera = GameObject.Find("SideViewCamera");

        //Debug.Log(headset_sphere);

        if (headset_sphere != null)
        {
            if (_visualScene.side_view_on_pc == true)
            {

                headset_sphere.SetActive(true);
                side_view_camera.SetActive(true);
            }
            else
            {
                headset_sphere.SetActive(false);
                side_view_camera.SetActive(false);
            }
        }


        // Change skybox
        Material mat = null;

        if (_visualScene.skybox.Equals("BrightMorning"))
        {
            mat = Resources.Load<Material>("skyboxes/SkyBrightMorning");
        }

        if (_visualScene.skybox.Equals("Afternoon"))
        {
            mat = Resources.Load<Material>("skyboxes/SkyAfterNoon");
        }

        if (_visualScene.skybox.Equals("Cloudy"))
        {
            mat = Resources.Load<Material>("skyboxes/SkyCloudy");
        }

        if (_visualScene.skybox.Equals("Sunset"))
        {
            mat = Resources.Load<Material>("skyboxes/SkySunset");
        }

        if (_visualScene.skybox.Equals("Night"))
        {
            mat = Resources.Load<Material>("skyboxes/SkyNight");
        }

        if (mat != null)

        {

            GameObject.Find("Left_EyeCamera").GetComponent<Skybox>().material = mat;
            GameObject.Find("Right_EyeCamera").GetComponent<Skybox>().material = mat;

        }

        if (_visualScene.global_lights_intensity != 1)
        {
            var lights = GameObject.Find("Lights");
            foreach (Transform child in lights.transform)
            {
                child.GetComponent<Light>().intensity = _visualScene.global_lights_intensity;

            }
        }

        //DynamicGI.UpdateEnvironment();

        ControllerVisibilityLeft(_visualScene.isLeftControllerVisibility);
        ControllerVisibilityRight(_visualScene.isRightControllerVisibility);
        HandVisibilityLeft(_visualScene.isLeftHandVisibility);
        HandVisibilityRight(_visualScene.isRightHandVisibility);
        //DataBank.Instance.SetData("scene_start_timestamp", System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss.fff"));
        t_scene = System.DateTime.Now;
    }


    /** Internal method to choose where to instantiate game object. If the parent id is lower or equal to 0 (default value), it's instantiated in the world. Otherwise, the objec is instantiated in the parent referential.
     * @param go The gameObject to instantiate.
     * @param parentId the parent game object id. The parent should already be instantiated and contained in SetTrialConfiguration::GameObjectCol.
     * @return the instantiated gameObject.
     */
    GameObject InstantiateInWorldOrParent(GameObject go, long parentId)
    {
        if (parentId > -9 && GameObjectCol.ContainsKey(parentId))
        {
            return Instantiate(go, GameObjectCol[parentId].transform);
        }

        // SceneManager.instance.visualTemplate
        return Instantiate(go, SceneManager.instance.visualTemplate);

    }

    public GameObject Objects(string _typeObject)
    {
        for (int count = 0; count < basicObjectsData.basicObjects.Count; count++)
        {
            if (basicObjectsData.basicObjects[count].typeObject == _typeObject)
            {
                return basicObjectsData.basicObjects[count].prefabObject;
            }
        }
        return null;
    }
    /** Creating each object. It identificate it then manage their propoerties.
     */

    public GameObject CreateObject(string _type,long _idParent)
    {
        GameObject go;
        go = InstantiateInWorldOrParent(Objects(_type), _idParent);
        return go;
    }
    public  GameObject SetupObject(PTVR.Stimuli.Objects.VisualObject _visualObj)
    {
        
        GameObject go ;
        SetCurrentCoordinatesSystemPropeties(_visualObj);

        switch (_visualObj.type)
        { //Create proper mesh and set correct size
            case "coordinated":
                PTVR.Stimuli.Objects.EmptyVisualObject coordinate = (PTVR.Stimuli.Objects.EmptyVisualObject)_visualObj;

                //create a simple empty gameobect. Easier, to get only GameObjects to manage instead of transforms.
                if (coordinate.isRelativeToHead)
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                go.tag = "Coordinate";
                //create a simple empty gameobect. Easier, to get only GameObjects to manage instead of transforms.
                SetCoordinatedProperties(go, coordinate);
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                break;
            case "cameraPTVR":
                PTVR.Stimuli.Objects.CameraPTVR cameraPTVR = (PTVR.Stimuli.Objects.CameraPTVR)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                SetCoordinatedProperties(go, cameraPTVR);
                SetSizeableProperties(go, cameraPTVR);
              
                go.GetComponent<CameraController>().Setup();
      
                go.GetComponent<CameraController>().fov = cameraPTVR.fov;
                go.GetComponent<Camera>().backgroundColor = cameraPTVR.backgroundColor.ToUnityColor();
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                for (int i = 0; i < cameraPTVR.pastRendererToMaterial.Count; i++)
                       go.GetComponent<CameraController>().SetMaterialToObject(cameraPTVR.pastRendererToMaterial[i]);
                for (int i = 0; i < cameraPTVR.pastShotToMaterial.Count; i++)
                {
                    go.GetComponent<CameraController>().SetupShot();
                    go.GetComponent<CameraController>().SetPhotoToObject(cameraPTVR.pastShotToMaterial[i]);
                }

                break;
            case "calculator":
                PTVR.Stimuli.Objects.Calculator calc = (PTVR.Stimuli.Objects.Calculator)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                SetCoordinatedProperties(go, calc);
                SetSizeableProperties(go, calc);
                go.GetComponent<CalculatorController>().calculatorData = calc;
                Debug.Log("test");
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                break;
            case "gizmo":
                PTVR.Stimuli.Objects.Gizmo gizmo = (PTVR.Stimuli.Objects.Gizmo)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                //SetColoratedProperties(go, gizmo);
                SetCoordinatedProperties(go, gizmo);
                SetSizeableProperties(go, gizmo);
                SetVisibilityProperties(go, gizmo);
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                if (gizmo.isROI)
                {
                    go.AddComponent<Target>().shader = outline;
                }
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                go.transform.GetChild(0).GetChild(0).localScale = new Vector3(gizmo.length /2 , gizmo.width,  gizmo.width);
                go.transform.GetChild(0).GetChild(1).localScale = new Vector3(gizmo.width, gizmo.length / 2, gizmo.width);
                go.transform.GetChild(0).GetChild(2).localScale = new Vector3(gizmo.width, gizmo.width, gizmo.length / 2);
                break;
            case "capsule":
                PTVR.Stimuli.Objects.Capsule caps = (PTVR.Stimuli.Objects.Capsule)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                SetColoratedProperties(go, caps);
                SetCoordinatedProperties(go, caps);
                SetSizeableProperties(go, caps);
                SetVisibilityProperties(go, caps);
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                if (caps.isROI)
                {
                    go.AddComponent<Target>().shader = outline;
                }
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                break;
            case "cube":
                PTVR.Stimuli.Objects.Cube cube = (PTVR.Stimuli.Objects.Cube)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                SetColoratedProperties(go, cube);
                SetCoordinatedProperties(go, cube);
                SetSizeableProperties(go, cube);
                SetVisibilityProperties(go, cube);
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                if (cube.isROI)
                {
                    go.AddComponent<Target>().shader = outline;
                }
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;

                
                break;

            case "light":
                PTVR.Stimuli.Objects.Light light = (PTVR.Stimuli.Objects.Light)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }

                SetSizeableProperties(go, light);
                SetCoordinatedProperties(go, light);
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;

                go.transform.GetChild(0).GetChild(0).GetComponent<Light>().range = light.lightRange;
                go.transform.GetChild(0).GetChild(0).GetComponent<Light>().intensity = light.intensity;
                go.transform.GetChild(0).GetChild(0).GetComponent<Light>().spotAngle = light.spotAngle;

                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;

                if (go.GetComponent<PrefabController>().currentCS.rot != null)
                    go.GetComponent<PrefabController>().currentCS.rot = current_cs.transform.rotation.eulerAngles;

                break;
            case "customObject":
                PTVR.Stimuli.Objects.CustomObject customModel = (PTVR.Stimuli.Objects.CustomObject)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                    var myLoadedAssetBundle = AssetBundle.LoadFromFile(customModel.fileName);
                    if (myLoadedAssetBundle == null)
                    {
                        Debug.Log("Failed to load AssetBundle!");
                    }
                    var prefab = myLoadedAssetBundle.LoadAsset<GameObject>(customModel.assetPath);

                    if (prefab == null)
                    {
                        Debug.Log("Failed to load prefab!");
                    }

                    var child = Instantiate(prefab);

                    child.transform.parent = go.transform.GetChild(0).GetChild(0);

                    if (customModel.make_camera_child)
                    {
                        Transform player = GameObject.Find("OpenXRPlayer").transform;
                        player.parent = go.transform;
                    }


                     myLoadedAssetBundle.Unload(false);

                }
                SetColoratedProperties(go, customModel);
                SetCoordinatedProperties(go, customModel);
                SetSizeableProperties(go, customModel);
                SetVisibilityProperties(go, customModel);
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                go.GetComponent<PrefabController>().isStatic = customModel.static_object;


                if (customModel.add_physics)
                {
                    BoxCollider bo = go.transform.GetChild(0).GetComponent<BoxCollider>();
                    Destroy(bo);

                    Rigidbody rb = go.AddComponent<Rigidbody>();
                    rb.useGravity = false;
                    rb.isKinematic = false;
                }

                // Change collider size
                if (!Enumerable.SequenceEqual(customModel.collider_size, new List<float> { 1.0f, 1.0f, 1.0f }))
                {
                    BoxCollider bo = go.transform.GetChild(0).GetComponent<BoxCollider>();

                    bo.size = new Vector3(customModel.collider_size[0],
                                          customModel.collider_size[1],
                                          customModel.collider_size[2]);
                }

                // Change collider center
                if (!Enumerable.SequenceEqual(customModel.collider_center, new List<float> { 0.0f, 0.5f, 0.0f }))
                {
                    BoxCollider bo = go.transform.GetChild(0).GetComponent<BoxCollider>();

                    bo.center = new Vector3(customModel.collider_center[0],
                                            customModel.collider_center[1],
                                            customModel.collider_center[2]);

                }

                // display green box around collider
                if (customModel.display_collider)
                {

                    BoxCollider bo = go.transform.GetChild(0).GetComponent<BoxCollider>();
                    go.transform.GetChild(0).GetChild(1).localScale = bo.size;
                    go.transform.GetChild(0).GetChild(1).localPosition = bo.center;

                } else
                {
                    go.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
                }

                if (customModel.calculate_dimensions)
                {
                    // Print mesh bounds for testing purposes
                    Vector3 meters_size = new Vector3(0, 0, 0);

                    // get ObjectBox PTVR to retrieve size in meters
                    foreach (Transform child in go.transform.GetChild(0).GetChild(0).GetChild(0).transform)
                    {
                        if (child.name == "ObjectBoxPTVR")
                        {
                            var sc = go.transform.localScale;

                            meters_size[0] = sc[0] * child.localScale[0];
                            meters_size[1] = sc[1] * child.localScale[1];
                            meters_size[2] = sc[2] * child.localScale[2];

                        }
                    }

                    go.GetComponent<PrefabController>().object_size_in_meters = meters_size;
                }


                // display grey box around mesh
                if (customModel.display_dimensions)
                {

                    bool flag_ObjectBoxPTVR = false;

                    Vector3 meters_size = new Vector3(0, 0, 0);
                    Vector3 object_box_position = new Vector3(0, 0, 0);

                    // get ObjectBox PTVR to retrieve size in meters
                    foreach (Transform child in go.transform.GetChild(0).GetChild(0).GetChild(0).transform)
                    {
                        if (child.name == "ObjectBoxPTVR")
                        {
                            flag_ObjectBoxPTVR = true;

                            var sc = go.transform.localScale;

                            meters_size[0] = sc[0] * child.localScale[0];
                            meters_size[1] = sc[1] * child.localScale[1];
                            meters_size[2] = sc[2] * child.localScale[2];

                            object_box_position = child.localPosition;


                            
                        }

                        go.GetComponent<PrefabController>().object_size_in_meters = meters_size;
                    }


                    // Print mesh bounds for testing purposes
                    if (flag_ObjectBoxPTVR)
                    {

                        go.transform.GetChild(0).GetChild(2).localScale = go.GetComponent<PrefabController>().object_size_in_meters;
                        go.transform.GetChild(0).GetChild(2).localPosition = object_box_position;

                        NumberFormatInfo nfi = new System.Globalization.NumberFormatInfo();
                        nfi.NumberDecimalSeparator = ".";


                        var text_for_text_box = "ObjectBoxPTVR Size\n" + "x : " + Math.Round((decimal)(meters_size[0]* go.transform.GetChild(0).localScale[0]), 2).ToString(nfi) + " m\n" +
                                                "y : " + Math.Round((decimal)(meters_size[1] * go.transform.GetChild(0).localScale[2]), 2).ToString(nfi) + " m\n" +
                                                "z : " + Math.Round((decimal)(meters_size[2] * go.transform.GetChild(0).localScale[2]), 2).ToString(nfi) + " m";


                        var text_object = Resources.Load("CubeObjectBox") as GameObject;

                        var text_object_go = Instantiate(text_object, go.transform.position + new Vector3(0, meters_size[1] * go.transform.GetChild(0).localScale[2] - 1.0f, 0), go.transform.rotation);

                        text_object_go.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = text_for_text_box;


                    } else
                    {
                        go.transform.GetChild(0).GetChild(2).gameObject.SetActive(false); // dimension cube
                        var text_for_text_box = "ObjectBoxPTVR not found";

                        var text_object = Resources.Load("CubeObjectBox") as GameObject;

                        var text_object_go = Instantiate(text_object, go.transform.position, go.transform.rotation);

                        text_object_go.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = text_for_text_box;
                    }
                }
                else
                {
                    go.transform.GetChild(0).GetChild(2).gameObject.SetActive(false); // dimension cube
                    //go.transform.GetChild(0).GetChild(3).gameObject.SetActive(false);
                }


                if (go.transform.GetChild(0).GetChild(0).GetChild(0).name.Contains("LetterCube"))
                {
                    if (customModel.pass_letter != null)
                    {
                        for (int i = 0; i < go.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).childCount; i++)
                       {
                            go.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(i).GetChild(0).GetComponent<TextMeshProUGUI>().text = customModel.pass_letter;
                        }
                    }
                }


                // put four images for grab letters game
                if (customModel.images_list.Count > 0)
                {
                    for (int i = 0; i < go.transform.GetChild(0).GetChild(0).GetChild(0).childCount; i++)
                    {
                        Texture2D Tex2D;
                        byte[] FileData;

                        FileData = File.ReadAllBytes(customModel.images_list[i]);
                        Tex2D = new Texture2D(2, 2);
                        Tex2D.LoadImage(FileData);

                        Sprite newSprite = Sprite.Create(Tex2D, new Rect(0, 0, Tex2D.width, Tex2D.height), new Vector2(0, 0));
                        
                        go.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(i).GetChild(0).GetChild(0).GetComponent<Image>().sprite = newSprite;
                    }
                }


                if (customModel.isROI)
                {
                    go.AddComponent<Target>().shader = outline;
                }
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                break;

            case "cylinder":
                PTVR.Stimuli.Objects.Cylinder cylinder = (PTVR.Stimuli.Objects.Cylinder)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                SetColoratedProperties(go, cylinder);
                SetCoordinatedProperties(go, cylinder);
                SetSizeableProperties(go, cylinder);
                SetVisibilityProperties(go, cylinder);
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                if (cylinder.isROI)
                {
                    go.AddComponent<Target>().shader = outline;
                }
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                break;
            case "plane":
                PTVR.Stimuli.Objects.Plane plane = (PTVR.Stimuli.Objects.Plane)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                SetColoratedProperties(go, plane);
                SetCoordinatedProperties(go, plane);
                SetSizeableProperties(go, plane);
                SetVisibilityProperties(go,plane);
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                if (plane.isROI)
                {
                    go.AddComponent<Target>().shader = outline;
                }
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                break;
            case "elevator":
                PTVR.Stimuli.Objects.Elevator elevator = (PTVR.Stimuli.Objects.Elevator)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                SetColoratedProperties(go, elevator);
                SetCoordinatedProperties(go, elevator);
                SetSizeableProperties(go, elevator);
                SetVisibilityProperties(go, elevator);
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                if (elevator.isROI)
                {
                    go.AddComponent<Target>().shader = outline;
                }
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;

                go.transform.Find("MovingPlatform").GetComponent<MovePlatform>().speed = elevator.speed;
                go.transform.Find("WayPointPath1").Find("Waypoint2").position = new Vector3(0,elevator.height,0);
                go.transform.Find("WayPointPath1").GetComponent<WaypointPath>().comeBack = elevator.comeback;

                break;
            case "forest":
                PTVR.Stimuli.Objects.Forest forest = (PTVR.Stimuli.Objects.Forest)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                SetColoratedProperties(go, forest);
                SetCoordinatedProperties(go, forest);
                SetSizeableProperties(go, forest);
                SetVisibilityProperties(go, forest);
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                if (forest.isROI)
                {
                    go.AddComponent<Target>().shader = outline;
                }
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                break;
            case "quad":
                PTVR.Stimuli.Objects.Quad quad = (PTVR.Stimuli.Objects.Quad)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                SetColoratedProperties(go, quad);
                SetCoordinatedProperties(go, quad);
                SetSizeableProperties(go, quad);
                SetVisibilityProperties(go, quad);
                go.GetComponent<PrefabController>().Setup();
                if (quad.isROI)
                {
                    go.AddComponent<Target>().shader = outline;
                }
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                break;
            case "sphere":
                PTVR.Stimuli.Objects.Sphere sphere = (PTVR.Stimuli.Objects.Sphere)_visualObj;

                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                SetColoratedProperties(go, sphere);
                SetCoordinatedProperties(go, sphere);
                SetSizeableProperties(go, sphere);
                SetVisibilityProperties(go, sphere);
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                if (sphere.isROI)
                {
                    go.AddComponent<Target>().shader = outline;
                }
                //go.GetComponent<PrefabController>().Setup();
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                break;
         
            case "line":
                //TODO add change rotate and translate
                PTVR.Stimuli.Objects.Line line = (PTVR.Stimuli.Objects.Line)_visualObj;
                bool areNotEndPoints = 
                    line.startPointObj != null &&
                    (!GameObjectCol.ContainsKey(line.startPointObj.id) || !GameObjectCol.ContainsKey(line.endPointObj.id));
                
                if (areNotEndPoints)
                {
                    objectsTMP2.Add(_visualObj);
                    return null;
                }

                go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId); //InstantiateInWorldOrParent(BasicObjects[12], _visualObj.parentId);
                LineRenderer lineRenderer = go.GetComponent<LineRenderer>();
                PrefabController prefabController = go.GetComponent<PrefabController>();
                go.GetComponent<PrefabController>().coloratedObject = line;

                lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
                // lineRenderer.SetColors(lr.lineColor.ToUnityColor(), lr.lineColor.ToUnityColor());//Obsolete
                lineRenderer.startColor = line.startColor.ToUnityColor();
                lineRenderer.endColor = line.endColor.ToUnityColor();
                // lineRenderer.SetWidth(0.06f, 0.06f);//Obsolete
                lineRenderer.startWidth = 0.06f;
                lineRenderer.endWidth = 0.06f;

                if (line.startPointObj != null)
                {
                    prefabController.startPoint = GameObjectCol[line.startPointObj.id].transform;
                    prefabController.endPoint = GameObjectCol[line.endPointObj.id].transform;
                    prefabController.currentCS.pos = current_cs.transform.position;
                }
                else
                {
                    GameObject startObj = InstantiateInWorldOrParent(Objects("coordinated"), _visualObj.parentId);
                    GameObject endObj = InstantiateInWorldOrParent(Objects("coordinated"), _visualObj.parentId);
                    startObj.transform.position = new Vector3(line.startPoint[0], line.startPoint[1], line.startPoint[2]);
                    endObj.transform.position = new Vector3(line.endPoint[0], line.endPoint[1], line.endPoint[2]);
                    prefabController.startPoint = startObj.transform;
                    prefabController.endPoint = endObj.transform;
                    prefabController.currentCS.pos = current_cs.transform.position;
                }
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                break;
            case "text":
                PTVR.Stimuli.Objects.Text tmp = (PTVR.Stimuli.Objects.Text)_visualObj;

                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }

                prefabController = go.GetComponent<PrefabController>();
           
                TMP_Text textmeshpro = go.GetComponent<TMP_Text>();
                RectTransform textRect = go.GetComponent<RectTransform>();
                SetColoratedProperties(go, tmp);

                SetCoordinatedProperties(go, tmp);
                SetSizeableProperties(go, tmp);
                float viewingDistanceInM = 0.0f;
               
                if (current_cs .transform!= null)
                     viewingDistanceInM = (current_cs.transform.position - go.transform.position).magnitude; // We used magnitude because we need to get the length of the vector between the origin and the text
                else
                     viewingDistanceInM = (Vector3.zero- go.transform.position).magnitude;
                textRect.sizeDelta = new Vector2(tmp.textBoxScale[0], tmp.textBoxScale[1]);

                textmeshpro.enableWordWrapping = tmp.isWrapping;
                textmeshpro.richText = true;
                textmeshpro.text = tmp.text;
                textmeshpro.color = tmp.color.ToUnityColor();

                String applicationPath = Application.dataPath;
                char[] c = applicationPath.ToCharArray();
                string path = string.Empty;
                if (isEditor)
                {
                    path = applicationPath.Remove(c.Length - 6);
                    path = path + "PTVR_Researchers/PTVR_Operators/resources/Fonts/" + tmp.fontName + ".ttf";
                }
                else
                {
                    path = applicationPath.Remove(c.Length - 9);
                    path = path + "/resources/Fonts/" + tmp.fontName + ".ttf";
                }
                Font fontLoad = new Font(path);

                TMP_FontAsset font = TMP_FontAsset.CreateFontAsset(fontLoad);
                
                textmeshpro.font = font;
              
                if (tmp.visualAngleOfCenteredXHeight != 0)
                {
                    textmeshpro.fontSize = TextHeightManager.instance.CalculateXPhysicalHeightPt(viewingDistanceInM, tmp.visualAngleOfCenteredXHeight, textmeshpro);
                }
                else
                {
                    textmeshpro.fontSize = tmp.fontSize;
                }

                textmeshpro.alignment = TextAlignmentOptions.Midline; // Alignement by default 

                //Set style

                if (tmp.isBold && tmp.isItalic)
                    textmeshpro.fontStyle = FontStyles.Bold & FontStyles.Italic;
                else if (tmp.isBold)
                    textmeshpro.fontStyle = FontStyles.Bold;
                else if (tmp.isItalic)
                    textmeshpro.fontStyle = FontStyles.Italic;

                // Set horizontal alignment
                if (tmp.alignHorizontal == "Left")
                    textmeshpro.horizontalAlignment = HorizontalAlignmentOptions.Left;
                if (tmp.alignHorizontal == "Right")
                    textmeshpro.horizontalAlignment = HorizontalAlignmentOptions.Right;
                if (tmp.alignHorizontal == "Center")
                    textmeshpro.horizontalAlignment = HorizontalAlignmentOptions.Center;
                if (tmp.alignHorizontal == "Justified")
                    textmeshpro.horizontalAlignment = HorizontalAlignmentOptions.Justified;
                if (tmp.alignHorizontal == "Flush")
                    textmeshpro.horizontalAlignment = HorizontalAlignmentOptions.Flush;

                if (tmp.alignVertical == "Middle")
                    // Special treatment is needed for this one, because unlike the other parameter, VerticalAlignmentOptions.Middle and TextAlignmentOptions.Midline
                    // are differents : the first one doesn't align the text on the center of the x. 
                    if (tmp.alignHorizontal == "Left")
                        textmeshpro.alignment = TextAlignmentOptions.MidlineLeft;
                    if (tmp.alignHorizontal == "Right")
                        textmeshpro.alignment = TextAlignmentOptions.MidlineRight;
                    if (tmp.alignHorizontal == "Center")
                        textmeshpro.alignment = TextAlignmentOptions.Midline;
                    if (tmp.alignHorizontal == "Justified")
                        textmeshpro.alignment = TextAlignmentOptions.MidlineJustified;
                if (tmp.alignVertical == "Capline")
                    textmeshpro.verticalAlignment = VerticalAlignmentOptions.Capline;
                if (tmp.alignVertical == "Baseline")
                    textmeshpro.verticalAlignment = VerticalAlignmentOptions.Baseline;
                if (tmp.alignVertical == "Bottom")
                    textmeshpro.verticalAlignment = VerticalAlignmentOptions.Bottom;
                if (tmp.alignVertical == "Top")
                    textmeshpro.verticalAlignment = VerticalAlignmentOptions.Top;


                if (tmp.alignVertical == "midline")
                    textmeshpro.alignment = TextAlignmentOptions.Midline;
                if (tmp.alignVertical == "capline")
                    textmeshpro.alignment = TextAlignmentOptions.Capline;
                if (tmp.alignVertical == "baseline")
                    textmeshpro.alignment = TextAlignmentOptions.Baseline;

               // Debug.Log("horizontal = " + textmeshpro.horizontalAlignment + " , vertical = " + textmeshpro.verticalAlignment);
                //Operator Mode possibility
                if (tmp.isVerifySentence)
                {
                    ExaminatorManager.instance.VerifySentencePronunciation(tmp.text);
                }
                prefabController.defaultText = textmeshpro.text;
                prefabController.coloratedObject = tmp;
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                break;
            case "sprite":
                PTVR.Stimuli.Objects.Sprite sprite = (PTVR.Stimuli.Objects.Sprite)_visualObj;
                // if (sprite.isFacingCam)
                //  BasicObjects[7].transform.LookAt(cam.transform); //to debug

                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }

                
                //go.GetComponent<PrefabController>().currentCS.pos = origin;
                //go.GetComponent<PrefabController>().coloratedObject = sprite;
                SpriteRenderer renderer = go.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();     //(SpriteRenderer) (go.GetComponent<PrefabController>().renderers[0]);
                                                                                                                   // SpriteRenderer renderer = go.GetComponent<SpriteRenderer>();

                var tex_width = 0f;
                var tex_height = 0f;
                if (sprite.imageFile != "default")
                {
                    Texture2D tex;
                    if (!SpriteImagesDict.TryGetValue(sprite.imageFile, out tex))
                    {
                        tex = new Texture2D(1, 1);
                        string url = prefixForWWWQuery + sprite.imageFile;
                        applicationPath = Application.dataPath;
                        c = applicationPath.ToCharArray();
                        path = string.Empty;
                        if (isEditor)
                        {
                            path = applicationPath.Remove(c.Length - 6);
                            path = "C:\\PTVR\\PTVR_Researchers\\PTVR_Operators\\resources\\Images\\"+ sprite.imageFile;
                        }
                        else
                        {
                            path = applicationPath.Remove(c.Length - 9);
                            path = path + "/resources/Images/" + sprite.imageFile;
                        }
                        WWW www = new WWW(path);//osolete

                        www.LoadImageIntoTexture(tex); //obsolete
                        SpriteImagesDict.Add(sprite.imageFile, tex);
                    }
                    Vector2 pivot = new Vector2(0, 0);

                    if (sprite.isCenterPivot)
                    {
                        pivot.x = 0.5f;
                        pivot.y = 0.5f;
                    }
                    
                    renderer.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), pivot);

                }

                Texture2D Tex = renderer.sprite.texture;
                float pixPerUnit = renderer.sprite.pixelsPerUnit;


                SetColoratedProperties(go, sprite);
                SetCoordinatedProperties(go, sprite);
                SetSizeableProperties(go, sprite, new Vector3(pixPerUnit / Tex.width, pixPerUnit / Tex.height, 1));


                renderer.material = Resources.Load<Material>("Material/Sprite");
                

                break;
            case "audio_source":
                PTVR.Stimuli.Objects.AudioSource audioSource = (PTVR.Stimuli.Objects.AudioSource)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                go.GetComponent<AudioSource>().playOnAwake = audioSource.isPlayDirectly;
                go.GetComponent<AudioSource>().volume = audioSource.volume;
                go.GetComponent<AudioSource>().loop = audioSource.isLoop;
                if (audioSource.audioFile != "")
                {

                    applicationPath = Application.dataPath;
                    c = applicationPath.ToCharArray();
                    path = string.Empty;
                    if (isEditor)
                    {
                        path = applicationPath.Remove(c.Length - 7);
                        path = path + "/PTVR_Researchers/PTVR_Operators/resources/Sounds/" + audioSource.audioFile;
                        audioSource.audioFile = audioSource.audioFile.Replace("\\", "\\");
                        Debug.Log("editor : " + path);
                    }
                    else
                    {
                        path = applicationPath.Remove(c.Length - 9);
                        Debug.Log(path+ "/resources/Sounds/" + audioSource.audioFile);
                        path = path + "/resources/Sounds/" + audioSource.audioFile;
                    }

                    AudioClip clip = LoadAudioFile(path);
                    if (clip != null)
                        go.GetComponent<AudioSource>().clip = clip;
                    else
                        Debug.LogError("sound file " + audioSource.audioFile + " does not exist");
                }
                if(audioSource.isPlayDirectly)
                    go.GetComponent<AudioSource>().enabled = true;
                if(go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                SetCoordinatedProperties(go, audioSource);
                break;
            case "flat_screen":
                PTVR.Stimuli.Objects.FlatScreen flatScreen = (PTVR.Stimuli.Objects.FlatScreen)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects(_visualObj.type), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                PrepareFlatScreen(go, flatScreen);
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                break;
            case "tangent_screen":
                PTVR.Stimuli.Objects.FlatScreen tangentScreen = (PTVR.Stimuli.Objects.FlatScreen)_visualObj;
                if (GameObjectCol.ContainsKey(_visualObj.id))
                {
                    go = GameObjectCol[_visualObj.id];
                }
                else
                {
                    go = InstantiateInWorldOrParent(Objects("flat_screen"), _visualObj.parentId);//InstantiateInWorldOrParent(BasicObjects[], _visualObj.parentId);
                }
                PrepareTangentScreen(go, tangentScreen);
                Debug.Log("current_cs : " +current_cs.transform.position);
                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                Debug.Log("prefab : " + go.GetComponent<PrefabController>().currentCS.pos);
                go.GetComponent<PrefabController>().Setup();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
                break;
            case "lineTool":
                PTVR.Stimuli.Objects.LineTool lineTool = (PTVR.Stimuli.Objects.LineTool)_visualObj;
                //1 Cr�er Line  
           
                lineTool.sizeY_po = (lineTool.EndPointToUnityPos() - lineTool.StartPointToUnityPos()).magnitude / 2.0f;
                if (GameObjectCol.ContainsKey(lineTool.id))
                {
                    go = GameObjectCol[lineTool.id];
                }
                else
                {
                    go = CreateLine(lineTool.StartPointToUnityPos(), lineTool.EndPointToUnityPos(), lineTool.scale[0], _visualObj.parentId, lineTool);
                    GameObjectCol[lineTool.id] = go;
                }

                if (go.GetComponent<PrefabController>().currentCS.pos != null)
                {  
                    go.GetComponent<PrefabController>().currentCS.pos = current_cs.transform.position;
                    go.GetComponent<PrefabController>().currentCS.rot = current_cs.transform.rotation.eulerAngles;
                }
                SetColoratedProperties(go, lineTool);

                SetCoordinatedProperties(go, lineTool);
                SetLine(go, lineTool.parentId, go.transform.localPosition + lineTool.StartPointToUnityPos(), lineTool.EndPointToUnityPos() + go.transform.localPosition, lineTool.scale[0]);
                //go.transform.position += lineTool.StartPointToUnityPos();
                go.GetComponent<PrefabController>().parentId = _visualObj.parentId;
       
                break;
            default:
                Debug.LogError("Unknown VisualObject type encountered.");
                go = null;
                break;
        }
        //here should go all generalistic setproperties under visualobject. TODO with colorated, sizeable etc.
        SetFrameProperties(go, _visualObj);
        if (go.GetComponentInChildren<ObjectCollider>() != null)
        { go.GetComponentInChildren<ObjectCollider>().id = _visualObj.id; }
        if (go.GetComponent<ObjectCollider>() != null)
        { go.GetComponent<ObjectCollider>().id = _visualObj.id; }
        return go;
    }



    void PrepareTangentScreen(GameObject go, PTVR.Stimuli.Objects.FlatScreen tangentScreen)
    {

        bool isKeepPerimetricAngles = tangentScreen.isKeepPerimetricAngles; ;
        bool isAutoRadialDistance = tangentScreen.isAutoRadialDistance;

        PrepareFlatScreen(go, tangentScreen);

        //tangentScreen.isFacingOrigin = true;
        tangentScreen.isKeepPerimetricAngles = isKeepPerimetricAngles;
        tangentScreen.isAutoRadialDistance = isAutoRadialDistance;

        SetCoordinatedProperties(go, tangentScreen);
    }

    void FullActivate(Transform theParent)
    {
        theParent.gameObject.SetActive(true);
        foreach (Transform child in theParent)
        {
            FullActivate(child);
        }
    }

    void PrepareFlatScreen(GameObject go, PTVR.Stimuli.Objects.FlatScreen flatScreen)
    {
        FullActivate(go.transform.Find("ScaledTransform"));
        //Debug.Log(flatScreen.isGetDataEyes);
        if ((flatScreen.isGetDataEyes || flatScreen.isGetDataHead) && !go.transform.GetChild(0).gameObject.GetComponent<FlatScreen>())
        {
            if (go.transform.GetChild(0).gameObject.GetComponent<FlatScreen>() == null)
            {
                if (isEditor)
                    go.transform.GetChild(0).gameObject.AddComponent<FlatScreen>().filenamePrefix =/* "PTVR_Researchers/PTVR_Operator/Result/" + */flatScreen.filenamePrefix;
                else
                    go.transform.GetChild(0).gameObject.AddComponent<FlatScreen>().filenamePrefix = flatScreen.filenamePrefix;
            }
            go.transform.GetChild(0).gameObject.GetComponent<FlatScreen>().metaData = flatScreen;
            go.transform.GetChild(0).gameObject.GetComponent<FlatScreen>().isGetDataHead = flatScreen.isGetDataHead;
            go.transform.GetChild(0).gameObject.GetComponent<FlatScreen>().isGetDataEyes = flatScreen.isGetDataEyes;
        }
        //go.transform.GetChild(0).transform.localScale = flatScreen.ToUnityScale(); //Set size

        if (flatScreen.isVisible == false)
        {
            go.transform.GetChild(0).GetChild(0).GetComponent<Renderer>().enabled = false;
            go.transform.GetChild(0).GetChild(1).GetComponent<Renderer>().enabled = false;
            go.transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
        //    go.transform.GetChild(0).GetChild(1).GetComponent<BoxCollider>().enabled = false;
        }
    
        SetColoratedProperties(go.gameObject, flatScreen);
        SetVisibilityPropertiesFlatScreen(go.gameObject, flatScreen);
        SetCoordinatedProperties(go, flatScreen);

        SetSizeableProperties(go, flatScreen);

        PrefabController prefabController = go.GetComponent<PrefabController>();
        if (prefabController != null)
        {
            prefabController.isKeepPerimetricAngles = false;
            prefabController.isAutoRadialDistance = false;
        }

        if (flatScreen.isROI)
        {

        }
    }
    void SetVisibilityProperties(GameObject _go, PTVR.Stimuli.Objects.ColoratedObject _coloratedObject)
    {
        PrefabController prefabController = _go.GetComponent<PrefabController>();
        prefabController.coloratedObject = _coloratedObject;
        prefabController.defaultVisiblity = _coloratedObject.isVisible;
        if (_go.transform.GetChild(0).GetChild(0).GetComponent<Renderer>()!=null)
        _go.transform.GetChild(0).GetChild(0).GetComponent<Renderer>().enabled = _coloratedObject.isVisible;
        
    }
    void SetVisibilityPropertiesFlatScreen(GameObject _go, PTVR.Stimuli.Objects.ColoratedObject _coloratedObject)
    {
        PrefabController prefabController = _go.GetComponent<PrefabController>();
        prefabController.coloratedObject = _coloratedObject;
        PTVR.Stimuli.Objects.FlatScreen flatScreen = (PTVR.Stimuli.Objects.FlatScreen)(_coloratedObject);
        for (int count = 0; count < prefabController.renderers.Count; count++)
        {if (_go.transform.GetChild(0).gameObject.GetComponent<FlatScreen>() == null)
            {
                if (isEditor)
                    _go.transform.GetChild(0).gameObject.AddComponent<FlatScreen>().filenamePrefix =/* "PTVR_Researchers/PTVR_Operator/Result/" + */flatScreen.filenamePrefix;
                else
                    _go.transform.GetChild(0).gameObject.AddComponent<FlatScreen>().filenamePrefix = flatScreen.filenamePrefix;
            }
            _go.transform.GetChild(0).gameObject.GetComponent<FlatScreen>().metaData = flatScreen;
            _go.transform.GetChild(0).gameObject.GetComponent<FlatScreen>().isGetDataHead = flatScreen.isGetDataHead;
            _go.transform.GetChild(0).gameObject.GetComponent<FlatScreen>().isGetDataEyes = flatScreen.isGetDataEyes;
        }
        //go.transform.GetChild(0).transform.localScale = flatScreen.ToUnityScale(); //Set size

        if (flatScreen.isVisible == false)

            if (_coloratedObject.isVisible == false)
            {
                _go.transform.GetChild(0).GetChild(0).GetComponent<Renderer>().enabled = false;
                _go.transform.GetChild(0).GetChild(1).GetComponent<Renderer>().enabled = false;
                _go.transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
                //    go.transform.GetChild(0).GetChild(1).GetComponent<BoxCollider>().enabled = false;
            }

       // SetColoratedProperties(_go.gameObject, flatScreen);
        SetCoordinatedProperties(_go, flatScreen);

        SetSizeableProperties(_go, flatScreen);

        if (prefabController != null)
        {
            prefabController.isKeepPerimetricAngles = false;
            prefabController.isAutoRadialDistance = false;
        }

        if (flatScreen.isROI)
            if (_coloratedObject.isVisible == true)
            {
                _go.transform.GetChild(0).GetChild(0).GetComponent<Renderer>().enabled = true;
                _go.transform.GetChild(0).GetChild(1).GetComponent<Renderer>().enabled = true;

            }
    }

    /** Check and add the dynamic_color property to the ptvr.stimuli.PrefabManager if any. Do nothing if it does not exist. */
    void SetColoratedProperties(GameObject _go, PTVR.Stimuli.Objects.ColoratedObject _coloratedObject)
    {
        PrefabController prefabController = _go.GetComponent<PrefabController>();
        prefabController.coloratedObject = _coloratedObject;
        if (prefabController == null)
            return;
        
        switch(_coloratedObject.color.type)
        {
            case "dynamicColor":
                PTVR.Stimuli.DynamicColor dynamiColor = (PTVR.Stimuli.DynamicColor)_coloratedObject.color;
                prefabController.defaultColor = dynamiColor;
                break;
            case "rgbcolor":
                PTVR.Stimuli.RGBColor rgbColor = (PTVR.Stimuli.RGBColor)_coloratedObject.color;
                prefabController.defaultColor = rgbColor;
                break;
            case "rgb255color":
                PTVR.Stimuli.RGB255Color rgb255Color = (PTVR.Stimuli.RGB255Color)_coloratedObject.color;
                prefabController.defaultColor = rgb255Color;
                break;

            case "hexcolor":
                PTVR.Stimuli.HexColor hexColor = (PTVR.Stimuli.HexColor)_coloratedObject.color;
                prefabController.defaultColor = hexColor;
                break;
            default:
                Debug.Log("warning no Color");
                break;
        }
        Material material = CreateMaterial(_coloratedObject.texture, prefabController.defaultColor);
        material.color = prefabController.defaultColor.ToUnityColor();
        material.EnableKeyword("_EMISSION");
        material.SetColor("_EmissionColor", prefabController.defaultColor.ToUnityColor() * 2f);

        foreach (Renderer renderer in prefabController.renderers)
        {
            renderer.material = material;
        }

    }

    //to be seperated in a setCoordinatedProperties:

    void SetCurrentCoordinatesSystemPropeties(PTVR.Stimuli.Objects.VirtualPoint _coordinated)
    {
        current_cs.transform.position = Vector3.zero;
        current_cs.transform.rotation = Quaternion.identity;

        for (int count = 0; count < _coordinated.current_coordinate_system.listTransformation.Count; count++)
        {
            switch (_coordinated.current_coordinate_system.listTransformation[count].type)
            {
                case "reset":
                    current_cs.transform.position = Vector3.zero;
                    current_cs.transform.rotation = Quaternion.identity;
                    break;
                case "rotation_current":
                    current_cs.transform.rotation *= Quaternion.Euler(_coordinated.current_coordinate_system.listTransformation[count].current_coordinate_system_transformation[0], _coordinated.current_coordinate_system.listTransformation[count].current_coordinate_system_transformation[1], _coordinated.current_coordinate_system.listTransformation[count].current_coordinate_system_transformation[2]);
                    break;
                case "rotation_global":
                    current_cs.transform.rotation *= Quaternion.Euler(_coordinated.current_coordinate_system.listTransformation[count].current_coordinate_system_transformation[0], _coordinated.current_coordinate_system.listTransformation[count].current_coordinate_system_transformation[1], _coordinated.current_coordinate_system.listTransformation[count].current_coordinate_system_transformation[2]);
                    break;
                case "translation_global":
                    current_cs.transform.Translate(_coordinated.current_coordinate_system.listTransformation[count].current_coordinate_system_transformation[0], _coordinated.current_coordinate_system.listTransformation[count].current_coordinate_system_transformation[1], _coordinated.current_coordinate_system.listTransformation[count].current_coordinate_system_transformation[2], Space.World);
                    break;
                case "translation_current":
                    current_cs.transform.Translate(_coordinated.current_coordinate_system.listTransformation[count].current_coordinate_system_transformation[0], _coordinated.current_coordinate_system.listTransformation[count].current_coordinate_system_transformation[1], _coordinated.current_coordinate_system.listTransformation[count].current_coordinate_system_transformation[2], current_cs.transform);
                    break;
            }
        }
    }
    void SetCoordinatedProperties(GameObject _go, PTVR.Stimuli.Objects.VirtualPoint _coordinated)
    {            //Set position and orientation

        //TODO: Add spherical, perimetric setup for text

        //Set rotation (we add rotation to initial because of initials set in prefab)
   
        if (_go.transform.parent.name == "VisualTemplate")
        {
            _go.transform.position = current_cs.transform.position;
            _go.transform.rotation = current_cs.transform.rotation;
        }
        else
        {
            _go.transform.localPosition = Vector3.zero;
            _go.transform.localRotation = Quaternion.identity;
        }
        PTVR.Stimuli.Objects.VisualScene actualScene = (PTVR.Stimuli.Objects.VisualScene)(The3DWorldManager.instance.currScene);
        for (int count = 0; count < _coordinated.transformationCoordinate.listTransformation.Count; count++)
        {
            switch (_coordinated.transformationCoordinate.listTransformation[count].type)
            {
                case "reset_position":
                    if (_go.transform.parent.name == "VisualTemplate")
                    {
                        _go.transform.position = current_cs.transform.position;
                    }
                    else
                    {
                        _go.transform.localPosition = Vector3.zero;
                    }
                    break;
                case "reset_orientation":
                    if (_go.transform.parent.name == "VisualTemplate")
                    {
                        _go.transform.rotation = current_cs.transform.rotation;
                    }
                    else
                    {
                        _go.transform.localRotation = Quaternion.identity;
                    }
                    break;
                case "rotation_current":
                    Quaternion rotationsX = Quaternion.AngleAxis(_coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[0], Vector3.right);
                    Quaternion rotationsY = Quaternion.AngleAxis(_coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[1], Vector3.up);
                    Quaternion rotationsZ = Quaternion.AngleAxis(_coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[2], Vector3.forward);
                
                    _go.transform.rotation *= rotationsX;
                    _go.transform.rotation *= rotationsY;
                    _go.transform.rotation *= rotationsZ;
                    break;
                case "translation_current":
                    //Debug.Log("translation current : " + new Vector3(_coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[0],
                    //    _coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[1],
                    //    _coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[2]));
                    _go.transform.Translate(new Vector3(_coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[0],
                        _coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[1],
                        _coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[2]),
                        current_cs);
                    //Debug.Log("position after: " + _go.transform.localPosition);
                    break;
                case "rotation_global":
                    Quaternion rotationsXGlobal = Quaternion.AngleAxis(_coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[0], current_cs.right);
                    Quaternion rotationsYGlobal = Quaternion.AngleAxis(_coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[1], current_cs.up);
                    Quaternion rotationsZGlobal = Quaternion.AngleAxis(_coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[2], current_cs.forward);

                    _go.transform.rotation *= rotationsXGlobal;
                    _go.transform.rotation *= rotationsYGlobal;
                    _go.transform.rotation *= rotationsZGlobal;
                break;
                case "translation_global":
                    _go.transform.Translate(new Vector3(_coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[0],
                        _coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[1],
                        _coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[2]),
                        Space.World);
                    break;
                case "rotation_local":
                    _go.transform.rotation *= Quaternion.Euler(new Vector3(_coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[0], 
                        _coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[1], 
                        _coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[2]));
                    break;
                case "translation_local":
                    _go.transform.Translate(new Vector3(_coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[0], 
                        _coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[1],
                        _coordinated.transformationCoordinate.listTransformation[count].transformationCoordinate[2]), 
                        _go.transform.parent); 
                    break;
            }
        }
        PrefabController prefabController = _go.GetComponent<PrefabController>();
        if (prefabController == null)
            return;


        if (current_coordinate_system_transform != null)
        {
            prefabController.currentCS.pos = current_cs.transform.position;
        }

        else
        {
            prefabController.currentCS.pos = Vector3.zero;
            Debug.Log("Test");
        }

    prefabController.isFacing = _coordinated.isFacing;
    
    prefabController.onOppositeDirection = _coordinated.onOppositeDirection;
    prefabController.idObjectToFacing = _coordinated.idObjectToFacing;
    SetFacing(_go, _coordinated);
    prefabController.isFacingPOV = _coordinated.isFacingHeadPOV;
    prefabController.isFacingPOVatCalibration = _coordinated.isFacingOrigin;
    prefabController.isKeepPerimetricAngles = _coordinated.isKeepPerimetricAngles;
    prefabController.isAutoRadialDistance = _coordinated.isAutoRadialDistance;
    //prefabController.perimetricCoordinates = _coordinated.perimetricCoordinates;
    prefabController.isTangentAngularCorrection = _coordinated.isTangentAngularCorrection;
    prefabController.infiniteCone = _coordinated.infiniteCone;
    prefabController.objData = _coordinated;
    }

    void SetFacing(GameObject _go, PTVR.Stimuli.Objects.VirtualPoint _coordinated)
    {
        PrefabController prefabController = _go.GetComponent<PrefabController>();
        switch (_coordinated.idObjectToFacing)
        {
            case -1:
                //HandLeft
                prefabController.goToFacing = SetVisualSettings.instance.controllerModels[0];
                break;
            case -2:
                //HandRight
                prefabController.goToFacing = SetVisualSettings.instance.controllerModels[1];
                break;
            case -3:
                //HeadSet
                prefabController.goToFacing = Camera.main.gameObject;
                break;
            case -4:
                //CurrentCS
                GameObject go = new GameObject();
                go.transform.position = SetVisualSettings.instance.current_cs.gameObject.transform.position;
                go.transform.rotation = SetVisualSettings.instance.current_cs.gameObject.transform.rotation;
                prefabController.goToFacing = go;
                break;
            case -5:
                //Global
                prefabController.goToFacing = SetVisualSettings.instance.gameObject;
                break;
        }
    }
    

    void SetSizeableProperties(GameObject _go, PTVR.Stimuli.Objects.Resizable _resizable, Vector3? specificScale = null)
    {
        Vector3 effectiveSpeScale = specificScale ?? Vector3.one;
        PTVR.Stimuli.Objects.VisualObject line = (PTVR.Stimuli.Objects.VisualObject)_resizable;
        if (_go.transform.childCount != 0&& _go.transform.Find("ScaledTransform")!=null)
        {
            //Debug.Log("resizable"+_resizable.ToUnityScale());
            Vector3 absoluteScale = _resizable.ToUnityScale();
            _go.transform.localScale = absoluteScale;

            Vector3 parentOnlyScale = _resizable.ToUnityScaleParentOnly();
            parentOnlyScale[0] = parentOnlyScale[0] * effectiveSpeScale[0];
            parentOnlyScale[1] = parentOnlyScale[1] * effectiveSpeScale[1];
            parentOnlyScale[2] = parentOnlyScale[2] * effectiveSpeScale[2];
            _go.transform.Find("ScaledTransform").transform.localScale = parentOnlyScale;

        }
        else if(line.type != "line")//TODO : add line gestion size
        {
            Vector3 absoluteScale = _resizable.ToUnityScale();
            absoluteScale[0] = absoluteScale[0] * effectiveSpeScale[0];
            absoluteScale[1] = absoluteScale[1] * effectiveSpeScale[1];
            absoluteScale[2] = absoluteScale[2] * effectiveSpeScale[2];
        
            _go.transform.localScale = absoluteScale;
        }

        PrefabController prefabController = _go.GetComponent<PrefabController>();
        if (prefabController == null)
            return;
        prefabController.specificScale = effectiveSpeScale;
        //1D props
        prefabController.orientationDeg = _resizable.orientationDeg;
        prefabController.angularSize = _resizable.angularSize;

        //2d props
        prefabController.angularSizeApprox = _resizable.approximative;
        prefabController.angularSize2d = new Vector2(_resizable.Xdeg * (float)Math.PI / 180, _resizable.Ydeg * (float)Math.PI / 180);
        prefabController.defaultScale = _go.transform.localScale;

        //if (_go.name.Contains("Sprite"))
        //{
        //    Vector3 new_scale = new Vector3(_go.transform.Find("ScaledTransform").transform.localScale[0],
        //                                _go.transform.Find("ScaledTransform").transform.localScale[1],
        //                                _go.transform.Find("ScaledTransform").transform.localScale[2]);

        //    prefabController.defaultScaledTransformSize = new Vector3(new_scale[0], new_scale[1], new_scale[2]);
        //    Debug.Log(_go.name);
        //    Debug.Log("SetSizeableProperties");
        //    Debug.Log(prefabController.defaultScaledTransformSize.ToString("F4"));
        //}
    }

    public void SetLine(GameObject _line,long _parentId,Vector3 _startPoint,Vector3 _endPoint,float _width)
    {
        Vector3 offSet = _endPoint - _startPoint;
        Vector3 scale = new Vector3(_width, offSet.magnitude/2, _width);
      //  _line.transform.localRotation = Quaternion.Euler(0, 0, 0);
 
        _line.transform.up = offSet;
  
        /*
        if (anchor == null)
        {
            anchor = _line.transform.parent.gameObject;
            anchor.transform.Rotate(_line.GetComponent<PrefabController>().currentCS.rot.x, _line.GetComponent<PrefabController>().currentCS.rot.y, _line.GetComponent<PrefabController>().currentCS.rot.z);
        }
        */
        GameObject endObj = InstantiateInWorldOrParent(Objects("coordinated"), _parentId);
        GameObject startObj = InstantiateInWorldOrParent(Objects("coordinated"), _parentId);
        if (_line.transform.parent == SetVisualSettings.instance.transform)
        {
            startObj.transform.localPosition = current_cs.position;
            startObj.transform.Translate(new Vector3(_startPoint[0], _startPoint[1], _startPoint[2]), current_cs);

            endObj.transform.localPosition = current_cs.position;
            endObj.transform.Translate(new Vector3(_endPoint[0], _endPoint[1], _endPoint[2]), current_cs);
        }
        else
        {
            startObj.transform.Translate(new Vector3(_startPoint[0], _startPoint[1], _startPoint[2]), Space.Self);
            startObj.transform.localRotation = startObj.transform.parent.transform.localRotation;
            endObj.transform.Translate(new Vector3(_endPoint[0], _endPoint[1], _endPoint[2]), Space.Self);
        }
        startObj.transform.LookAt(endObj.transform.position);
        _line.transform.GetChild(0).gameObject.transform.localRotation = startObj.transform.localRotation;
        _line.transform.GetChild(0).gameObject.transform.localRotation *= Quaternion.Euler(90, 0, 0);
        _line.transform.localRotation = Quaternion.identity;
        Destroy(endObj);
        Destroy(startObj);

        _line.transform.Find("ScaledTransform").localScale = scale;
    }

    public GameObject CreateLine(Vector3 _startPoint, Vector3 _endPoint, float _width, long parentId,PTVR.Stimuli.Objects.VirtualPoint lineTool)
    {
        Vector3 position = _startPoint;// + (offSet / 2.0f);
        GameObject line = null;
        // GameObject go = Instantiate(new GameObject(), SceneManager.instance.visualTemplate);
        if (parentId > -9 && GameObjectCol.ContainsKey(parentId))
        {
             line = Instantiate(Objects("lineTool"), GameObjectCol[parentId].transform);
        }
        else
        {
            line = Instantiate(Objects("lineTool"), SceneManager.instance.visualTemplate);//,GameObjectCol[parentId].transform);
        }
        
        return line;
    }

    void SetFrameProperties(GameObject _go, PTVR.Stimuli.Objects.Frame _frame)
    {
        if (_frame.isHeadPOVcontingent)
            _go.transform.SetParent(camRight.transform);
    }

    /** Use the texture data member in ptvr library and unroll it into a Unity material that can be easily set
     */
    private Material CreateMaterial(PTVR.Stimuli.Texture _texture,PTVR.Stimuli.Color _color)
    {
        Material mat;
        switch (_texture.shader) //Set proper shader
        {
            case "diffuse":
                //mat = new Material(diffuse);
                if (_color.ToUnityColor().a >= 1)
                    mat = new Material(opaqueMat);
                else
                {
                    mat = new Material(transparencyMat);
                }
                break;
            case "unlit":
                mat = new Material(unlit);
                break;
            default:
                mat = new Material(diffuse);
                Debug.Log("Unknown shader encountered. Defaulting to Diffuse.");
                break;
        }
        if (_texture.img.Length > 0) //If an image texture needs to be put
        {
            /*   UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(prefixForWWWQuery + _texture.img);
               mat.mainTexture = DownloadHandlerTexture.GetContent(uwr);*/
            //Obsolete
            // WWW imgPathRetriever = new WWW(prefixForWWWQuery + _texture.img);
            Texture2D _texture2D = new Texture2D(1, 1);
            byte[] _bytes = File.ReadAllBytes(Application.dataPath.Remove(Application.dataPath.Length-7)+"\\PTVR_Researchers\\PTVR_Operators\\resources\\Images\\Textures\\" + _texture.img);
            _texture2D.LoadImage(_bytes);

            mat.mainTexture = _texture2D;
        }
        mat.color = _texture.color.ToUnityColor(); //Set proper color
        if (mat.color.a >= 255)
        {
            mat.SetOverrideTag("RenderType", "Opaque");
        }
        return mat;
    }
}
