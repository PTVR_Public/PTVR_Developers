using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualFulfilmentCriteria : Criteria
{
	public static VisualFulfilmentCriteria instance;
	public override void Fulfilled(string _button)
	{
		
		if (The3DWorldManager.instance.state == SystemState.VISUAL_NOW ||
			The3DWorldManager.instance.state == SystemState.END_VISUAL_NOW)
		{
			The3DWorldManager.instance.state = SystemState.INITIALIZED;

			foreach (var objPair in GetComponent<SetVisualSettings>().GameObjectCol)
			{ 
				var child = objPair.Value;
				if (child.gameObject.tag != "holder")
					child.SetActive(false);
					//Destroy(child);
			}
		
			base.Fulfilled(_button);
			//SceneManager.instance.UnputVisual();
		}
	}

    public override void TimerFunction()
    {
		if (The3DWorldManager.instance.state == SystemState.VISUAL_NOW)
		{
			base.TimerFunction();
		}
		else if (The3DWorldManager.instance.state == SystemState.END_VISUAL_NOW)
		{

			Fulfilled("EndSceneEvent");
		}
	}
}
