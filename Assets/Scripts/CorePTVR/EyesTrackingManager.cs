using UnityEngine;
using ViveSR.anipal.Eye;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Collections.Concurrent;
/// <summary>
/// eye tracking callback
/// Note: Callback runs on a separate thread to report at ~120hz.
/// Unity is not threadsafe and cannot call any UnityEngine api from within callback thread.
/// </summary>
public class EyesTrackingManager : MonoBehaviour
{
    public static EyesTrackingManager instance;//Singleton

    #region Inspector
    [SerializeField]
    public Transform eyeOrbiteRightBase;
    [SerializeField]
    public Transform eyeOrbiteLeftBase;
    [SerializeField]
    public Transform eyeOrbiteCombinedBase;

    [Tooltip("correspond to eyeOrbite Left inside the Headset simulate the orientation of the eye")]
    public Transform eyeOrbiteLeft;
    [Tooltip("correspond to eyeOrbite Right inside the Headset simulate the orientation of the eye")]
    public Transform eyeOrbiteRight;
    [Tooltip("correspond to eyeOrbite Combine inside the Headset simulate the orientation of the eye")]
    public Transform eyeOrbiteCombine;
    public Vector3 leftGazeDirectionWorldSpace;
    public Vector3 rightGazeDirectionWorldSpace;
    public EyeDataPTVR eyeDataCurrent = new EyeDataPTVR();
    public SingleEye eyeDataLastValidityLeft = new SingleEye();
    public SingleEye eyeDataLastValidityRight = new SingleEye();
    public bool hasEyeData = false;

    public List<Vector3> updateSyncLeftEyeGaze = new List<Vector3>();
    public List<Vector3> updateSyncRightEyeGaze = new List<Vector3>();
    public Transform basePositionTool;
    #endregion Inspector

    #region Privates
    private static EyeData_v2 eyeData = new EyeData_v2();
    
    public static List<EyeData_v2> updateSyncEyeDataColl = new List<EyeData_v2>();
    private static bool eye_callback_registered = false;
    private static ConcurrentQueue<EyeData_v2> eyeDataColl = new ConcurrentQueue<EyeData_v2>();
    public Vector3 leftDirection;
    public Vector3 rightDirection;
    public float leftOpeness;
    public float rightOpeness;
    Transform headCamera;
    #endregion Privates

    #region UnityCall
    private void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance);
        }
        headCamera = Camera.main.transform;  
    }

    private void Update()
    {
        if (SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.WORKING) return;

        if (SRanipal_Eye_Framework.Instance.EnableEyeDataCallback == true && eye_callback_registered == false)
        {
            SRanipal_Eye_v2.WrapperRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye_v2.CallbackBasic)EyeCallback));
            eye_callback_registered = true;
        }
        else if (SRanipal_Eye_Framework.Instance.EnableEyeDataCallback == false && eye_callback_registered == true)
        {
            SRanipal_Eye.WrapperUnRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye_v2.CallbackBasic)EyeCallback));
            eye_callback_registered = false;
        }
        if (The3DWorldManager.instance.curr3DWorld != null)
        {
            while (eyeDataColl.TryDequeue(out eyeData))
            {
                updateSyncEyeDataColl.Add(eyeData);
                eyeDataCurrent.eyes = eyeData;
                eyeDataLastValidityLeft.verboseEye = eyeData.verbose_data.left;
                eyeDataLastValidityLeft.expressionEye = eyeData.expression_data.left;

                eyeDataLastValidityRight.verboseEye = eyeData.verbose_data.right;
                eyeDataLastValidityRight.expressionEye = eyeData.expression_data.right;
                
                eyeDataCurrent.eyeLeftGazeDirectionValidity = eyeData.verbose_data.left.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_GAZE_DIRECTION_VALIDITY);
                eyeDataCurrent.eyeLeftGazeOriginValidity = eyeData.verbose_data.left.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_GAZE_ORIGIN_VALIDITY);
                eyeDataCurrent.eyeLeftOpenessValidity= eyeData.verbose_data.left.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_EYE_OPENNESS_VALIDITY);
           
                eyeDataCurrent.eyeLeftPupilPositionInSensorValidity = eyeData.verbose_data.left.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_PUPIL_POSITION_IN_SENSOR_AREA_VALIDITY);
                eyeDataCurrent.eyeLeftPupilDiameterValidity = eyeData.verbose_data.left.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_PUPIL_DIAMETER_VALIDITY);

                eyeDataCurrent.eyeRightGazeDirectionValidity = eyeData.verbose_data.right.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_GAZE_DIRECTION_VALIDITY);
                eyeDataCurrent.eyeRightGazeOriginValidity = eyeData.verbose_data.right.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_GAZE_ORIGIN_VALIDITY);
                eyeDataCurrent.eyeRightOpenessValidity = eyeData.verbose_data.right.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_EYE_OPENNESS_VALIDITY);
                eyeDataCurrent.eyeRightPupilPositionInSensorValidity = eyeData.verbose_data.right.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_PUPIL_POSITION_IN_SENSOR_AREA_VALIDITY);
                eyeDataCurrent.eyeRightPupilDiameterValidity = eyeData.verbose_data.right.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_PUPIL_DIAMETER_VALIDITY);
              
                if (eyeDataCurrent.eyeLeftOpenessValidity  )
                {
                    eyeDataLastValidityLeft.verboseEye = eyeDataCurrent.eyes.verbose_data.left;
                    eyeDataLastValidityLeft.expressionEye = eyeDataCurrent.eyes.expression_data.left;
                }
                if (eyeDataCurrent.eyeRightOpenessValidity)
                {
                    eyeDataLastValidityRight.verboseEye = eyeDataCurrent.eyes.verbose_data.right;
                    eyeDataLastValidityRight.expressionEye = eyeDataCurrent.eyes.expression_data.right;
                }
           

                hasEyeData = true;
            
                CalculateAbsoluteGazeUnityCall(eyeDataLastValidityLeft, eyeDataLastValidityRight);
           

                if (The3DWorldManager.instance.curr3DWorld.outputGaze)
                {
                    ExporterManager.instance.WriteEyeToFileAsync(eyeDataCurrent);
                }
            }
        }
    }

    void LateUpdate()
    {
        updateSyncEyeDataColl.Clear();
        updateSyncLeftEyeGaze.Clear();
        updateSyncRightEyeGaze.Clear();
        hasEyeData = false;
    }

    private void OnDisable()
    {
        Release();
    }

    void OnApplicationQuit()
    {
        Release();
    }
    #endregion UnityCall



    void CalculateAbsoluteGazeUnityCall(SingleEye eyeDataLeft, SingleEye eyeDataRight)
    {

        if (eyeDataLeft.verboseEye.pupil_position_in_sensor_area != new Vector2(-1f, -1f))
            leftDirection = eyeDataLeft.verboseEye.gaze_direction_normalized;
        else
            leftDirection = new Vector3(0, 0, 1);

        if (eyeDataRight.verboseEye.pupil_position_in_sensor_area != new Vector2(-1f, -1f))
            rightDirection = eyeDataRight.verboseEye.gaze_direction_normalized;
        else
            rightDirection = new Vector3(0, 0, 1);


        //the eyetracker gives data in right hand and unity work in left hand we have to do -x instead of x.
        leftDirection = Utils.instance.RightCoordinatesToLeftCoordinates(leftDirection);
        rightDirection = Utils.instance.RightCoordinatesToLeftCoordinates(rightDirection);

        //Transform data Gaze Local to World
        leftGazeDirectionWorldSpace = headCamera.TransformDirection(leftDirection);
        rightGazeDirectionWorldSpace = headCamera.TransformDirection(rightDirection);

        updateSyncLeftEyeGaze.Add(leftGazeDirectionWorldSpace);
        updateSyncRightEyeGaze.Add(rightGazeDirectionWorldSpace);

        // Simulate in unity position and orientation of the eye
        eyeOrbiteLeft.transform.localPosition = Utils.instance.RightCoordinatesToLeftCoordinates(eyeDataLeft.verboseEye.gaze_origin_mm * 0.001f);
        eyeOrbiteRight.transform.localPosition = Utils.instance.RightCoordinatesToLeftCoordinates(eyeDataRight.verboseEye.gaze_origin_mm * 0.001f);

        eyeOrbiteLeftBase.transform.LookAt(leftGazeDirectionWorldSpace);
        eyeOrbiteRightBase.transform.LookAt(rightGazeDirectionWorldSpace);
        eyeOrbiteCombinedBase.transform.LookAt((leftGazeDirectionWorldSpace * eyeDataLeft.verboseEye.eye_openness + rightGazeDirectionWorldSpace * eyeDataRight.verboseEye.eye_openness) / (eyeDataLeft.verboseEye.eye_openness + eyeDataRight.verboseEye.eye_openness));

        eyeOrbiteLeft.transform.rotation = eyeOrbiteLeftBase.rotation; // LookAt(leftGazeDirectionWorldSpace + headCamera.transform.position);
        eyeOrbiteRight.transform.rotation = eyeOrbiteRightBase.localRotation; //.LookAt(rightGazeDirectionWorldSpace + headCamera.transform.position);
        eyeOrbiteCombine.transform.rotation = eyeOrbiteCombinedBase.localRotation; //.LookAt(((leftGazeDirectionWorldSpace+ rightGazeDirectionWorldSpace)/2) + headCamera.transform.position);// Eye Combine is corresponding to the origin of the headset
    }
    /// <summary>
    /// Release callback thread when disabled or quit
    /// </summary>
    private static void Release()
    {
        if (eye_callback_registered == true)
        {
            SRanipal_Eye.WrapperUnRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye_v2.CallbackBasic)EyeCallback));
            eye_callback_registered = false;
        }
    }

    /// <summary>
    /// Required class for IL2CPP scripting backend support
    /// </summary>
    internal class MonoPInvokeCallbackAttribute : System.Attribute
    {
        public MonoPInvokeCallbackAttribute() { }
    }

    /// <summary>
    /// Eye tracking data callback thread.
    /// Reports data at ~120hz
    /// MonoPInvokeCallback attribute required for IL2CPP scripting backend
    /// </summary>
    /// <param name="eye_data">Reference to latest eye_data</param>
    [MonoPInvokeCallback]
    private static void EyeCallback(ref EyeData_v2 eye_data)
    {
        eyeData = eye_data;
        // do stuff with eyeData...
        eyeDataColl.Enqueue(eye_data);
    }
}

public class EyeDataPTVR
{
    public EyeData_v2 eyes;

    public bool eyeLeftOpenessValidity;
    public bool eyeLeftPupilPositionInSensorValidity;
    public bool eyeLeftPupilDiameterValidity;
    public bool eyeLeftGazeDirectionValidity;
    public bool eyeLeftGazeOriginValidity;

    public bool eyeRightOpenessValidity;
    public bool eyeRightPupilPositionInSensorValidity;
    public bool eyeRightPupilDiameterValidity;
    public bool eyeRightGazeDirectionValidity;
    public bool eyeRightGazeOriginValidity;
}

public class SingleEye
{
    public SingleEyeData verboseEye;
    public SingleEyeExpression expressionEye;

}