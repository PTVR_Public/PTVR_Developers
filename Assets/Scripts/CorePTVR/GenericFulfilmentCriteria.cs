﻿// Execute fulfilment criteria that is generic across different kinds of scenes ---> starts timer, sets up listener to events etc.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GenericFulfilmentCriteria : MonoBehaviour
{
    #region Inspector
    public static GenericFulfilmentCriteria instance;//singleton
    public UnityEvent startTimer;
    public FloatEvent setTimeForTrial;
    public StringEvent Fulfiled;
    #endregion Inspector

    #region Privates

    private Dictionary<string, bool> currentButton = new Dictionary<string, bool>();
    #endregion Privates

    #region UnityCallBack
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            Debug.LogWarning("GenericFulfilmentCriteria destroy all exept one");
        }
        else
        {
            instance = this;
        }
    }
    #endregion UnityCallBack

    public void SetFulfilmentVariables(/*PTVR.Data.Input.Display _display*/) // What is the display type?
    {
     //   if (_display == null)
       //     Debug.LogError("Display cannot be null");

       /* switch (_display.type)
        {
            case "user_option":
                //Terminate when a user option is given
                PTVR.Data.Input.UserOptionDisplay userOptionDisplay = (PTVR.Data.Input.UserOptionDisplay)_display;
                switch(userOptionDisplay.events.type)
                {
                    case "controllerVR":
                        PTVR.Data.Input.ControllerVR controllerVR = (PTVR.Data.Input.ControllerVR) userOptionDisplay.events;
                        if (controllerVR.validResponses.Count > 0)
                        {
                            foreach (string valResp in controllerVR.validResponses)
                            {
				                currentButton[valResp] = true;
                            }
                        }
                        break;
                    case "keyboard":
                        PTVR.Data.Input.Keyboard keyboard = (PTVR.Data.Input.Keyboard)userOptionDisplay.events;
                        if (keyboard.validResponses.Count > 0)
                        {
                            foreach (string valResp in keyboard.validResponses)
                            {
                                currentButton[valResp] = true;
                            }
                        }
                        break;
                    default:
                        Debug.LogError("Invalid/Not yet implemented Input");
                        break;
                }
                break;

            case "timed":
                //Terminate after a given time
                PTVR.Data.Input.TimedDisplay td = (PTVR.Data.Input.TimedDisplay)_display;
                setTimeForTrial.Invoke(td.timeInSeconds());
                startTimer.Invoke();
                break;
            default:
                Debug.LogError("Invalid/Not yet implemented Display");
                break;
        }
       */
    }

    public void UnSetFulfilmentVariables()
    {
        //Set every receiver switch to false
	    currentButton.Clear();
    }

    public bool HasActiveButton(string _val)
    {
	return (currentButton.ContainsKey(_val) &&
		currentButton[_val]);
    }

    //Message receivers
    public void Button (string _nameButton)
    {
        if (HasActiveButton(_nameButton))
        {
            Debug.Log(_nameButton);
            Fulfiled.Invoke(_nameButton);
        }
    }

    public void RightTouchTwo()
    {
        if (HasActiveButton("right_touch_two"))
        {
            Fulfiled.Invoke("right_touch_two");
        }
    }

    public void RightTouchPress()
    {
        if (HasActiveButton("right_touch_press_pos"))
        {
            Fulfiled.Invoke("right_touch_press@(" + The3DWorldManager.instance.controller[0].touchPadPositionX + "," + The3DWorldManager.instance.controller[0].touchPadPositionY + "]");
        }
	if (HasActiveButton("right_touch_press"))
        {
	    string leftOrRight="";
	    if (The3DWorldManager.instance.controller[0].touchPadPositionX < 0)
		leftOrRight = "left";
	    else
		leftOrRight = "right";
            Fulfiled.Invoke("right_touch_press_" + leftOrRight);
        }
    }

    //Left controller

    public void LeftTouchTwo()
    { 
        if (HasActiveButton("left_touch_two"))
        {
            Fulfiled.Invoke("left_touch_two");
        }
    }

    public void LeftTouchPress()
    {
        if (HasActiveButton("left_touch_press_pos"))
        {
            Fulfiled.Invoke("left_touch_press_pos@(" + The3DWorldManager.instance.controller[1].touchPadPositionX + "," + The3DWorldManager.instance.controller[1].touchPadPositionY + "]");
        }
	    if (HasActiveButton("left_touch_press"))
        {
	      string leftOrRight="";
	         if (The3DWorldManager.instance.controller[1].touchPadPositionX < 0)
		    leftOrRight = "left";
	         else
		    leftOrRight = "right";
            Fulfiled.Invoke("left_touch_press_" + leftOrRight);
        }
    }

    string stringEdit;
    public void stringToEdit(string _value)
    {
        stringEdit = _value;
    }

    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 200, 20), stringEdit);

        if (GUI.changed)
        {
            Debug.Log("Text field has changed.");
        }
    }
}


[System.Serializable]
public class IntEvent : UnityEvent<int>
{
}

[System.Serializable]
public class StringEvent : UnityEvent<string>
{
}

[System.Serializable]
public class FloatEvent : UnityEvent<float>
{
}