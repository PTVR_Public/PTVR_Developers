using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using System;

public class CallbackManager : MonoBehaviour
{
    #region Inspector
    public static CallbackManager instance;
    public SteamVRBasedController leftController;
    public SteamVRBasedController rightController;

    public Dictionary<long, PTVR.Data.Callback.Callback> events= new Dictionary<long, PTVR.Data.Callback.Callback>();

    #endregion Inspector

    #region Privates
    int countInput = 0;
    #endregion Privates

    #region UnityCall
    private void Awake()
    {
        if (instance != null)
            Destroy(this);
        else
            instance = this;
    }
    #endregion UnityCall

    public System.DateTime t_scene;

    public void GestionInputEvent(List<PTVR.Interaction> _interaction)
    {
        for (int count = 0; count < _interaction.Count; count++)
        {
            CheckInput(_interaction, count);
        
        }
        //TimerManager.instance.SetTimerScene();
        t_scene = System.DateTime.Now;
        KeyboardBasedController.instance.SetupKeyCap();
    }

    void CheckInput(List<PTVR.Interaction> _interaction, int _countInteraction)
    {
        for (int count = 0; count < _interaction[_countInteraction].events.Count; count++)
        {
            switch (_interaction[_countInteraction].events[count].type)
            {
                case "keyboard":
                    PTVR.Data.Event.Keyboard keyboard = (PTVR.Data.Event.Keyboard)_interaction[_countInteraction].events[count];
                    if (keyboard.validResponses.Count > 0)
                    {
                        for (int countValidResponses = 0; countValidResponses < keyboard.validResponses.Count; countValidResponses++)
                        {
                            for (int countCallback = 0; countCallback < GetCallback(_interaction, _countInteraction).Count; countCallback++)
                            {
                                
                                SetUpEvent(CheckInputKeyboard(keyboard.validResponses[countValidResponses], keyboard.eventName, keyboard.language), keyboard.mode, CheckCallback(GetCallback(_interaction, _countInteraction)[countCallback]));
                                if (keyboard.validResponses[countValidResponses] == "enter")
                                {
                                    SetUpEvent(CheckInputKeyboard("numPadEnter", keyboard.eventName, keyboard.language), keyboard.mode, CheckCallback(GetCallback(_interaction, _countInteraction)[countCallback]));
                                }
                            }
                        }
                    }
                    break;
                case "handController":
                    PTVR.Data.Event.HandController handController = (PTVR.Data.Event.HandController)_interaction[_countInteraction].events[count];
                    if (handController.validResponses.Count > 0)
                    {
                        for (int countValidResponses = 0; countValidResponses < handController.validResponses.Count; countValidResponses++)
                        {
                            for (int countEvent = 0; countEvent < GetCallback(_interaction, _countInteraction).Count; countEvent++)
                            {
                                SetUpEvent(CheckInputHandController(handController.validResponses[countValidResponses],handController.eventName), handController.mode, CheckCallback(GetCallback(_interaction, _countInteraction)[countEvent]));
                            }
                        }
                    }
                    break;
                case "timer":
                    PTVR.Data.Event.Timer timer = (PTVR.Data.Event.Timer)_interaction[_countInteraction].events[count];
                     TimerManager.instance.CreateTimer(timer.id, timer.delayInMs, timer.timerStart,timer.eventName);
                    for (int countEvent = 0; countEvent < GetCallback(_interaction, _countInteraction).Count; countEvent++)
                    {
                        SetUpEvent(CheckInputTimed(timer.id), timer.mode, CheckCallback(GetCallback(_interaction, _countInteraction)[countEvent]));
                    }
                    TimerManager.instance.isTimingScene = true;
                    break;
                case "pointedAt":
                    PTVR.Data.Event.PointedAt pointedAt = (PTVR.Data.Event.PointedAt)_interaction[_countInteraction].events[count];
                    PointedAtManager.instance.SetPointed(pointedAt);
                    for (int countEvent = 0; countEvent < GetCallback(_interaction, _countInteraction).Count; countEvent++)
                    {
                        SetUpEvent(CheckInputPointedAt(pointedAt), pointedAt.mode, CheckCallback(GetCallback(_interaction, _countInteraction)[countEvent]));
                    }
                    break;
                case "button":
                    PTVR.Data.Event.Button button = (PTVR.Data.Event.Button)_interaction[_countInteraction].events[count];
                    for (int countEvent = 0; countEvent < GetCallback(_interaction, _countInteraction).Count; countEvent++)
                    {
                        SetUpEvent(ButtonUIManager.instance.LinkButtonUI(button), button.mode, CheckCallback(GetCallback(_interaction, _countInteraction)[countEvent]));
                    }
                    break;
                case "distanceFromPoint":
                    PTVR.Data.Event.DistanceFromPoint distanceFromPoint = (PTVR.Data.Event.DistanceFromPoint)_interaction[_countInteraction].events[count];
                    for (int countEvent = 0; countEvent < GetCallback(_interaction, _countInteraction).Count; countEvent++)
                    {
                        SetUpEvent(DistanceFromPointManager.instance.TakeDistanceFromPoint(distanceFromPoint), distanceFromPoint.mode, CheckCallback(GetCallback(_interaction, _countInteraction)[countEvent]));
                    }
                    break;
            }
        }
    }

    #region Event
    void SetUpEvent(InputUnityEvent _input, string _inputMode, PTVR.Data.Callback.Callback _event)
    {
        switch (_event.effect)
        {
            case "activate":
                CheckModeActivate(_input, _inputMode, _event);
                break;
            case "deactivate":
                CheckModeDeactivate(_input, _inputMode, _event);
                break;
        }
    }

    void CheckModeActivate(InputUnityEvent _input, string _inputMode, PTVR.Data.Callback.Callback _event)
    {
        switch (_inputMode)
        {
            case "press":
                if (!_input.hasEventOnPress)
                {
                    _input.hasEventOnPress = true;
                }
                _event.mode = "press";
                _event.button = _input;
               
                _input.OnInputPress.AddListener( events[_event.id].Activate);
                break;
            case "release":

                if (!_input.hasEventOnRelease)
                {
                    _input.hasEventOnRelease = true;
                }
                _event.mode = "release";
                _event.button = _input;
                _input.OnInputRelease.AddListener(events[_event.id].Activate);
                break;
            case "hold":

                if (!_input.hasEventOnHold)
                {
                    _input.hasEventOnHold = true;
                }
                _event.mode = "hold";
                _event.button = _input;
                _input.OnInputHold.AddListener(events[_event.id].Activate);

                break;
        }
    }
    void CheckModeDeactivate(InputUnityEvent _input, string _inputMode, PTVR.Data.Callback.Callback _event)
    {
        _event.button = new InputUnityEvent();
        switch (_inputMode)
        {
            case "press":
                if (!_input.hasEventOnPress)
                {
                    _input.hasEventOnPress = true;
                }
                _event.button = _input;
                _event.mode = "press";
                _input.OnInputPress.AddListener(events[_event.id].Deactivate);
                break;
            case "release":
                if (!_input.hasEventOnRelease)
                {
                    _input.hasEventOnRelease = true;
                }
                _event.button = _input;
                _event.mode = "release";
                _input.OnInputRelease.AddListener(events[_event.id].Deactivate);
                break;
            case "hold":
                if (!_input.hasEventOnHold)
                {
                    _input.hasEventOnHold = true;
                }
                _event.button = _input;
                _event.mode = "hold";
                _input.OnInputHold.AddListener(events[_event.id].Deactivate);
                break;
        }
    }

    List<PTVR.Data.Callback.Callback> GetCallback(List<PTVR.Interaction> interaction, int _countInteraction)
    {
        List<PTVR.Data.Callback.Callback> eventsI = new List<PTVR.Data.Callback.Callback>();

        for (int count = 0; count < interaction[_countInteraction].callbacks.Count; count++)
        {
            eventsI.Add(interaction[_countInteraction].callbacks[count]);
        }

        return eventsI;
    }
    public void CleanSpecificEvent(List<PTVR.Interaction> _interaction)
    {

        for (int count = 0; count < _interaction.Count; count++)
        {
            UnityAction tempAction = null;
            for (int countE = 0; countE < _interaction[count].callbacks.Count; countE++)
            {
                if(events[_interaction[count].callbacks[countE].id]!=null)
                switch (_interaction[count].callbacks[countE].effect)
                {
                    //event 
                    case "activate":
                        tempAction += new UnityAction( events[_interaction[count].callbacks[countE].id].Activate);
                        break;
                    case "deactivate":
                        tempAction += new UnityAction(events[_interaction[count].callbacks[countE].id].Deactivate);
                        break;
                    default:
                        break;
                }
            }

            for (int countI = 0; countI < _interaction[count].events.Count; countI++)
            {
                switch (_interaction[count].events[countI].type)
                {
                    case "handController":
                        PTVR.Data.Event.HandController handController = (PTVR.Data.Event.HandController)_interaction[count].events[countI];
                        switch (handController.mode)
                        {
                            case "press":
                                if (handController.validResponses.Count > 0)
                                {
                                    for (int countValidResponses = 0; countValidResponses < handController.validResponses.Count; countValidResponses++)
                                    {
                                        CheckInputHandController(handController.validResponses[countValidResponses], handController.eventName).OnInputPress.RemoveListener(tempAction);
                                    }
                                }
                                break;
                            case "release":
                                if (handController.validResponses.Count > 0)
                                {
                                    for (int countValidResponses = 0; countValidResponses < handController.validResponses.Count; countValidResponses++)
                                    {
                                        CheckInputHandController(handController.validResponses[countValidResponses], handController.eventName).OnInputRelease.RemoveListener(tempAction);
                                    }
                                }
                                break;
                            case "hold":
                                if (handController.validResponses.Count > 0)
                                {
                                    for (int countValidResponses = 0; countValidResponses < handController.validResponses.Count; countValidResponses++)
                                    {
                                        CheckInputHandController(handController.validResponses[countValidResponses],handController.eventName).OnInputHold.RemoveListener(tempAction);
                                    }
                                }
                                break;

                        }
                        break;
                    case "keyboard":
                        PTVR.Data.Event.Keyboard keyboard = (PTVR.Data.Event.Keyboard)_interaction[count].events[countI];
                        switch (keyboard.mode)
                        {
                            case "press":
                                Debug.Log("Press");
                                if (keyboard.validResponses.Count > 0)
                                {
                                    for (int countValidResponses = 0; countValidResponses < keyboard.validResponses.Count; countValidResponses++)
                                    {
                                        if (tempAction != null&& CheckInputKeyboard(keyboard.validResponses[countValidResponses], keyboard.eventName, keyboard.language).OnInputPress.ListenerCount>0)
                                        {
                                             CheckInputKeyboard(keyboard.validResponses[countValidResponses],keyboard.eventName, keyboard.language).OnInputPress.RemoveListener( tempAction);
                                        }
                                        if (tempAction != null && CheckInputKeyboard(keyboard.validResponses[countValidResponses], keyboard.eventName, keyboard.language).OnInputPress.ListenerCount == 0)
                                            {
                                            CheckInputKeyboard(keyboard.validResponses[countValidResponses], keyboard.eventName, keyboard.language).OnInputPress.RemoveAllListeners();
                                        }
                                    }
                                }
                                break;
                            case "release":
                                if (keyboard.validResponses.Count > 0)
                                {
                                    for (int countValidResponses = 0; countValidResponses < keyboard.validResponses.Count; countValidResponses++)
                                    {
                                        if (tempAction != null)
                                            CheckInputKeyboard(keyboard.validResponses[countValidResponses], keyboard.eventName, keyboard.language).OnInputRelease.RemoveListener(tempAction);
                                    }
                                }
                                break;
                            case "hold":
                                if (keyboard.validResponses.Count > 0)
                                {
                                    for (int countValidResponses = 0; countValidResponses < keyboard.validResponses.Count; countValidResponses++)
                                    {
                                        if (tempAction != null)
                                            CheckInputKeyboard(keyboard.validResponses[countValidResponses], keyboard.eventName, keyboard.language).OnInputHold.RemoveListener(tempAction);
                                    }
                                }
                                break;
                        }
                        break;

                    case "timer":
                        PTVR.Data.Event.Timer timer = (PTVR.Data.Event.Timer)_interaction[count].events[countI];
                        TimerManager.instance.CleanSpecificTimed(timer.id);
                        break;

                    case "pointedAt":
                        PTVR.Data.Event.PointedAt pointedAt = (PTVR.Data.Event.PointedAt)_interaction[count].events[countI];
                        switch (pointedAt.mode)
                        {
                            case "press":
                                CheckInputPointedAt(pointedAt).OnInputPress.RemoveListener(tempAction);
                                break;
                            case "release":
                                CheckInputPointedAt(pointedAt).OnInputRelease.RemoveListener(tempAction);
                                break;
                            case "hold":
                                CheckInputPointedAt(pointedAt).OnInputHold.RemoveListener(tempAction);
                                break;
                        }

                        break;
                    case "distanceFromPoint":
                        PTVR.Data.Event.DistanceFromPoint distanceFromPoint = (PTVR.Data.Event.DistanceFromPoint)_interaction[count].events[countI];
                        switch (distanceFromPoint.mode)
                        {
                            case "press":
                                DistanceFromPointManager.instance.TakeDistanceFromPoint(distanceFromPoint).OnInputPress.RemoveListener(tempAction);
                                break;
                            case "release":
                                DistanceFromPointManager.instance.TakeDistanceFromPoint(distanceFromPoint).OnInputRelease.RemoveListener(tempAction);
                                break;
                            case "hold":
                                DistanceFromPointManager.instance.TakeDistanceFromPoint(distanceFromPoint).OnInputHold.RemoveListener(tempAction);
                                break;
                        }

                        break;

                    case "button":
                        PTVR.Data.Event.Button button = (PTVR.Data.Event.Button)_interaction[count].events[countI];
                        break;
                }
            }
        }
    }

    PTVR.Data.Callback.Callback CheckCallback(PTVR.Data.Callback.Callback _callback)
    {
       
        switch (_callback.type)
        {
            case "ChangeBackgroundColor":
                PTVR.Data.Callback.ChangeBackgroundColor changeBackground = new PTVR.Data.Callback.ChangeBackgroundColor((PTVR.Data.Callback.ChangeBackgroundColor)_callback);
                if (!events.ContainsKey(changeBackground.id))
                {
                    changeBackground.isCallbackPausable = _callback.isCallbackPausable;
                    events[changeBackground.id] = changeBackground;
                }
                return events[changeBackground.id];
            case "ChangeObjectColor":
                PTVR.Data.Callback.ChangeObjectColor changeObjectColor = new PTVR.Data.Callback.ChangeObjectColor((PTVR.Data.Callback.ChangeObjectColor)_callback);
                if (!events.ContainsKey(changeObjectColor.id))
                {
                
                    changeObjectColor.isCallbackPausable = _callback.isCallbackPausable;
                    events[changeObjectColor.id] = changeObjectColor;
                }
                return events[changeObjectColor.id];

            case "ChangeObjectOutline":
                PTVR.Data.Callback.ChangeObjectOutline changeObjectOutline = new PTVR.Data.Callback.ChangeObjectOutline((PTVR.Data.Callback.ChangeObjectOutline)_callback);
                if (!events.ContainsKey(changeObjectOutline.id))
                {
                    changeObjectOutline.isCallbackPausable = _callback.isCallbackPausable;
                    events[changeObjectOutline.id] = changeObjectOutline;
                }

                return events[changeObjectOutline.id];
            case "Move_with_periodic_function":
                PTVR.Data.Callback.Move_with_periodic_function periodic_movement_function = new PTVR.Data.Callback.Move_with_periodic_function((PTVR.Data.Callback.Move_with_periodic_function)_callback);
                if (!events.ContainsKey(periodic_movement_function.id))
                {
                    periodic_movement_function.isCallbackPausable = _callback.isCallbackPausable;
                    events[periodic_movement_function.id] = periodic_movement_function;
                }

                return events[periodic_movement_function.id];
            case "Translate":
                PTVR.Data.Callback.Translate translate = new PTVR.Data.Callback.Translate((PTVR.Data.Callback.Translate)_callback);
                if (!events.ContainsKey(translate.id))
                {
                    translate.isCallbackPausable = _callback.isCallbackPausable;
                    events[translate.id] = translate;
                }

                return events[translate.id];
                

            case "Rotate":
                PTVR.Data.Callback.Rotate rotate = new PTVR.Data.Callback.Rotate((PTVR.Data.Callback.Rotate)_callback);
                if (!events.ContainsKey(rotate.id))
                {
                    rotate.isCallbackPausable = _callback.isCallbackPausable;
                    events[rotate.id] = rotate;
                }

                return events[rotate.id];
            case "AnimateObject":
                PTVR.Data.Callback.AnimateObject animateObject = new PTVR.Data.Callback.AnimateObject((PTVR.Data.Callback.AnimateObject)_callback);
                if (!events.ContainsKey(animateObject.id))
                {
                    animateObject.isCallbackPausable = _callback.isCallbackPausable;
                    events[animateObject.id] = animateObject;
                }

                return events[animateObject.id]; 
            case "Grab":
                PTVR.Data.Callback.Grab grab = new PTVR.Data.Callback.Grab((PTVR.Data.Callback.Grab)_callback);
                if (!events.ContainsKey(grab.id))
                {
                    grab.isCallbackPausable = _callback.isCallbackPausable;
                    events[grab.id] = grab;
                }
                return events[grab.id];
            case "MakeGrabable":
                PTVR.Data.Callback.MakeGrabable isgrabable = new PTVR.Data.Callback.MakeGrabable((PTVR.Data.Callback.MakeGrabable)_callback);
                if (!events.ContainsKey(isgrabable.id))
                {
                    isgrabable.isCallbackPausable = _callback.isCallbackPausable;
                    events[isgrabable.id] = isgrabable;
                }
                return events[isgrabable.id];
            case "Attractor":
                PTVR.Data.Callback.Attractor attract = new PTVR.Data.Callback.Attractor((PTVR.Data.Callback.Attractor)_callback);
                if (!events.ContainsKey(attract.id))
                {
                    attract.isCallbackPausable = _callback.isCallbackPausable;
                    events[attract.id] = attract;
                }
                return events[attract.id];
            case "Attractive":
                PTVR.Data.Callback.Attractive attractive = new PTVR.Data.Callback.Attractive((PTVR.Data.Callback.Attractive)_callback);
                if (!events.ContainsKey(attractive.id))
                {
                    attractive.isCallbackPausable = _callback.isCallbackPausable;
                    events[attractive.id] = attractive;
                }
                return events[attractive.id];
            case "ResetPosition":
                PTVR.Data.Callback.ResetPosition resetPosition = new PTVR.Data.Callback.ResetPosition((PTVR.Data.Callback.ResetPosition)_callback);
                if (!events.ContainsKey(resetPosition.id))
                {
                    resetPosition.isCallbackPausable = _callback.isCallbackPausable;
                    events[resetPosition.id] = resetPosition;
                }
                return events[resetPosition.id];
                
            case "ChangeObjectScale":
                PTVR.Data.Callback.ChangeObjectScale changeObjectScale = new PTVR.Data.Callback.ChangeObjectScale((PTVR.Data.Callback.ChangeObjectScale)_callback);
                if (!events.ContainsKey(changeObjectScale.id))
                {
                    changeObjectScale.isCallbackPausable = _callback.isCallbackPausable;
                    events[changeObjectScale.id] = changeObjectScale;
                }

                return events[changeObjectScale.id];
            case "ChangeObjectEnable":
                PTVR.Data.Callback.ChangeObjectEnable changeObjectEnable = new PTVR.Data.Callback.ChangeObjectEnable((PTVR.Data.Callback.ChangeObjectEnable)_callback);
                if (!events.ContainsKey(changeObjectEnable.id))
                {
                    changeObjectEnable.isCallbackPausable = _callback.isCallbackPausable;
                    events[changeObjectEnable.id] = changeObjectEnable;
                }
                return events[changeObjectEnable.id];
            case "ChangeObjectVisibility":
                PTVR.Data.Callback.ChangeObjectVisibility changeObjectVisibility = new PTVR.Data.Callback.ChangeObjectVisibility((PTVR.Data.Callback.ChangeObjectVisibility)_callback);
                if (!events.ContainsKey(changeObjectVisibility.id))
                {
                    changeObjectVisibility.isCallbackPausable = _callback.isCallbackPausable;
                    events[changeObjectVisibility.id] = changeObjectVisibility;
                }
                return events[changeObjectVisibility.id];
            case "PauseCurrentScene":
                PTVR.Data.Callback.PauseCurrentScene pauseCurrentScene = new PTVR.Data.Callback.PauseCurrentScene((PTVR.Data.Callback.PauseCurrentScene)_callback);
                if (!events.ContainsKey(pauseCurrentScene.id))
                {
                    pauseCurrentScene.isCallbackPausable |= _callback.isCallbackPausable;
                    events[pauseCurrentScene.id] = pauseCurrentScene;
                }
                return events[pauseCurrentScene.id];

            case "RestartCurrentScene":
                PTVR.Data.Callback.RestartCurrentScene restartCurrentScene = new PTVR.Data.Callback.RestartCurrentScene((PTVR.Data.Callback.RestartCurrentScene)_callback);
                if (!events.ContainsKey(restartCurrentScene.id))
                {
                    restartCurrentScene.isCallbackPausable = _callback.isCallbackPausable;
                    events[restartCurrentScene.id] = restartCurrentScene;
                }
                return events[restartCurrentScene.id];
            case "RestartCurrentTrial":
                PTVR.Data.Callback.RestartCurrentTrial restartCurrentTrial = new PTVR.Data.Callback.RestartCurrentTrial((PTVR.Data.Callback.RestartCurrentTrial)_callback);
                if (!events.ContainsKey(restartCurrentTrial.id))
                {
                    restartCurrentTrial.isCallbackPausable = _callback.isCallbackPausable;
                    events[restartCurrentTrial.id]= restartCurrentTrial;
                }
                return events[restartCurrentTrial.id];
            case "EndCurrentScene":
                PTVR.Data.Callback.EndCurrentScene endCurrentScene = new PTVR.Data.Callback.EndCurrentScene((PTVR.Data.Callback.EndCurrentScene)_callback);
                if (!events.ContainsKey(endCurrentScene.id))
                {  
                    endCurrentScene.isCallbackPausable = _callback.isCallbackPausable;
                    events[endCurrentScene.id] = endCurrentScene;
                }
                return events[endCurrentScene.id];
            case "EndCurrentTrial":
                PTVR.Data.Callback.EndCurrentTrial endCurrentTrial = new PTVR.Data.Callback.EndCurrentTrial((PTVR.Data.Callback.EndCurrentTrial)_callback);
                if (!events.ContainsKey(endCurrentTrial.id))
                {
                    endCurrentTrial.isCallbackPausable = _callback.isCallbackPausable;
                    events[endCurrentTrial.id] = endCurrentTrial;
                }
                return events[endCurrentTrial.id];
            case "Quit":
                PTVR.Data.Callback.Quit quit = new PTVR.Data.Callback.Quit((PTVR.Data.Callback.Quit)_callback);
                if (!events.ContainsKey(quit.id))
                {
                    quit.isCallbackPausable = _callback.isCallbackPausable;
                    events[quit.id] = quit;
                }
                return events[quit.id];
            case "ExecutePythonScript":
                PTVR.Data.Callback.ExecutePythonScript executePythonScript = new PTVR.Data.Callback.ExecutePythonScript((PTVR.Data.Callback.ExecutePythonScript)_callback);
                if (!events.ContainsKey(executePythonScript.id))
                {
                    executePythonScript.isCallbackPausable = _callback.isCallbackPausable;
                    events[executePythonScript.id] = executePythonScript;
                }
                return events[executePythonScript.id];

            case "ControlFromPythonScript":
                PTVR.Data.Callback.ControlFromPythonScript controlFromPythonScript = new PTVR.Data.Callback.ControlFromPythonScript((PTVR.Data.Callback.ControlFromPythonScript)_callback);
                if (!events.ContainsKey(controlFromPythonScript.id))
                {
                    controlFromPythonScript.isCallbackPausable = _callback.isCallbackPausable;
                    events[controlFromPythonScript.id] = controlFromPythonScript;
                }
                return events[controlFromPythonScript.id];

            case "PrintResultFileLine":
                PTVR.Data.Callback.PrintResultFileLine printResultFileLine = new PTVR.Data.Callback.PrintResultFileLine((PTVR.Data.Callback.PrintResultFileLine)_callback);
                if (!events.ContainsKey(printResultFileLine.id))
                {
                    printResultFileLine.isCallbackPausable = _callback.isCallbackPausable;
                    events[printResultFileLine.id] = printResultFileLine;
                }
                return events[printResultFileLine.id];
            case "SaveText":
                PTVR.Data.Callback.SaveText saveText = new PTVR.Data.Callback.SaveText((PTVR.Data.Callback.SaveText)_callback);
                Debug.Log("SaveText Event Manager");
              
                if (!events.ContainsKey(saveText.id))
                {
                    saveText.InitializeSentence();
                    Debug.Log(saveText.nameAppendString);
                    saveText.isCallbackPausable = _callback.isCallbackPausable;
                    events[saveText.id] = saveText;
                }
                return events[saveText.id];
            case "FillInResultsFileColumn":
                PTVR.Data.Callback.FillInResultsFileColumn fillInResultsFileColumn = new PTVR.Data.Callback.FillInResultsFileColumn((PTVR.Data.Callback.FillInResultsFileColumn)_callback);
                if (!events.ContainsKey(fillInResultsFileColumn.id))
                {
                    fillInResultsFileColumn.InitializeSentence();
                    fillInResultsFileColumn.isCallbackPausable = _callback.isCallbackPausable;
                    events[fillInResultsFileColumn.id] = fillInResultsFileColumn;
                }
                return events[fillInResultsFileColumn.id];
            case "TakePicture":
                PTVR.Data.Callback.TakePicture takePicture = new PTVR.Data.Callback.TakePicture((PTVR.Data.Callback.TakePicture)_callback);
                if (!events.ContainsKey(takePicture.id))
                {
                    takePicture.isCallbackPausable = _callback.isCallbackPausable;
                    events[takePicture.id] = takePicture;
                }
                return events[takePicture.id];
            case "ChangeObjectText":
                PTVR.Data.Callback.ChangeObjectText changeObjectText = new PTVR.Data.Callback.ChangeObjectText((PTVR.Data.Callback.ChangeObjectText)_callback);
                if (!events.ContainsKey(changeObjectText.id))
                {
                    changeObjectText.isCallbackPausable = _callback.isCallbackPausable;
                    events[changeObjectText.id] = changeObjectText;
                }
                return events[changeObjectText.id];
            case "UpdateLastPoint":
                PTVR.Data.Callback.UpdateLastPoint updateLastPoint = new PTVR.Data.Callback.UpdateLastPoint((PTVR.Data.Callback.UpdateLastPoint)_callback);
                if (!events.ContainsKey(updateLastPoint.id))
                {
                    updateLastPoint.isCallbackPausable = _callback.isCallbackPausable;
                    events[updateLastPoint.id] = updateLastPoint;
                }
                return events[updateLastPoint.id];
            case "ShowAllPoints":
                PTVR.Data.Callback.ShowAllPoints showGraphPoints = new PTVR.Data.Callback.ShowAllPoints((PTVR.Data.Callback.ShowAllPoints)_callback);
                if (!events.ContainsKey(showGraphPoints.id))
                {
                    showGraphPoints.isCallbackPausable = _callback.isCallbackPausable;
                    events[showGraphPoints.id] = showGraphPoints;
                }
                return events[showGraphPoints.id];
            case "AddNewPoint":
                PTVR.Data.Callback.AddNewPoint addNewPoint = new PTVR.Data.Callback.AddNewPoint((PTVR.Data.Callback.AddNewPoint)_callback);
                if (!events.ContainsKey(addNewPoint.id))
                {
                    addNewPoint.isCallbackPausable = _callback.isCallbackPausable;
                    events[addNewPoint.id] = addNewPoint;
                }
                return events[addNewPoint.id];
            case "DeleteLastPoint":
                PTVR.Data.Callback.DeleteLastPoint deleteLastPoint = new PTVR.Data.Callback.DeleteLastPoint((PTVR.Data.Callback.DeleteLastPoint)_callback);
                if (!events.ContainsKey(deleteLastPoint.id))
                {
                    deleteLastPoint.isCallbackPausable = _callback.isCallbackPausable;
                    events[deleteLastPoint.id] = deleteLastPoint;
                }
                return events[deleteLastPoint.id];
            case "SaveLastPoint":
                PTVR.Data.Callback.SaveLastPoint saveLastPoint = new PTVR.Data.Callback.SaveLastPoint((PTVR.Data.Callback.SaveLastPoint)_callback);
                if (!events.ContainsKey(saveLastPoint.id))
                {
                    saveLastPoint.isCallbackPausable = _callback.isCallbackPausable;
                    events[saveLastPoint.id] = saveLastPoint;
                }
                return events[saveLastPoint.id];
            case "CreateNewResultFile":
                PTVR.Data.Callback.CreateNewResultFile createNewResultFile = new PTVR.Data.Callback.CreateNewResultFile((PTVR.Data.Callback.CreateNewResultFile)_callback);
                if (!events.ContainsKey(createNewResultFile.id))
                {
                    createNewResultFile.isCallbackPausable = _callback.isCallbackPausable;
                    events[createNewResultFile.id] = createNewResultFile;
                }
                return events[createNewResultFile.id];
            case "AddInteractionPassedToCallback":
                PTVR.Data.Callback.AddInteractionPassedToCallback addInteractionPassedToEvent = new PTVR.Data.Callback.AddInteractionPassedToCallback((PTVR.Data.Callback.AddInteractionPassedToCallback)_callback);

                if (!events.ContainsKey(addInteractionPassedToEvent.id))
                {
                    addInteractionPassedToEvent.isCallbackPausable = _callback.isCallbackPausable;
                    events[addInteractionPassedToEvent.id] = addInteractionPassedToEvent;
                }
         
                return events[addInteractionPassedToEvent.id];
            case "ChangeAudioSettings":
                PTVR.Data.Callback.ChangeAudioSettings changeAudioSettings = new PTVR.Data.Callback.ChangeAudioSettings((PTVR.Data.Callback.ChangeAudioSettings)_callback);
               
                if (!events.ContainsKey(changeAudioSettings.id))
                {
                    changeAudioSettings.isCallbackPausable = _callback.isCallbackPausable;
                    events[changeAudioSettings.id] = changeAudioSettings;
                }

                return events[changeAudioSettings.id];
            case "ChangeHapticSettings":
                PTVR.Data.Callback.ChangeHapticSettings changeHapticSettings = new PTVR.Data.Callback.ChangeHapticSettings((PTVR.Data.Callback.ChangeHapticSettings)_callback);

                if (!events.ContainsKey(changeHapticSettings.id))
                {
                    Debug.Log("frequency = " + changeHapticSettings.frequency);
                    Debug.Log("controller = " + changeHapticSettings.controller);
                    Debug.Log("amplitude = " + changeHapticSettings.amplitude);
                    changeHapticSettings.isCallbackPausable = _callback.isCallbackPausable;
                    events[changeHapticSettings.id] = changeHapticSettings;
                }

                return events[changeHapticSettings.id];
            case "CalibrationEyesTracking":
                PTVR.Data.Callback.CalibrationEyesTracking calibrationEyesTracking = new PTVR.Data.Callback.CalibrationEyesTracking((PTVR.Data.Callback.CalibrationEyesTracking)_callback);

                if (!events.ContainsKey(calibrationEyesTracking.id))
                {
                    calibrationEyesTracking.isCallbackPausable = _callback.isCallbackPausable;
                    events[calibrationEyesTracking.id] = calibrationEyesTracking;
                }

                return events[calibrationEyesTracking.id];
            case "AugmentationCamera":
                PTVR.Data.Callback.AugmentationCamera augmentationCamera = new PTVR.Data.Callback.AugmentationCamera((PTVR.Data.Callback.AugmentationCamera)_callback);

                if (!events.ContainsKey(augmentationCamera.id))
                {
                    augmentationCamera.isCallbackPausable = _callback.isCallbackPausable;
                    events[augmentationCamera.id] = augmentationCamera;
                }

                return events[augmentationCamera.id];
            case "ChangeObjectRotateTo":
                PTVR.Data.Callback.ChangeObjectRotateTo changeObjectToLookAt = new PTVR.Data.Callback.ChangeObjectRotateTo((PTVR.Data.Callback.ChangeObjectRotateTo)_callback);
                if (!events.ContainsKey(changeObjectToLookAt.id))
                {
                    changeObjectToLookAt.isCallbackPausable = _callback.isCallbackPausable;
                    events[changeObjectToLookAt.id] = changeObjectToLookAt;
                }
                return events[changeObjectToLookAt.id];
            case "ChangeObjectParent":
                PTVR.Data.Callback.ChangeObjectParent removeObjectParenting = new PTVR.Data.Callback.ChangeObjectParent((PTVR.Data.Callback.ChangeObjectParent)_callback);
                if (!events.ContainsKey(removeObjectParenting.id))
                {
                    removeObjectParenting.isCallbackPausable = _callback.isCallbackPausable;
                    events[removeObjectParenting.id] = removeObjectParenting;
                }
                return events[removeObjectParenting.id];
            case "InstantiateObject":
                PTVR.Data.Callback.InstantiateObject instantiateObject = new PTVR.Data.Callback.InstantiateObject((PTVR.Data.Callback.InstantiateObject)_callback);
                if (!events.ContainsKey(instantiateObject.id))
                {
                    instantiateObject.isCallbackPausable = _callback.isCallbackPausable;
                    events[instantiateObject.id] = instantiateObject;
                }
                return events[instantiateObject.id];
            default:
                return null;

        }
    }

    public void CleanAllEvent()
    {

        //for(int count = 0; count < exampleInputController.exampleInput.Count; count++)
        //{
        //exampleInputController.exampleInput[count].input.inputPress.RemoveAllListeners();
        //exampleInputController.exampleInput[count].input.inputHold.RemoveAllListeners();
        //exampleInputController.exampleInput[count].input.inputRelease.RemoveAllListeners();
        //}
   
        foreach (DistanceFromPointUnityEvent entry in DistanceFromPointManager.instance.controllers[0].distanceFromPoint.Values)
        {
            entry.input.OnInputPress.RemoveAllListeners();
            entry.input.actionPress = null;
            entry.input.OnInputHold.RemoveAllListeners();
            entry.input.actionHold = null;
            entry.input.OnInputRelease.RemoveAllListeners();
            entry.input.actionRelease = null;
        }
    
        foreach (KeyboardUnityEvent entry in KeyboardBasedController.instance.keyboardKeys.Values)
        {
            entry.input.OnInputPress.RemoveAllListeners();
            entry.input.actionPress = null;
            entry.input.OnInputHold.RemoveAllListeners();
            entry.input.actionHold = null;
            entry.input.OnInputRelease.RemoveAllListeners();
            entry.input.actionRelease = null;
        }
 
        foreach(Alarm entry in TimerManager.instance.alarms.Values)
        {
            entry.input.actionPress = null;
            entry.input.OnInputPress.RemoveAllListeners();
            entry.input.actionHold = null;
            entry.input.OnInputHold.RemoveAllListeners();
            entry.input.actionRelease = null;
            entry.input.OnInputRelease.RemoveAllListeners();
        }

        TimerManager.instance.isTimingScene = false;
        TimerManager.instance.alarms.Clear();
        TimerManager.instance.alarm.Clear();

        foreach (PointedAtUnityEvent ptue in PointedAtManager.instance.pointedAt.Values)
        {
            ptue.input.actionPress = null;
            ptue.input.OnInputPress.RemoveAllListeners();
            ptue.input.actionHold = null;
            ptue.input.OnInputHold.RemoveAllListeners();
            ptue.input.actionRelease = null;
            ptue.input.OnInputRelease.RemoveAllListeners();
        }
        PointedAtManager.instance.pointedAt.Clear();
        foreach (GameObject go in PointedAtManager.instance.pointedAtCone.Values)
        {
            Destroy(go);
        }
        PointedAtManager.instance.pointedAt.Clear();

        PointedAtManager.instance.pointedAtCone.Clear();
        
        for (int count = 0; count < rightController.vrInput.Count; count++)
        {
            rightController.vrInput[count].input.actionPress = null;
            rightController.vrInput[count].input.OnInputPress.RemoveAllListeners();
            rightController.vrInput[count].input.actionHold = null;
            rightController.vrInput[count].input.OnInputHold.RemoveAllListeners();
            rightController.vrInput[count].input.actionRelease = null;
            rightController.vrInput[count].input.OnInputRelease.RemoveAllListeners();
        }

        for (int count = 0; count < leftController.vrInput.Count; count++)
        {
            leftController.vrInput[count].input.actionPress = null;
            leftController.vrInput[count].input.OnInputPress.RemoveAllListeners();
            leftController.vrInput[count].input.actionHold = null;
            leftController.vrInput[count].input.OnInputHold.RemoveAllListeners();
            leftController.vrInput[count].input.actionRelease = null;
            leftController.vrInput[count].input.OnInputRelease.RemoveAllListeners();
        }

        for (int count = 0; count < The3DWorldManager.instance.currScene.GetMetaHeaders().Count; count++)
        {
            DataBank.Instance.SetData(The3DWorldManager.instance.currScene.GetMetaHeaders()[count], "");
        }
        events.Clear();
    }

    #endregion Event

    #region Input
    //  InputUnityEvent CheckInputExempleInput()
    InputUnityEvent CheckInputPointedAt(PTVR.Data.Event.PointedAt _pointedAt)
    {
        return PointedAtManager.instance.TakePointedAt(_pointedAt);
    }

    InputUnityEvent CheckInputTimed(long _id)
    {
        return TimerManager.instance.TakeTimer(_id);
    }

    InputUnityEvent CheckInputHandController(string _valResp,string _inputName)
    {
        
        switch (_valResp)
        {
            
            case "right_touch_press":
                rightController.TakeHandController(_valResp).inputName = _inputName;
                rightController.TakeHandController(_valResp).button = "right_touch_press";
                return rightController.TakeHandController(_valResp);
            case "right_trigger":
                rightController.TakeHandController(_valResp).inputName = _inputName;
                rightController.TakeHandController(_valResp).button = "right_trigger";
                return rightController.TakeHandController(_valResp);
            case "right_grip":
                rightController.TakeHandController(_valResp).inputName = _inputName;
                rightController.TakeHandController(_valResp).button = "right_grip";
                return rightController.TakeHandController(_valResp);
            case "right_touch":
                rightController.TakeHandController(_valResp).inputName = _inputName;
                rightController.TakeHandController(_valResp).button = "right_touch";
                return rightController.TakeHandController(_valResp);
            case "left_trigger":
                leftController.TakeHandController(_valResp).inputName = _inputName;
                leftController.TakeHandController(_valResp).button = "left_trigger";
                return leftController.TakeHandController(_valResp);
            case "left_grip":
                leftController.TakeHandController(_valResp).inputName = _inputName;
                leftController.TakeHandController(_valResp).button = "left_grip";
                return leftController.TakeHandController(_valResp);
            case "left_touch":
                leftController.TakeHandController(_valResp).inputName = _inputName;
                leftController.TakeHandController(_valResp).button = "left_touch";
                return leftController.TakeHandController(_valResp);
            default:
                return null;
        }
    }
   
    InputUnityEvent CheckInputKeyboard(string _valResp,string _inputName,string _language)
    {
        countInput = 0;
        switch (_valResp)
        {
            case "a":
                if(_language== "French - France")
                    return KeyboardBasedController.instance.TakeKey(Key.Q, _valResp,_inputName);
                else
                    return KeyboardBasedController.instance.TakeKey(Key.A, _valResp, _inputName);
            case "b":
                return KeyboardBasedController.instance.TakeKey(Key.B, _valResp, _inputName);
            case "c":
                return KeyboardBasedController.instance.TakeKey(Key.C, _valResp, _inputName);
            case "d":
                return KeyboardBasedController.instance.TakeKey(Key.D, _valResp, _inputName);
            case "e":
                return KeyboardBasedController.instance.TakeKey(Key.E, _valResp, _inputName);
            case "f":
                return KeyboardBasedController.instance.TakeKey(Key.F, _valResp, _inputName);
            case "g":
                return KeyboardBasedController.instance.TakeKey(Key.G, _valResp, _inputName);
            case "h":
                return KeyboardBasedController.instance.TakeKey(Key.H, _valResp, _inputName);
            case "i":
                return KeyboardBasedController.instance.TakeKey(Key.I, _valResp, _inputName);
            case "j":
                return KeyboardBasedController.instance.TakeKey(Key.J, _valResp, _inputName);
            case "k":
                return KeyboardBasedController.instance.TakeKey(Key.K, _valResp, _inputName);
            case "l":
                return KeyboardBasedController.instance.TakeKey(Key.L, _valResp, _inputName);
            case "m":
                if (_language == "French - France")
                    return KeyboardBasedController.instance.TakeKey(Key.Semicolon, _valResp, _inputName);
                else
                    return KeyboardBasedController.instance.TakeKey(Key.M, _valResp, _inputName);
            case "n":
                return KeyboardBasedController.instance.TakeKey(Key.N, _valResp, _inputName);
            case "o":
                return KeyboardBasedController.instance.TakeKey(Key.O, _valResp, _inputName);
            case "p":
                return KeyboardBasedController.instance.TakeKey(Key.P, _valResp, _inputName);
            case "q":
                if (_language == "French - France")
                    return KeyboardBasedController.instance.TakeKey(Key.A, _valResp, _inputName);
                else
                    return KeyboardBasedController.instance.TakeKey(Key.Q, _valResp, _inputName);
            case "r":
                return KeyboardBasedController.instance.TakeKey(Key.R, _valResp, _inputName);
            case "s":
                return KeyboardBasedController.instance.TakeKey(Key.S, _valResp, _inputName);
            case "t":
                return KeyboardBasedController.instance.TakeKey(Key.T, _valResp, _inputName);
            case "u":
                return KeyboardBasedController.instance.TakeKey(Key.U, _valResp, _inputName);
            case "v":
                return KeyboardBasedController.instance.TakeKey(Key.V, _valResp, _inputName);
            case "w":
                if (_language == "French - France")
                    return KeyboardBasedController.instance.TakeKey(Key.Z, _valResp, _inputName);
                else
                    return KeyboardBasedController.instance.TakeKey(Key.W, _valResp, _inputName);
            case "x":
                return KeyboardBasedController.instance.TakeKey(Key.X, _valResp, _inputName);
            case "y":
                return KeyboardBasedController.instance.TakeKey(Key.Y, _valResp, _inputName);
            case "z":
                if (_language == "French - France")
                    return KeyboardBasedController.instance.TakeKey(Key.W, _valResp, _inputName);
                else
                    return KeyboardBasedController.instance.TakeKey(Key.Z, _valResp, _inputName);
            case "capsLock":
                return KeyboardBasedController.instance.TakeKey(Key.CapsLock, _valResp, _inputName);
            case "enter":
                return KeyboardBasedController.instance.TakeKey(Key.Enter, _valResp, _inputName);
            case "space":
                return KeyboardBasedController.instance.TakeKey(Key.Space, _valResp, _inputName);
            case "scroll lock":
                return KeyboardBasedController.instance.TakeKey(Key.ScrollLock, _valResp, _inputName);
            case "numlock":
                return KeyboardBasedController.instance.TakeKey(Key.NumLock, _valResp, _inputName);
            case "caps lock":
                return KeyboardBasedController.instance.TakeKey(Key.CapsLock, _valResp, _inputName);
            case "right shift":
                return KeyboardBasedController.instance.TakeKey(Key.RightShift, _valResp, _inputName);
            case "left shift":
                return KeyboardBasedController.instance.TakeKey(Key.LeftShift, _valResp, _inputName);
            case "right ctrl":
                return KeyboardBasedController.instance.TakeKey(Key.RightCtrl, _valResp, _inputName);
            case "left ctrl":
                return KeyboardBasedController.instance.TakeKey(Key.LeftCtrl, _valResp, _inputName);
            case "right alt":
                return KeyboardBasedController.instance.TakeKey(Key.RightAlt, _valResp, _inputName);
            case "left alt":
                return KeyboardBasedController.instance.TakeKey(Key.LeftAlt, _valResp, _inputName);
            case "backspace":
                return KeyboardBasedController.instance.TakeKey(Key.Backspace, _valResp, _inputName);
            case "delete":
                return KeyboardBasedController.instance.TakeKey(Key.Delete, _valResp, _inputName);
            case "tab":
                return KeyboardBasedController.instance.TakeKey(Key.Tab, _valResp, _inputName);
            case "pause":
                return KeyboardBasedController.instance.TakeKey(Key.Pause, _valResp, _inputName);
            case "escape":
                return KeyboardBasedController.instance.TakeKey(Key.Escape, _valResp, _inputName);
            case "insert":
                return KeyboardBasedController.instance.TakeKey(Key.Insert, _valResp, _inputName);
            case "home":
                return KeyboardBasedController.instance.TakeKey(Key.Home, _valResp, _inputName);
            case "end":
                return KeyboardBasedController.instance.TakeKey(Key.End, _valResp, _inputName);
            case "page up":
                return KeyboardBasedController.instance.TakeKey(Key.PageUp, _valResp, _inputName);
            case "page down":
                return KeyboardBasedController.instance.TakeKey(Key.PageDown, _valResp, _inputName);
            case "f1":
                return KeyboardBasedController.instance.TakeKey(Key.F1, _valResp, _inputName);
            case "f2":
                return KeyboardBasedController.instance.TakeKey(Key.F2, _valResp, _inputName);
            case "f3":
                return KeyboardBasedController.instance.TakeKey(Key.F3, _valResp, _inputName);
            case "f4":
                return KeyboardBasedController.instance.TakeKey(Key.F4, _valResp, _inputName);
            case "f5":
                return KeyboardBasedController.instance.TakeKey(Key.F5, _valResp, _inputName);
            case "f6":
                return KeyboardBasedController.instance.TakeKey(Key.F6, _valResp, _inputName);
            case "f7":
                return KeyboardBasedController.instance.TakeKey(Key.F7, _valResp, _inputName);
            case "f8":
                return KeyboardBasedController.instance.TakeKey(Key.F8, _valResp, _inputName);
            case "f9":
                return KeyboardBasedController.instance.TakeKey(Key.F9, _valResp, _inputName);
            case "f10":
                return KeyboardBasedController.instance.TakeKey(Key.F10, _valResp, _inputName);
            case "f11":
                return KeyboardBasedController.instance.TakeKey(Key.F11, _valResp, _inputName);
            case "f12":
                return KeyboardBasedController.instance.TakeKey(Key.F12, _valResp, _inputName);
            case "numPad0":
                return KeyboardBasedController.instance.TakeKey(Key.Numpad0, _valResp, _inputName);
            case "numPad1":
                return KeyboardBasedController.instance.TakeKey(Key.Numpad1, _valResp, _inputName);
            case "numPad2":
                return KeyboardBasedController.instance.TakeKey(Key.Numpad2, _valResp, _inputName);
            case "numPad3":
                return KeyboardBasedController.instance.TakeKey(Key.Numpad3, _valResp, _inputName);
            case "numPad4":
                return KeyboardBasedController.instance.TakeKey(Key.Numpad4, _valResp, _inputName);
            case "numPad5":
                return KeyboardBasedController.instance.TakeKey(Key.Numpad5, _valResp, _inputName);
            case "numPad6":
                return KeyboardBasedController.instance.TakeKey(Key.Numpad6, _valResp, _inputName);
            case "numPad7":
                return KeyboardBasedController.instance.TakeKey(Key.Numpad7, _valResp, _inputName);
            case "numPad8":
                return KeyboardBasedController.instance.TakeKey(Key.Numpad8, _valResp, _inputName);
            case "numPad9":
                return KeyboardBasedController.instance.TakeKey(Key.Numpad9, _valResp, _inputName);
            case "numPad*":
                return KeyboardBasedController.instance.TakeKey(Key.NumpadMultiply, _valResp, _inputName);
            case "numPad+":
                return KeyboardBasedController.instance.TakeKey(Key.NumpadPlus, _valResp, _inputName);
            case "numPad/":
                return KeyboardBasedController.instance.TakeKey(Key.NumpadDivide, _valResp, _inputName);
            case "numPad=":
                return KeyboardBasedController.instance.TakeKey(Key.NumpadEquals, _valResp, _inputName);
            case "numPad-":
                return KeyboardBasedController.instance.TakeKey(Key.NumpadMinus, _valResp, _inputName);
            case "numPadEnter":
                return KeyboardBasedController.instance.TakeKey(Key.NumpadEnter, _valResp, _inputName);
            case "left":
                return KeyboardBasedController.instance.TakeKey(Key.LeftArrow, _valResp, _inputName);
            case "right":
                return KeyboardBasedController.instance.TakeKey(Key.RightArrow, _valResp, _inputName);
            case "down":
                return KeyboardBasedController.instance.TakeKey(Key.DownArrow, _valResp, _inputName);
            case "up":
                return KeyboardBasedController.instance.TakeKey(Key.UpArrow, _valResp, _inputName);

            default:
                Debug.Log("Alerte");
                return null;

        }
    }
    #endregion Input

}
[Serializable]
public class InputUnityEvent
{
    public bool hasEventOnPress;
    public UnityAction actionPress;
    public CustomUnityEvent OnInputPress;

    public bool hasEventOnHold;
    public UnityAction actionHold;
    public CustomUnityEvent OnInputHold;

    public bool hasEventOnRelease;
    public UnityAction actionRelease;
    public CustomUnityEvent OnInputRelease;
    public string button;

    public bool isSetup = false;

    public string inputName;
}

