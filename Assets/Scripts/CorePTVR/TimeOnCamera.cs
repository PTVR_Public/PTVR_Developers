using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeOnCamera : MonoBehaviour
{

    #region Inspector
    public The3DWorldManager three_d_world;
    public CallbackManager callback_manager;
    public SetVisualSettings set_visual_settings;
    #endregion Inspector

    private List<string> string_times;
    private bool file_saved = false;
    private int n_frame = 0;
    private int n_frame_rendered = 0;
    private System.DateTime t_update;
    private System.DateTime t_late_update;
    private System.DateTime t_actual;
    private bool save_to_file = false;
    private bool scene_start_recorded = false;


    // Start is called before the first frame update
    void Start()
    {
        string_times = new List<string>();
        string_times.Add("frame t_0 t_m diff t_update t_post_render t_deserial" +
                        " t_event_time_start t_scene_time_start t_late_update t_scene_finished t_trial_finished scene_rendered");
    }

    // Update is called once per frame
    void Update()
    {
        t_update = System.DateTime.Now;
    }
    void LateUpdate()
    {
        t_late_update = System.DateTime.Now;
    }

    private void OnPostRender()
    {

        t_actual = System.DateTime.Now;
        if (three_d_world.reset_rendering_count)
        {
            TimerManager.instance.SetTimeScene();
            TimerManager.instance.isTimingScene = true;

            DataBank.Instance.SetData("scene_start_timestamp", format_date_time(t_actual).ToString());
            three_d_world.reset_rendering_count = false;
        }

        //print(three_d_world.scene_started);
        if (three_d_world.scene_started)
        {
            // if scene has started, all frames contain visual objects visible on screen
            n_frame_rendered += 1;

            
        } //else


        n_frame += 1;

        //string_times.Add(n_frame.ToString() + " " +
        //                 format_date_time(three_d_world.t0).ToString() + " " +
        //                 format_date_time(three_d_world.t_m).ToString() + " " +
        //                 format_date_time_span(dt_diff_since_last_t).ToString() + " " +
        //                 format_date_time(t_update).ToString() + " " +
        //                 format_date_time(t_actual).ToString() + " " +
        //                 format_date_time(three_d_world.t_deserial).ToString() + " " +
        //                 format_date_time(callback_manager.t_scene).ToString() + " " +
        //                 format_date_time(set_visual_settings.t_scene).ToString() + " " +
        //                 format_date_time(t_late_update).ToString() + " " +
        //                 format_date_time(three_d_world.t_scene_finished).ToString() + " " +
        //                 format_date_time(three_d_world.t_trial_finished).ToString() + " " +
        //                 three_d_world.scene_started.ToString());


        //if ((n_frame_rendered >= 300) & (file_saved == false))
        //{
        //    var path_save = getPathForRecord();
        //    var folder_path = path_save + "times";
        //    var path_record_subject = folder_path + ".txt";

        //    //save data to file
        //    string[] array = string_times.ToArray();
        //    System.IO.File.WriteAllLines(path_record_subject, array);
        //    print("file saved");
        //    file_saved = true;
        //}
    }

    public int format_date_time_span(System.TimeSpan date)
    {
        return date.Minutes * 60000 + date.Seconds * 1000 + date.Milliseconds;
    }

    public int format_date_time(System.DateTime date)
    {
        return date.Hour * 3600000 + date.Minute * 60000 + date.Second * 1000 + date.Millisecond;
    }

    private string getPathForRecord()
    {
#if UNITY_EDITOR
        return Application.dataPath + "/CSV/";//+ "Saved_data.csv";
#elif UNITY_ANDROID
                        return Application.persistentDataPath+"Saved_data.csv";
#elif UNITY_IPHONE
                        return Application.persistentDataPath+"/"+"Saved_data.csv";
#else
                        return Application.dataPath +"/CSV/";
#endif
    }
}
