using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

using PTVR.Core.Experimental;

namespace PTVR.Converters
{
    public class ObjectPropertyValueConverter : JsonConverter
    {
        public override bool CanConvert(System.Type objectType)
        {
            return (objectType == typeof(System.Object));
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new System.NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
        {

            if (reader.TokenType == JsonToken.Boolean)
            {
                return (bool) reader.Value;
            } else if (reader.TokenType == JsonToken.Integer)
            {
                int result;
                if (int.TryParse(reader.Value.ToString(), out result))
                {
                    return result;
                }
            } else if (reader.TokenType == JsonToken.Float)
            {
                float result;
                if (float.TryParse(reader.Value.ToString(), out result))
                {
                    return result;
                }
            } else if (reader.TokenType == JsonToken.String)
            {
                return (string) reader.Value;
            }

            JObject jo = JObject.Load(reader);

            bool isBool = Boolean.TryParse(jo.ToString(), out _);
            if (isBool)
            {
                return (bool)jo;
            }

            string dataType = jo["dataType"].Value<string>();
            switch(dataType)
            {
                case "Color":
                    return jo.ToObject<Color>(serializer);
                case "Vector3":
                    return jo.ToObject<Vector3>(serializer);
                case "_GetPropertyValueCallback":
                    return jo.ToObject<RefToDataProvider>(serializer);
                case "_GetFormattedStringCallback":
                    return jo.ToObject<RefToDataProvider>(serializer);
                default:
                    Debug.LogError(string.Format("DataType {} Not Recognized", dataType));
                    return null;
            }
        }
    }

}

