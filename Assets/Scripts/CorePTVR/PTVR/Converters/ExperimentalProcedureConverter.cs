using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PTVR.Converters
{
    public class ExperimentalProcedureConverter : JsonConverter
    {
        public override bool CanConvert(System.Type objectType)
        {
            return (objectType == typeof(PTVR.ExperimentalDesign.ExperimentalProcedure));
        }
        public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jo = JObject.Load(reader);
            switch(jo["experimentalProcedureType"].Value<string>())
            {
                case "FactorialDesign":
                    return jo.ToObject<PTVR.ExperimentalDesign.FactorialDesign>(serializer);
                default:
                    return null;
            }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new System.NotImplementedException();
        }
    }

}

