//Helps converting between various input event types in ptvr.data.input
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PTVR
{
    namespace Converters
    {
        public class CallbackConverter : JsonConverter
        {
            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(PTVR.Data.Callback.Callback));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
            {//Convert to specific child class
                JObject jo = JObject.Load(reader);
                switch(jo["type"].Value<string>())
                {
                    //case "ExampleEvent":
                    //return jo.ToObject<PTVR.Data.Event.ExampleEvent>(serializer);
                    case "Quit":
                        return jo.ToObject<PTVR.Data.Callback.Quit>(serializer);
                    case "PauseCurrentScene":
                        return jo.ToObject<PTVR.Data.Callback.PauseCurrentScene>(serializer);
                    case "EndCurrentScene":
                        return jo.ToObject<PTVR.Data.Callback.EndCurrentScene>(serializer);
                    case "EndCurrentTrial":
                        return jo.ToObject<PTVR.Data.Callback.EndCurrentTrial>(serializer);
                    case "RestartCurrentScene":
                        return jo.ToObject<PTVR.Data.Callback.RestartCurrentScene>(serializer);
                    case "RestartCurrentTrial":
                        return jo.ToObject<PTVR.Data.Callback.RestartCurrentTrial>(serializer);
                    case "PrintResultFileLine":
                        return jo.ToObject<PTVR.Data.Callback.PrintResultFileLine>(serializer);
                    case "ExecutePythonScript":
                        return jo.ToObject<PTVR.Data.Callback.ExecutePythonScript>(serializer);
                    case "ControlFromPythonScript":
                        return jo.ToObject<PTVR.Data.Callback.ControlFromPythonScript>(serializer);
                    case "TakePicture":
                        return jo.ToObject<PTVR.Data.Callback.TakePicture>(serializer);
                    case "Move_with_periodic_function":
                        return jo.ToObject<PTVR.Data.Callback.Move_with_periodic_function>(serializer);
                    case "Translate": 
                        return jo.ToObject<PTVR.Data.Callback.Translate>(serializer); 
                    case "AnimateObject":
                        return jo.ToObject<PTVR.Data.Callback.AnimateObject>(serializer);
                    case "Rotate":
                        return jo.ToObject<PTVR.Data.Callback.Rotate>(serializer); 
                    case "Grab":
                        return jo.ToObject<PTVR.Data.Callback.Grab>(serializer); 
                    case "MakeGrabable":
                        return jo.ToObject<PTVR.Data.Callback.MakeGrabable>(serializer);
                    case "Attractor":
                        return jo.ToObject<PTVR.Data.Callback.Attractor>(serializer);
                    case "Attractive":
                        return jo.ToObject<PTVR.Data.Callback.Attractive>(serializer);
                    case "ResetPosition":
                        return jo.ToObject<PTVR.Data.Callback.ResetPosition>(serializer);
                    case "ChangeObjectColor":
                        return jo.ToObject<PTVR.Data.Callback.ChangeObjectColor>(serializer);
                    case "ChangeObjectScale":
                        return jo.ToObject<PTVR.Data.Callback.ChangeObjectScale>(serializer);
                    case "ChangeObjectOutline":
                        return jo.ToObject<PTVR.Data.Callback.ChangeObjectOutline>(serializer);
                    case "ChangeObjectEnable":
                        return jo.ToObject<PTVR.Data.Callback.ChangeObjectEnable>(serializer);
                    case "ChangeObjectVisibility":
                        return jo.ToObject<PTVR.Data.Callback.ChangeObjectVisibility>(serializer);
                    case "ChangeBackgroundColor":
                        return jo.ToObject<PTVR.Data.Callback.ChangeBackgroundColor>(serializer);
                    case "SaveText":
                        return jo.ToObject<PTVR.Data.Callback.SaveText>(serializer);
                    case "FillInResultsFileColumn":
                        return jo.ToObject<PTVR.Data.Callback.FillInResultsFileColumn>(serializer);
                    case "ChangeObjectText":
                        return jo.ToObject<PTVR.Data.Callback.ChangeObjectText>(serializer);
                    case "UpdateLastPoint":
                        return jo.ToObject<PTVR.Data.Callback.UpdateLastPoint>(serializer);
                    case "AddNewPoint":
                        return jo.ToObject<PTVR.Data.Callback.AddNewPoint>(serializer);
                    case "ShowAllPoints":
                        return jo.ToObject<PTVR.Data.Callback.ShowAllPoints>(serializer);
                    case "DeleteLastPoint":
                        return jo.ToObject<PTVR.Data.Callback.DeleteLastPoint>(serializer);
                    case "SaveLastPoint":
                        return jo.ToObject<PTVR.Data.Callback.SaveLastPoint>(serializer);
                    case "AddInteractionPassedToCallback":
                        return jo.ToObject<PTVR.Data.Callback.AddInteractionPassedToCallback>(serializer);
                    case "ChangeAudioSettings":
                        return jo.ToObject<PTVR.Data.Callback.ChangeAudioSettings>(serializer);
                    case "CreateNewResultFile":
                        return jo.ToObject<PTVR.Data.Callback.CreateNewResultFile>(serializer);
                    case "ChangeHapticSettings":
                        return jo.ToObject<PTVR.Data.Callback.ChangeHapticSettings>(serializer);
                    case "CalibrationEyesTracking":
                        return jo.ToObject<PTVR.Data.Callback.CalibrationEyesTracking>(serializer);
                    case "AugmentationCamera":
                        return jo.ToObject<PTVR.Data.Callback.AugmentationCamera>(serializer);
                    case "ChangeObjectRotateTo":
                        return jo.ToObject<PTVR.Data.Callback.ChangeObjectRotateTo>(serializer);
                    case "ChangeObjectParent":
                        return jo.ToObject<PTVR.Data.Callback.ChangeObjectParent>(serializer);
                    case "InstantiateObject":
                        return jo.ToObject<PTVR.Data.Callback.InstantiateObject>(serializer);
                    default:
                        return null;
                }
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
