using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTVR.ExperimentalDesign;

namespace PTVR.Converters
{
    public class PresentationModeConverter : JsonConverter
    {
        public override bool CanConvert(System.Type objectType)
        {
            return (objectType == typeof(PTVR.ExperimentalDesign.PresentationMode));
        }
        public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
        {//Convert to specific child class
            JObject jo = JObject.Load(reader);
            string presentationMode = jo["presentationMode"].Value<string>();
            switch(presentationMode)
            {
                case "InSequencePresentationMode":
                    return jo.ToObject<InSequencePresentationMode>(serializer);
                case "RandomizedPresentationMode":
                    return jo.ToObject<RandomizedPresentationMode>(serializer);
                default:
                    Debug.LogError(string.Format("PresentationMode {} Not Recognized", presentationMode));
                    return null;
            }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new System.NotImplementedException();
        }
    }

}

