﻿//Helps converting between various input event types in ptvr.data.input
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PTVR
{
    namespace Converters
    {
        public class EventConverter : JsonConverter
        {
            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(PTVR.Data.Event.InputEvent));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
            {//Convert to specific child class
                JObject jo = JObject.Load(reader);
                switch(jo["type"].Value<string>())
                {
                    //case "exempleInput"
                    //return jo.ToObject<PTVR.Data.Input.ExampleInput>(serializer);
                    case "keyboard":
                        return jo.ToObject<PTVR.Data.Event.Keyboard>(serializer);
                    case "handController":
                         return jo.ToObject<PTVR.Data.Event.HandController>(serializer);
                    case "timer":
                        return jo.ToObject<PTVR.Data.Event.Timer>(serializer);
                    case "pointedAt":
                        return jo.ToObject<PTVR.Data.Event.PointedAt>(serializer);
                    case "button":
                        return jo.ToObject<PTVR.Data.Event.Button>(serializer);
                    case "toggleUI":
                        return jo.ToObject<PTVR.Data.Event.ToggleUI>(serializer);
                    case "distanceFromPoint":
                        return jo.ToObject<PTVR.Data.Event.DistanceFromPoint>(serializer);
                    default:
                        return null;
                }
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
