using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PTVR
{
    namespace Converters
    {
        public class CameraOperatorConverter : JsonConverter
        {
            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(PTVR.CameraOperator));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
            {//Convert to specific child class
                JObject jo = JObject.Load(reader);
                switch (jo["type"].Value<string>())
                { 
                    case "cameraOperator":
                        return jo.ToObject<PTVR.CameraOperator>(serializer);
                 
                    default:
                        return null;
                }
            }
            public override bool CanWrite
            {
                get { return false; }
            }
            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
            throw new System.NotImplementedException();
            }

        }
    }
}
