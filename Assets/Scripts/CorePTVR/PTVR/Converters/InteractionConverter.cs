
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace PTVR
{
    namespace Converters
    {

        public class InteractionConverter : JsonConverter
        {

            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(PTVR.Interaction));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
            {//Convert to specific child class
                JObject jo = JObject.Load(reader);

                if (jo["type"].Value<string>() == "interaction")
                    return jo.ToObject<PTVR.Interaction>(serializer);

                return null;
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}