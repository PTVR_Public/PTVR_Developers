﻿//Helps converting between various scene types
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace PTVR
{
    namespace Converters
    {
        //Inspired from example at https://blog.codeinside.eu/2015/03/30/json-dotnet-deserialize-to-abstract-class-or-interface/public
        public class SceneConverter : JsonConverter
        {
            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(PTVR.Stimuli.Objects.Scene));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
            {//Convert to specific child class
                JObject jo = JObject.Load(reader);
                if (jo["type"].Value<string>() == "FIXATION_SCENE")
                    return jo.ToObject<PTVR.Stimuli.Objects.FixationScene>(serializer);
                if (jo["type"].Value<string>() == "VISUAL_SCENE")
                       return jo.ToObject<PTVR.Stimuli.Objects.VisualScene>(serializer);
                if (jo["type"].Value<string>() == "RESPONSE_SCENE")
                    return jo.ToObject<PTVR.Stimuli.Objects.ResponseScene>(serializer);

                return null;
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
