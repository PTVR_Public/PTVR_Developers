//Helps converting between various color formats
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace PTVR
{
    namespace Converters
    {
        public class VisualObjectTypeConverter : JsonConverter
        {
            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(PTVR.Stimuli.Objects.VisualObject));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
            {//Convert to specific child class

                JObject jo = JObject.Load(reader);

                if (jo["type"].Value<string>() == "capsule")
                    return jo.ToObject<PTVR.Stimuli.Objects.Capsule>(serializer);

                if (jo["type"].Value<string>() == "cube")
                    return jo.ToObject<PTVR.Stimuli.Objects.Cube>(serializer);

                if (jo["type"].Value<string>() == "elevator")
                    return jo.ToObject<PTVR.Stimuli.Objects.Elevator>(serializer);

                if (jo["type"].Value<string>() == "forest")
                    return jo.ToObject<PTVR.Stimuli.Objects.Forest>(serializer);

                if (jo["type"].Value<string>() == "light")
                    return jo.ToObject<PTVR.Stimuli.Objects.Light>(serializer);

                if (jo["type"].Value<string>() == "customObject")
                    return jo.ToObject<PTVR.Stimuli.Objects.CustomObject>(serializer);


                if (jo["type"].Value<string>() == "plane")
                    return jo.ToObject<PTVR.Stimuli.Objects.Plane>(serializer);

                if (jo["type"].Value<string>() == "sphere")
                    return jo.ToObject<PTVR.Stimuli.Objects.Sphere>(serializer);

                if (jo["type"].Value<string>() == "quad")
                    return jo.ToObject<PTVR.Stimuli.Objects.Quad>(serializer);

                if (jo["type"].Value<string>() == "cylinder")
                    return jo.ToObject<PTVR.Stimuli.Objects.Cylinder>(serializer);

                if (jo["type"].Value<string>() == "text")
                    return jo.ToObject<PTVR.Stimuli.Objects.Text>(serializer);

                if (jo["type"].Value<string>() == "sprite")
                    return jo.ToObject<PTVR.Stimuli.Objects.Sprite>(serializer);

				if (jo["type"].Value<string>() == "audio_source")
		   			return jo.ToObject<PTVR.Stimuli.Objects.AudioSource>(serializer);

                if (jo["type"].Value<string>() == "flat_screen")
                    return jo.ToObject<PTVR.Stimuli.Objects.FlatScreen>(serializer);

                if (jo["type"].Value<string>() == "tangent_screen")
                    return jo.ToObject<PTVR.Stimuli.Objects.FlatScreen>(serializer);

                if (jo ["type"].Value<string> () == "coordinated") 
					return jo.ToObject<PTVR.Stimuli.Objects.EmptyVisualObject> (serializer);

                if (jo["type"].Value<string>() == "line")
                    return jo.ToObject<PTVR.Stimuli.Objects.Line>(serializer);

                if (jo["type"].Value<string>() == "lineTool")
                    return jo.ToObject<PTVR.Stimuli.Objects.LineTool>(serializer);

                if (jo["type"].Value<string>() == "calculator")
                    return jo.ToObject<PTVR.Stimuli.Objects.Calculator>(serializer);

                if (jo["type"].Value<string>() == "cameraPTVR")
                    return jo.ToObject<PTVR.Stimuli.Objects.CameraPTVR>(serializer);
                if (jo["type"].Value<string>() == "gizmo")
                    return jo.ToObject<PTVR.Stimuli.Objects.Gizmo>(serializer);

                Debug.LogError("cannot convert the object of type " + jo["type"].Value<string>());
                return null;
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
