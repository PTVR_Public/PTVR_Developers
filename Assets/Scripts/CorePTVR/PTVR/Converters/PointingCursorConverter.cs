﻿//Helps converting between various color formats
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace PTVR
{
    namespace Converters
    {
	//[System.Serializable]
        public class PointingCursorConverter : JsonConverter
        {
            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(PTVR.PointingCursor));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
			{//Convert to specific child class
				JObject jo = JObject.Load (reader);

				if (jo ["contingency_type"].Value<string> () == "HEADSET")
					return jo.ToObject<PTVR.HeadPovContingent> (serializer);
                if (jo["contingency_type"].Value<string>() == "LEFT_EYE")
                    return jo.ToObject<PTVR.GazeContingent>(serializer);
                if (jo["contingency_type"].Value<string>() == "RIGHT_EYE")
                    return jo.ToObject<PTVR.GazeContingent>(serializer);
                if (jo["contingency_type"].Value<string>() == "GAZE")
                    return jo.ToObject<PTVR.GazeContingent>(serializer);
                if (jo["contingency_type"].Value<string>() == "LEFT_HAND")
                    return jo.ToObject<PTVR.HandContingent>(serializer);
                if (jo["contingency_type"].Value<string>() == "RIGHT_HAND")
                    return jo.ToObject<PTVR.HandContingent>(serializer);


                Debug.LogError ("Pointer type " + jo ["origin"].Value<string> () + " not implemented. Falling back on the default");
				return jo.ToObject<PTVR.HeadPovContingent> (serializer);
			}

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
