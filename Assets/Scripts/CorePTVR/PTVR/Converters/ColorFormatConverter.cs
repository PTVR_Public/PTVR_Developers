﻿//Helps converting between various color formats
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
namespace PTVR
{
    namespace Converters
    {
        //Inspired from example at https://blog.codeinside.eu/2015/03/30/json-dotnet-deserialize-to-abstract-class-or-interface/public 
        public class ColorFormatConverter : JsonConverter
        {
            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(PTVR.Stimuli.Color));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
            {   //Convert to specific child class
                JObject jo = JObject.Load(reader);
                switch (jo["type"].Value<string>())
                {
                    case "rgbcolor":
                        return jo.ToObject<PTVR.Stimuli.RGBColor>(serializer);

                    case "rgb255color":
                        return jo.ToObject<PTVR.Stimuli.RGB255Color>(serializer);

                    case "hexcolor":
                        return jo.ToObject<PTVR.Stimuli.HexColor>(serializer);

                    case "dynamicColor":
                        Debug.Log("DynamicColor");
                        jo.ToObject<PTVR.Stimuli.DynamicColor>(serializer).Setup();
                        return jo.ToObject<PTVR.Stimuli.DynamicColor>(serializer);

                    default:
                        Debug.Log("Color not exist");
                        return null;
                }
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
