using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTVR.Data.Event;

using PTVR.Core.Experimental;

namespace PTVR.Converters.Core.Experimental
{
    public class CallbackConverter2 : JsonConverter
    {
        public override bool CanConvert(System.Type objectType)
        {
            return (objectType == typeof(Callback));
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new System.NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jo = JObject.Load(reader);
            string callbackType = jo["callbackType"].Value<string>();
            switch(callbackType)
            {
                case "_SetObjectPropertyCallback":
                    return jo.ToObject<SetObjectPropertyCallback>(serializer);
                case "_FinishCurrentTrialCallback":
                    return jo.ToObject<FinishCurrentTrialCallback>(serializer);
                case "_GetPropertyValueCallback":
                    return jo.ToObject<GetPropertyValueCallback>(serializer);
                case "_StartExperimentalProcedureCallback":
                    return jo.ToObject<StartExperimentalProcedureCallback>(serializer);
                case "_GetFormattedStringCallback":
                    return jo.ToObject<GetFormattedStringCallback>(serializer);
                default:
                    Debug.LogError(string.Format("Callback {} Not Recognized", callbackType));
                    return null;
            }
        }
    }

}

