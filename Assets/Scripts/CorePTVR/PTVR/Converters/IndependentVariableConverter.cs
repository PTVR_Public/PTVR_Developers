using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTVR.ExperimentalDesign;

namespace PTVR.Converters
{
    public class IndependentVariableConverter : JsonConverter
    {
        public override bool CanConvert(System.Type objectType)
        {
            return (objectType == typeof(PTVR.ExperimentalDesign.IndependentVariable));
        }
        public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
        {//Convert to specific child class
            JObject jo = JObject.Load(reader);
            string ivType = jo["independentVariableType"].Value<string>();
            switch(ivType)
            {
                case "DiscreteBalancedVariable":
                    return jo.ToObject<DiscreteBalancedVariable>(serializer);
                case "DiscreteRandomVariable":
                    return jo.ToObject<DiscreteRandomVariable>(serializer);
                case "ContinuousVariable":
                    return jo.ToObject<ContinuousVariable>(serializer);
                case "RandomUniformVariable":
                    return jo.ToObject<RandomUniformVariable>(serializer);
                case "RandomNormalVariable":
                    return jo.ToObject<RandomNormalVariable>(serializer);
                default:
                    Debug.LogError(string.Format("IndependentVariable {} Not Recognized", ivType));
                    return null;
            }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new System.NotImplementedException();
        }
    }

}

