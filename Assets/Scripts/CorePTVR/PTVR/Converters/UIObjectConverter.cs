using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PTVR
{
    namespace Converters
    {
        public class UIObjectConverter : JsonConverter
        {
            public override bool CanConvert(System.Type objectType)
            {
                return (objectType == typeof(PTVR.Stimuli.Objects.UIObject));
            }

            public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
            {//Convert to specific child class
                JObject jo = JObject.Load(reader);
                switch (jo["type"].Value<string>())
                {
                    case "textUI":
                        return jo.ToObject<PTVR.Stimuli.Objects.TextUI>(serializer);
                    case "buttonUI":
                        return jo.ToObject<PTVR.Stimuli.Objects.ButtonUI>(serializer);
                    case "imageUI":
                        return jo.ToObject<PTVR.Stimuli.Objects.ImageUI>(serializer);
                    case "graphUI":
                        return jo.ToObject<PTVR.Stimuli.Objects.GraphUI>(serializer);
                    default:
                        return null;
                }
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
