using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using PTVR.Core.Experimental;

namespace PTVR.Converters
{
    public class EventConverter2 : JsonConverter
    {
        public override bool CanConvert(System.Type objectType)
        {
            return (objectType == typeof(Event2));
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new System.NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, System.Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jo = JObject.Load(reader);
            string eventType = jo["eventType"].Value<string>();
            switch(eventType)
            {
                case "KeyEvent":
                    return jo.ToObject<KeyEvent>(serializer);
                case "KeyPressEvent":
                    return jo.ToObject<KeyPressEvent>(serializer);
                case "KeyReleaseEvent":
                    return jo.ToObject<KeyReleaseEvent>(serializer);
                case "PTVREvent":
                    return jo.ToObject<PTVREvent>(serializer);
                default:
                    Debug.LogError(string.Format("Event {} Not Recognized", eventType));
                    return null;
            }
        }
    }

}

