using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTVR.ExperimentalDesign
{
    // enum values are not typed as C# expects them
    // but this is done to help matching the enums values
    // coming from python
    public enum SequenceOrder
    {
        Given_Order,
        Random_Order,
        At_Runtime_Order
    }

    public class PresentationMode { }

    public class InSequencePresentationMode : PresentationMode
    {
        public SequenceOrder sequenceOrder;
    }

    public class RandomizedPresentationMode : PresentationMode { }

    public class DiscreteVariable : IndependentVariable
    {
        public System.Object[] values;
    }

    public class DiscreteBalancedVariable : DiscreteVariable
    {
        public PresentationMode presentationMode;
    }

    // this method need to have more testing
    public class DiscreteRandomVariable : DiscreteVariable, IRandomVariable
    {
        public float[] weights;
        public object GetRandomValue()
        {
            float accum = 0f;
            float randomValue = UnityEngine.Random.value;
            for (int i = 0; i < weights.Length; i++)
            {
                accum += weights[i];
                if (randomValue < accum)
                {
                    return values[i];
                }
            }
            throw new InvalidOperationException("No random value selected with prob=");
        }
    }

}

