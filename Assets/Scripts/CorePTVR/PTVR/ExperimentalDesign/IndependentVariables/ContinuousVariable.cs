using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTVR.ExperimentalDesign
{
    public class ContinuousVariable : IndependentVariable
    {
        public float minValue;
        public float maxValue;
    }

    public abstract class ContinuousRandomVariable : IndependentVariable, IRandomVariable
    {
        public abstract object GetRandomValue();
    }

    // scalar continuous random variables

    public abstract class RandomContinuosScalarVariable : ContinuousRandomVariable
    {
        public string distribution;
    }

    public class RandomUniformVariable : RandomContinuosScalarVariable
    {
        public float minInclusive;
        public float maxInclusive;

        public override object GetRandomValue()
        {
            return UnityEngine.Random.Range(minInclusive, maxInclusive);
        }
    }

    // TODO: implement later the other distributions
    // probably we will end up using a library like MathNet.Numerics
    public class RandomNormalVariable : RandomContinuosScalarVariable
    {
        public float meam;
        public float standardDeviation;

        public override object GetRandomValue()
        {
            // TODO: implement the right sampling from this distribution
            return UnityEngine.Random.Range(0f, 1f);
        }
    }

    public class RandomExponentalVariable : RandomContinuosScalarVariable
    {
        public float scale;

        public override object GetRandomValue()
        {
            // TODO: implement the right sampling from this distribution
            return UnityEngine.Random.Range(0f, 1f);
        }
    }

    public class RandomPoissonVariable : RandomContinuosScalarVariable
    {
        public float lambda;

        public override object GetRandomValue()
        {
            // TODO: implement the right sampling from this distribution
            return UnityEngine.Random.Range(0f, 1f);
        }
    }

    public class RandomBinomialVariable : RandomContinuosScalarVariable
    {
        public int n;
        public float p;

        public override object GetRandomValue()
        {
            // TODO: implement the right sampling from this distribution
            return UnityEngine.Random.Range(0f, 1f);
        }
    }

    public class RandomGammaVariable : RandomContinuosScalarVariable
    {
        public float shape;
        public float scale;

        public override object GetRandomValue()
        {
            // TODO: implement the right sampling from this distribution
            return UnityEngine.Random.Range(0f, 1f);
        }
    }

    // non-scalar continuous random variables
    public class RandomPointInsideCircleRandomVariable : ContinuousRandomVariable
    {
        public Vector3 origin;
        public float radius;

        public override object GetRandomValue()
        {
            Vector2 randomPosition = UnityEngine.Random.insideUnitCircle * radius;
            return origin + new Vector3(randomPosition.x, randomPosition.y, 0);
        }
    }

    public class RandomPointInsideSphereRandomVariable : ContinuousRandomVariable
    {
        public Vector3 origin;
        public float radius;

        public override object GetRandomValue()
        {
            return origin + UnityEngine.Random.insideUnitSphere * radius;
        }
    }

    public class RandomPointOnSphereRandomVariable : ContinuousRandomVariable
    {
        public Vector3 origin;
        public float radius;

        public override object GetRandomValue()
        {
            return origin + UnityEngine.Random.onUnitSphere * radius;
        }
    }

    public class RandomColorRandomVariable : ContinuousRandomVariable
    {
        public override object GetRandomValue()
        {
            return UnityEngine.Random.ColorHSV();
        }
    }

    public class RandomRotationRandomVariable : ContinuousRandomVariable
    {
        public override object GetRandomValue()
        {
            return UnityEngine.Random.rotationUniform;
        }
    }

}

