using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTVR.ExperimentalDesign
{
    [System.Serializable]
    public class IndependentVariable
    {
        public string name;
    }

    public interface IRandomVariable
    {
        System.Object GetRandomValue();
    }
}
