using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.Events;
using System.IO.Pipes;

using PTVR.ExperimentalDesign;

namespace PTVR
{
    public class ExperimentManager : MonoBehaviour
    {
        public static ExperimentManager Instance { get; private set; }
        // this is only expeirment data and logic
        public Experiment experiment;

        public bool Loaded { get; private set; } = false;
        public bool Initialized { get; private set; } = false;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            } else
            {
                Instance = this;
            }
        }

        public void LoadFromJson(string jsonData)
        {
            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new PTVR.Converters.ExperimentalProcedureConverter());
            settings.Converters.Add(new PTVR.Converters.ObjectPropertyValueConverter());
            settings.Converters.Add(new PTVR.Converters.IndependentVariableConverter());
            settings.Converters.Add(new PTVR.Converters.PresentationModeConverter());
            experiment = JsonConvert.DeserializeObject<Experiment>(jsonData, settings);

            Loaded = true;
            print("experimental design loaded");
        }

        public void Initialize()
        {
            experiment.Initialize();
            Initialized = true;
        }

        /*
         * This method is called from WorldManager after finishing loading the initial objects.
         * That's the theory, in reality, there is a custom event that is triggered.
         * This is just the entry point to start the experiment coroutine
         * */
        public void StartExperiment()
        {
            if (!Initialized) Initialize();

            print("starting experiment, registering callbacks and starting first trial");

            StartCoroutine(RunExperimentCoroutine());
        }

        private IEnumerator RunExperimentCoroutine()
        {

            experiment.onExperimentStartEvent.Trigger();
            while (!experiment.HasFinished)
            {
                if (experiment.HasBlockDesign)
                {
                    while (!experiment.BlocksHaveFinished)
                    {
                        experiment.StartBlock();
                        experiment.onBlockStartEvent.Trigger();
                        while (!experiment.TrialsInBlockHaveFinished)
                        {
                            experiment.StartTrial();
                            experiment.onTrialStartEvent.Trigger();
                            yield return experiment.TrialMainCoroutine();
                            experiment.onTrialEndEvent.Trigger();
                            experiment.EndTrial();
                        }
                        experiment.onBlockEndEvent.Trigger();
                        experiment.EndBlock();
                    }
                } else
                {
                    experiment.StartTrial();
                    experiment.onTrialStartEvent.Trigger();
                    yield return experiment.TrialMainCoroutine();
                    experiment.onTrialEndEvent.Trigger();
                    experiment.EndTrial();
                }

            }
            experiment.onExperimentEndEvent.Trigger();

            The3DWorldManager.instance.FinishExperiment();
        }

        public void FinishMainTrialCoroutine()
        {
            print("Ending trial!!!");
            experiment.EndTrialMainCoroutine();
            // trial.trialMainCoroutineCompleted= true;
            // before ending the trial, we should just update the flag to continue the corutine
            // StartTrial();
        }
    }


}

