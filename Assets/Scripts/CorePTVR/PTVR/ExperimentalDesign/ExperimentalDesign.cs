using PTVR.Data.Event;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements.Experimental;
using Valve.VR;

using PTVR.Core.Experimental;

namespace PTVR.ExperimentalDesign
{
    using BlockID = System.Int32;
    using TrialID = System.Int32;
    using BlockSize = System.Int32;
    public enum ShufflingMode
    {
        Shuffle_Once,
        Shuffle_Every_Time
    }

    public enum TrialGroupingMechanism
    {
        Not_Grouped,
        By_Block_Size,
        By_Nr_Blocks,
        By_Variable,
    }

    [System.Serializable]
    public abstract class ExperimentalProcedure
    {

        public TrialGroupingMechanism trialGroupingMechanism;

        public virtual bool HasFinished
        {
            get { return false; }
        }

        public bool hasBlockVariable;

        public virtual bool HasFinishedBlockVariables
        {
            get { return true; }
        }

        public virtual bool HasFinishedNonBlockVariables
        {
            get { return true; }
        }

        public bool HasBlockDesign
        {
            get {
                return trialGroupingMechanism != TrialGroupingMechanism.Not_Grouped;
            }

            private set { }
        }

        public virtual bool BlocksHaveFinished
        {
            get { return false; }
        }

        public virtual bool TrialsInBlockHaveFinished
        {
            get {
                return false;
            }
        }

        public abstract System.Object GetValue(string property);

        public virtual void Initialize() { }

        public virtual void EndTrial() { }

        public virtual void StartBlock() { }
        public virtual void EndBlock() { }
    }

    [System.Serializable]
    public class FactorialDesign : ExperimentalProcedure
    {
        public IndependentVariable[] independentVariables;
        public int nrTrialRepetitions;
        public int trialRepetitionDensity;
        public ShufflingMode shufflingMode;

        private int nrTrials = 0;

        private TrialID trialPointer = 0; // pointer to the trial in the trial table
        private BlockID blockPointer = 0;

        private int trialInBlockPointer = 0;
        public int nrBlocks = 0;
        public int blockSize = 0;
        // private Dictionary<BlockID, BlockSize> blockSizes = new Dictionary<BlockID, BlockID> ();
        public int[] blockSizes;

        // private List<Dictionary<string, System.Object>> trialTable = new List<Dictionary<string, System.Object>>();
        private Dictionary<string, System.Object[]> trialTable = new Dictionary<string, System.Object[]> ();

        public override void Initialize()
        {
            var random = new System.Random();

            List<DiscreteBalancedVariable> discreteBalancedVariables = independentVariables
                .Where(iv => iv.GetType() == typeof(DiscreteBalancedVariable))
                .Cast<DiscreteBalancedVariable>()
                .ToList();

            List<DiscreteBalancedVariable> inSequenceVariables = discreteBalancedVariables
                .Where(iv => iv.presentationMode.GetType() == typeof(InSequencePresentationMode))
                .ToList();

            List<DiscreteBalancedVariable> randomizedVariables = discreteBalancedVariables
                .Where(iv => iv.presentationMode.GetType() == typeof(RandomizedPresentationMode))
                .ToList();

            int nrInSequenceVariables = inSequenceVariables.Count;
            int nrRandomizedVariables = randomizedVariables.Count;

            nrTrials = nrTrialRepetitions * discreteBalancedVariables.Aggregate(1, (current,x) => current*x.values.Length);

            bool hasRepetitions = nrTrialRepetitions > 1;
            bool hasRandomization = randomizedVariables.Count > 0;
            int insertionIdx = trialRepetitionDensity;
            bool repetitionInRandom = insertionIdx > nrInSequenceVariables;

            int nrColumns = (hasRepetitions) ? independentVariables.Length + 1 : independentVariables.Length;
            List<List<System.Object>> data = new List<List<object>>(nrTrials);
            for (int i = 0; i < nrTrials; i++)
            {
                data.Add(new List<System.Object>(nrColumns));
                for (int j = 0; j < nrColumns; j++)
                {
                    data[i].Add(null);
                }
            }

            // add repetition column if needed
            if (hasRepetitions)
            {
                int[] repetitions = new int[nrTrialRepetitions];
                for (int i = 0; i < repetitions.Length; i++) { repetitions[i] = i+1; }
                if (repetitionInRandom)
                {
                    // array has no 'insert at'. check if list has it
                    // randomizedVariables.
                    DiscreteBalancedVariable repetitionVariable = new DiscreteBalancedVariable();
                    repetitionVariable.name = "rep";
                    repetitionVariable.values = Array.ConvertAll(repetitions, item => (System.Object)item);
                    repetitionVariable.presentationMode = new RandomizedPresentationMode();
                    randomizedVariables.Add(repetitionVariable);
                } else
                {
                    DiscreteBalancedVariable repetitionVariable = new DiscreteBalancedVariable();
                    repetitionVariable.name = "rep";
                    repetitionVariable.values = Array.ConvertAll(repetitions, item => (System.Object)item);
                    repetitionVariable.presentationMode = new InSequencePresentationMode();
                    ((InSequencePresentationMode)repetitionVariable.presentationMode).sequenceOrder = SequenceOrder.Given_Order;
                    inSequenceVariables.Insert(insertionIdx, repetitionVariable);
                }
            }

            if (hasRandomization)
            {
                List<System.Object[]> inSequenceIVSValues = inSequenceVariables.Select(x => x.values).ToList();
                List<System.Object[]> randomizedIVSValues = randomizedVariables.Select(x => x.values).ToList();

                var randomVarsCombinations = randomizedIVSValues
                    .Aggregate(
                        Enumerable.Repeat(Enumerable.Empty<object>(), 1),
                        (acc, list) => acc.SelectMany(x => list.Select(y => x.Concat(new[] { y }))))
                    .ToList();
                var randomVarsCombinationsList = randomVarsCombinations.Select(x => x.ToArray()).ToList();
                int colOffset = inSequenceIVSValues.Count;
                inSequenceIVSValues.Add(randomVarsCombinationsList.ToArray());
                int nrPatternRepetition = inSequenceVariables.Aggregate(1, (current,x) => current*x.values.Length);
                if (shufflingMode == ShufflingMode.Shuffle_Once)
                {
                    // equivalent to shuffle
                    // custom comparison between two elements in the domain [-1, 1]
                    randomVarsCombinationsList.Sort((a, b) => random.Next(2) * 2 - 1);
                    List<System.Object[]> randomEntries = randomVarsCombinationsList
                        .SelectMany(x => Enumerable.Repeat(x, nrPatternRepetition))
                        .ToList();
                    for (int row=0; row<randomEntries.Count; row++)
                    {
                        var entry = randomEntries[row];
                        for (int col = 0; col < entry.Length; col++)
                        {
                            data[row][colOffset+col] = entry[col];
                        }
                    }
                } else if (shufflingMode == ShufflingMode.Shuffle_Every_Time)
                {
                    int nrCombinations = randomVarsCombinationsList.Count;
                    for (int rep=0; rep < nrPatternRepetition; rep++)
                    {
                        randomVarsCombinationsList.Sort((a, b) => random.Next(2) * 2 - 1);
                        for (int row=0; row<randomVarsCombinationsList.Count; row++)
                        {
                            var entry = randomVarsCombinationsList[row];
                            for (int col = 0; col < entry.Length; col++)
                            {
                                data[rep*nrCombinations+row][colOffset+col] = entry[col];
                            }
                        }
                    }
                }
                // add the remaining in-sequence variables
                var combinations = inSequenceIVSValues
                    .Aggregate(
                        Enumerable.Repeat(Enumerable.Empty<object>(), 1),
                        (acc, list) => acc.SelectMany(x => list.Select(y => x.Concat(new[] { y }))))
                    .ToList();
                var combinationsArray = combinations.Select(x => x.ToArray()).ToList();
                for (int row=0; row<combinationsArray.Count; row++)
                {
                    var entry = combinationsArray[row];
                    for (int col = 0; col < entry.Length; col++)
                    {
                        if (col < colOffset)
                        {
                            data[row][col] = entry[col];
                        }
                    }
                }
            } else
            {
                List<System.Object[]> inSequenceIVSValues = inSequenceVariables.Select(x => x.values).ToList();
                var combinations = inSequenceIVSValues
                    .Aggregate(
                        Enumerable.Repeat(Enumerable.Empty<object>(), 1),
                        (acc, list) => acc.SelectMany(x => list.Select(y => x.Concat(new[] { y }))))
                    .ToList();
                var combinationsArray = combinations.Select(x => x.ToArray()).ToList();

                for (int row=0; row<combinationsArray.Count; row++)
                {
                    var entry = combinationsArray[row];
                    for (int col = 0; col < entry.Length; col++)
                    {
                        data[row][col] = entry[col];
                    }
                }
            }

            Debug.Log("testing trial table");

            // generate headers
            var headers = inSequenceVariables.Select(x => x.name)
                                 .Concat(randomizedVariables.Select(x => x.name))
                                 .ToList();

            // add additional columns to the table (trial id, block id, etc)
            AddTrialInfoColumns(data, headers);

            // populate trial table
            for (int col = 0; col < headers.Count; col++)
            {
                System.Object[] columnValues = new System.Object[data.Count];
                for (int row = 0; row < data.Count; row++)
                {
                    columnValues[row] = data[row][col];
                }
                trialTable.Add(headers[col], columnValues);
            }
        }

        public override bool HasFinished
        {
            // experiment is done once we go through all the combination of variables
            get {
                return trialPointer >= nrTrials;
            }
        }
        public override bool BlocksHaveFinished
        {
            get { return blockPointer >= nrBlocks; }
        }

        public override bool TrialsInBlockHaveFinished
        {
            get {
                return trialInBlockPointer >= blockSizes[blockPointer];
            }
        }

        public override System.Object GetValue(string property)
        {
            return trialTable[property][trialPointer];
        }

        // public TrialID InBlockTrialPointer
        // {
        //     get {
        //         // substract from the global pointer the nr of trials of the previous blocks
        //         int remainingTrials = trialPointer;
        //         for (int pastBlockID = blockPointer-1; pastBlockID >= 0; pastBlockID--)
        //         {
        //             remainingTrials -= blockSizes[pastBlockID];
        //         }
        //         return remainingTrials;
        //     }
        // }


        public override void EndTrial()
        {
            // increase counters in trial table
            trialPointer++;
            if (HasBlockDesign)
            {
                trialInBlockPointer++;
            }
        }

        public override void StartBlock()
        {
            trialInBlockPointer = 0;
        }

        public override void EndBlock()
        {
            if (trialInBlockPointer >= blockSizes[blockPointer])
            {
                blockPointer++;
            }
        }

        private void AddTrialInfoColumns(List<List<System.Object>> partialTable, List<string> partialHeaders)
        {
            // if (blockSizes.Count > 0)
            if (false)
            {

            } else
            {
                for (int trialId = 0; trialId < partialTable.Count; trialId++)
                {
                    partialTable[trialId].Insert(0, trialId + 1);
                }
                partialHeaders.InsertRange(0, new List<string> {"trial"});
            }
        }
    }

    public class PTVRObjectsManager
    {
        private static readonly PTVRObjectsManager instance = new PTVRObjectsManager();

        private PTVRObjectsManager() { }

        public static PTVRObjectsManager Instance { get { return instance; }  }

        private Dictionary<string, System.Object> objects = new Dictionary<string, System.Object>();

        public void RegisterObject(string uuid, System.Object obj)
        {
            objects.Add(uuid, obj);
        }

        public System.Object GetObject(string uuid)
        {
            return objects[uuid];
        }
    }


    [System.Serializable]
    public class Experiment : PTVR.Core.Experimental.IDataProvider
    {
        // Serialized data comming form the JSON
        public string uuid;

        public string onTrialStartEventUUID;
        public string onTrialEndEventUUID;
        public string onBlockStartEventUUID;
        public string onBlockEndEventUUID;
        public string onExperimentStartEventUUID;
        public string onExperimentEndEventUUID;

        public Event2 onTrialStartEvent;
        public Event2 onTrialEndEvent;
        public Event2 onBlockStartEvent;
        public Event2 onBlockEndEvent;
        public Event2 onExperimentStartEvent;
        public Event2 onExperimentEndEvent;

        public ExperimentalProcedure experimentalProcedure;

        // public TrialEventManager trialEventManager;
        // public BlockEventManager blockEventManager;
        // public ExperimentEventManager experimentEventManager;

        private bool trialMainCoroutineCompleted = false;

        public bool HasFinished
        {
            get {
                return experimentalProcedure.HasFinished;
            }

            private set { }
        }

        public bool BlocksHaveFinished
        {
            get {
                return experimentalProcedure.BlocksHaveFinished;
            }

            private set { }
        }

        public bool TrialsInBlockHaveFinished
        {
            get {
                return experimentalProcedure.TrialsInBlockHaveFinished;
            }

            private set { }
        }

        public bool HasBlockDesign
        {
            get {
                return experimentalProcedure.HasBlockDesign;
            }

            private set { }
        }

        public void Initialize()
        {
            PTVRObjectsManager.Instance.RegisterObject(uuid, this);

            // get references to PTVR.Events
            onTrialStartEvent = PTVR.Core.Experimental.EventManager.Instance.GetReferenceToEvent(onTrialStartEventUUID);
            onTrialEndEvent = PTVR.Core.Experimental.EventManager.Instance.GetReferenceToEvent(onTrialEndEventUUID);
            onBlockStartEvent = PTVR.Core.Experimental.EventManager.Instance.GetReferenceToEvent(onBlockStartEventUUID);
            onBlockEndEvent = PTVR.Core.Experimental.EventManager.Instance.GetReferenceToEvent(onBlockEndEventUUID);
            onExperimentStartEvent = PTVR.Core.Experimental.EventManager.Instance.GetReferenceToEvent(onExperimentStartEventUUID);
            onExperimentEndEvent = PTVR.Core.Experimental.EventManager.Instance.GetReferenceToEvent(onExperimentEndEventUUID);

            experimentalProcedure.Initialize();
        }

        public void EndTrialMainCoroutine()
        {
            trialMainCoroutineCompleted = true;
        }

        public object GetData(string propertyName)
        {
            return experimentalProcedure.GetValue(propertyName);
        }

        public IEnumerator TrialMainCoroutine()
        {
            while (!trialMainCoroutineCompleted)
            {
                yield return null;
            }
        }

        public void StartTrial()
        {
            trialMainCoroutineCompleted = false;
        }

        public void EndTrial()
        {
            experimentalProcedure.EndTrial();
        }

        public void StartBlock()
        {
            experimentalProcedure.StartBlock();
        }


        public void EndBlock()
        {
            experimentalProcedure.EndBlock();
        }
    }




}

