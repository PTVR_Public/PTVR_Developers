using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PTVR.Stimuli.Objects;
using System;
public enum ContingentType
{
    DEFAULT ,
    HEADSET ,
    GAZE ,
    LEFT_EYE ,
    RIGHT_EYE ,
    LEFT_HAND ,
    RIGHT_HAND 

}

public enum MaskedEye
{
    DEFAULT ,
    LEFT_EYE ,
    RIGHT_EYE ,
    NONE 
}

namespace PTVR
{
    public class PointingCursor : VirtualPoint2D
    {
	public string name = "default"; // not yet used. Form possible multiple pointers
	public bool isActive = true;
	public string type = "straight"; //only straight for now
    public PTVR.Stimuli.Color color ;
	public string sprite = "default";
    public float spriteAngularRadiusY = 0f;
    public float spriteAngularRadiusX = 0f;
    public float maxDistance = 500f;
    public float distance_if_empty_pointing_cone = 0.5f;
    public bool isProjected = true;
    public bool distanceRescale;
    public int smoothingStep = 0;
    public float alphaSmooth = 0.0f;
    public bool isShowDistanceInM = false;
    public bool isRenderedOverObjects = false;
    public PTVR.Stimuli.Color laserColor;
    public float laserWidth = 0.01f;
    public ContingentType contingency_type;
    public MaskedEye maskedEye; //data for the unity side
    public float widthCollision = 1.0f;
    public string spritePath = "";
    public string pointing_method = "";
    public bool display_pointing_method = false;
    public bool is_distant_constant = false;

    }



    public class HeadPovContingent : PointingCursor
    {
    }

    public class GazeContingent : PointingCursor
    {
        
    }

    public class HandContingent : PointingCursor
    {
      
    }

}
