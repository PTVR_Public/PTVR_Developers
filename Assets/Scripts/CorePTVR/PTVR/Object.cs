using PTVR.Stimuli.Objects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder;
using UnityEngine.UI;

namespace PTVR
{
    public class Object : MonoBehaviour
    {
        /// <summary>
        /// Linking between a Unity GameObject and PTVR
        /// </summary>
        /// 

        // internal reference kept for backward compatibility
        private VisualObject internalReference;
        
        private List<ObjectProperty> properties = new List<ObjectProperty>();

        // public void SetProperty(string name, )

        public T AddProperty<T>() where T : ObjectProperty, new ()
        {
            // T property = new T(); // T() doens't accept any parameter here
            // next line was used when we had to pass data to the constructor
            // T property = System.Activator.CreateInstance(typeof(T), this) as T; 

            T property = new T(); // T() doens't accept any parameter here
            property.Initialize(this);
            properties.Add(property);
            return property;
        }

        public void SetPropertyValue<T>(System.Object value) where T : ObjectProperty, new()
        {
            foreach(ObjectProperty property in properties)
            {
                if (property is T)
                {
                    property.SetValue(value);
                    return;
                }
            }

            // property not found -> add it
            // T propertyToAdd = System.Activator.CreateInstance(typeof(T), this) as T;
            T propertyToAdd = new T(); // T() doens't accept any parameter here
            propertyToAdd.Initialize(this);
            properties.Add(propertyToAdd);
            propertyToAdd.SetValue(value);
        }
    }

    public enum ObjectPropertyType
    {
        Position,
        Local_Position,
        Size,
        Color,
        Visibility,
        Text
    }

    [SerializeField]
    public class ObjectPropertyValue
    {


    }

    public abstract class ObjectProperty
    {
        protected Object ptvrObject;

        // we need some late initialization for properties
        // as these are going to be fully set up only when we have instantiated the actual object
        // on which this properties are going to be applied
        public virtual void Initialize(Object ptvrObject)
        {
            this.ptvrObject = ptvrObject;
        }

        public virtual void SetValue(System.Object value)
        {

        }
    }

    // this should be the equivalent of Unity's side components
    // but simplified to expose only the basic settings to the python API
    // public class Material : PTVRObjectProperty
    // {
    //     public Material(PTVRObject ptvrObject) : base(ptvrObject) { }
    // }

    // public class Transform : PTVRObjectProperty
    // {
    //     public Transform(PTVRObject ptvrObject) : base(ptvrObject) { }
    // }

    public class PositionProperty : ObjectProperty
    {
        /// <summary>
        /// Set the world space position
        /// </summary>
        private Transform transform;
        public override void Initialize(Object ptvrObject)
        {
            base.Initialize(ptvrObject);
            transform = ptvrObject.transform;
        }

        public override void SetValue(System.Object value)
        {
            transform.position = (Vector3)value;
        }
    }

    public class SizeProperty : ObjectProperty
    {
        /// <summary>
        /// Set the world space position
        /// </summary>
        private Transform transform;
        public override void Initialize(Object ptvrObject)
        {
            base.Initialize(ptvrObject);
            transform = ptvrObject.transform;
        }

        public override void SetValue(System.Object value)
        {
            float size = (float) value;
            Vector3 newSize = new Vector3 (size, size, size);
            transform.localScale = newSize;
        }
    }

    public class LocalPositionProperty : ObjectProperty
    {
        /// <summary>
        /// Set the world space position
        /// </summary>
        private Transform transform;
        public override void Initialize(Object ptvrObject)
        {
            base.Initialize(ptvrObject);
            transform = ptvrObject.transform;
        }

        public override void SetValue(System.Object value)
        {
            transform.localPosition = (Vector3)value;
        }
    }

    public class VisibilityProperty : ObjectProperty
    {
        /// <summary>
        /// Set the world space position
        /// </summary>
        private GameObject go;

        // as a simplification for researchers,
        // visibility should be a shortcut for everything:
        // - meshRendering, meshCollision, etc -> it should be the whole gameObject
        // for now, it is just the renderer because of the shitty prefab controller
        private MeshRenderer meshRenderer;
        public override void Initialize(Object ptvrObject)
        {
            base.Initialize(ptvrObject);
            go = ptvrObject.gameObject;
            meshRenderer = go.GetComponentInChildren<MeshRenderer>();
        }

        public override void SetValue(System.Object value)
        {
            // go.SetActive((bool)value);
            meshRenderer.enabled = (bool)value;
        }
    }

    public class TextProperty : ObjectProperty
    {
        /// <summary>
        /// Set the world space position
        /// </summary>
        private TMPro.TextMeshPro? textMeshPro;
        // private TMPro.TMP_Text? textMeshPro;
        private float fontSize;
        // we need to get rid of as soon as possible of this shitty prefabController
        private PrefabController prefabController;
        public override void Initialize(Object ptvrObject)
        {
            base.Initialize(ptvrObject);
            textMeshPro = ptvrObject.gameObject.GetComponentInChildren<TMPro.TextMeshPro>();
            fontSize = textMeshPro.fontSize;
            prefabController = ptvrObject.gameObject.GetComponentInChildren<PrefabController>();
            // textMeshPro = ptvrObject.gameObject.GetComponent<TMPro.TMP_Text>();
        }

        public override void SetValue(System.Object value)
        {
            // if (textMeshPro) textMeshPro.text = (string)value;
            // if (textMeshPro) prefabController.ChangeText((string)value, fontSize);
            if (textMeshPro) prefabController.ChangeText(value.ToString(), fontSize);
        }
    }

    public class ColorProperty : ObjectProperty
    {
        private TMPro.TextMeshPro? textMeshPro;
        private TMPro.TextMeshProUGUI? textMeshProUGUI;
        private RawImage? rawImage;
        private Image? image;
        private LineRenderer? lineRenderer;
        private Renderer? renderer;
        private Material[] materials;

        // we need to get rid of as soon as possible of this shitty prefabController
        private PrefabController prefabController;
        private PTVR.Stimuli.Color color;
        public override void Initialize(Object ptvrObject)
        {
            base.Initialize(ptvrObject);

            // for now we are going to find in all children nodes but
            // we should also take into account parent relationships
            prefabController = ptvrObject.gameObject.GetComponentInChildren<PrefabController>();

            // textMeshPro = ptvrObject.gameObject.GetComponentInChildren<TMPro.TextMeshPro>();
            // textMeshProUGUI = ptvrObject.gameObject.GetComponentInChildren<TMPro.TextMeshProUGUI>();
            // rawImage = ptvrObject.gameObject.GetComponentInChildren<RawImage>();
            // image = ptvrObject.gameObject.GetComponentInChildren<Image>();
            // lineRenderer = ptvrObject.gameObject.GetComponentInChildren<LineRenderer>();
            // renderer = ptvrObject.gameObject.GetComponentInChildren<Renderer>();
            // if (renderer)
            // {
            //     materials = renderer.materials;
            // }
        }

        public override void SetValue(System.Object value)
        {
            //handle all the potential components that might need to update their colors
            // if (textMeshPro) textMeshPro.color = (Color)value;
            // if (textMeshProUGUI) textMeshProUGUI.color = (Color)value;
            // if (rawImage) rawImage.color = (Color)value;
            // if (image) image.color = (Color)value;
            // if (lineRenderer)
            // {
            //     lineRenderer.startColor = (Color)value;
            //     lineRenderer.endColor = (Color)value;
            // }
            // foreach (var mat in materials)
            // {
            //     mat.color = (Color)value;
            //     mat.EnableKeyword("_EMISSION");
            //     mat.SetColor("_EmissionColor", (Color)value);
            // }

            // STUPID CODE BECAUSE OF THE SHITTY PREFAB CONTROLLER
            Color new_color = (Color)value;
            PTVR.Stimuli.RGBColor ptvrColor = new PTVR.Stimuli.RGBColor();
            ptvrColor.r = new_color.r;
            ptvrColor.g = new_color.g;
            ptvrColor.b = new_color.b;
            ptvrColor.a = new_color.a;
            prefabController.actualColor = ptvrColor;
        }
    }

}

