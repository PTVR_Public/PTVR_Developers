using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

namespace PTVR.Core.Experimental
{
    public class CallbackManager : MonoBehaviour
    {
        // public PTVR.Data.CallbackManagerData callbackManagerData;
        public Dictionary<string, Callback> callbacks;// = new Dictionary<string, Callback>();

        public static CallbackManager Instance { get; private set; }

        public bool Loaded { get; private set; } = false;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            } else
            {
                Instance = this;
            }
        }

        public Callback GetReferenceToCallback(string uuid)
        {
            return callbacks[uuid];
        }

        public void LoadFromJson(string jsonData)
        {
            print("loading new event system json");
            var settings = new JsonSerializerSettings();
            // {
            //     PreserveReferencesHandling = PreserveReferencesHandling.All
            // };
            // settings.Converters.Add(new PTVR.Converters.EventConverter2());
            settings.Converters.Add(new PTVR.Converters.Core.Experimental.CallbackConverter2());
            settings.Converters.Add(new PTVR.Converters.ObjectPropertyValueConverter());
            // callbackManagerData = JsonConvert.DeserializeObject<PTVR.Data.CallbackManagerData>(jsonData, settings);
            callbacks = JsonConvert.DeserializeObject<Dictionary<string, Callback>>(jsonData, settings);
            // callbackManagerData.Initialize();

            Loaded = true;

            print("hello world");
        }
    }

}

