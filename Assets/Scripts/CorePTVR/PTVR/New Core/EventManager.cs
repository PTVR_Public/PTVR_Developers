using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Newtonsoft.Json;
using PTVR.Data;

namespace PTVR.Core.Experimental
{
    public class EventManager : MonoBehaviour
    {
        public static EventManager Instance { get; private set; }

        public Dictionary<string, Event2> events;// = new Dictionary<string, Callback>();

        public bool Loaded { get; private set; } = false;
        public bool Initialized { get; private set; } = false;

        #region unity events
        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            } else
            {
                Instance = this;
            }
        }

        private void Update()
        {
            if (!Initialized) return;
            CheckInputEvents();
        }
        #endregion unity events

        #region public API
        public void LoadFromJson(string jsonData)
        {
            print("loading new event system json");
            var settings = new JsonSerializerSettings();
            // {
            //     PreserveReferencesHandling = PreserveReferencesHandling.All
            // };
            settings.Converters.Add(new PTVR.Converters.EventConverter2());
            // settings.Converters.Add(new PTVR.Converters.CallbackConverter());
            settings.Converters.Add(new PTVR.Converters.ObjectPropertyValueConverter());
            // eventManagerData = JsonConvert.DeserializeObject<PTVR.Data.EventManagerData>(jsonData, settings);
            events = JsonConvert.DeserializeObject<Dictionary<string, Event2>>(jsonData, settings);
            Loaded = true;
        }

        public void Initialize()
        {
            foreach (var evt in events)
            {
                evt.Value.Initialize();
            }
            Initialized = true;
        }

        public Event2 GetReferenceToEvent(string uuid)
        {
            // for backward compatibility,
            // in the case the event manager, has not been initialized, return null
            if (!Loaded) return null;

            return events[uuid];
        }
        #endregion public API

        #region private methods
        private void CheckInputEvents()
        {
            foreach (var evt in  events.Values)
            {
                if (evt is KeyEvent)
                {
                    KeyEvent keyEvent = (KeyEvent)evt;
                    bool wasPressed = UnityEngine.InputSystem.Keyboard.current[keyEvent.key].wasPressedThisFrame;
                    bool wasRelease = UnityEngine.InputSystem.Keyboard.current[keyEvent.key].wasReleasedThisFrame;
                    if (keyEvent.mode == KeyEvent.Mode.Press && wasPressed)
                    {
                        keyEvent.Trigger();
                    } else if (keyEvent.mode == KeyEvent.Mode.Release && wasRelease)
                    {
                        keyEvent.Trigger();
                    }
                }
            }
        }
        #endregion private methods
    }
}

