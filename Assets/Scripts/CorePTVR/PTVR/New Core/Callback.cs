using Newtonsoft.Json;
using PTVR.Core.Experimental;
using PTVR.Data.Event;
using PTVR.ExperimentalDesign;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.VirtualTexturing;

namespace PTVR.Core.Experimental
{
    public interface IDataProvider
    {
        System.Object GetData(string propertyName);
    }

    [System.Serializable]
    public class RefToDataProvider
    {
        public string UUID;

        [JsonIgnore]
        public IGetCallback provider;
    }


    [System.Serializable]
    public class Callback
    {
        public Event2 onExecutedEvent;

        protected bool initialized = false;
        public virtual void Initialize()
        {
            onExecutedEvent.Initialize();
            initialized = true;
        }

        public virtual void Execute(Event2 ptvrEvent)
        {

        }
    }

    public interface IGetCallback
    {
        System.Object GetData();
    }

    [System.Serializable]
    public class StartExperimentalProcedureCallback : Callback
    {
        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Execute(Event2 ptvrEvent)
        {
            ExperimentManager.Instance.StartExperiment();
            onExecutedEvent.Trigger();
        }
    }

    [System.Serializable]
    public class FinishCurrentTrialCallback : Callback
    {
        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Execute(Event2 ptvrEvent)
        {
            ExperimentManager.Instance.FinishMainTrialCoroutine();
            onExecutedEvent.Trigger();
        }
    }

    [System.Serializable]
    public class GetPropertyValueCallback : Callback, IGetCallback
    {
        public string propertyName;
        public string dataProviderUUID;
        private IDataProvider dataProvider;

        public override void Initialize()
        {
            base.Initialize();
            dataProvider = (IDataProvider) PTVRObjectsManager.Instance.GetObject(dataProviderUUID);
        }

        public override void Execute(Event2 ptvrEvent)
        {
            dataProvider.GetData(propertyName);
            onExecutedEvent.Trigger();
        }

        public System.Object GetData()
        {
            return dataProvider.GetData(propertyName);
        }
    }

    [System.Serializable]
    public class GetFormattedStringCallback : Callback, IGetCallback
    {
        public string formatSpecifier;
        public System.Object parameters;

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Execute(Event2 ptvrEvent)
        {
            onExecutedEvent.Trigger();
        }

        public System.Object GetData()
        {
            return string.Format(formatSpecifier, parameters);
        }
    }

    [System.Serializable]
    public class SetObjectPropertyCallback : Callback
    {
        public long ptvrObjectId;
        public ObjectPropertyType objectPropertyType;
        public System.Object value;

        private GetPropertyValueCallback provider;

        private ObjectProperty objectProperty;

        private GameObject go = null;
        private PTVR.Object ptvrObject;
        public override void Initialize()
        {
            onExecutedEvent.Initialize();

            go = The3DWorldManager.instance.GameObjectCol[ptvrObjectId];
            PTVR.Object ptvrObjectComponent = go.GetComponent<PTVR.Object>();
            if (ptvrObjectComponent == null)
            {
                ptvrObject = go.AddComponent<PTVR.Object>();

            } else
            {
                ptvrObject = ptvrObjectComponent;
            }

            // disable stupid PrefabController shit that's implemented very badly
            // go.GetComponent<PrefabController>().enabled = false;

            switch(objectPropertyType)
            {
                case ObjectPropertyType.Position:
                    objectProperty = new PositionProperty(); break;
                case ObjectPropertyType.Local_Position:
                    objectProperty = new LocalPositionProperty(); break;
                case ObjectPropertyType.Size:
                    objectProperty = new SizeProperty(); break;
                case ObjectPropertyType.Color:
                    objectProperty = new ColorProperty(); break;
                case ObjectPropertyType.Visibility:
                    objectProperty = new VisibilityProperty(); break;
                case ObjectPropertyType.Text:
                    objectProperty = new TextProperty(); break;
                default: Debug.LogError("Object Property Type not implemented yey"); break;
            }

            objectProperty.Initialize(ptvrObject);

            if (value is RefToDataProvider)
            {
                RefToDataProvider refToDataProvider = (RefToDataProvider)value;
                provider = CallbackManager.Instance.GetReferenceToCallback(refToDataProvider.UUID) as GetPropertyValueCallback;
                provider.Initialize();
            }

            initialized = true;
        }

        public override void Execute(Event2 ptvrEvent)
        {
            // using reflection (not needed anymore)
            // System.Type type = objectProperty.GetType(); 
            // System.Reflection.MethodInfo method = ptvrObject.GetType()
            //     .GetMethod("SetPropertyValue").MakeGenericMethod(type);
            // method.Invoke(ptvrObject, new System.Object[] {value});

            if (value is RefToDataProvider)
            //if (value is IGetCallback)
            {
                // IGetCallback callback = (IGetCallback) value;
                objectProperty.SetValue(provider.GetData());
            } else
            {
                objectProperty.SetValue(value);
            }

            onExecutedEvent.Trigger();
        }
    }

}
