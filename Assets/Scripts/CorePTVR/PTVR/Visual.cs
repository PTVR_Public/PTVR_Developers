﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PTVR.Stimuli.Objects;
namespace PTVR //Toolbox namespace
{
	[System.Serializable]
    public class The3DWorld
    {
	  public string name; //Name of the experiment

	  public string details3DWorld;
	  public List<long> id_scene;
	  public string pathResult;
	  public string user;
      public string created; //date-time when created

	  public bool outputHead;

	  public bool outputGaze;

   	  public int headSaveSamplingPeriod;

	  public bool leftBlind;
	  public bool rightBlind;

	  public bool hasOperator;

      public Dictionary<long, PTVR.Stimuli.Objects.Scene> scenes; //List of scenes in the experiment
	  public CalculatorManager calculatorManager;
	  public List<string> headersMeta = null;
	  public Dictionary<string,string> stringMeta = null;
	  public Dictionary<string,float>  floatMeta = null;

	public List<string> GetHeadersMeta()
	{
	    if (headersMeta == null)
	    {
		headersMeta = new List<string>();
		foreach( PTVR.Stimuli.Objects.Scene sc in scenes.Values)
		{
		    List<string> a_list = sc.GetMetaHeaders();
                    foreach (string ahead in a_list)
                    {
                        if (!headersMeta.Contains(ahead))
                            headersMeta.Add(ahead);
                    }
		}
	    }
	    return headersMeta;
	}

    }
}
