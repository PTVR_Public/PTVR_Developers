﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using System.Globalization;

namespace PTVR //Toolbox namespace
{
    public class Calculator : Stimuli.Objects.Frame
    {
        public string calculation = "";
        public List<long> idsForCalculation;
        public List<float> points;
        string resultFileData;
        bool isInvariantCulture;

        //artefact. TODO: clean the frame role
        public override Vector3 ToUnityPos(GameObject go = null) { return Vector3.zero; }

    }

    public class CalculatorManager : Stimuli.Objects.MetaData
    {
        public Dictionary<long, Calculator> calculatorList;
        Calculator calculatorData;
        string resultFileData;
        bool isInvariantCulture;


        public float ResultFloat(long calcId)
        {

            if(!calculatorList.TryGetValue(calcId, out calculatorData))
            {
                Debug.LogError("can't find calculator with this id.");
                return float.MaxValue;
            }
            switch (calculatorData.calculation)
            {
                case "AngleAtOriginBetweenTwoVisualObjects":
                    return AngleAtOriginBetweenTwoVisualObjects();
                    break;
                case "RadialDistanceFromOrigin":
                    return RadialDistanceFromOrigin();
                    break;
                case "AngleAtHeadPOVBetweenTwoVisualObjects":
                    return AngleAtHeadPOVBetweenTwoVisualObjects();
                    break;
                case "RadialDistanceFromHeadPOV":
                    return RadialDistanceFromHeadPOV();
                    break;
                case "DistanceBetweenTwoVisualObjects":
                    return DistanceBetweenTwoVisualObjects();
                    break;
                case "AngleAtApexBetweenTwoVisualObjects":
                    return AngleAtApexBetweenTwoVisualObjects();
                    break;
                case "ElaspedTimeCurrentScene":
                    return ElaspedTimeCurrentScene();
                case "Data":
                    /*
                    if (dataType == "string")
                        return StringData(resultFileData);
                    else*/
                        return Data(resultFileData, isInvariantCulture);
                default:
                    Debug.LogError("Calculator: unknown operation : " + calculatorData.calculation);
                    break;
            }
            return 0;
        }

        public Vector3 ResultVector3(long calcId)
        {
            Vector3 toReturn = Vector3.zero;
            if (!calculatorList.TryGetValue(calcId, out calculatorData))
            {
                Debug.LogError("can't find calculator with this id.");
                return Vector3.positiveInfinity;
            }

            bool positiveOperator = true;
            string textToProcess = calculatorData.calculation;
            while (true)
            {
                int indexofMarkerFirst = calculatorData.calculation.IndexOf("%Pos");

                if (indexofMarkerFirst == -1)
                    return toReturn;

                textToProcess = textToProcess.Substring(indexofMarkerFirst + 1);
                string potentialId = Regex.Match(textToProcess, @"\d+").Value;

                long number;
                bool success = long.TryParse(potentialId, out number);

                if (!success)
                    return toReturn;

                if (positiveOperator)
                    toReturn = toReturn + SetVisualSettings.instance.GameObjectCol[number].transform.position;
                else
                    toReturn = toReturn - SetVisualSettings.instance.GameObjectCol[number].transform.position;

                positiveOperator = true;
                int indexofOperator = textToProcess.IndexOf("+");
                if (indexofOperator == -1)
                {
                    indexofOperator = textToProcess.IndexOf("-");
                    if (indexofOperator == -1)
                    {
                        return toReturn;
                    }
                    positiveOperator = false;
                }
                string theOperator = textToProcess.Substring(indexofOperator, 1);

                textToProcess = textToProcess.Substring(indexofOperator);

            }
            return toReturn;
        }

        float AngleAtOriginBetweenTwoVisualObjects()
        {
            Vector3 origin = SetVisualSettings.instance.origin;
            Vector3 point1 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[0]].transform.position;
            Vector3 point2 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[1]].transform.position;

            point1 = point1 - origin;
            point2 = point2 - origin;

            return Vector3.Angle(point1, point2);
        }

        float RadialDistanceFromOrigin()
        {
            Vector3 origin = SetVisualSettings.instance.origin;
            //The point setup in json
            Vector3 point = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[0]].transform.position;
            //Vector3.Distance(origin,point) = (point  - origin).magnitude
            return (point - origin).magnitude;
        }
        float AngleAtHeadPOVBetweenTwoVisualObjects()
        {

            Vector3 headPOV = Camera.main.transform.position;
            //The point1 setup in json
            Vector3 point1 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[0]].transform.position;
            //The point1 setup in json
            Vector3 point2 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[1]].transform.position;

            point1 = point1 - headPOV;
            point2 = point2 - headPOV;

            return Vector3.Angle(point1, point2);
        }

        float RadialDistanceFromHeadPOV()
        {
            Vector3 headPOV = Camera.main.transform.position;
            //The point setup in json
            //Debug.Log("CALCULATOR DATA : " + calculatorData.idsForCalculation[0]);
            Vector3 point = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[0]].transform.position;
            
            //Vector3.Distance(headPOV,point ) = (point  - headPOV).magnitude
            return (point - headPOV).magnitude;
        }

        float DistanceBetweenTwoVisualObjects()
        {
            //The point1 setup in json
            Vector3 point1 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[0]].transform.position;
            //The point2 setup in json
            Vector3 point2 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[1]].transform.position;
            //Vector3.Distance(point1 ,point2) = (point1 - point2).magnitude
            return (point1 - point2).magnitude;
        }


        float AngleAtApexBetweenTwoVisualObjects()
        {
            //The apex setup in json
            Vector3 vVertex = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[0]].transform.position;
            //The point1 setup in json
            Vector3 point1 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[1]].transform.position;
            //The point2 setup in json
            Vector3 point2 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[2]].transform.position;


            point1 = point1 - vVertex;
            point2 = point2 - vVertex;


            return Vector3.Angle(point1, point2);
        }

        float ElaspedTimeCurrentScene()
        {
            return (float)(The3DWorldManager.instance.elapsedTimeCurrentScene / 1000);
        }
        float Data(string resultFileData,bool isInvariantCulture)
        {
            
            if (isInvariantCulture)
            {
                return float.Parse(DataBank.Instance.GetData(resultFileData), CultureInfo.InvariantCulture);
            }
            else
            {
                return float.Parse(DataBank.Instance.GetData(resultFileData));
            }
        }
        /*
        string StringData(string resultFileData)
        {
            return DataBank.Instance.GetData(resultFileData);
        }*/

    }

}
    
