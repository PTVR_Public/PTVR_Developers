﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTVR
{
    namespace Stimuli
    {
        public  class Texture
        {
            public string shader;
            public PTVR.Stimuli.Color color;
            public string img;

            //public abstract UnityEngine.Texture ToUnityTexture();
        }
    }
}
