using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTVR
{
    namespace Stimuli
    {
        namespace Objects
        {
            public class UIObject : ColoratedObject
            {
                
            }

            public class TextUI : UIObject
            {
                public string text, fontName, alignHorizontal, alignVertical;
                public float fontSize;
                public bool isBold, isItalic;
                public bool isVerifySentence;
                public bool isTypographyDemo, isMnreadFormat, isWrapping;

                public float visualAngleOfCenteredXHeight; // size of the text in term of degrees
                public float textboxWidth;
                public float textboxHeight;

               /*
                public TextUI(string _type)
                {
                    type = _type;
                }
               */
              //  public string type { set; get; }
            }
            public class ImageUI : UIObject
            {
                public float width;
                public float height;
                public string imageFile;
                public string imageType;
            }
            public class ButtonUI : UIObject,Button
            {
                public string nameButton { set; get; }
                public float width;
                public float height;
                public List<UIObject> UIObjects;
            }

            public class GraphUI : UIObject
            {
                public List<float> size;
                public float minimumYValue;
                public float maximumYValue;
                public float minimumXValue;
                public float maximumXValue;
                public float graduationsXRange;
                public float graduationsYRange;
                public string xAxisName;
                public string yAxisName;
                public bool grid;

                public bool secondXAxis;
                public bool secondYAxis;
                public string xUpAxisName;
                public string YRightAxisName;
                public List<float> XUpLabels;
                public List<float> YRightLabels;
                public bool xIsInvariantCulture;
                public bool yIsInvariantCulture;

                public string xValuesName;
                public string yValuesName;
                public string operationX;
                public string operationY;

                public PTVR.Stimuli.Color dotColor;
                public PTVR.Stimuli.Color dotConnectionColor;
                public PTVR.Stimuli.Color frameColor;
                public PTVR.Stimuli.Color textColor;
                public PTVR.Stimuli.Color gridColor;
                public float fontSizeInPoints;

            }
        }
    }
}