using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PTVR
{
    namespace Stimuli
    {
        namespace Objects
        {
            public class Scene : MetaData
            {
                public long id; //A (not-neseccarily) unique identifier for the scene
                public int trial;
                public string type;

                public bool isLeftControllerVisibility = true;
                public bool isRightControllerVisibility = true;
                public bool isLeftHandVisibility = true;
                public bool isRightHandVisibility = true;

                //  public PTVR.Data.Input.Display display; //Holds information on termination criteria and user inputs possible
                public PTVR.Stimuli.Color leftBackgroundColor;
                public PTVR.Stimuli.Color rightBackgroundColor;
                public string skybox;
                public float global_lights_intensity;
                public List<PointingCursor> pointingCursors;
                public VirtualPoint origin;
                public bool side_view_on_pc = false;
            }

            public class FixationScene : Scene
            {
                public string text;
            }

            public class VisualScene : Scene
            {
                public List<VisualObject> objects; //Objects in the visual scene
                public List<UIObject> uiObjects;
                public long current_coordinate_system_id;
                //public List<PointedAtEvent> pointedAtEvent = new List<PointedAtEvent>(); //TODO add to Input
                public List<PTVR.Interaction> interaction = new List<PTVR.Interaction>();
                //InputEvent
                // public PTVR.Data.Event2 onLoadedEventUUID;
                public string onLoadedEventUUID;

                [JsonIgnore]
                public PTVR.Core.Experimental.Event2 onLoadedEvent;
            }

            public class ResponseScene : Scene  //User response screen
            {
                public string text;
            }
        }
    }

}