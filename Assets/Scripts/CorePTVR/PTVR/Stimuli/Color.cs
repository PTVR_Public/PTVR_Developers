﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace PTVR
{
    namespace Stimuli
    {
        [System.Serializable]
        public abstract class Color
        {
            public string type; //The type of Color specification 

            public abstract UnityEngine.Color ToUnityColor();
        }

        public class RGBColor : Color //Similar to the default Unity encoding
        {
            public float r, g, b, a;

            public override UnityEngine.Color ToUnityColor()
            {
                return new UnityEngine.Color(r, g, b, a);
            }
        }

        public class HexColor : Color
        {
            public string colorHex; //hexadecimal value
            public float a;
            public override UnityEngine.Color ToUnityColor()
            {
                string colorString = colorHex.ToString();
                colorString = "#" + colorString; //Convert to string and add a '#' to indicate hex value
                UnityEngine.Color col;
                ColorUtility.TryParseHtmlString(colorString, out col);
                col.a = a;
                return col;
            }
        }

        public class RGB255Color : Color
        {
            public int r,g,b; //values 0 to 255
            public float a;

            public override UnityEngine.Color ToUnityColor()
            {
                return new UnityEngine.Color(r/255f, g/255f, b/255f, a);
            }
        }

		public class DynamicColor : Color
		{
			public Dictionary<float, Color> colorCollection = new Dictionary<float, Color> ();
			private int _counter { get; set;} = 0;
			public int Counter {get{return _counter;}}
			private Dictionary<float, Color>.Enumerator _enumerator;
            public override UnityEngine.Color ToUnityColor()
            {
                float key = colorCollection.Keys.First();
                return new UnityEngine.Color(colorCollection[key].ToUnityColor().r * 255f, colorCollection[key].ToUnityColor().g * 255f, colorCollection[key].ToUnityColor().b *  255f, colorCollection[key].ToUnityColor().a);
            }
            public void Restart ()
			{
				_counter = 0;
			}

            public void Setup()
            {
                float key = colorCollection.Keys.First();
                //  return new UnityEngine.Color(colorCollection[key].ToUnityColor().r / 255f, colorCollection[key].ToUnityColor().g / 255f, colorCollection[key].ToUnityColor().b / 255f, colorCollection[key].ToUnityColor().a);
                Debug.Log(colorCollection[key].ToUnityColor().r );
            }
	        public KeyValuePair<float,Color> GetNextTimeColor ()
	    	{
			    if (colorCollection.Count == 0)
                {
				KeyValuePair<float,Color> toreturn = new KeyValuePair<float, Color>(-1, null);
				return toreturn;
			    }

			    if (_counter == 0)
				_enumerator = colorCollection.GetEnumerator ();
				
			    if (!_enumerator.MoveNext ())
                {
				_enumerator = colorCollection.GetEnumerator ();
				_enumerator.MoveNext ();
				_counter = 0;
		        }

				_counter++;
				return _enumerator.Current;
	        }

		}
    }
}
