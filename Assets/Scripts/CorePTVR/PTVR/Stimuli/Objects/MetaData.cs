using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTVR
{
    namespace Stimuli
    {
        namespace Objects
        {

            public class MetaData
            {
                public Dictionary<string, string> fillInResultsFileColumnString = new Dictionary<string, string>();
    
                public List<string> GetMetaHeaders()
                {
                    List<string> toReturn = new List<string>();
                    foreach (string key in fillInResultsFileColumnString.Keys)
                        toReturn.Add(key);

                    return toReturn;
                }

                public List<string> GetMetaStrings()
                {
                    List<string> toReturn = new List<string>();
                    foreach (string val in fillInResultsFileColumnString.Values)
                        toReturn.Add(val);

                    return toReturn;
                }

                public List<string> GetAllMetaAsString ()
                {
                    List<string> toReturn = new List<string>();
                    foreach (string val in fillInResultsFileColumnString.Values)
                        toReturn.Add(val);
                    return toReturn;
                }

                public List<string> GetAllTheseMeta(List<string> headers)
                {
                    List<string> toReturn = new List<string>();
                    foreach (string key in headers)
                    {
                        if (fillInResultsFileColumnString.ContainsKey(key))
                            toReturn.Add(fillInResultsFileColumnString[key]);
                        else
                            toReturn.Add("");
                    }
                    return toReturn;

                }
            }
        }
    }
}