using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTVR
{
    namespace Stimuli
    {
        namespace Objects
        {
            public interface Button 
            {
                public string nameButton { set; get; }
            }
        }
    }
}