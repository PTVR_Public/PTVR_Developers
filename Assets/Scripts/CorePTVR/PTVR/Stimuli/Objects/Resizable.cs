using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace PTVR
{
    namespace Stimuli
    {
        namespace Objects
        {
            public class Resizable1D : VirtualPoint
            {
                public float sizeX;
                public float sizeX_po;
                public float orientationDeg = 0;
                public float angularSize = 0;

            }
            public class Resizable2D : Resizable1D
            {
                public float sizeY;
                public float sizeY_po;
                public float Xdeg = 0;
                public float Ydeg = 0;
                public bool approximative = false;
                public void CalculateAngularSize ()
                {
                    //first approx calculation. Deprecated
                    Vector3 aPos = ToUnityPos();
                    if ((Xdeg == 0 && Ydeg == 0) || aPos.magnitude == 0)
                        return;
                    sizeX = (float)Math.Tan(Xdeg) * aPos.magnitude;
                    sizeY = (float)Math.Tan(Ydeg) * aPos.magnitude;

                }
                public Vector2 ToUnityScale()
                {
                    CalculateAngularSize();
                    return new Vector2(sizeX, sizeY);
                }
                public Vector2 ToUnityScaleParentOnly()
                {
                    //TODO! CalculateAngularSize();
                    return new Vector2(sizeX_po, sizeY_po);
                }
            }

            public class Resizable : Resizable2D
            {
                public float sizeZ;
                public float sizeZ_po;
                public Vector3 ToUnityScale()
                {
                    CalculateAngularSize();
                    return new Vector3(sizeX, sizeY, sizeZ);
                }
                public Vector3 ToUnityScaleParentOnly()
                {
                    //TODO! CalculateAngularSize();
                    return new Vector3(sizeX_po, sizeY_po, sizeZ_po);
                }
            }
        }
    }
}