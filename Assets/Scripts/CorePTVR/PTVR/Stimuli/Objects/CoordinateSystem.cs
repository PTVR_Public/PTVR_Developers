using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace PTVR
{
    namespace Stimuli
    {
        namespace Objects
        {
            public class CoordinateSystemTransformation : Frame
            {
                public string type;
                public List<float> current_coordinate_system_transformation;
                public override Vector3 ToUnityPos(GameObject go = null)
                {
                    return Vector3.zero;
                }
            }
            public class CoordinateSystem : Frame
            {
                public List<CoordinateSystemTransformation> listTransformation;
                public override Vector3 ToUnityPos(GameObject go = null)
                {
                    return Vector3.zero;
                }
            }

            public class TransformationCoordinate : Frame
            {
                public string type;
                public List<float> transformationCoordinate;
                public override Vector3 ToUnityPos(GameObject go = null)
                {
                    return Vector3.zero;
                }
            }

            public class TransformationCoordinateSequence : Frame
            {
                public List<TransformationCoordinate> listTransformation;
                public override Vector3 ToUnityPos(GameObject go = null)
                {
                    return Vector3.zero;
                }
            }
        }
    }
}