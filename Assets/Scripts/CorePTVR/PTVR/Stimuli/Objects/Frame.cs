using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PTVR
{
    namespace Stimuli
    {
        namespace Objects
        {
            public abstract class Frame: MetaData
            {
                public long id;
                public long parentId;
                public bool isHeadPOVcontingent = false;
                public abstract Vector3 ToUnityPos(GameObject go = null);
            }
        }
    }
}