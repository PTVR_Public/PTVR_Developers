using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PTVR.Stimuli.Objects;

namespace PTVR //Toolbox namespace
{
   
            public class CameraOperator : MetaData
            {
                public long idCamera;
                public bool isFollowVRView;
                public float xView;
                public float yView;
                public PTVR.Stimuli.Color backgroundColor;
                public PTVR.Stimuli.Objects.VirtualPoint point;
            }
        
    
}