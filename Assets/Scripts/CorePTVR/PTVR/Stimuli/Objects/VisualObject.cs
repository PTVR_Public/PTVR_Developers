using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTVR
{
    namespace Stimuli
    {
        namespace Objects
        {
            public class VisualObject : Resizable
            {
                public List<float> scale;
                public string type; //The type of object, eg- cube, sphere, text_box etc.
                public bool isActive;
                public bool isVisible;
            }

            public class Calculator : VisualObject
            {
                public string calculation = "";
                public List<long> idsForCalculation;
                public List<float> points;
            }
            public class EmptyVisualObject : VisualObject
            {
                public bool isRelativeToHead;
            }

            public class CameraPTVR : VisualObject
            {
                public PTVR.Stimuli.Color backgroundColor;
                public float fov;
                public List <long> pastRendererToMaterial;
                public List<long> pastShotToMaterial;
            }
    

            public class ColoratedObject : VisualObject
            {
               
                public PTVR.Stimuli.Color color;
                public PTVR.Stimuli.Texture texture;
                public PTVR.Stimuli.DynamicColor dynamicColor = new DynamicColor();

                //ROI
                public bool isROI;

                //Visual FeedBack to add
                public PTVR.Stimuli.Color enterColor;
                public PTVR.Stimuli.Color interactColor;
                public float widthROI;

                //Sound FeedBack to add 
                public PTVR.Stimuli.Objects.AudioSource enterSound;
                public PTVR.Stimuli.Objects.AudioSource exitSound;
            }

            public class AudioSource : VisualObject
            {
                public float volume;
                public string audioFile;
                public bool isLoop;
                public bool isPlayDirectly;
            }

            public class Light : VisualObject
            {
                public float intensity;
                public float lightRange;
                public float spotAngle;


            }


            public class Line : ColoratedObject
            {
                public PTVR.Stimuli.Objects.VirtualPoint startPointObj, endPointObj;
                public List<float> startPoint, endPoint;
                public Color startColor, endColor;

            }

            public class Text : ColoratedObject
            {
                public string text, fontName, alignHorizontal, alignVertical;
                public float fontSize;
                public bool isBold, isItalic;
                public bool isVerifySentence;
                public bool isWrapping;

                public float visualAngleOfCenteredXHeight; // size of the text in term of degrees
                public float textBoxWidthCoefficient; // coefficient used in the mnread's format to calculate the width of the text's box
                public List<float> textBoxScale;



            }

            public class Gizmo : ColoratedObject
            {
                //scale is side
                public float width;
                public float length;
            }


            public class Capsule : ColoratedObject
            {
                //scale is side

            }

            public class Plane : ColoratedObject
            {
                //scale is side

            }

            public class Cube : ColoratedObject
            {
                //scale is side

            }

            public class Elevator : ColoratedObject
            {
                //scale is side
                public float speed;
                public float height;
                public bool comeback;

            }

            public class Forest : ColoratedObject
            {
                //scale is side

            }

            public class CustomObject : ColoratedObject
            {
                public string fileName;
                public string assetPath;
                public bool make_camera_child;
                public string pass_letter;
                public bool add_physics;
                public bool static_object;
                public List<float> collider_center; //
                public List<float> collider_size; //
                public bool display_collider;
                public bool display_dimensions;
                public bool calculate_dimensions;
                public List<string> images_list;

            }


            public class Sphere : ColoratedObject
            {
                //scale is radius

            }

            public class Cylinder : ColoratedObject
            {
                //scale is height
                public float radius;

            }

            public class Quad : ColoratedObject
            {

            }

            public class Sprite : ColoratedObject
            {
                public bool isFacingCam = false; //TODO: implement it in the VR side
                public bool isCenterPivot = true;
                public string imageFile;


            }

            public class FlatScreen : ColoratedObject
            {
                public string filenamePrefix;


                public bool isGetDataEyes;
                public bool isGetDataHead;



            }

            public class TangentScreen : FlatScreen
            {

            }

            public class LineTool : ColoratedObject
            {
                public List<float> startPoint;
                public List<float> endPoint;
                public float width;
             
                public  Vector3 StartPointToUnityPos()
                {
                    if (startPoint.Count == 2)
                        return new Vector3(startPoint[0], startPoint[1], 0);
                    return new Vector3(startPoint[0], startPoint[1], startPoint[2]);
                }
                public Vector3 EndPointToUnityPos()
                {
                    if (endPoint.Count == 2)
                        return new Vector3(endPoint[0], endPoint[1], 0);
                    return new Vector3(endPoint[0], endPoint[1], endPoint[2]);
                }
            }
        }
    }
}
