using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace PTVR
{
    namespace Stimuli
    {
        namespace Objects
        {
            public class VirtualPoint2D : Frame
            {
                public bool isKeepPerimetricAngles = false;
                public bool isAutoRadialDistance = false;
                public float eccentricity;
                public float half_meridian;
                public override Vector3 ToUnityPos(GameObject go = null)
                {
                    return new Vector3();
                }
            }
            public class VirtualPoint : VirtualPoint2D
            {
                public List<float> position_current_CS; //in xyz
                public CoordinateSystem  current_coordinate_system;
                public TransformationCoordinateSequence transformationCoordinate;
                public string calculatedPosition = "";

                public bool onOppositeDirection;
                public bool isFacing;
                public long idObjectToFacing;

                public List<float> fickCoordinates;//lattitude/longitude coordinate system

                private Transform rotationTool = null;

                public bool isFacingHeadPOV = false;
                public bool isFacingOrigin = false;

                public bool isTangentAngularCorrection = false;

                public InfiniteCircularCone infiniteCone = null;
                public Vector3 ToUnityUI(GameObject go = null)
                {
                     Vector3 final_position = new Vector3(transformationCoordinate.listTransformation[transformationCoordinate.listTransformation.Count - 1].transformationCoordinate[0], transformationCoordinate.listTransformation[transformationCoordinate.listTransformation.Count - 1].transformationCoordinate[1], transformationCoordinate.listTransformation[transformationCoordinate.listTransformation.Count-1].transformationCoordinate[2]);
                     return final_position;
                }
                public override Vector3 ToUnityPos( GameObject go = null)
                {
                    //first the current position
                    Vector3 final_position = Vector3.zero;
                  

                    return final_position;
                }
                public Vector3 ToUnityRotation(Transform current_CS,GameObject _go=null)
                {
             
                    return rotationTool.rotation.eulerAngles;

                }

            }
        }
    }
}

[Serializable]
public struct CurrentCS
{
    public Vector3 pos;
    public Vector3 rot;
}