using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTVR
{
    namespace Stimuli
    {
        namespace Objects
        {
            public class InfiniteCircularCone : MetaData
            {
                public string circularOrEllipseBase = "ellipse";
                public VirtualPoint CAPOC = new VirtualPoint();
                public bool tangentCorrection = false;
                public float angleAtApex = 0;
                public float LengthToBeConvertedInAngle = 0;
                public VirtualPoint apexPosition;
            }
        }
    }
}