using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using PTVR.Stimuli.Objects;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Audio;
using ViveSR.anipal.Eye;
using System.Diagnostics;

namespace PTVR //Toolbox namespace
{
    namespace Data
    {
        namespace Callback
        {
            public class Callback : MetaData
            {
                public string type { get; set; }
                public string mode { get; set; }
                public string effect { get; set; }
                public bool isPaused { get; set; }
                public long id { get; set; }
                public bool isCallbackPausable { get; set; }
                public InputUnityEvent button;

                public string callbackName { get; set; }
                public virtual void Activate()
                {
                    if (isCallbackPausable)
                    {
                        if (The3DWorldManager.instance.state == SystemState.PAUSED)
                        { isPaused = true; }
                        else
                        {
                            isPaused = false;
                        }
                    }
                    else
                    {
                        isPaused = false;
                    }

                    ExporterManager.instance.WriteNewLineValidate("_Main_", button, mode, type, effect, System.DateTime.Now, callbackName);

                }
                public virtual void Deactivate()
                {
                    if (isCallbackPausable)
                    {
                        if (The3DWorldManager.instance.state == SystemState.PAUSED)
                        { isPaused = true; }
                        else
                        {
                            isPaused = false;
                        }
                    }
                    else
                    {
                        isPaused = false;
                    }

                    ExporterManager.instance.WriteNewLineValidate("_Main_", button, mode, type, effect, System.DateTime.Now, callbackName);
                }
            }

            //public class ExampleCallback: Callback
            //{
            // public override void DoIt(){}
            // public override void UnDoIt(){}   
            //}

            public class ChangeBackgroundColor : Callback
            {
                public PTVR.Stimuli.Color newColor { get; set; }
                public bool isSmooth { get; set; }
                public string whichEye { get; set; }
                float t = 0;
                public ChangeBackgroundColor() { }
                public ChangeBackgroundColor(ChangeBackgroundColor _callback)
                {
                    newColor = _callback.newColor;
                    isSmooth = _callback.isSmooth;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    whichEye = _callback.whichEye;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        /* if(isSmooth)
                         {
                             if(t<1)
                             t+= Time.deltaTime*0.01f;
                             UnityEngine.Debug.Log("Smooth :" + t);
                             Camera.main.GetComponent<Camera>().backgroundColor = Color.Lerp(Camera.main.GetComponent<Camera>().backgroundColor,evtColor.ToUnityColor(),t) ;
                         }*/
                        // else
                        //TODO check if color is already change to
                   
                        if(whichEye == "Both")
                        {
                            SetVisualSettings.instance.camLeft.backgroundColor = newColor.ToUnityColor();
                            SetVisualSettings.instance.camRight.backgroundColor = newColor.ToUnityColor();
                        }
                        if (whichEye == "Left")
                        {
                            SetVisualSettings.instance.camLeft.backgroundColor = newColor.ToUnityColor();
                        }
                        if (whichEye == "Right")
                        {
                            SetVisualSettings.instance.camRight.backgroundColor = newColor.ToUnityColor();
                        }
                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        /*if(isSmooth)
                        {
                            if(t>0)
                            t-= Time.deltaTime*0.01f;
                               UnityEngine.Debug.Log("Smooth:" + t);
                            Camera.main.GetComponent<Camera>().backgroundColor = Color.Lerp(originColor,Camera.main.GetComponent<Camera>().backgroundColor,t) ;
                        }*/
                        // else
                        //TODO check if color is already change to
                        if (whichEye == "Both")
                        {
                            Color originColorRight = The3DWorldManager.instance.GetCurrentPTVRScene().rightBackgroundColor.ToUnityColor();
                            SetVisualSettings.instance.camRight.backgroundColor = originColorRight;
                            Color originColorLeft = The3DWorldManager.instance.GetCurrentPTVRScene().leftBackgroundColor.ToUnityColor();
                            SetVisualSettings.instance.camLeft.backgroundColor = originColorLeft;
                        }
                        if (whichEye == "Left")
                        {
                            Color originColorLeft = The3DWorldManager.instance.GetCurrentPTVRScene().leftBackgroundColor.ToUnityColor();
                            SetVisualSettings.instance.camLeft.backgroundColor = originColorLeft;
                        }
                        if (whichEye == "Right")
                        {
                            Color originColorRight = The3DWorldManager.instance.GetCurrentPTVRScene().rightBackgroundColor.ToUnityColor();
                            SetVisualSettings.instance.camRight.backgroundColor = originColorRight;
                        }
                    }
                }
            }

            public class Move_with_periodic_function : Callback
            {
                public long objectId { set; get; }
                GameObject ObjToChange;
                PrefabController prefabController;
                public float temporal_period_in_sec;
                public List<float> start_position;
                public List<float> end_position;
                public List<float> movement_direction;
                public string function_shape;
                public bool look_in_movement_direction;
                public string stop_mode;
                public Move_with_periodic_function() { }
                public Move_with_periodic_function(Move_with_periodic_function _callback)
                {
                    objectId = _callback.objectId;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    temporal_period_in_sec = _callback.temporal_period_in_sec;
                    end_position = _callback.end_position;
                    function_shape = _callback.function_shape;
                    look_in_movement_direction = _callback.look_in_movement_direction;
                    stop_mode = _callback.stop_mode;
                }

                public void InitWithGO(GameObject _go)
                {
                    ObjToChange = _go;
                    prefabController = ObjToChange.GetComponent<PrefabController>();
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {

                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        prefabController.isMoving = true;
                        prefabController.period = temporal_period_in_sec;
                        prefabController.start_periodic_position = new Vector3 (0,0,0) ;
                        prefabController.end_periodic_position = new Vector3(end_position[0], end_position[1], end_position[2]);
                        prefabController.periodic_function = function_shape;
                        prefabController.flag_start_periodic_movement = true;
                        prefabController.flag_periodic_movement = true;
                        prefabController.flag_periodic_function_up = true;
                        prefabController.look_in_movement_direction = look_in_movement_direction;

                        if (!prefabController.dict_periodic_functions.ContainsKey(function_shape))
                        {
                            prefabController.dict_periodic_functions[function_shape] = new Periodic_function();
                            prefabController.dict_periodic_functions[function_shape].period = temporal_period_in_sec;
                            prefabController.dict_periodic_functions[function_shape].start_periodic_position = new Vector3(0, 0, 0);
                            prefabController.dict_periodic_functions[function_shape].end_periodic_position = new Vector3(end_position[0], end_position[1], end_position[2]);
                            prefabController.dict_periodic_functions[function_shape].periodic_function = function_shape;
                            prefabController.dict_periodic_functions[function_shape].flag_start_periodic_movement = true;
                            prefabController.dict_periodic_functions[function_shape].flag_periodic_movement = true;
                            prefabController.dict_periodic_functions[function_shape].flag_periodic_function_up = true;
                            prefabController.dict_periodic_functions[function_shape].look_in_movement_direction = look_in_movement_direction;
                        }
                        else
                        {
                            prefabController.dict_periodic_functions[function_shape].flag_start_periodic_movement = true;
                        }
                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {

                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);

                        if (stop_mode.Equals("pause"))
                        {
                            //UnityEngine.Debug.Log("pause");
                            prefabController.stop_mode = "pause";

                        } else
                        {
                            //UnityEngine.Debug.Log("reset");
                            prefabController.stop_mode = "reset";
                        }

                        prefabController.isMoving = false;
                        prefabController.periodic_function = "";
                    }
                }
            }

            public class Translate : Callback
            {
                public long objectId { set; get; }
                GameObject ObjToChange;
                PrefabController prefabController;
                public float speed;
                public float defaultSpeed;
                public bool forward_movement;
                public bool reverse_movement;
                public bool upward_movement;
                public bool downward_movement;
                public bool left_movement;
                public bool right_movement;
                public List<float> movement_direction;
                public Translate() { }
                public Translate(Translate _callback)
                {
                    objectId = _callback.objectId;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    speed = _callback.speed;
                    movement_direction = _callback.movement_direction;
                    forward_movement = _callback.forward_movement;
                    reverse_movement = _callback.reverse_movement;
                    upward_movement = _callback.upward_movement;
                    downward_movement = _callback.downward_movement;
                    left_movement = _callback.left_movement;
                    right_movement = _callback.right_movement;
                }

                public void InitWithGO(GameObject _go)
                {
                    ObjToChange = _go;
                    prefabController = ObjToChange.GetComponent<PrefabController>();
                    defaultSpeed = prefabController.defaultSpeed;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {

                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        prefabController.isMoving = true;
                        prefabController.speed = speed;
                        prefabController.forward_movement = forward_movement;
                        prefabController.reverse_movement = reverse_movement;
                        prefabController.upward_movement = upward_movement;
                        prefabController.downward_movement = downward_movement;
                        prefabController.left_movement = left_movement;
                        prefabController.right_movement = right_movement;
                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {

                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        prefabController.isMoving = false;
                        prefabController.speed = defaultSpeed;
                    }
                }
            }


            public class Rotate : Callback
            {
                public long objectId { set; get; }
                GameObject ObjToChange;
                PrefabController prefabController;
                public bool left_rotate;
                public bool right_rotate;
                public Rotate() { }
                public Rotate(Rotate _callback)
                {
                    objectId = _callback.objectId;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    left_rotate = _callback.left_rotate;
                    right_rotate = _callback.right_rotate;

                }

                public void InitWithGO(GameObject _go)
                {
                    ObjToChange = _go;
                    prefabController = ObjToChange.GetComponent<PrefabController>();
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {

                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        prefabController.right_rotate = right_rotate;
                        prefabController.left_rotate = left_rotate;


                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {

                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        prefabController.right_rotate = false;
                        prefabController.left_rotate = false;
                    }
                }
            }

            public class ChangeObjectScale : Callback
            {
                public long objectId { set; get; }
                GameObject ObjToChange;
                public float scalingFactor;
                public Vector3 defaultScalingFactor;
                public ChangeObjectScale() { }
                public ChangeObjectScale(ChangeObjectScale _callback)
                {
                    objectId = _callback.objectId;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    scalingFactor = _callback.scalingFactor;
                }

                public void InitWithGO(GameObject _go)
                {
                    ObjToChange = _go;
                    defaultScalingFactor = ObjToChange.GetComponent<PrefabController>().defaultScale;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                   
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        ObjToChange.transform.localScale *= scalingFactor;                 
                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        ObjToChange.transform.localScale = defaultScalingFactor;
                    }
                }
            }

            public class AnimateObject : Callback
            {
                public long objectId { set; get; }
                GameObject ObjToChange;

                public AnimateObject() { }
                public AnimateObject(AnimateObject _callback)
                {
                    objectId = _callback.objectId;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }

                public void InitWithGO(GameObject _go)
                {
                    ObjToChange = _go;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {

                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        ObjToChange.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<Animation>().Play();
                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {

                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        ObjToChange.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<Animation>().Stop();
                    }
                }
            }

            public class ChangeObjectColor : Callback
            {
                public PTVR.Stimuli.Color newColor { set; get; }
                //  public PTVR.Stimuli.DynamicColor evtDynamicColor = new PTVR.Stimuli.DynamicColor();
                public long objectId { set; get; }
                Vector4 originColor { set; get; } //minus one is to know that it's not ini
                //     PTVR.Stimuli.DynamicColor originDynamicColor = new PTVR.Stimuli.DynamicColor(); //minus one is to know that it's not ini
                GameObject ObjToChange;
                public ChangeObjectColor() { }
                public ChangeObjectColor(ChangeObjectColor _callback)
                {
                    newColor = _callback.newColor;
                    objectId = _callback.objectId;
                    originColor = _callback.originColor;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }

                public void InitWithGO(GameObject _go)
                {
                    ObjToChange = _go;

                    PrefabController prefabController = _go.GetComponent<PrefabController>();

                    if (prefabController != null)
                    {
                        switch (prefabController.coloratedObject.type)
                        {
                            case "text":
                                originColor = ObjToChange.GetComponent<TextMeshPro>().color;

                                break;
                            case "textUI":
                                originColor = ObjToChange.GetComponent<TextMeshProUGUI>().color;
                                break;
                            case "imageUI":
                                originColor = ObjToChange.GetComponent<RawImage>().color;
                                break;
                            case "buttonUI":
                                originColor = ObjToChange.GetComponent<Image>().color;
                                break;
                            case "line":
                                originColor = ObjToChange.GetComponent<LineRenderer>().startColor;
                                break;
                            default:
                                originColor = ObjToChange.GetComponentInChildren<Renderer>().material.color;
                                break;
                        }
                        // prefabController.defaultColor = originColor;
                    }
                    else if (prefabController == null)
                    {
                        if (ObjToChange.GetComponent<PointingCursorManager>() != null)
                        {
                            //originColor = ObjToChange.GetComponent<PointingCursorManager>().defaultColor.ToUnityColor();
                        }

                        return;
                    }
                    //  originDynamicColor = prefabController.dynamicColor;
                }

                public override void Activate()
                {
                    base.Activate();

                    //TODO check if color is already change to
                    if (!isPaused)
                    {
                        UnityEngine.Debug.Log("Color");
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        if (ObjToChange.GetComponent<PrefabController>() != null)
                        {
                            PrefabController prefabController = ObjToChange.GetComponent<PrefabController>();

                            if (newColor.type == "dynamicColor")
                            {
                                PTVR.Stimuli.DynamicColor dynamiColor = (PTVR.Stimuli.DynamicColor)newColor;
                                prefabController.actualColor = dynamiColor;
                            }
                            else
                            {
                                prefabController.actualColor = newColor;
                            }


                            //TODO: Manager in Prefacontroller
                            switch (prefabController.coloratedObject.type)
                            {
                                case "text":
                                    ObjToChange.GetComponent<TextMeshPro>().color = prefabController.actualColor.ToUnityColor();
                                    break;
                                case "textUI":
                                    ObjToChange.GetComponent<TextMeshProUGUI>().color = prefabController.actualColor.ToUnityColor();
                                    break;
                                case "imageUI":
                                    ObjToChange.GetComponent<RawImage>().color = prefabController.actualColor.ToUnityColor();
                                    break;
                                case "buttonUI":
                                    ObjToChange.GetComponent<Image>().color = prefabController.actualColor.ToUnityColor();
                                    break;
                                case "line":
                                    ObjToChange.GetComponent<LineRenderer>().startColor = prefabController.actualColor.ToUnityColor();
                                    ObjToChange.GetComponent<LineRenderer>().endColor = prefabController.actualColor.ToUnityColor();
                                    break;
                                default:
                                    ObjToChange.GetComponentInChildren<Renderer>().material.color = prefabController.actualColor.ToUnityColor();
                                    ObjToChange.GetComponentInChildren<Renderer>().material.EnableKeyword("_EMISSION");
                                    ObjToChange.GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", prefabController.actualColor.ToUnityColor());
                                    break;
                            }

                        }
                        if (ObjToChange.GetComponent<PrefabController>() == null)
                        {
                            if (ObjToChange.GetComponent<PointingCursorManager>() != null)
                            {
                                //TODO Add gestion Dynamic Color
                                // ObjToChange.GetComponent<PointingCursorManager>().defaultCursor.GetComponent<PrefabController>().renderers[0].GetComponent<SpriteRenderer>().color = evtColor.ToUnityColor();

                                ObjToChange.GetComponent<PointingCursorManager>().actualColor = newColor;
                            }
                            return;
                        }

                    }

                }
                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        if (ObjToChange.GetComponent<PrefabController>() != null)
                        {
                            PrefabController prefabController = ObjToChange.GetComponent<PrefabController>();

                            if (prefabController.actualColor.type == "dynamicColor")
                            {

                                PTVR.Stimuli.DynamicColor dynamiColor = (PTVR.Stimuli.DynamicColor)(prefabController.actualColor);

                                prefabController.StopDynamicColor(dynamiColor);
                            }
                            if (prefabController.defaultColor.type == "dynamicColor")
                            {
                                PTVR.Stimuli.DynamicColor dynamiColor = (PTVR.Stimuli.DynamicColor)prefabController.defaultColor;
                                prefabController.actualColor = dynamiColor;
                            }
                            else
                            {
                                prefabController.actualColor = prefabController.defaultColor;
                            }
                            //   prefabController.StopDynamicColor();
                            switch (prefabController.coloratedObject.type)
                            {
                                case "text":
                                    ObjToChange.GetComponent<TextMeshPro>().color = prefabController.defaultColor.ToUnityColor();
                                    break;
                                case "textUI":
                                    ObjToChange.GetComponent<TextMeshProUGUI>().color = prefabController.defaultColor.ToUnityColor();
                                    break;
                                case "imageUI":
                                    ObjToChange.GetComponent<RawImage>().color = prefabController.defaultColor.ToUnityColor();
                                    break;
                                case "buttonUI":
                                    ObjToChange.GetComponent<Image>().color = prefabController.defaultColor.ToUnityColor();
                                    break;
                                case "line":
                                    ObjToChange.GetComponent<LineRenderer>().startColor = prefabController.defaultColor.ToUnityColor();
                                    ObjToChange.GetComponent<LineRenderer>().endColor = prefabController.defaultColor.ToUnityColor();
                                    break;

                                default:
                                    ObjToChange.GetComponentInChildren<Renderer>().material.color = prefabController.defaultColor.ToUnityColor();
                                    ObjToChange.GetComponentInChildren<Renderer>().material.EnableKeyword("_EMISSION");
                                    ObjToChange.GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", prefabController.defaultColor.ToUnityColor());
                                    break;
                            }
                        }
                        else if (ObjToChange.GetComponent<PrefabController>() == null)
                        {
                            if (ObjToChange.GetComponent<PointingCursorManager>() != null)
                            {
                                //TODO Add gestion Dynamic Color

                                //  ObjToChange.GetComponent<PointingCursorManager>().defaultCursor.GetComponent<PrefabController>().actualColor = ObjToChange.GetComponent<PointingCursorManager>().defaultColor;
                                // ObjToChange.GetComponent<PointingCursorManager>().defaultCursor.GetComponent<PrefabController>().renderers[0].GetComponent<SpriteRenderer>().color = ObjToChange.GetComponent<PointingCursorManager>().actualColor.ToUnityColor();
                                ObjToChange.GetComponent<PointingCursorManager>().actualColor = ObjToChange.GetComponent<PointingCursorManager>().defaultColor;
                            }
                            return;
                        }
                        //  prefabController.dynamicColor = originDynamicColor;
                    }
                }

            }

            public class ChangeObjectOutline : Callback
            {
                public PTVR.Stimuli.Color outlineColor { set; get; }
                //  public PTVR.Stimuli.DynamicColor evtDynamicColor = new PTVR.Stimuli.DynamicColor();
                public long objectId { set; get; }
                Color originOutlineColor { set; get; } //minus one is to know that it's not ini


                GameObject ObjToChange;
                public float outlineWidth;
                float originOutlineWidth;
                List<Outline> outlineObject;



                public ChangeObjectOutline() { }
                public ChangeObjectOutline(ChangeObjectOutline _callback)
                {

                    outlineColor = _callback.outlineColor;
                    objectId = _callback.objectId;
                    outlineWidth = _callback.outlineWidth;
                    originOutlineColor = _callback.originOutlineColor;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    originOutlineWidth = _callback.originOutlineWidth;
                }

                public void InitWithGO(GameObject _go)
                {
                    outlineObject = new List<Outline>();
                    ObjToChange = _go;
                    //Check if is Text + Text UI

                    //Check if is UIObject

                    //Check if is Flat/Tangent screen
                    if (ObjToChange.transform.Find("ScaledTransform"))
                    {
                        Outline[] outlines = ObjToChange.transform.Find("ScaledTransform").GetComponentsInChildren<Outline>();

                        foreach (Outline line in outlines)
                        {

                            outlineObject.Add(line);

                      

                        }
                    }
                    //Default
                    else if (ObjToChange.GetComponent<Outline>() != null)
                    {
                        outlineObject.Add(ObjToChange.GetComponent<Outline>());
              
                    }
                    else if (ObjToChange.GetComponent<UnityEngine.UI.Outline>() != null)
                    {
                        originOutlineWidth = ObjToChange.GetComponentInChildren<UnityEngine.UI.Outline>().effectDistance.x;
                        originOutlineColor = ObjToChange.GetComponentInChildren<UnityEngine.UI.Outline>().effectColor;
                    }
                    if (ObjToChange.GetComponentInChildren<TMP_Text>() != null)
                    {

                        originOutlineWidth = ObjToChange.GetComponentInChildren<TMP_Text>().outlineWidth;
                        originOutlineColor = ObjToChange.GetComponentInChildren<TMP_Text>().outlineColor;
                    }

                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        //TODO check if color is already change to
                        // originOutlineColor = outline.OutlineColor;
                        //  originOutlineWidth = outline.OutlineWidth;
                        for (int count = 0; count < outlineObject.Count; count++)
                        {
                            outlineObject[count].OutlineColor = outlineColor.ToUnityColor();
                            outlineObject[count].OutlineWidth = outlineWidth;
                        }
                        if (ObjToChange.GetComponentInChildren<TMP_Text>() != null)
                        {
                            ObjToChange.GetComponentInChildren<TMP_Text>().outlineWidth = outlineWidth;
                            ObjToChange.GetComponentInChildren<TMP_Text>().outlineColor = outlineColor.ToUnityColor();
                        }
                        if (ObjToChange.GetComponent<UnityEngine.UI.Outline>() != null)
                        {
                            ObjToChange.GetComponentInChildren<UnityEngine.UI.Outline>().effectDistance = new Vector2(outlineWidth, outlineWidth);
                            ObjToChange.GetComponentInChildren<UnityEngine.UI.Outline>().effectColor = outlineColor.ToUnityColor();
                        }
                    }
                }
                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        //TODO check if color is already change to
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        UnityEngine. Debug.Log("Test");
                        for (int count = 0; count < outlineObject.Count; count++)
                        {
                            UnityEngine.Debug.Log("here" + originOutlineWidth);
                              outlineObject[count].OutlineColor = originOutlineColor;
                            outlineObject[count].OutlineWidth = originOutlineWidth;
                        }
                        if (ObjToChange.GetComponentInChildren<TMP_Text>() != null)
                        {
                            ObjToChange.GetComponentInChildren<TMP_Text>().outlineWidth = originOutlineWidth;
                            ObjToChange.GetComponentInChildren<TMP_Text>().outlineColor = originOutlineColor;
                        }
                        if (ObjToChange.GetComponent<UnityEngine.UI.Outline>() != null)
                        {
                            ObjToChange.GetComponentInChildren<UnityEngine.UI.Outline>().effectDistance = new Vector2(originOutlineWidth, originOutlineWidth);
                            ObjToChange.GetComponentInChildren<UnityEngine.UI.Outline>().effectColor = originOutlineColor;
                        }
                    }
                }

            }

            public class ChangeObjectEnable : Callback
            {
                public long objectId { set; get; }
                GameObject ObjToChange;

                public ChangeObjectEnable() { }
                public ChangeObjectEnable(ChangeObjectEnable _callback)
                {
                    objectId = _callback.objectId;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }

                public void InitWithGO(GameObject _go)
                {
                    ObjToChange = _go;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        if (!ObjToChange.active)
                            ObjToChange.SetActive(true);
                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        //TODO check if object is already set
                        if (ObjToChange.active)
                            ObjToChange.SetActive(false);
                    }
                }
            }

            public class ChangeObjectVisibility : Callback
            {
                public long objectId { set; get; }
                GameObject ObjToChange;
                public bool isVisible;

                public ChangeObjectVisibility() { }
                public ChangeObjectVisibility(ChangeObjectVisibility _callback)
                {
                    objectId = _callback.objectId;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }

                public void InitWithGO(GameObject _go)
                {
                    ObjToChange = _go;
                    if (_go.GetComponent<PrefabController>() != null)
                    {
                        _go.GetComponentInChildren<Renderer>().enabled = _go.GetComponent<PrefabController>().defaultVisiblity;
                    }
                    else if (_go.GetComponent<PointingCursorManager>() != null)
                    {
                        _go.GetComponentInChildren<Renderer>().enabled = _go.GetComponent<PointingCursorManager>().defaultCursor.GetComponent<PrefabController>().defaultVisiblity;
                    }

                    isVisible = _go.GetComponentInChildren<Renderer>().enabled;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        UnityEngine.Debug.Log(ObjToChange.name);
                        ObjToChange.GetComponentInChildren<Renderer>().enabled = !isVisible;
                       
                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        ObjToChange.GetComponentInChildren<Renderer>().enabled = isVisible;
                    }
                }
            }
            public class CreateNewResultFile : Callback
            {
                public string dataColumns { set; get; }
                public string fileName { set; get; }
                public List<string> data { set; get; }
                public CreateNewResultFile() { }
                public CreateNewResultFile(CreateNewResultFile _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    dataColumns = _callback.dataColumns;
                    data = _callback.data;
                    fileName = _callback.fileName;
                    callbackName = _callback.callbackName;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        ExporterManager.instance.Setup(fileName, dataColumns);
                        ExporterManager.instance.WriteFile(fileName, data);
                    }
                }

            }
            
            public class AddNewPoint : Callback
            {
                public long graphId { set; get; }
                GameObject graph;

                public AddNewPoint() { }
                public AddNewPoint(AddNewPoint _callback)
                {
                    graphId = _callback.graphId;
                    
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }
                public void InitWithGO(GameObject _go)
                {
                    graph = _go;
                }

                public override void Activate()
                {
                    base.Activate();

                    GraphUIManager.instance.addNewPoint();
                    
                }

            }

            public class ShowAllPoints : Callback
            {
                public long graphId { set; get; }
                GameObject graph;

                public ShowAllPoints() { }
                public ShowAllPoints(ShowAllPoints _callback)
                {
                    graphId = _callback.graphId;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }
                public void InitWithGO(GameObject _go)
                {
                    graph = _go;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        GraphUIManager.instance.ShowAllPoints();
                    }
                }

            }

            public class UpdateLastPoint : Callback
            {
                public long graphId { set; get; }
                GameObject graph;

                public UpdateLastPoint() { }
                public UpdateLastPoint(UpdateLastPoint _callback)
                {
                    graphId = _callback.graphId;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }
                public void InitWithGO(GameObject _go)
                {
                    graph = _go;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        GraphUIManager.instance.UpdateLastPoint();
                    }
                }

            }

            public class DeleteLastPoint : Callback
            {
                public long graphId { set; get; }
                GameObject graph;

                public DeleteLastPoint() { }
                public DeleteLastPoint(DeleteLastPoint _callback)
                {
                    graphId = _callback.graphId;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }
                public void InitWithGO(GameObject _go)
                {
                    graph = _go;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        GraphUIManager.instance.DeleteLastPoint();
                    }
                }

            }

            public class SaveLastPoint : Callback
            {
                public long graphId { set; get; }
                GameObject graph;

                public SaveLastPoint() { }
                public SaveLastPoint(SaveLastPoint _callback)
                {
                    graphId = _callback.graphId;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }
                public void InitWithGO(GameObject _go)
                {
                    graph = _go;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        GraphUIManager.instance.SaveLastPoint();
                    }
                }
            }

            public class PauseCurrentScene : Callback
            {
                public PauseCurrentScene() { }
                public PauseCurrentScene(PauseCurrentScene _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }

                public override void Activate()
                {
                    // if (!isPaused)
                    base.Activate();
                    UnityEngine.Debug.LogError("Pause Current Scene.");
                    if (The3DWorldManager.instance.state == SystemState.PAUSED || The3DWorldManager.instance.state == SystemState.INITIALIZED)
                        return;
                    else
                    {
                        The3DWorldManager.instance.state = SystemState.PAUSED;
                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    UnityEngine.Debug.LogError("Resume Current Scene.");
                    if (The3DWorldManager.instance.state == SystemState.INITIALIZED)
                        return;
                    The3DWorldManager.instance.state = The3DWorldManager.instance.lastState;
                    isPaused = false;
                }
            }


            public class EndCurrentScene : Callback
            {
                public long idNextScene { set; get; }

                public EndCurrentScene() { }
                public EndCurrentScene(EndCurrentScene _callback)
                {
                    idNextScene = _callback.idNextScene;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    button = _callback.button;
                    callbackName = _callback.callbackName;

                }

                public int format_date_time(System.DateTime date)
                {
                    return date.Hour * 3600000 + date.Minute * 60000 + date.Second * 1000 + date.Millisecond;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        TimerManager.instance.StopTimerScene();
                        if (idNextScene == -1)
                        {
                            bool isFound = false;
                            foreach (var scene in The3DWorldManager.instance.curr3DWorld.scenes)
                            {
                                if (isFound)
                                {
                                    idNextScene = scene.Key;
                                    isFound = false;
                                    break;
                                }
                                if (scene.Key == The3DWorldManager.instance.currScene.id)
                                {
                                    isFound = true;
                                }
                            }

                            /* for (int count = 0;count< ExperimentManager.instance.currExperiment.scenes.Count;count++ )
                             {
                                 if (ExperimentManager.instance.currExperiment.scenes[count].id == ExperimentManager.instance.currScene.id)
                                     idNextScene = ExperimentManager.instance.currExperiment.scenes[count].id;
                             }*/

                        }

                        The3DWorldManager.instance.state = SystemState.END_VISUAL_NOW;
                        SceneManager.instance.UnputVisual(idNextScene);
                    }

                }
                /*
               public override void Deactivate()
               {
                    ExperimentManager.instance.RestartScene();
               }
                */
            }


            public class AugmentationCamera : Callback
            {
                public float zoom { set; get; }
                public long cameraId { set; get; }
                CameraController cameraPTVR;
                public AugmentationCamera() { }
                public AugmentationCamera(AugmentationCamera _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    zoom = _callback.zoom;
                    cameraId = _callback.cameraId;
                }

                public void InitWithGO(GameObject _go)
                {
                    cameraPTVR = _go.GetComponent<CameraController>();
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                       
                       InitWithGO(The3DWorldManager.instance.GameObjectCol[cameraId]);
                            cameraPTVR.fov += zoom;
                    }
                }
                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[cameraId]);
                        cameraPTVR.Reset();
                    }
                }
            }

            public class ChangeObjectRotateTo : Callback
            {
            
                public long objectId { set; get; }
                public string rotateType { set; get; }
                public long objectToLookId { set; get; }

                PrefabController prefab;
                bool isFacing;
                bool onOppositeDirection;
                public ChangeObjectRotateTo() { }
                public ChangeObjectRotateTo(ChangeObjectRotateTo _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    objectId = _callback.objectId;
                    rotateType = _callback.rotateType; 
                    objectToLookId = _callback.objectToLookId;
                }

                public void InitWithGO(GameObject _go)
                {
                    prefab = _go.GetComponent<PrefabController>();
                    isFacing = prefab.isFacing;
                    onOppositeDirection = prefab.onOppositeDirection;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                     
                        if (rotateType == "rotate_to_look_at_in_opposite_direction")
                        {   
                            prefab.isFacing = false;
                            prefab.goToFacing = SetVisualSettings.instance.GameObjectCol[objectToLookId];
                            prefab.onOppositeDirection = true;
                        }
                        else if (rotateType == "rotate_to_look_at")
                        {
                            prefab.onOppositeDirection = false;
                            prefab.goToFacing = SetVisualSettings.instance.GameObjectCol[objectToLookId];
                            prefab.isFacing = true;
                        }
                       /* else if (rotateType == "classical_rotate")
                        {
                            prefab.isFacing = false;
                            prefab.goToFacing = SetVisualSettings.instance.GameObjectCol[objectToLookId];

                            prefab.onOppositeDirection = false;
                        }*/
                        UnityEngine.Debug.Log("id look "+ objectToLookId + ","+ SetVisualSettings.instance.GameObjectCol.ContainsKey(objectToLookId));
               
                    }
                }
                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);

                        if (isFacing && !prefab.isFacing)
                            prefab.isFacing = true;
                        if (onOppositeDirection && !prefab.onOppositeDirection)
                            prefab.onOppositeDirection = true;
                    }
                }
            }

            public class ChangeObjectParent : Callback
            {

                public long objectId { set; get; }
                public long parentId { set; get; }
                public long old_parentId { set; get; }
                GameObject go;
                public ChangeObjectParent() { }
                public ChangeObjectParent(ChangeObjectParent _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    objectId = _callback.objectId;
                    parentId = _callback.parentId;
                    
                }

                public void InitWithGO(GameObject _go)
                {
                    go = _go;
                    old_parentId = go.transform.parent.GetInstanceID();
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);

                        go.transform.parent = SetVisualSettings.instance.GameObjectCol[parentId].transform;

                        
                    }
                }
                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        go.transform.parent = SetVisualSettings.instance.GameObjectCol[go.GetComponent<PrefabController>().parentId].transform;
                        //Return to default parent
                    }
                }
            }


            public class NextWord : Callback
            {

                public long objectId { set; get; }

                public string expectedString { set; get; }

                GameObject go;

                public NextWord() { }
                public NextWord(NextWord _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    objectId = _callback.objectId;
                    expectedString = _callback.expectedString;

                }

                public void InitWithGO(GameObject _go)
                {
                    go = _go;

                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        //go.transform.GetComponent<PrefabController>().isAttractive = true;

                    }
                }
                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                    }
                }
            }

            public class Attractive : Callback
            {

                public long objectId { set; get; }
                public long readSupportId { set; get; }
                public long readingAttractorID { set; get; }
                public List<long> attractors_list { set; get; }
                public List<float> attraction_distance;
                public bool isRead { set; get; }
                GameObject go;

                public Attractive() { }
                public Attractive(Attractive _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    objectId = _callback.objectId;
                    attractors_list = _callback.attractors_list;
                    attraction_distance = _callback.attraction_distance;
                    isRead = _callback.isRead;
                    readSupportId = _callback.readSupportId;
                    readingAttractorID = _callback.readingAttractorID;

                }

                public void InitWithGO(GameObject _go)
                {
                    go = _go;

                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        go.transform.GetComponent<PrefabController>().isAttractive = true;
                        go.transform.GetComponent<PrefabController>().AttractorsTransformList = new List<Transform>();
                        go.transform.GetComponent<PrefabController>().AttractorsPCList = new List<PrefabController>();
                        go.transform.GetComponent<PrefabController>().attraction_distance = new Vector3 (attraction_distance[0], attraction_distance[1], attraction_distance[2]);
                        go.transform.GetComponent<PrefabController>().readingAttractorID = readingAttractorID;


                        if (readingAttractorID != -1)
                        {
                            go.transform.GetComponent<PrefabController>().isRead = isRead;
                        }

                        foreach (long id_ in attractors_list)
                        {
                            go.transform.GetComponent<PrefabController>().AttractorsTransformList.Add(SetVisualSettings.instance.GameObjectCol[id_].transform);
                            go.transform.GetComponent<PrefabController>().AttractorsPCList.Add(SetVisualSettings.instance.GameObjectCol[id_].GetComponent<PrefabController>());
                            
                        }
                    }
                }
                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        go.transform.GetComponent<PrefabController>().isAttractive = false;
                    }
                }
            }

            public class Attractor : Callback
            {

                public long objectId { set; get; }
                public long readingAttractorID { set; get; }

                public string expectedString { set; get; }

                public long readSupportId { set; get; }
                GameObject go;

                public Attractor() { }
                public Attractor(Attractor _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    objectId = _callback.objectId;
                    readingAttractorID = _callback.readingAttractorID;
                    readSupportId = _callback.readSupportId;
                    expectedString = _callback.expectedString;
                }

                

                public void InitWithGO(GameObject _go)
                {
                    go = _go;

                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        go.transform.GetComponent<PrefabController>().isAttractor = true;

                        if (readingAttractorID == objectId)
                        {
                            go.transform.GetComponent<PrefabController>().readingAttractor = true;
                            go.transform.GetComponent<PrefabController>().readString = "";
                            go.transform.GetComponent<PrefabController>().expectedString = expectedString;
                            go.transform.GetComponent<PrefabController>().textRead = SetVisualSettings.instance.GameObjectCol[readSupportId].transform.GetChild(1).GetComponent<TextMeshPro>();
                        }
                    }
                }
                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        go.transform.GetComponent<PrefabController>().isAttractor = false;
                    }
                }
            }

            public class ResetPosition : Callback
            {

                public long objectId { set; get; }

                GameObject go;

                public ResetPosition() { }
                public ResetPosition(ResetPosition _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    objectId = _callback.objectId;

                }

                public void InitWithGO(GameObject _go)
                {
                    go = _go;

                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        go.transform.GetComponent<PrefabController>().ResetPos = true;

                    }
                }
                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        go.transform.GetComponent<PrefabController>().ResetPos = false;
                    }
                }
            }

            public class MakeGrabable : Callback
            {

                public long objectId { set; get; }
                public Vector3 defaultScalingFactor { set; get; }
                GameObject go;

                public MakeGrabable() { }
                public MakeGrabable(MakeGrabable _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    objectId = _callback.objectId;
                    defaultScalingFactor = new Vector3(1, 1, 1);
                }

                public void InitWithGO(GameObject _go)
                {
                    go = _go;

                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        //GameObject go_ = The3DWorldManager.instance.GameObjectCol[objectId];
                        //GameObject go_clone = UnityEngine.Object.Instantiate(go_,
                        //                                                go_.GetComponent<PrefabController>().initialPos,
                        //                                                go_.GetComponent<PrefabController>().initialRot);
                        //go_clone.transform.localScale = new Vector3(1, 1, 1);
                        //InitWithGO(go_clone);

                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        defaultScalingFactor = go.transform.localScale;
                        go.transform.localScale *= 1.5f;
                        go.transform.GetComponent<PrefabController>().isGrabable = true;
                    }
                }
                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        go.transform.localScale = defaultScalingFactor;
                        go.transform.GetComponent<PrefabController>().isGrabable = false;
                    }
                }
            }
            public class Grab : Callback
            {

                public long objectId { set; get; }
                public long parentId { set; get; }

                GameObject go;

                Transform old_parent;
                public Grab() { }
                public Grab(Grab _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    objectId = _callback.objectId;
                    parentId = _callback.parentId;

                }

                public void InitWithGO(GameObject _go)
                {
                    go = _go;
                    
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        old_parent = go.transform.parent;
                        if (go.transform.GetComponent<PrefabController>().isGrabable)
                        {
                            go.transform.parent = SetVisualSettings.instance.GameObjectCol[parentId].transform;
                            go.transform.GetComponent<PrefabController>().isGrabed = true;
                            go.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<Renderer>().material.SetColor("_Color", new Color(1f, 1f, 0.0f, 1.0f));
                        }
                    }
                }
                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[objectId]);
                        go.transform.parent = old_parent;
                        go.transform.GetComponent<PrefabController>().isGrabed = false;
                        go.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<Renderer>().material.SetColor("_Color", new Color(1f, 1f, 1.0f, 1.0f));
                    }
                }
            }

            public class InstantiateObject : Callback
            {
                public VisualObject visualObject { set; get; }

                GameObject go;
                public InstantiateObject() { }
                public InstantiateObject(InstantiateObject _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    visualObject = _callback.visualObject;
                }


                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        go = SetVisualSettings.instance.SetupObject(visualObject);
                        if(go.transform.parent.GetComponent<PrefabController>() != null)
                        {
                            // Changer si SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform.parent.GetComponent<PointingCursorManager>() alors prendre sprite
                            if ( go.transform.parent.GetComponentInChildren<ObjectCollider>().id != visualObject.parentId)
                            {
                              
                                if (SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform.GetComponent<PointingCursorManager>() != null)
                                {
                                    go.transform.parent = SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform;
                      
                                    Vector3 temp = new Vector3 (visualObject.scale[0], visualObject.scale[1], visualObject.scale[2]);
                                    UnityEngine.Debug.Log(go.transform.parent.name + " " + temp);
                                    //125 for have value
                                    go.transform.localScale = new Vector3 (SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform.GetComponentInChildren<PrefabController>().transform.localScale.x*temp.x, 
                                        SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform.GetComponentInChildren<PrefabController>().transform.localScale.y * temp.y,
                                        SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform.GetComponentInChildren<PrefabController>().transform.localScale.z* temp.z) * 5/temp.x;
                                }
                                else
                                {
                                   go.transform.parent = SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform;
                                }
                              go.transform.localPosition = Vector3.zero; // TODO Reset Default Position 
                           }
                        }
                        else
                        {
                            UnityEngine.Debug.Log(SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform.name);
                            if (SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform.GetComponent<PointingCursorManager>() != null)
                            {
                                go.transform.parent = SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform;

                                Vector3 temp = new Vector3(visualObject.scale[0], visualObject.scale[1], visualObject.scale[2]);
                                UnityEngine.Debug.Log(go.transform.parent.name + " " + temp);
                                go.transform.localScale = new Vector3(SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform.GetComponentInChildren<PrefabController>().transform.localScale.x * temp.x,
                                                          SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform.GetComponentInChildren<PrefabController>().transform.localScale.y * temp.y,
                                                          SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform.GetComponentInChildren<PrefabController>().transform.localScale.z * temp.z) * 5 / temp.x;
                            }
                            else
                            {
                                go.transform.parent = SetVisualSettings.instance.GameObjectCol[visualObject.parentId].transform;
                            }
                            go.transform.localPosition = Vector3.zero; // / TODO Reset Default Position 
                        }
                        if(!SetVisualSettings.instance.GameObjectCol.ContainsKey(visualObject.id))
                        {
                            SetVisualSettings.instance.GameObjectCol[visualObject.id] = go;
                        }
                    }
                }
                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        go = SetVisualSettings.instance.GameObjectCol[visualObject.id];
                        SetVisualSettings.instance.GameObjectCol.Remove(visualObject.id);
                        UnityEngine.Object.Destroy(go);
                   
                        // Delete
                    }
                }
            }

            public class EndCurrentTrial : Callback
            {
                public int idNextTrial { set; get; }

                public EndCurrentTrial() { }
                public EndCurrentTrial(EndCurrentTrial _callback)
                {
                    idNextTrial = _callback.idNextTrial;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        The3DWorldManager.instance.state = SystemState.END_TRIAL_NOW;
                        The3DWorldManager.instance.currentTrial = idNextTrial;
                        SceneManager.instance.UnputVisual();
                    }

                }

            }

            public class RestartCurrentScene : Callback
            {
                public RestartCurrentScene() { }
                public RestartCurrentScene(RestartCurrentScene _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }
                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        UnityEngine.Debug.LogError("RestartCurrentScene");
                        The3DWorldManager.instance.state = SystemState.RESTART_CURRENT_VISUAL_NOW;

                        SceneManager.instance.UnputVisual();

                    }
                }
            }

            public class RestartCurrentTrial : Callback
            {
                public RestartCurrentTrial() { }
                public RestartCurrentTrial(RestartCurrentTrial _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }
                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        UnityEngine.Debug.LogError("RestartCurrentTrial");
                        The3DWorldManager.instance.state = SystemState.RESTART_CURRENT_TRIAL_NOW;
                        SceneManager.instance.UnputVisual();
                    }
                }
            }

            public class Quit : Callback
            {
                public Quit() { }
                public Quit(Quit _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }
                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        UnityEngine.Debug.Log("Quit");
                        Application.Quit();
                    }
                }
            }

            public class ExecutePythonScript : Callback
            {
                public string scriptPath { set; get; }
                public string scriptName { set; get; }
                public string pythonExecutablePath { set; get; }
                public ExecutePythonScript() { }
                public ExecutePythonScript(ExecutePythonScript _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    scriptPath = _callback.scriptPath;
                    scriptName = _callback.scriptName;
                    pythonExecutablePath = _callback.pythonExecutablePath;
                }
                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        String applicationPath = Application.dataPath;
                        char[] c = applicationPath.ToCharArray();
                        string appPath = string.Empty;
                        string pathToScript = string.Empty;
                        if (SetVisualSettings.instance.isEditor)
                        {
                            appPath = applicationPath.Remove(c.Length - 6); // Only works with the .exe
                        }
                        else
                        {
                            appPath = applicationPath.Remove(c.Length - 41); // Only works with the .exe
                        }

                        

                        pathToScript = scriptPath + "/" + scriptName;//appPath + "PTVR_Researchers/" + scriptPath + "/" + scriptName ;
                        UnityEngine.Debug.Log("path to script is : " + pathToScript);
                        
                        ProcessStartInfo start = new ProcessStartInfo();
                        start.WorkingDirectory = scriptPath;//appPath + "PTVR_Researchers/" + scriptPath;
                        start.FileName = pythonExecutablePath;//full path to python.exe
                        var args = " 1 2 3";
                        start.Arguments = pathToScript + args;//args is path to .py file and any cmd line args
                        start.UseShellExecute = false;
                        start.CreateNoWindow = true;
                        start.RedirectStandardOutput = true;
                        start.RedirectStandardError = true;
                        UnityEngine.Debug.Log("This is Unity -> python :" + args);
                        using (Process process = Process.Start(start))
                        {
                            using (StreamReader reader = process.StandardOutput)
                            {
                                
                                string stderr = process.StandardError.ReadToEnd(); // Here are the exceptions from our Python script
                                //UnityEngine.Debug.Log(stderr);
                                string result = reader.ReadToEnd();
                                UnityEngine.Debug.Log("This is python -> Unity: "+ result);
                                //UnityEngine.Debug.Log(result);
                            }
                        }
                        

                    }
                }
            }
               

            public class ControlFromPythonScript : Callback
            {
                public string scriptPath { set; get; }
                public string scriptName { set; get; }
                public string script_option { set; get; }
                public string pythonExecutablePath { set; get; }
                public List<long> objects_list { set; get; }
                public string object_attribute { set; get; }
                public List<float> attribute_dimensions { set; get; }

                public int initial_staircase_id { set; get; }

                public ControlFromPythonScript() { }
                public ControlFromPythonScript(ControlFromPythonScript _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                    scriptPath = _callback.scriptPath;
                    scriptName = _callback.scriptName;
                    script_option = _callback.script_option;
                    objects_list = _callback.objects_list;
                    object_attribute = _callback.object_attribute;
                    attribute_dimensions = _callback.attribute_dimensions;
                    pythonExecutablePath = _callback.pythonExecutablePath;
                    initial_staircase_id = _callback.initial_staircase_id;
                }
                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        String applicationPath = Application.dataPath;
                        char[] c = applicationPath.ToCharArray();
                        string appPath = string.Empty;
                        string pathToScript = string.Empty;
                        if (SetVisualSettings.instance.isEditor)
                        {
                            appPath = applicationPath.Remove(c.Length - 6); // Only works with the .exe
                        }
                        else
                        {
                            appPath = applicationPath.Remove(c.Length - 41); // Only works with the .exe
                        }

                        pathToScript = scriptPath + "/" + scriptName;//appPath + "PTVR_Researchers/" + scriptPath + "/" + scriptName ;
                        UnityEngine.Debug.Log("path to script is : " + pathToScript);


                        // check with object if firsttime in staircase
                        ProcessStartInfo start = new ProcessStartInfo();
                        start.WorkingDirectory = scriptPath;//appPath + "PTVR_Researchers/" + scriptPath;
                        start.FileName = pythonExecutablePath;//full path to python.exe

                        var pc = SetVisualSettings.instance.GameObjectCol[objects_list[0]].GetComponent<PrefabController>();
                        if (pc.first_staircase)
                        {
                            pc.current_staircase_id = initial_staircase_id;
                            pc.first_staircase = false;
                        }
                            


                        var args = " " + script_option + ";" + pc.current_staircase_id.ToString();

                        


                        start.Arguments = pathToScript + args;//args is path to .py file and any cmd line args
                        start.UseShellExecute = false;
                        start.CreateNoWindow = true;
                        start.RedirectStandardOutput = true;
                        start.RedirectStandardError = true;
                        UnityEngine.Debug.Log("This is Unity -> python :" + args);
                        string result = "";
                        using (Process process = Process.Start(start))
                        {
                            using (StreamReader reader = process.StandardOutput)
                            {

                                string stderr = process.StandardError.ReadToEnd(); // Here are the exceptions from our Python script
                                UnityEngine.Debug.Log(stderr);
                                result = reader.ReadToEnd();

                            }
                        }

                        var answser = result.Split(';');

                        UnityEngine.Debug.Log(result);

                        var obj_id = long.Parse(answser[0]);
                        var obj_attr = answser[1];
                        var attr_dim = answser[2].Split('_');
                        var next_staircase_id = int.Parse(answser[3]);

                        var attr_dim_float = new List<float> { float.Parse(attr_dim[0], System.Globalization.CultureInfo.InvariantCulture),
                                                               float.Parse(attr_dim[1], System.Globalization.CultureInfo.InvariantCulture),
                                                               float.Parse(attr_dim[2], System.Globalization.CultureInfo.InvariantCulture)};


                        var go_ = SetVisualSettings.instance.GameObjectCol[obj_id].transform;
                        pc.current_staircase_id = next_staircase_id;

                        if (obj_attr.Equals("scale"))
                        {
                            go_.GetChild(0).localScale = new Vector3(attr_dim_float[0], attr_dim_float[1], attr_dim_float[2]);
                        }

                    }
                }
            }

            public class PrintResultFileLine : Callback
            {
                public PrintResultFileLine() { }
                public PrintResultFileLine(PrintResultFileLine _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }
                public override void Activate()
                {
                    base.Activate();

                }

                public override void Deactivate()
                {
                    base.Deactivate();

                }
            }




            //Become CancelTrial
            /*
            public class SaveResult : Callback
            {
                public SaveResult() { }
                public SaveResult(SaveResult _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                }
                public override void Activate()
                {
                    base.Activate();

                }

                public override void Deactivate()
                {
                    base.Deactivate();

                }
            }
            */
            public class FillInResultsFileColumn : Callback
            {
                //TODO Get Value nameAppendString le decortiquer
                // r�ecrire avec les valeur wordNum 
                //Set Data nameAppendString
                public string fillInResultsFileColumn { set; get; }
                //public string defaultFillInResultsFileColumn { set; get; }
                public string columnValue { set; get; }
                public string columnName { set; get; }
                public string saveValue { set; get; }

                public bool isCalculatorId { set; get; }
                //Check Text
                public FillInResultsFileColumn() { }
                public FillInResultsFileColumn(FillInResultsFileColumn _callback)
                {
                    id = _callback.id;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    columnName = _callback.columnName;
                    columnValue = _callback.columnValue;
                    fillInResultsFileColumn = _callback.fillInResultsFileColumn;
                    callbackName = _callback.callbackName;
                    isCalculatorId = _callback.isCalculatorId;
                }

                public void InitializeSentence()
                {
                }

                public override void Activate()
                {

                    if (isCalculatorId)
                    {

                        char[] charsToTrim = { '%', 'i' };
                        columnValue = columnValue.Trim(charsToTrim);
                        float result = The3DWorldManager.instance.curr3DWorld.calculatorManager.ResultFloat((long)Convert.ToDouble(columnValue));
                        columnValue = result.ToString("F3");
                    }

                    if (The3DWorldManager.instance.state == SystemState.PAUSED)
                    { isPaused = true; }
                    else
                    {
                        isPaused = false;
                    }

                    for (int count = 0; count < The3DWorldManager.instance.currScene.GetMetaHeaders().Count; count++)
                    {
                        if (The3DWorldManager.instance.currScene.GetMetaHeaders()[count] == columnName)
                        {
                            saveValue = DataBank.Instance.GetData(The3DWorldManager.instance.currScene.GetMetaHeaders()[count]);
                            if(!saveValue.Contains(columnValue))
                            {
                                saveValue += columnValue;
                            }
                        }
                    }

                    DataBank.Instance.SetData(columnName, saveValue);

                    ExporterManager.instance.WriteNewLineValidate("_Main_", button, mode, type, effect, System.DateTime.Now, callbackName);
                }

                public override void Deactivate()
                {
                    if (The3DWorldManager.instance.state == SystemState.PAUSED)
                    { isPaused = true; }
                    for (int count = 0; count < The3DWorldManager.instance.currScene.GetMetaHeaders().Count; count++)
                    {
                        if (The3DWorldManager.instance.currScene.GetMetaHeaders()[count] == columnName)
                        {
                   
                            saveValue = DataBank.Instance.GetData(The3DWorldManager.instance.currScene.GetMetaHeaders()[count]);
                            if (saveValue.Contains(columnValue))
                            {
                                int valueLength = columnValue.Length;
                                int valueToRemove = saveValue.IndexOf(columnValue);
                                string afterFounder = saveValue.Remove(valueToRemove, valueLength);
                                saveValue = afterFounder;
                            }
                        }
                    }

                    DataBank.Instance.SetData(columnName, saveValue);
                    // }
                    ExporterManager.instance.WriteNewLineValidate("_Main_", button, mode, type, effect, System.DateTime.Now, callbackName);
                }
            }

            public class SaveText : Callback
            {
                //TODO Get Value nameAppendString le decortiquer
                // r�ecrire avec les valeur wordNum 
                //Set Data nameAppendString
                public string nameAppendString { set; get; }
                public string defaultAppendString { set; get; }
                public List<int> wordNum { set; get; }
                //public string text;
                public string[] word { set; get; }
                public string saveText { set; get; }
                //Check Text
                public SaveText() { }
                public SaveText(SaveText _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    nameAppendString = _callback.nameAppendString;
                    word = _callback.word;
                    id = _callback.id;
                    wordNum = _callback.wordNum;
                    saveText = _callback.saveText;
                    callbackName = _callback.callbackName;
                }

                public void InitializeSentence()
                {
                    UnityEngine.Debug.Log("SaveTex Initialize sentence");
                    for (int count = 0; count < The3DWorldManager.instance.currScene.GetMetaHeaders().Count; count++)
                    {
                        if (The3DWorldManager.instance.currScene.GetMetaHeaders()[count] == nameAppendString)
                        {
                            word = The3DWorldManager.instance.currScene.GetMetaStrings()[count].Split();
                            defaultAppendString = The3DWorldManager.instance.currScene.GetMetaStrings()[count];
                            DataBank.Instance.SetData(The3DWorldManager.instance.currScene.GetMetaHeaders()[count], "");
                        }
                    }
                    if (!The3DWorldManager.instance.wordSave.ContainsKey(nameAppendString))
                    {
                        The3DWorldManager.instance.wordSave.Add(nameAppendString, new Dictionary<int, string>());
                    }
                    //ExperimentManager.instance.wordSave.Clear();
                    for (int count = 0; count < word.Length; count++)
                    {
                        //    UnityEngine.Debug.Log("count : "+count);
                        if (!The3DWorldManager.instance.wordSave[nameAppendString].ContainsKey(count))
                        {
                            The3DWorldManager.instance.wordSave[nameAppendString].Add(count, "");
                        }
                        if (!The3DWorldManager.instance.wordSave[nameAppendString].ContainsKey(count))
                        {
                            The3DWorldManager.instance.wordSave[nameAppendString].Add(count, "");
                        }
                        else
                        {
                            The3DWorldManager.instance.wordSave[nameAppendString][count] = "";
                        }

                    }
                   
                    //UnityEngine.Debug.Log("wordSave : " + ExperimentManager.instance.wordSave[nameAppendString].Count);
                    // DataBank.Instance.SetData(nameAppendString, defaultAppendString);
                    // UnityEngine.Debug.Log("text ="+ text +" size word save " + ExperimentManager.instance.wordSave.Count);
                }

                public override void Activate()
                {
                    UnityEngine.Debug.Log("SaveText Activate");
                    if (The3DWorldManager.instance.state == SystemState.PAUSED)
                    { isPaused = true; }
                    else
                    {
                        isPaused = false;
                    }

                    for (int count = 0; count < The3DWorldManager.instance.currScene.GetMetaHeaders().Count; count++)
                    {
                        if (The3DWorldManager.instance.currScene.GetMetaHeaders()[count] == nameAppendString)
                        {
                            DataBank.Instance.SetData(The3DWorldManager.instance.currScene.GetMetaHeaders()[count], The3DWorldManager.instance.currScene.GetMetaStrings()[count]);
                        }
                    }


                    UnityEngine.Debug.Log(The3DWorldManager.instance.wordSave.Count);
                    for (int count = 0; count < wordNum.Count; count++)
                    {
                        //  UnityEngine.Debug.Log(wordNum[count]);
                        UnityEngine.Debug.Log(wordNum[count]);
                        //ExperimentManager.instance.wordSave[countAppendString][wordNum[count]] = word[wordNum[count]] ;
                        The3DWorldManager.instance.wordSave[nameAppendString][wordNum[count]] = word[wordNum[count]];
                        //  ExperimentManager.instance.wordSave[wordNum[count]] = word[wordNum[count]];
                    }
                    saveText = "";
                    for (int count = 0; count < The3DWorldManager.instance.wordSave[nameAppendString].Count; count++)
                    {
                        if (The3DWorldManager.instance.wordSave[nameAppendString][count] != string.Empty)
                        {
                            saveText += The3DWorldManager.instance.wordSave[nameAppendString][count] + " ";
                        }
                        else
                        {
                            saveText += The3DWorldManager.instance.wordSave[nameAppendString][count] + "";
                        }
                        // saveText += ExperimentManager.instance.wordSave[count] + " ";
                    }
                    UnityEngine.Debug.Log("------SAVE TEXT---------");
                    UnityEngine.Debug.Log(nameAppendString);
                    UnityEngine.Debug.Log(saveText);
                    DataBank.Instance.SetData(nameAppendString, saveText);
                    UnityEngine.Debug.Log("------GET DATA IN Callback.CS---------");
                    UnityEngine.Debug.Log(DataBank.Instance.GetData(nameAppendString));

                    ExporterManager.instance.WriteNewLineValidate("_Main_", button, mode, type, effect, System.DateTime.Now,callbackName);
                }

                public override void Deactivate()
                {
                    if (The3DWorldManager.instance.state == SystemState.PAUSED)
                    { isPaused = true; }
                    /*
                    for (int count = 0; count < ExperimentManager.instance.currScene.GetMetaHeaders().Count; count++)
                    {
                        DataBank.Instance.SetData(ExperimentManager.instance.currScene.GetMetaHeaders()[count], "");
                    }
                    */
                    for (int count = 0; count < wordNum.Count; count++)
                    {
                        The3DWorldManager.instance.wordSave[nameAppendString][wordNum[count]] = "";
                        //  ExperimentManager.instance.wordSave[wordNum[count]] = "";
                    }
                    saveText = "";
                    for (int count = 0; count < The3DWorldManager.instance.wordSave[nameAppendString].Count; count++)
                    {
                        //saveText += ExperimentManager.instance.wordSave[count] + " ";
                        if (The3DWorldManager.instance.wordSave[nameAppendString][count] != string.Empty)
                        {
                            saveText += The3DWorldManager.instance.wordSave[nameAppendString][count] + " ";
                        }
                        else
                        {
                            saveText += The3DWorldManager.instance.wordSave[nameAppendString][count] + "";
                        }
                    }
                    UnityEngine.Debug.Log(saveText);
                    /*  if (saveText == "")
                      {
                          DataBank.Instance.SetData(nameAppendString, defaultAppendString);
                          //ToDo return default value
                      }

                      else
                      {*/
                    DataBank.Instance.SetData(nameAppendString, saveText);
                    // }
                    ExporterManager.instance.WriteNewLineValidate("_Main_", button, mode, type, effect, System.DateTime.Now, callbackName);
                }
            }
            public class TakePicture : Callback
            {
                public int resWidth;
                public int resHeight;
                public string filename;
                //Add Gestion multi Camera
                public Camera camera;
                public TakePicture() { }
                
                public TakePicture(TakePicture _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    resWidth = _callback.resWidth;
                    resHeight = _callback.resHeight;
                    filename = _callback.filename;
                    callbackName = _callback.callbackName;
                    camera = Camera.main;
                }

                public override void Activate()
                {
                    // if (!isPaused)
                    base.Activate();
                    if (The3DWorldManager.instance.state == SystemState.PAUSED )
                    {

                    }
                    else
                    {
                        RenderTexture oldRT = camera.targetTexture;
                        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
                        //TODO : Add Gestion multi Camera instead of main
                        camera.targetTexture = rt; // Gives image to screenshot
                        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
                        camera.Render();
                        RenderTexture.active = rt;
                        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
                        camera.targetTexture = oldRT;
                        RenderTexture.active = null;
                        //Destroy(rt);
                        byte[] bytes = screenShot.EncodeToPNG();
                        bool isEditor = false;
                        #if UNITY_EDITOR
                        isEditor = true;
                        string pathEditor = "PTVR_Researchers/PTVR_Operators/Results/" + The3DWorldManager.instance.curr3DWorld.name + "/" + 
                            The3DWorldManager.instance.curr3DWorld.name + "_"+The3DWorldManager.instance.userId+"_" + filename + "_" + System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".png";
                        UnityEngine.Debug.LogWarning(pathEditor);
                        File.WriteAllBytes(pathEditor, bytes);
                        #endif
                        #if UNITY_STANDALONE_WIN
                        if (!isEditor)
                        {
                            string path = Application.dataPath + "/../Results/" + The3DWorldManager.instance.curr3DWorld.name + "/" +
                              The3DWorldManager.instance.curr3DWorld.name + "_" + The3DWorldManager.instance.userId + "_" + filename + "_" + System.DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".png";
                            UnityEngine.Debug.LogWarning(path);
                            File.WriteAllBytes(path, bytes);
                        }
                        #endif
                        UnityEngine.Debug.Log(string.Format("Took screenshot to: {0}", filename));
                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (The3DWorldManager.instance.state == SystemState.PAUSED)
                    {

                    }
                    else
                    {

                    }
                }
            }


            public class ChangeAudioSettings : Callback
            {
                public GameObject audioToPlay;
                public bool isLoop { set; get; }
                public long audioId { set; get; }
                public ChangeAudioSettings() { }
                public ChangeAudioSettings(ChangeAudioSettings _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    audioId = _callback.audioId;
                    isLoop = _callback.isLoop;
                    callbackName = _callback.callbackName;
                }
                public void InitWithGO(GameObject _go)
                {
                    if (_go.GetComponent<UnityEngine.AudioSource>() == null)
                    {
                        UnityEngine.Debug.LogError("Not a Audio Object");
                    }
                    audioToPlay = _go;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[audioId]);
                        audioToPlay.GetComponent<UnityEngine.AudioSource>().loop = isLoop;
                        audioToPlay.GetComponent<UnityEngine.AudioSource>().enabled = true;
                        audioToPlay.GetComponent<UnityEngine.AudioSource>().Play();
                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[audioId]);
                        audioToPlay.GetComponent<UnityEngine.AudioSource>().Stop();

                        audioToPlay.GetComponent<UnityEngine.AudioSource>().loop = false;
                    }
                }
            }

            public class ChangeHapticSettings : Callback
            {

                public string controller;
                public float frequency;
                public float amplitude;
                public ChangeHapticSettings() { }
                public ChangeHapticSettings(ChangeHapticSettings _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    id = _callback.id;
                    frequency = _callback.frequency;
                    controller = _callback.controller;
                    amplitude = _callback.amplitude;   
                    callbackName = _callback.callbackName;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        if(controller == "RIGHT")
                            HapticPulse.HapticPulseSteam(Valve.VR.SteamVR_Input_Sources.RightHand,frequency,amplitude);
                        else
                            HapticPulse.HapticPulseSteam(Valve.VR.SteamVR_Input_Sources.LeftHand,frequency, amplitude);
                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {

                    }
                }
            }

            public class AddInteractionPassedToCallback : Callback
            {
                public List<PTVR.Interaction> interaction = new List<PTVR.Interaction>();
                public AddInteractionPassedToCallback() { }
                public AddInteractionPassedToCallback(AddInteractionPassedToCallback _callback)
                {
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    interaction = _callback.interaction;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        CallbackManager.instance.GestionInputEvent(interaction);
                    }
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        CallbackManager.instance.CleanSpecificEvent(interaction);
                    }
                }
            }


            

            public class ChangeObjectText : Callback
            {
                public long textObjectId { set; get; }
                public string text { set; get; }
                public string defaultText { set; get; }
                GameObject ObjToChange { set; get; }
                float originFontSize { set; get; }


                public float fontSize;

                public Color colorText;
                public ChangeObjectText() { }
                public ChangeObjectText(ChangeObjectText _callback)
                {
                    textObjectId = _callback.textObjectId;
                    text = _callback.text;
                    defaultText = _callback.defaultText;
                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    fontSize = _callback.fontSize;
                    id = _callback.id;
                    callbackName = _callback.callbackName;
                }

                public void InitWithGO(GameObject _go)
                {
                    ObjToChange = _go;
                    if (_go.GetComponent<TextMeshProUGUI>() != null)
                    {
                        defaultText = _go.GetComponent<PrefabController>().defaultText;

                        originFontSize = _go.GetComponent<PrefabController>().originFontSize;

                    }

                    else if (_go.GetComponent<TextMeshPro>() != null)
                    {
                        defaultText = _go.GetComponent<PrefabController>().defaultText;

                        originFontSize = _go.GetComponent<PrefabController>().originFontSize;
                    }

                    else if (_go.GetComponent<TMP_Text>() != null)
                    {
                        defaultText = _go.GetComponent<PrefabController>().defaultText;

                        originFontSize = _go.GetComponent<PrefabController>().originFontSize;
                    }
                    PrefabController prefabController = ObjToChange.GetComponent<PrefabController>();
                    prefabController.defaultText = defaultText;
                }

                public override void Activate()
                {
                    base.Activate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[textObjectId]);
                        if (fontSize == 0)
                        {
                            fontSize = originFontSize;
                        }
                        ObjToChange.GetComponent<PrefabController>().ChangeText(text, fontSize);
                    }
                    /*
                    if (ObjToChange.GetComponent<TextMeshPro>() != null)
                    {
                        ObjToChange.GetComponent<PrefabController>().ChangeText(text,fontSize,bold,italic,colorText);
                       //ObjToChange.GetComponent<TextMeshPro>().text = text;
                       // UnityEngine.Debug.Log(ObjToChange.GetComponent<TextMeshPro>().text);
                    }
                    else if (ObjToChange.GetComponent<TextMeshProUGUI>() != null)
                    {
                        ObjToChange.GetComponent<PrefabController>().ChangeText(text);
                        // ObjToChange.GetComponent<PrefabController>().ChangeText(text);
                        // ObjToChange.GetComponent<TextMeshProUGUI>().text = text;
                    }
                    else if (ObjToChange.GetComponent<TMP_Text>() != null)
                    {
                        ObjToChange.GetComponent<PrefabController>().ChangeText(text);
                        // ObjToChange.GetComponent<PrefabController>().ChangeText(text);
                        // ObjToChange.GetComponent<TextMeshProUGUI>().text = text;
                    }
                    */
                }

                public override void Deactivate()
                {
                    base.Deactivate();
                    if (!isPaused)
                    {
                        InitWithGO(The3DWorldManager.instance.GameObjectCol[textObjectId]);
                        ObjToChange.GetComponent<PrefabController>().ChangeText(defaultText, originFontSize);
                    }
                    /*
                    if (ObjToChange.GetComponent<TextMeshPro>() != null)
                    {
                        ObjToChange.GetComponent<PrefabController>().ChangeText(defaultText,originFontSize,originBold,originItalic,originColor);
                    }
                    else if (ObjToChange.GetComponent<TextMeshProUGUI>() != null)
                    {
                        ObjToChange.GetComponent<PrefabController>().ChangeText(defaultText);
                    }
                    else if (ObjToChange.GetComponent<TMP_Text>() != null)
                    {
                        ObjToChange.GetComponent<PrefabController>().ChangeText(defaultText);
                    }
                    */
                }
            }

            

            public class CalibrationEyesTracking : Callback
            {
                public CalibrationEyesTracking() { }
                public CalibrationEyesTracking(CalibrationEyesTracking _callback)
                {

                    effect = _callback.effect;
                    mode = _callback.mode;
                    type = _callback.type;
                    callbackName = _callback.callbackName;
                    id = _callback.id;
                }
                public override void Activate()
                {
                    base.Activate();
                    SRanipal_Eye.LaunchEyeCalibration();
                }

            }
        }
    }
}