using Newtonsoft.Json.Bson;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTVR.Coroutines
{
    public class PTVRCoroutine : MonoBehaviour
    {
        private bool completed = false;

        public void StartPTVRCoroutine()
        {
            StartCoroutine(Loop());
        }

        public void RestartPTVRCoroutine()
        {
            completed = false;
            StartPTVRCoroutine();
        }

        public void FinishPTVRCoroutine()
        {
            completed = true;
        }

        private IEnumerator Loop()
        {
            while (!completed)
            {
                yield return null;
            }
        }
    }

}

