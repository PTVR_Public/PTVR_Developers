using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PTVR.Stimuli.Objects;

namespace PTVR //Toolbox namespace
{
    public class Interaction: MetaData
    {
        public List<PTVR.Data.Event.InputEvent> events ;
        public List<PTVR.Data.Callback.Callback> callbacks;
    }
}