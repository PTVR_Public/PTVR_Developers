using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace PTVR.Core.Experimental
{
    // as Newtonsoft json has issues converting to a base class,
    // this class can be later ranmed to PTVREventBase or something like that
    [System.Serializable]
    public class Event2 // to be renamed Event once we get rid off existing PTVR Event
    {
        // public Callback[] callbacks;
        public string uuid;
        public string[] callbackUUIDs;
        public string eventType;

        public UnityEvent<Event2> internalUnityEvent = new UnityEvent<Event2>();

        // for late initialization.
        // this should not be used but as the system is being integrated with the old one,
        // we have some events that are instantiated when the system is not fully loaded
        protected bool initialized = false;


        public void Initialize()
        {
            if (initialized) return;

            foreach (var callbackUUID in callbackUUIDs)
            {
                Callback callback = CallbackManager.Instance.callbacks[callbackUUID];
                callback.Initialize();
                internalUnityEvent.AddListener(callback.Execute);
            }
            initialized = true;
        }

        public void Trigger()
        {
            if (!initialized) Initialize();

            internalUnityEvent.Invoke(this);
        }
    }

    [System.Serializable]
    public class KeyEvent : Event2
    {
        public enum Mode
        {
            Any,
            Press,
            Release
        }

        public Key key;
        public Mode mode;
    }

    // it seems Newtonsoft has issues returning an instance of the base class.
    // let's use a child class for now
    [System.Serializable]
    public class PTVREvent : Event2
    {
    }

    [System.Serializable]
    public class KeyPressEvent : KeyEvent
    {
    }

    [System.Serializable]
    public class KeyReleaseEvent : KeyEvent
    {
    }
}

