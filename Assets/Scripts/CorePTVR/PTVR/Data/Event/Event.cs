using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PTVR.Stimuli.Objects;
using System;
namespace PTVR
{
    namespace Data
    {
        namespace Event
        {
            public  interface InputEvent
            {
                    public string type { set; get; } //The type of input event
                    public string mode { set; get; } //3 options available "press", "hold" and "release"

                    public string eventName { set; get; }

                     public long id { set; get; }
            }

            //public class ExampleInput : InputSystem
            //{
            //}

            public class Keyboard : InputEvent
            {
                private string[] allowableKeys = {
                 "backspace",
                 "delete",
                 "tab",
                 "pause",
                 "escape",
                 "space",
                 "up",
                 "down",
                 "right",
                 "left",
                 "insert",
                 "home",
                 "end",
                 "page up",
                 "page down",
                 "f1",
                 "f2",
                 "f3",
                 "f4",
                 "f5",
                 "f6",
                 "f7",
                 "f8",
                 "f9",
                 "f10",
                 "f11",
                 "f12",
                 "numPad0",
                 "numPad1",
                 "numPad2",
                 "numPad3",
                 "numPad4",
                 "numPad5",
                 "numPad6",
                 "numPad7",
                 "numPad8",
                 "numPad9",
                 "numPad*",
                 "numPad+",
                 "numPad-",
                 "numPad/",
                 "numPad=",
                 "a",
                 "b",
                 "c",
                 "d",
                 "e",
                 "f",
                 "g",
                 "h",
                 "i",
                 "j",
                 "k",
                 "l",
                 "m",
                 "n",
                 "o",
                 "p",
                 "q",
                 "r",
                 "s",
                 "t",
                 "u",
                 "v",
                 "w",
                 "x",
                 "y",
                 "z",
                 "numlock",
                 "caps lock",
                 "scroll lock",
                 "right shift",
                 "left shift",
                 "right ctrl",
                 "left ctrl",
                 "right alt",
                 "left alt"
                };
                public List<string> validResponses; //This is obtained from the Python-end, defines valid responses, given a display

                public Keyboard(string _type, string _mode)
                {
                    type = _type;
                    mode = _mode;
                }

                // Property implementation:
                public string type { get; set; }

                public string mode { get; set; }

                public string eventName { set; get; }

                public string language { set; get; }

                public long id { set; get; }
            }


            public class HandController : InputEvent
            {
                private string[] allowableKeys =
                {
                    "right_trigger",
                    "right_grip",
                    "right_touch_one",
                    "right_touch_two",
                    "left_trigger",
                    "left_grip",
                    "left_touch_one",
                    "left_touch_two",
                    "right_touch_press",
                    "left_touch_press",
                    "right_touch_press_pos",
                    "left_touch_press_pos"
                };

                public List<string> validResponses; //This is obtained from the Python-end, defines valid responses, given a display

                public HandController(string _type, string _mode)
                {
                    type = _type;
                    mode = _mode;
                }

                // Property implementation:
                public string type { get; set; }

                public string mode { get; set; }

                public string eventName { set; get; }

                public long id { set; get; }
            }

            [Serializable]
             public class PointedAt : InputEvent
             {
                public long coneHash;

                public float pointingConeRadiusDeg;

                public float pointingConeDepth;

                public float eccentricity;
                public float halfMeridian;
               

                //public PTVR.Stimuli.World.VirtualPoint2D pointerEccHM;

                public string origin;
                public long coneOriginid;
                public GameObject originIs;
                public long targetId { set; get; }

                public GameObject target;

                public void InitWithGO(GameObject _go)
                {
                    target = _go;
                }

                public PointedAt(string _type, string _mode)
                {
                    type = _type;
                    mode = _mode;
                    switch (coneOriginid)
                    {
                        case -1:
                            //HandLeft
                            originIs = SetVisualSettings.instance.controllerModels[0];
                            break;
                        case -2:
                            //HandRight
                            originIs = SetVisualSettings.instance.controllerModels[1];
                            break;
                        case -3:
                            //HeadSet
                            originIs = Camera.main.gameObject;
                            break;
                        case -4:
                            //CurrentCS
                            originIs = SetVisualSettings.instance.current_cs.gameObject;
                            break;
                        case -5:
                            //Global
                            originIs = SetVisualSettings.instance.gameObject;
                            break;
                        // Left Eye
                        case -6:
                            originIs = EyesTrackingManager.instance.eyeOrbiteLeft.gameObject;
                            break;
                        // Right Eye
                        case -7:
                            originIs = EyesTrackingManager.instance.eyeOrbiteRight.gameObject;
                            break;
                        // Combine Eye
                        case -8:
                          //  pointedAtCone[_pointedAt.coneHash] = (Instantiate(new GameObject(), EyesTrackingManager.instance.eyeOrbiteCombine.transform));
                            break;
                        // Basic Object
                        default:
                          //  pointedAtCone[_pointedAt.coneHash] = (Instantiate(new GameObject(), SetVisualSettings.instance.GameObjectCol[_pointedAt.idContingent].transform));
                            break;

                    }
                }

                // Property implementation:
                public string type { get; set; }

                public string mode { get; set; }

                public string eventName { set; get; }

                public long id { set; get; }
            }

            public class Timer : InputEvent
            {
                public float delayInMs;
                //public float durationInMs;
                public string timerStart = "Current Time";//Start Scene , Start Trial, Start Experiment, Current Time
                
                public Timer(string _type, string _mode)
                {
                    type = _type;
                    mode = _mode;

                }

                // Property implementation:
                public string type { get; set; }

                public string mode { get; set; }

                public string eventName { set; get; }

                public long id { set; get; }

            }

            public class DistanceFromPoint : InputEvent
            {
                public List<float> pointCoordinates;
                public float distanceFromPointInMeters;
                public string objectTracked;

                public DistanceFromPoint(string _type, string _mode)
                {
                    type = _type;
                    mode = _mode;

                }

                // Property implementation:
                public string type { get; set; }

                public string mode { get; set; }

                public string eventName { set; get; }

                public long id { set; get; }

            }

            public class Button : InputEvent
            {
                public long idObjectButton;
                
                public Button(string _type, string _mode)
                {
                    type = _type;
                    mode = _mode;
                }

                // Property implementation:
                public string type { get; set; }

                public string mode { get; set; }

                public string eventName { set; get; }

                public long id { set; get; }
            }

            //TODO Verify if is relevant to keep
            public class ToggleUI : InputEvent
            {
                public ToggleUI(string _type, string _mode)
                {
                    type = _type;
                    mode = _mode;
                }

                public string label;
                public List<Data.Callback.Callback> deactivateEvents;


                // Property implementation:
                public string type { get; set; }

                public string mode { get; set; }

                public string eventName { set; get; }

                public long id { set; get; }
            }

        }
    }
}