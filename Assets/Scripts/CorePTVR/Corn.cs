using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
public class Corn : MonoBehaviour
{
	public int subdivisions = 30;
	public float radius = 1f;
	public float height = 2f;
	public List <PointedAtUnityEvent> pointedAtUnityEvent = new List<PointedAtUnityEvent>();
  
    public void Setup (PointedAtUnityEvent _pointed)
    {

		transform.localPosition = new Vector3(0, -height);
		GetComponent<MeshFilter>().sharedMesh = Create(subdivisions, radius, height);
		gameObject.AddComponent(typeof(MeshCollider));
		MeshCollider meshCollider = GetComponent<MeshCollider>();
		gameObject.layer = 8;
		meshCollider.convex = true;
		meshCollider.isTrigger = true;

	}

	public void SetupTarget(PointedAtUnityEvent _pointed)
    {
		pointedAtUnityEvent.Add(_pointed);
		foreach (PointedAtUnityEvent ptue in pointedAtUnityEvent)
		{
			if (The3DWorldManager.instance.GameObjectCol[ptue.pointed.targetId].GetComponentInChildren<Rigidbody>() == null)
			{
				//ExperimentManager.instance.GameObjectCol[ptue.pointed.targetId].GetComponentInChildren<ObjectCollider>().gameObject.AddComponent<Rigidbody>().useGravity =false;
				//ExperimentManager.instance.GameObjectCol[ptue.pointed.targetId].GetComponentInChildren<ObjectCollider>().gameObject.GetComponent<Rigidbody>().isKinematic = true;
			}
		}
		//Debug.Log(_pointed.pointed.target);
	}


	Mesh Create(int subdivisions, float radius, float height)
	{
		Mesh mesh = new Mesh();

		Vector3[] vertices = new Vector3[subdivisions + 2];
		Vector2[] uv = new Vector2[vertices.Length];
		int[] triangles = new int[(subdivisions * 2) * 3];

		vertices[0] = Vector3.zero;
		uv[0] = new Vector2(0.5f, 0f);
		for (int i = 0, n = subdivisions - 1; i < subdivisions; i++)
		{
			float ratio = (float)i / n;
			float r = ratio * (Mathf.PI * 2f);
			float x = Mathf.Cos(r) * radius;
			float z = Mathf.Sin(r) * radius;
			vertices[i + 1] = new Vector3(x, 0f, z);

			//Debug.Log(ratio);
			uv[i + 1] = new Vector2(ratio, 0f);
		}
		vertices[subdivisions + 1] = new Vector3(0f, height, 0f);
		uv[subdivisions + 1] = new Vector2(0.5f, 1f);

		// construct bottom

		for (int i = 0, n = subdivisions - 1; i < n; i++)
		{
			int offset = i * 3;
			triangles[offset] = 0;
			triangles[offset + 1] = i + 1;
			triangles[offset + 2] = i + 2;
		}

		// construct sides

		int bottomOffset = subdivisions * 3;
		for (int i = 0, n = subdivisions - 1; i < n; i++)
		{
			int offset = i * 3 + bottomOffset;
			triangles[offset] = i + 1;
			triangles[offset + 1] = subdivisions + 1;
			triangles[offset + 2] = i + 2;
		}

		mesh.vertices = vertices;
		mesh.uv = uv;
		mesh.triangles = triangles;
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();

		return mesh;
	}
	private void OnTriggerEnter(Collider hitObject)
	{
		//Debug.Log("Enter1");
		if (hitObject.gameObject.layer != LayerMask.NameToLayer("PointedAt"))
		{
			//Debug.Log("Enter2");
			foreach (PointedAtUnityEvent ptue in pointedAtUnityEvent)
			{
				if (hitObject.gameObject.GetComponent<ObjectCollider>() != null)
				{
					//Debug.Log("Enter3 " + hitObject.gameObject.GetComponent<ObjectCollider>().id +
					  //" targetId:" + ptue.pointed.targetId);
					if (hitObject.gameObject.GetComponent<ObjectCollider>().id == ptue.pointed.targetId)
					{
						//Debug.Log("Enter4");
						DataBank.Instance.SetData("event_name", ptue.input.inputName);
						DataBank.Instance.SetData("event", ptue.input.button);
						DataBank.Instance.SetData("event_mode", "press");
						ptue.input.OnInputPress.Invoke();
						//Debug.Log("Enter " + hitObject.gameObject.transform.parent.name);
					}
				}
			}
		}
	}
	private void OnCollisionEnter(Collision hitObject)
	{
		if (hitObject.gameObject.layer != LayerMask.NameToLayer("PointedAt"))
		{
			foreach (PointedAtUnityEvent ptue in pointedAtUnityEvent)
			{
				if (hitObject.gameObject.GetComponent<ObjectCollider>() != null)
				{
					if (hitObject.gameObject.GetComponent<ObjectCollider>().id == ptue.pointed.targetId)
					{
						DataBank.Instance.SetData("event_name", ptue.input.inputName);
						DataBank.Instance.SetData("event", ptue.input.button);
						DataBank.Instance.SetData("event_mode", "press");
						ptue.input.OnInputPress.Invoke();
						Debug.Log("Enter " + hitObject.gameObject.transform.parent.name);
					}
				}
			}
		}
	}

	private void OnCollisionStay(Collision hitObject)
	{
		if (hitObject.gameObject.layer != LayerMask.NameToLayer("PointedAt"))
		{
			foreach (PointedAtUnityEvent ptue in pointedAtUnityEvent)
			{
				if (hitObject.gameObject.GetComponent<ObjectCollider>() != null)
				{
					if (hitObject.gameObject.GetComponent<ObjectCollider>().id == ptue.pointed.targetId)
					{
						DataBank.Instance.SetData("event_name", ptue.input.inputName);
						DataBank.Instance.SetData("event", ptue.input.button);
						DataBank.Instance.SetData("event_mode", "hold");
						ptue.input.OnInputHold.Invoke();
					}
				}
			}
		}
	}
	private void OnCollisionExit(Collision hitObject)
	{
		Debug.Log("Exit");
		if (hitObject.gameObject.layer != LayerMask.NameToLayer("PointedAt"))
		{
			foreach (PointedAtUnityEvent ptue in pointedAtUnityEvent)
			{
				if (hitObject.gameObject.GetComponent<ObjectCollider>() != null)
				{
					if (hitObject.gameObject.GetComponent<ObjectCollider>().id == ptue.pointed.targetId)
					{
						DataBank.Instance.SetData("event_name", ptue.input.inputName);
						DataBank.Instance.SetData("event", ptue.input.button);
						DataBank.Instance.SetData("event_mode", "release");
						ptue.input.OnInputRelease.Invoke();
						Debug.Log("Exit" + hitObject.gameObject.transform.parent.name);
					}
				}
			}
		}

	}

	private void OnTriggerExit(Collider hitObject)
	{
	
		if (hitObject.gameObject.layer != LayerMask.NameToLayer("PointedAt"))
		{
			foreach (PointedAtUnityEvent ptue in pointedAtUnityEvent)
			{
			
				if (hitObject.gameObject.GetComponent<ObjectCollider>() != null)
				{
					if (hitObject.gameObject.GetComponent<ObjectCollider>().id == ptue.pointed.targetId)
					{
						DataBank.Instance.SetData("event_name", ptue.input.inputName);
						DataBank.Instance.SetData("event", ptue.input.button);
						DataBank.Instance.SetData("event_mode", "release");
						ptue.input.OnInputRelease.Invoke();
						//Debug.Log("Exit" + hitObject.gameObject.transform.parent.name);
					}
				}
			}

		}

	}

}
