using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.Events;
using PTVR;
using PTVR.Core.Experimental;

/// <summary>
/// Manage The3DWorld inside the Json choose in LoaderManager
/// </summary>

public class The3DWorldManager : MonoBehaviour
{
    #region Inspector
    public static The3DWorldManager instance; // Singleton

    public string userId;
    public SystemState state = SystemState.INITIALIZED;
    public SystemState lastState;
    public bool hasModeExaminator;

    //UnityEvent
    [Tooltip("The Event happen when the next scene was load.")]
    public UnityEvent startTask;
    [Tooltip("The Event happen when the current scene was finished.")]
    public UnityEvent endTask;
    [Tooltip("Controller 0 is Right and Controller 1 is Left")]
    public SteamVRBasedController[] controller;
    public PTVR.The3DWorld curr3DWorld; //List of scenes
    public Dictionary<long, GameObject> GameObjectCol = new Dictionary<long, GameObject>(); //!< List of created GameObject based on uniq IDs to avoid collision

    public double elapsedTimeCurrentScene;

    public double elapsedTimeCurrentTrial;

    public double elapsedTimeCurrentExperiment;

    public PTVR.Stimuli.Objects.Scene currScene;
    public int currentTrial;
    //public List<string> wordSave;
    //public Dictionary<int, string> wordSave = new Dictionary<int, string>();
    public Dictionary<string, Dictionary<int, string>> wordSave = new Dictionary<string, Dictionary<int, string>>();
    #endregion Inspector

    #region Privates
    long currentSceneNb = 0;

    #endregion Privates

    public bool scene_started = false;
    public bool reset_rendering_count = true;
    public System.DateTime t0;
    public System.DateTime t_m;
    public System.DateTime t_actual;
    public System.DateTime t_deserial;
    public System.DateTime t_scene_finished;
    public System.DateTime t_trial_finished;

    #region UnityCall
    void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            Debug.LogWarning("Double ExperimentManager.cs destroy all exept one");
        }
        else
        {
            instance = this;
        }
    }


    #endregion UnityCall

    public int format_date_time(System.TimeSpan date)
        {
          return date.Minutes * 60000 + date.Seconds * 1000 + date.Milliseconds;
        }

    public long CurrentSceneNb
        {
            get { return currentSceneNb; }
        }

    // Initialize the Json
    public void Initialize(string _readData, string _userId)
    {
        /*Camera.main.transform.rotation = new Quaternion();
        Camera.main.transform.position = Vector3.zero;*/

        userId = _userId;
        curr3DWorld = new PTVR.The3DWorld(); //The experiment structure is initialized

        t_deserial = System.DateTime.Now;

        var settings = new JsonSerializerSettings();
 
        settings.Converters.Add(new PTVR.Converters.PointingCursorConverter());
        settings.Converters.Add(new PTVR.Converters.SceneConverter());
        settings.Converters.Add(new PTVR.Converters.VisualObjectTypeConverter());
        settings.Converters.Add(new PTVR.Converters.UIObjectConverter());
        settings.Converters.Add(new PTVR.Converters.ColorFormatConverter());

        settings.Converters.Add(new PTVR.Converters.EventConverter());
        settings.Converters.Add(new PTVR.Converters.CallbackConverter());
        ;


        curr3DWorld = JsonConvert.DeserializeObject<PTVR.The3DWorld>(_readData, settings);

        userId = curr3DWorld.user;
        if (curr3DWorld.id_scene != null)
        {
           // Debug.Log(currExperiment.id_scene[0]);
        }
        else
            Debug.Log("no id");
        //PositionManager.instance.Initialize();
        if(curr3DWorld.hasOperator)/* if someone is bellow computer to manage the3DWorld load */
        {
            hasModeExaminator = true;
        }
        ExporterManager.instance.metaHeaders = curr3DWorld.GetHeadersMeta(); /* Data setup in python with fill in result column while be write inside the .csv*/ 
        ////Data Header from py to add////

        ExporterManager.instance.SetupDefaultData(System.DateTime.Now); /* Data setup in python with fill in result column*/

        string header = string.Empty;

        //Read StringMeta

        foreach (string key in DataBank.Instance.StringData.Keys)
        {
            header = header + key + ";";
        }
        for (int count = 0; count < ExporterManager.instance.metaHeaders.Count; count++)
        {
            header = header + curr3DWorld.headersMeta[count] + ";";
        }
        ExporterManager.instance.Setup("_Main_",header );


        if (curr3DWorld.outputHead == true) /* Verifiy if we create the .csv of headset setup the column and the file */
        {
            PositionManager.instance.SetSaveCameraPosRot(true);
            PositionManager.instance.SaveDelayms = curr3DWorld.headSaveSamplingPeriod;
        }
        if (curr3DWorld.outputGaze == true) /* Verifiy if we create the .csv of gaze setup the column and the file */
        {
            ExporterManager.instance.SetupGazeFile(_userId, "trial;scene_id;left_gaze_x;Left_gaze_y;left_gaze_z;right_gaze_x;right_gaze_y;right_gaze_z;left_eye_position_in_sensor;right_eye_position;left_eye_perimetric_ecc;left_eye_perimetric_HM;right_eye_perimetric_ecc;right_eye_perimetric_HM;Timestamp;ElapsedFrames");
        }

        TimerManager.instance.SetTimerExperiment(); /*Set the timer from the begining */
        t_m = System.DateTime.Now;
        // StartCoroutine(WaitForSubSystemsToLoad());
        NextScene(curr3DWorld.id_scene[0]); /* Load the First Scene of the3DWorld*/

        t0 = System.DateTime.Now;

        

    }

    IEnumerator WaitForSubSystemsToLoad()
    {
        while (!PTVR.Core.Experimental.EventManager.Instance.Loaded)
        {
            yield return null;
        }
        NextScene(curr3DWorld.id_scene[0]); /* Load the First Scene of the3DWorld*/
    }

    public PTVR.Stimuli.Objects.Scene GetCurrentPTVRScene()
    {
        if (curr3DWorld.scenes.ContainsKey(currentSceneNb))
            return curr3DWorld.scenes[currentSceneNb];
        else
        {
            foreach(var scene in curr3DWorld.scenes)
            {
                if(scene.Key == currScene.id)
                {

                }
            }
            return currScene;
        }
            
    }
    void NextTrial (int _idNextTrial)
    {
        long idNextScene = 0;
        bool isFound = false;
        foreach (PTVR.Stimuli.Objects.Scene scene in curr3DWorld.scenes.Values)
        {
            if(scene == currScene)
            {
                isFound = true;
            }
            if(isFound)
            {
                if (scene.trial == _idNextTrial)
                {
                    idNextScene = scene.id;
                    break;
                }
            }
        
        }
        NextScene(idNextScene);
    }

    void NextScene(long _idNextScene)
    {
        if (curr3DWorld.scenes.ContainsKey(_idNextScene))
        {
            currScene = curr3DWorld.scenes[_idNextScene];
            if (currScene.pointingCursors != null)
            {
                SceneManager.instance.PutPointingCursor(currScene.pointingCursors);
            }
            //What type of scene is the top-most scene
            switch (currScene.type)
            {
                case "VISUAL_SCENE":  
                    Debug.Log("Changing state to VISUAL_SCENE");
                    state = SystemState.VISUAL_NOW;
                    lastState = state;
                    PTVR.Stimuli.Objects.VisualScene visualSceneToLoad = (PTVR.Stimuli.Objects.VisualScene)currScene;
                    SceneManager.instance.PutVisual(visualSceneToLoad);
                    
                    // late initialization -> to correct later once we get rid of this shitty "worldLoader"
                    if (visualSceneToLoad.onLoadedEvent == null)
                    {
                        visualSceneToLoad.onLoadedEvent = PTVR.Core.Experimental.EventManager.Instance.GetReferenceToEvent(visualSceneToLoad.onLoadedEventUUID);
                    }
                    if (visualSceneToLoad.onLoadedEvent != null)
                    {
                        visualSceneToLoad.onLoadedEvent.Trigger();
                    }
                    break;
                default:
                    Debug.LogError("Unknown scene by scene type encountered");
                    break;
            }

            startTask.Invoke();
        }
        else /* We finish all the Scene in The3DWorld*/
        {
            FinishExperiment();
        }

        scene_started = true;
        reset_rendering_count = true;
    }

    public void FinishExperiment()
    {
        Debug.Log("scene id does not exists. Thus I guess the experiment finished");
        state = SystemState.END_EXPERIMENT;
        TimerManager.instance.isTimingScene = false;
        TimerManager.instance.isTimingTrial = false;
        TimerManager.instance.isTimingExperiment = false;
           //LastScene End the PTVR.Exe
        print("EXPERIMENT DONE");
        Application.Quit();
    }

    public void CurrentTrialEnd()
    {
        endTask.Invoke();

        t_trial_finished = System.DateTime.Now;

        NextTrial(currentTrial);
    }

    public void CurrentSceneFinished(long _idNextScene)
    {


        endTask.Invoke();
        currentSceneNb = _idNextScene;

        NextScene(_idNextScene);
        //print("Scene Finished");
    }

    public void RestartScene()
    {
        GenericFulfilmentCriteria.instance.UnSetFulfilmentVariables();
        CallbackManager.instance.CleanAllEvent();
        endTask.Invoke();
        NextScene(currScene.id);
    }

    public void RestartTrial()
    {
        GenericFulfilmentCriteria.instance.UnSetFulfilmentVariables();
        CallbackManager.instance.CleanAllEvent();
        endTask.Invoke();
        NextTrial(currScene.trial);
    }

    public int format_date_time(System.DateTime date)
    {
        return date.Hour * 3600000 + date.Minute * 60000 + date.Second * 1000 + date.Millisecond;
    }
}



public enum SystemState
{
    INITIALIZED,
    PAUSED,
    FIXATION_NOW,
    VISUAL_NOW,
    RESPONSE_NOW,
    RESTART_CURRENT_VISUAL_NOW,
    END_VISUAL_NOW,
    RESTART_CURRENT_TRIAL_NOW,
    END_TRIAL_NOW,
    END_EXPERIMENT
};