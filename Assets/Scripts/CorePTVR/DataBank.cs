using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class DataBank
{
    private static DataBank instance = null;
    private static readonly object padlock = new object();
    private Dictionary<string, string> stringData = new Dictionary<string, string>();
    public Dictionary<string, string> StringData
    {
        get { return stringData; }
    }

    private Dictionary<string, string> stringDataDefault = new Dictionary<string, string>();

    DataBank()
    {
    }

    public void SetData(string _key, string _value)
    {
        if (!stringDataDefault.ContainsKey(_key))
            stringDataDefault[_key] = _value;
        stringData[_key] = _value;
    }

    public void ResetData()
    {
        foreach (string key in stringData.Keys)
            stringData[key] = stringDataDefault[key];
    }

    public string GetData(string _key)
    {
        if (stringDataDefault.ContainsKey(_key))
            return stringData[_key];
        return "";
    }

    public static DataBank Instance
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new DataBank();
                }
                return instance;
            }
        }
    }
}