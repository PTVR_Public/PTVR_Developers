using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using System.IO;
using UnityEngine.UI;

//TO DO
//Relie Button Changer de Json
//Valider et Charger les fichiers json en build et project
//Comment Decliner multiple Dossier

public class TestFolder : MonoBehaviour
{
    #region Inspector
    public Color buttonUse;
    public Color buttonDefault;
    public GameObject buttonFolder;
    public GameObject dropDown;
    public GameObject panelButton;
    public GameObject panelDropDown;

    List<GameObject> buttonList;
    List <TMP_Dropdown> dropdownList;
    #endregion Inspector

    void Start()
    {
        string[] folder = Directory.GetDirectories("PTVR_Researchers/PTVR_Operator/Release/");
        List<string> textList = new List<string>();
        dropdownList = new List<TMP_Dropdown>();
        buttonList = new List<GameObject>();
        for (int count = 0; count < folder.Length; count++)
        {
            string[] textSplit = folder[count].Split('/');
            GameObject button = Instantiate(buttonFolder);
            button.transform.parent = panelButton.transform;
            button.GetComponentInChildren<TextMeshProUGUI>().text = textSplit[textSplit.Length - 1];
            button.GetComponent<Image>().color = buttonDefault;
            button.GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
            buttonList.Add(button);
            string [] fileFolder  = Directory.GetFiles(folder[count]);
            List<string> fileList = new List<string>();
            for (int countSub = 0; countSub < fileFolder.Length ; countSub ++)
            {
                string[] fileFolderSplit = fileFolder[countSub].Split('\\');
                fileList.Add(fileFolderSplit[fileFolderSplit.Length - 1 ]);
            }
            GameObject drop = Instantiate(dropDown);
            drop.transform.parent = panelDropDown.transform;
            drop.GetComponent<TMP_Dropdown>().AddOptions(fileList);
            dropdownList.Add(drop.GetComponent<TMP_Dropdown>());
            button.GetComponent<Button>().onClick.AddListener(delegate { ButtonMenu(drop.GetComponent<TMP_Dropdown>(),button.GetComponent<Image>()); });
            drop.SetActive(false);
        }
        ExpandCheckFolder(folder);
    }

    void ExpandCheckFolder(string[] _folder,GameObject _fatherObject=null)
    {
        for (int count = 0; count < _folder.Length; count++)
        {
            GameObject go = new GameObject();
            if (_fatherObject != null)
                go.transform.parent = _fatherObject.transform;

            string[] textSplit = _folder[count].Split('/');

            if (textSplit[textSplit.Length - 1].Contains("\\"))
            {
                string[] textSplitSub = textSplit[textSplit.Length - 1].Split('\\');
                go.name = textSplitSub[textSplitSub.Length - 1];
            }
            else
            {
                go.name = textSplit[textSplit.Length - 1];
            }
            string[] subFolder = System.IO.Directory.GetDirectories(_folder[count]);
            ExpandCheckFolder(subFolder,go);
        }
    }

    public void ButtonMenu(TMP_Dropdown _drop,Image _buttonImage)
    {
        for(int count = 0; count < dropdownList.Count; count++)
        {
            dropdownList[count].gameObject.SetActive(false);
        }
        for (int count = 0; count < buttonList.Count; count++)
        {
            buttonList[count].GetComponent<Image>().color = buttonDefault;
        }
        _buttonImage.color = buttonUse;
        
        _drop.gameObject.SetActive(true);
    }
}

