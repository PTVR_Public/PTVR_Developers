using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMenuCurve : MonoBehaviour
{
    #region Inspector
    public float spacing;
    public GameObject prefab;
    public int numberFileExample;
    //public List<GameObject> go;
    #endregion Inspector

    #region Privates

    #endregion Privates

    #region UnityCallBack
    void Start()
    {
        for (int count = 0; count < numberFileExample; count++)
        {
            GameObject go = Instantiate(prefab, transform);
            go.transform.localPosition = new Vector3(-count * 35, (spacing * count), 0);
            //go.GetComponent<CanvasGroup>().alpha = 1.0f / (count + 0.15f);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #endregion UnityCallBack
}
