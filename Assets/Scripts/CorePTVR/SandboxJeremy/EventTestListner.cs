using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class EventTestListner : MonoBehaviour
{
    bool spaceIsPress;
    bool escapeIsPress;

    public GameObject one;
    public GameObject two;

    public UnityEvent spacePress;
    public UnityEvent escapePress;
    public UnityEvent spaceRelease;
    public UnityEvent escapeRelease;

    UnityAction action1;
    UnityAction action2;

    void Start()
    {
        action1 = null;
        action1 = new UnityAction(delegate { FunctionOnePress(); SetFunction(); });
        action2 = new UnityAction(delegate { FunctionOneRelease(); UnSetFunction(); });
        spacePress.AddListener(action1);
        spaceRelease.AddListener(action2);

    }

    void Update()
    {
        //Press
       if( Keyboard.current[Key.Space].isPressed)
        {
            if (!spaceIsPress)
            {
                spacePress.Invoke();
                spaceIsPress = true;
            }
        }
       //Release
        if (!Keyboard.current[Key.Space].isPressed)
        {
            if(spaceIsPress)
            {
                spaceRelease.Invoke();
                spaceIsPress = false;
            }
        }

        if (Keyboard.current[Key.Escape].isPressed)
        {
            if (!escapeIsPress)
            {
                escapePress.Invoke();
                escapeIsPress = true;
            }
        }
        //Release
        if (!Keyboard.current[Key.Escape].isPressed)
        {
            if (escapeIsPress)
            {
                escapeRelease.Invoke();
                escapeIsPress = false;
            }
        }
    }

 
    public void FunctionOnePress()
    {
        one.GetComponent<MeshRenderer>().material.color = Color.green;
    }

    public void FunctionOneRelease()
    {
        one.GetComponent<MeshRenderer>().material.color = Color.red;
    }

    public void FunctionTwoPress()
    {
        two.GetComponent<MeshRenderer>().material.color = Color.green;
    }
    public void FunctionTwoRelease()
    {
        two.GetComponent<MeshRenderer>().material.color = Color.red;
    }
    UnityAction action3 = null;
    UnityAction action4 = null;
    public void SetFunction()
    {
        action3 = null;
        action4 = null;
        action4 = new UnityAction(delegate { FunctionTwoRelease(); });
        action3 = new UnityAction(delegate { FunctionTwoPress(); });
        escapePress.AddListener(action3);
        escapeRelease.AddListener(action4);
    }
    public void UnSetFunction()
    {
        escapePress.RemoveListener(action3);
        escapeRelease.RemoveListener(action4);
    }
}
