using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickerTest : MonoBehaviour
{
    public bool isFlicker;
    public Color colorA;
    public Color colorB;
    public float frequence;

    // Update is called once per frame
    void Update()
    {
        if(isFlicker)
        {

            GetComponent<MeshRenderer>().material.color = Color.Lerp(colorA, colorB, Gaussienne());
           
        }
        
    }
    public float Gaussienne()
    {
        float result = -frequence * Mathf.Cos(2 * Mathf.PI * Time.realtimeSinceStartup) + frequence;
        return result;
       
    }
}


