using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestColliderCone : MonoBehaviour
{
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collision)
    {
        Debug.Log("Enter"+collision.gameObject.name);
    }
    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Exit"+
            other.gameObject.name);
    }
}
