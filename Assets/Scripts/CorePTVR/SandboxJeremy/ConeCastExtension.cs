﻿using System.Collections.Generic;
using UnityEngine;

public static class ConeCastExtension
{
    public static RaycastHit[] ConeCastAll(this Physics physics,float maxRadius, Vector3 direction, float maxDistance, float coneAngle)
    {
        RaycastHit[] sphereCastHits = Physics.SphereCastAll(Camera.main.transform.position - new Vector3(0, 0, maxRadius), maxRadius, direction, maxDistance);
        List<RaycastHit> coneCastHitList = new List<RaycastHit>();

        if (sphereCastHits.Length > 0)
        {
            for (int i = 0; i < sphereCastHits.Length; i++)
            {

                //
                Vector3 hitPoint = sphereCastHits[i].point;
                Vector3 directionToHit = hitPoint - Camera.main.transform.position;

                // Angle Between direction head pov , head pov position and PositionHit
                float angleToHit = Vector3.Angle(direction, directionToHit);
                Debug.Log(angleToHit);

                //check if angleToHit is inside the cone Angle
                if (angleToHit <= coneAngle)
                {
                    coneCastHitList.Add(sphereCastHits[i]);
                }
            }
        }

        RaycastHit[] coneCastHits = new RaycastHit[coneCastHitList.Count];
        coneCastHits = coneCastHitList.ToArray();

        return coneCastHits;
    }
}



/*
public static class ConeCastExtension
{
    public static RaycastHit[] ConeCastAll(this Physics physics, Vector3 origin, float maxRadius, Vector3 direction, float maxDistance, float coneAngle)
    {
        RaycastHit[] sphereCastHits = Physics.SphereCastAll(Camera.main.transform.position - new Vector3(0,0,maxRadius), maxRadius, direction, maxDistance);
        List<RaycastHit> coneCastHitList = new List<RaycastHit>();
        
        if (sphereCastHits.Length > 0)
        {
            for (int i = 0; i < sphereCastHits.Length; i++)
            {
                sphereCastHits[i].collider.gameObject.GetComponent<Renderer>().material.color = new Color(1f, 1f, 1f);
                Vector3 hitPoint = sphereCastHits[i].point;
                Vector3 directionToHit = hitPoint - Camera.main.transform.position;
               // float angleToHit = Vector3.Angle(Camera.main.transform.position, directionToHit);
                // float angleToHit = Vector3.Angle(direction, directionToHit);
                //     Debug.Log(angleToHit);
                float angleToHit = Vector3.Angle( A - B directionToHit - Camera.main.transform.position,origin - Camera.main.transform.position C - B );
              //  Debug.Log(angleToHit);
                if (angleToHit < coneAngle)
                {
                    Debug.DrawLine(origin, hitPoint);
                    coneCastHitList.Add(sphereCastHits[i]);
                }
                else
                {
                 //   Debug.Log(angleToHit);
                }
            }
        }

        RaycastHit[] coneCastHits = new RaycastHit[coneCastHitList.Count];
        coneCastHits = coneCastHitList.ToArray();

        return coneCastHits;
    }

}
*/