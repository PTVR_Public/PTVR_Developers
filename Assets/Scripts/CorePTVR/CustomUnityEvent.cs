using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class CustomUnityEvent : UnityEvent
{ 
    private int _listenerCount;

    public int ListenerCount { get { return _listenerCount; } }

    public CustomUnityEvent()
    {
        _listenerCount = 0;
    }

    new public void AddListener(UnityAction call)
    {
        base.AddListener(call);
        _listenerCount++;
    }

    new public void RemoveListener(UnityAction call)
    {
        base.RemoveListener(call);
        _listenerCount--;
    }
}
