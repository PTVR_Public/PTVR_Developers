using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.XR;

//System Data EyeTracking Generalisation
public class EyeTrackingManager : MonoBehaviour
{
    public List<XRNodeState> nodeStates;
    private XRNodeState single_xrnode;

    private Vector3 leftEyePos, rightEyePos, centerEyePos;
    private Quaternion leftEyeRot, rightEyeRot, centerEyeRot;

    // inspiration https://github.com/varjocom/VarjoUnityXRPlugin/blob/master/Samples~/HDRP/EyeTracking/Scripts/EyeTrackingExample.cs//
    private List<InputDevice> devices = new List<InputDevice>();
    private InputDevice device;
    public Vector3 leftEyePosition, rightEyePosition,centerEyePosition;
    public Quaternion leftEyeRotation, rightEyeRotation,centerEyeRotation;

   // public float openAmountLeftEye, openAmountRightEye;
    public Vector3 fixationPoint;
    private Eyes eyes;
    // Start is called before the first frame update
    void Start()
    {
        nodeStates = new List<XRNodeState>();

        // Add left eye node to the nodeStates list, which will be represented by nodeStates[0]
        single_xrnode.nodeType = XRNode.LeftEye;
        nodeStates.Add(single_xrnode);

        // Add right eye node to the nodeStates list, which will be represented by nodeStates[1]
        single_xrnode.nodeType = XRNode.RightEye;
        nodeStates.Add(single_xrnode);
        single_xrnode.nodeType = XRNode.CenterEye;
        nodeStates.Add(single_xrnode);
        /*
        InputDevices.GetDevicesAtXRNode(XRNode.CenterEye, devices);
        device = devices.FirstOrDefault();
        */
    }

    // Update is called once per frame
    void Update()
    {
        InputTracking.GetNodeStates(nodeStates);

        // Try to get the parameters and place them into the correct variables
        nodeStates[0].TryGetPosition(out leftEyePos);
        nodeStates[0].TryGetRotation(out leftEyeRot);


        nodeStates[1].TryGetPosition(out rightEyePos);
        nodeStates[1].TryGetRotation(out rightEyeRot);

        nodeStates[2].TryGetPosition(out rightEyePos);
        nodeStates[2].TryGetRotation(out rightEyeRot);
        
        Debug.Log("Left eye position: " + leftEyePos);
        Debug.Log("Left eye rotation: " + leftEyeRot);
        Debug.Log("Right eye position: " + rightEyePos);
        Debug.Log("Right eye rotation: " + rightEyeRot);
        Debug.Log("Center eye position: " + centerEyePos);
        Debug.Log("Center eye rotation: " + centerEyeRot);
        /*
        if (device.TryGetFeatureValue(CommonUsages.eyesData, out eyes))
        {
            if (eyes.TryGetLeftEyePosition(out leftEyePosition))
            {
                Debug.Log("Device leftEyePosition " + leftEyePosition);
            }

            if (eyes.TryGetLeftEyeRotation(out leftEyeRotation))
            {
                Debug.Log("Device leftEyeRotation " + leftEyeRotation);
            }

            if (eyes.TryGetRightEyePosition(out rightEyePosition))
            {
                Debug.Log("Device rightEyePosition " + rightEyePosition);
            }

            if (eyes.TryGetRightEyeRotation(out rightEyeRotation))
            {
                Debug.Log("Device rightEyeRotation " + rightEyeRotation);
            }

            if (eyes.TryGetFixationPoint(out fixationPoint))
            {
                Debug.Log("Device fixationPoint " + fixationPoint);
            }

        }
        */
    }

    /*
    #region Inspector
    public static EyeTrackingManager instance; // singleton
    public EyesData leftEyes;
    public EyesData rightEyes;
    public Vector3 fixationPoint;

    private Eyes eyes;
    private InputDevice device;
    private List<InputDevice> devices = new List<InputDevice>();
    #endregion Inspector
    void GetDevice()
    {
   
        InputDevices.GetDevicesAtXRNode(XRNode.CenterEye, devices);
        device = devices.FirstOrDefault();
    }

    #region UnityCall
    void Start()
    {
        if (!device.isValid)
        {
            GetDevice();
        }
    }

    void Update()
    {
        if (device.TryGetFeatureValue(CommonUsages.eyesData, out eyes))
        {
            if (eyes.TryGetLeftEyePosition(out leftEyes.eyePosition))
            {

            }

            if (eyes.TryGetLeftEyeRotation(out leftEyes.eyeRotation))
            {

            }

            if (eyes.TryGetRightEyePosition(out rightEyes.eyePosition))
            {

            }

            if (eyes.TryGetRightEyeRotation(out rightEyes.eyeRotation))
            {
            }
            
            if (eyes.TryGetFixationPoint(out fixationPoint))
            {
            }
            
        }
    }
    #endregion UnityCall
    */
}

[Serializable]
public struct EyesData
{
    public float openAmount;
    public Vector3 eyePosition;
    public Quaternion eyeRotation;
}