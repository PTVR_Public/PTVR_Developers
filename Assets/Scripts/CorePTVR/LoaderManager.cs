﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using TMPro;

/// <summary>
/// Read and Choose which Json Open when have multiple in Release
/// </summary>

public class LoaderManager : MonoBehaviour
{
    #region Inspector
    public static LoaderManager instance; // Singleton
    public GameObject buttonFolder;
    public GameObject dropDown;
    public GameObject panelButton;
    public GameObject panelDropDown;
    public PTVR.ExperimentManager experimentManager;
    public PTVR.Core.Experimental.EventManager eventManager;
    public PTVR.Core.Experimental.CallbackManager callbackManager;

    public string projectRoot;

    [SerializeField]
    GameObject canvasJson;

    [SerializeField]
    GameObject buttonLaunchSingle;
    [SerializeField]
    GameObject buttonLaunchMultiple;

    [SerializeField]
    TMP_InputField inputField;

    [HideInInspector]
    public bool isArg;
    public string userId = "DummyUser";
    #endregion Inspector

    #region Privates
    string filename = "demo.json";

    char[] letterPathDecompose;

    bool isEditor;
    public Color buttonUse;
    public Color buttonDefault;

    List<GameObject> buttonList;
    List<TMP_Dropdown> dropdownList;
    string path = string.Empty;
    TMP_Dropdown dropdown = null;
    #endregion Privates

    #region UnityCall
    void Awake()
    {
        if(instance != null)
        {
            Destroy(this);
            Debug.LogWarning("Double Loader.cs");
        }
        else
        {
            instance = this;
        }
#if UNITY_EDITOR
        isEditor = true;
#endif
    }
    // void Start()
    // {
    //     Screen.fullScreen = false;
    //     /* Verify if give argument from spyder using LaunchThe3DWorld depending on argument*/
    //     /* If argument given the ui will not be show and this Load will be quickly finish*/
    //     if (System.Environment.GetCommandLineArgs().Length == 3)
    //     {
    //         filename = System.Environment.GetCommandLineArgs()[1];
    //         userId = System.Environment.GetCommandLineArgs()[2];
    //         canvasJson.SetActive(false);
    //         isArg = true;
    //         GenerateRootProject();
    //         LoadJson(filename);
    //     }
    //     else if (System.Environment.GetCommandLineArgs().Length == 2)
    //     {
    //         filename = System.Environment.GetCommandLineArgs()[1];
    //         canvasJson.SetActive(false);
    //         isArg = true;
    //         GenerateRootProject();
    //         LoadJson(filename);
    //     }
    // }

    public void Load(string path)
    {
        canvasJson.SetActive(false);
        isArg = true;
        GenerateRootProject();
        LoadJson(path);
    }
    #endregion UnityCall

    
    private void GenerateRootProject ()
    {
        string[] splitString = Regex.Split(filename, @"\\");

        projectRoot = splitString[0];
  
        for (int i = 1; i < splitString.Length - 5; i++)
        {
            projectRoot = projectRoot + "\\" + splitString[i];
        }
        if (splitString[splitString.Length - 5] == "PTVR_Researchers")
        {
            projectRoot = projectRoot + "\\" + splitString[splitString.Length - 5];
        }
        Debug.Log(projectRoot);
    }
    
    public void LoadJson(string _filename)
    {
        // check if new callback system is available
        string callbackSystemFilename = Path.Combine(
            Path.GetDirectoryName(_filename),
            Path.GetFileNameWithoutExtension(_filename) + "_Callbacks" + Path.GetExtension(_filename));
        if (System.IO.File.Exists(callbackSystemFilename))
        {
            if (callbackManager != null)
            {
                callbackManager.LoadFromJson(File.ReadAllText(callbackSystemFilename));
            }
        }

        // check if new event system is available
        string eventSystemFilename = Path.Combine(
            Path.GetDirectoryName(_filename),
            Path.GetFileNameWithoutExtension(_filename) + "_Events" + Path.GetExtension(_filename));
        if (System.IO.File.Exists(eventSystemFilename))
        {
            if (eventManager != null)
            {
                eventManager.LoadFromJson(File.ReadAllText(eventSystemFilename));
            }
        }

        // check if experimental design is available
        string xpDesignFilename = Path.Combine(
            Path.GetDirectoryName(_filename),
            Path.GetFileNameWithoutExtension(_filename) + "_XP_Design" + Path.GetExtension(_filename));
        if (System.IO.File.Exists(xpDesignFilename))
        {
            if (experimentManager != null)
            {
                experimentManager.LoadFromJson(File.ReadAllText(xpDesignFilename));
            }
        }

        StreamReader reader; //Reads the experiment description

        Debug.Log("Open experiment :"+_filename);

        reader = new StreamReader(_filename);

        //Read the file
        string readData = reader.ReadToEnd(); //CAUTION: MAY BE A CAUSE OF WORRY FOR VERY LARGE FILE SIZES

        The3DWorldManager.instance.Initialize(readData,userId); // Give the Json selected to the 3D World Manager
        // UIManager.instance.canvas.gameObject.SetActive(false); // Hide UI Selection Json

        if (eventManager != null && eventManager.Loaded)
        {
            eventManager.Initialize();
        }
    }

}

