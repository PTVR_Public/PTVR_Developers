using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraExaminator : MonoBehaviour
{
    #region Inspector
    [SerializeField]
    float maxValue;
    #endregion Inspector

    #region Privates
    bool isEnabled;
    float interpolateTime;
    Camera cam;
    Camera mainCam;
    #endregion Privates

    #region UnityCallBack
    void Start()
    {
        mainCam = Camera.main;
        cam = GetComponent<Camera>();
        cam.enabled = true;
    }

    void Update()
    {
        if (The3DWorldManager.instance.hasModeExaminator)
        {
            if (isEnabled)
            {
                OnDisable();
            }
            else
            {
                OnActivate();
            }
            transform.position = mainCam.transform.position;
            transform.rotation = mainCam.transform.rotation;
            if (cam.backgroundColor != mainCam.backgroundColor)
            {
                cam.backgroundColor = mainCam.backgroundColor;
            }
        }
    }
    #endregion UnityCallBack

    void OnActivate()
    {
        if (interpolateTime<1)
        {
            interpolateTime += Time.deltaTime*2;
        }
        Rect rect = new Rect();
        rect.width = Mathf.Lerp(0, maxValue, interpolateTime);
        rect.height = Mathf.Lerp(0, maxValue, interpolateTime);
        cam.rect = rect; 
      
    }

    void OnDisable()
    {
        if (interpolateTime > 0)
        {
            interpolateTime -= Time.deltaTime*2;
        }

        Rect rect = new Rect();
        rect.width = Mathf.Lerp(0, maxValue, interpolateTime);
        rect.height = Mathf.Lerp(0, maxValue, interpolateTime);
        cam.rect = rect;
    }

    public void Press()
    {
        isEnabled = !isEnabled;
    }
}
