using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleGestion : MonoBehaviour
{
    #region Inspector
    public bool[] isValidate;
    public Toggle[] toggle;
    #endregion Inspector

    #region Privates
    bool isInitialize;
    #endregion Privates

    public void CheckToggles()
    {
        for (int count = 0; count < toggle.Length; count++)
        {
            if (isValidate[count] != toggle[count].isOn)
                isValidate[count] = toggle[count].isOn;
        }
    }

    public void InitializeToggle()
    {
        toggle = GetComponentsInChildren<Toggle>();
        isValidate = new bool[toggle.Length];
        isInitialize = true;
    }
    //TODO: Send Data Essai and after Destroy

    #region UnityCallBack
    void Update()
    {
        if (isInitialize)
            CheckToggles();
    }
    #endregion UnityCallBack
}
