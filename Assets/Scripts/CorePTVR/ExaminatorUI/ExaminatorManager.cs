using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ExaminatorManager : MonoBehaviour
{
    #region Inspector
    public static ExaminatorManager instance; //Singleton

    [SerializeField]
    GameObject prefabButton;

    [SerializeField]
    GameObject prefabToggle;

    [SerializeField]
    GameObject prefabTextUI;

    [SerializeField]
    GameObject prefabPanelExaminator;

    [SerializeField]
    GameObject canvasExaminator;
    #endregion Inspector

    #region Private
    List<string> examinatorHeaders;
    string wordNotPronunciate;
    GameObject panelUI;
    string examinatorLine;
    #endregion Private

    #region UnityCall
    void Awake()
    {
       if(instance!=null)
       {
            Destroy(this);
       }
       else
       {
            instance = this;
       }
    }

    #endregion UnityCall

    public void Initialize()
    {
        examinatorHeaders.Add("Verify Word Not Read");

        SaveHeaderExaminator();
    }

    #region FunctionExperimentCriteria
    public void VerifySentencePronunciation(string _sentence)
    {
        panelUI = Instantiate(prefabPanelExaminator);
        SetUI(panelUI, canvasExaminator);
        string[] word = _sentence.Split(' ');//add /n
        GameObject textUI = Instantiate(prefabTextUI);
        SetUI(textUI,panelUI);
        for (int count = 0; count < word.Length; count++)
        {
            GameObject toggle = Instantiate(prefabToggle);
            SetUI(toggle,panelUI);
            toggle.GetComponentInChildren<TextMeshProUGUI>().text = word[count];
        }
        GameObject button = Instantiate(prefabButton);
        Button btn = button.GetComponent<Button>();
        btn.onClick.AddListener(CleanUI);
        //btn.onClick.AddListener(ExperimentManager.instance.CurrentSceneFinished();
        //Add Scene Data btn

        SetUI(button,panelUI);
        panelUI.GetComponent<ToggleGestion>().InitializeToggle();
        panelUI.GetComponent<ToggleGestion>().CheckToggles();

    }
    #endregion FunctionExperimentCriteria

    void SaveHeaderExaminator()
    {
        examinatorLine = string.Empty;
        for (int count = 0; count < examinatorHeaders.Count; count++)
        {
            examinatorLine = examinatorLine + examinatorHeaders[count] + ";";
        }
    }

    public void SaveExaminatorData()
    {
        examinatorLine = string.Empty;
        for (int count = 0; count < examinatorHeaders.Count; count++)
        {
            examinatorLine = examinatorLine + DataBank.Instance.GetData(examinatorHeaders[count]) +";";
        }
    }

    #region CoreExaminator
    public void CleanUI()
    {
        for (int count = 0; count < panelUI.GetComponent<ToggleGestion>().isValidate.Length; count++)
        {
            if(!panelUI.GetComponent<ToggleGestion>().isValidate[count])
            {
                wordNotPronunciate = wordNotPronunciate + " "+ panelUI.GetComponent<ToggleGestion>().toggle[count].GetComponentInChildren<TextMeshProUGUI>().text;
            }
        }
        //Send to result wordNotPronunciate
        //DataBank.Instance.SetData("Verify Word Not Read", wordNotPronunciate);
        SaveExaminatorData();
        wordNotPronunciate = string.Empty;
        Destroy(panelUI);
    }

    public void SetUI(GameObject _uiObject, GameObject _parentObject)
    {
        _uiObject.transform.SetParent(_parentObject.transform);
        _uiObject.transform.localPosition = new Vector3(0, 0, 0);
        _uiObject.transform.localScale = new Vector3(1, 1, 1);
    }
    #endregion CoreExaminator
}
