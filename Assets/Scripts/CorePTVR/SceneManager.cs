﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PTVR;

public class SceneManager : MonoBehaviour
{
    #region Inspector
    public static SceneManager instance;
    public Transform visualTemplate;
    #endregion Inspector

    #region Privates
    int tempTrial =-2;
    Dictionary<string, bool> currentButton = new Dictionary<string, bool>();
    #endregion Privates

    #region UnityCall
    void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            Debug.LogWarning("Double SceneManager destroy all exept one");
        }
        else
        {
            instance = this;
        }
    }
    #endregion UnityCall

    //----------------- Visual------------------------
    public void PutVisual (PTVR.Stimuli.Objects.VisualScene _visualScene)
    {
        Debug.Log("Start Visual Scene");
        visualTemplate.gameObject.SetActive(true);
        /*Set Fill in Result Column of the scene load*/
        for (int count = 0; count < The3DWorldManager.instance.currScene.GetMetaHeaders().Count; count++)
        {
            DataBank.Instance.SetData(The3DWorldManager.instance.currScene.GetMetaHeaders()[count], The3DWorldManager.instance.currScene.GetMetaStrings()[count]);
        }

        SetVisualSettings.instance.SetScene(_visualScene); // Create VisualObjects inside the Scene
        TimerManager.instance.StopTimerScene();// Stop Timer Scene between scene
         //Set generic fulfilment criteria

        CallbackManager.instance.GestionInputEvent(_visualScene.interaction);//Setup Interaction Events and callback of the scene selected

        

        //if ChangeTrial number inside scene PTVR
        if (The3DWorldManager.instance.currentTrial != tempTrial)
        {
            TimerManager.instance.isTimingTrial = false;
            TimerManager.instance.SetTimerTrial(); 
            tempTrial = The3DWorldManager.instance.currentTrial; 
        }
        TimerManager.instance.SetTimeScene();
        TimerManager.instance.isTimingScene = true;

        // TODO : verifier le timer - pertinent de le laisser dans l 'input ?
    }



    private void UnputChildren(Transform theParent)
    {
        foreach (Transform child in theParent)
        {
            UnputChildren(child);
            child.gameObject.SetActive(false);
        }
    }

    // When a scene is finish clean the scene
    public void UnputVisual(long _idNextScene = 0)
    {
        Debug.Log("End Visual Scene");
        foreach (Transform child in visualTemplate.transform)
        {
            child.gameObject.SetActive(false);
            UnputChildren(child);
        }
        foreach (Transform child in SetVisualSettings.instance.GameObjectCol[-1].transform)
        {
            child.gameObject.SetActive(false);
            UnputChildren(child);
        }
        foreach (Transform child in SetVisualSettings.instance.GameObjectCol[-2].transform)
        {
            child.gameObject.SetActive(false);
            UnputChildren(child);
        }


        SetUISettings.instance.CleanUI();
        GenericFulfilmentCriteria.instance.UnSetFulfilmentVariables();
        CallbackManager.instance.CleanAllEvent();
        FinishThis(_idNextScene);

        //t_scene_finished = ;
    }


    public void PutPointingCursor(List<PointingCursor> _pointingCursor)
    {
    	foreach (PointingCursor pointingCursor in _pointingCursor)
	    {
	      if (pointingCursor == null)
	      {
	    	Debug.LogError("Error: trying to init a null on screen pointer.");
	    	continue;
	      }
            PointingCursorGenerator.instance.StartNewPointer(pointingCursor);
        }
    }

    public void FinishThis(long _idNextScene)
    {
        if (The3DWorldManager.instance.state == SystemState.END_TRIAL_NOW)
        {
            The3DWorldManager.instance.CurrentTrialEnd();
        }
        else if (The3DWorldManager.instance.state == SystemState.RESTART_CURRENT_TRIAL_NOW)
        {
            The3DWorldManager.instance.RestartTrial();
        }
        else if(The3DWorldManager.instance.state == SystemState.END_VISUAL_NOW)
        { 
            The3DWorldManager.instance.CurrentSceneFinished(_idNextScene);
        }
        else if (The3DWorldManager.instance.state == SystemState.RESTART_CURRENT_VISUAL_NOW)
        {
            The3DWorldManager.instance.RestartScene();
        }
    }
}

