using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Valve.VR;

public class TestingDurationFrame : MonoBehaviour
{
    #region Inspector
    public string url;
    public uint lastFrame;
    public float timeBeforeEndFrame;
    public float secondSinceLastVsync;
    #endregion Inspector

    #region Privates
    StreamWriter writer;
    bool hasFinishRecord;
    ulong test;
    #endregion Privates

    void Awake()
    {
        writer = new StreamWriter(url+".csv");
    }

    void OnApplicationQuit()
    {
        writer.Close();
    }
    void Start()
    {
        StartCoroutine("WaitForEndOfFrame");

        QualitySettings.vSyncCount = 1;
        Application.targetFrameRate = Screen.currentResolution.refreshRate;

        Application.targetFrameRate = 90;
    }


    void Update()
    {
        /*
        if(Time.frameCount > Screen.currentResolution.refreshRate)
        {
            hasFinishRecord = true;
        }
        */
        //Verifier avec ffffff
   
     //   if (!hasFinishRecord)
     //   writer.WriteLine("FrameCount;" + Time.frameCount );
       // if (!hasFinishRecord)
       //     writer.WriteLine("Time;" + Time.time);
        long value = long.Parse(System.DateTime.Now.ToString("ss")) * 100000 + long.Parse(System.DateTime.Now.ToString("fffff"));
        if (!hasFinishRecord)
            writer.WriteLine("Update;" + value);
        Debug.Log("Update");
    //    if (!hasFinishRecord)
     //       writer.WriteLine("DeltaTime;" + Time.deltaTime);


        var vr = SteamVR.instance;
        if (vr != null)
        {

            lastFrame = SteamVR.instance.compositor.GetLastFrameRenderer();
            timeBeforeEndFrame = SteamVR.instance.compositor.GetFrameTimeRemaining();
            writer.WriteLine(".GetLastFrameRenderer;" + lastFrame);
            writer.WriteLine("TimeBeforeEndFrame;" + timeBeforeEndFrame);
            SteamVR.instance.hmd.GetTimeSinceLastVsync(ref secondSinceLastVsync, ref test);
            writer.WriteLine("TimeSinceLastVsync;"+ secondSinceLastVsync);
        }
    }

    void LateUpdate()
    {
        long value = long.Parse(System.DateTime.Now.ToString("ss")) * 100000 + long.Parse(System.DateTime.Now.ToString("fffff"));
        if (!hasFinishRecord)
            writer.WriteLine("LateUpdate;" + value);
        Debug.Log("LateUpdate");
    }

    void OnWillRenderObject()
    {
        long value = long.Parse(System.DateTime.Now.ToString("ss")) * 100000 + long.Parse(System.DateTime.Now.ToString("fffff"));
        if (!hasFinishRecord)
       //     writer.WriteLine("OnWillRenderObject;" + value);
        Debug.Log("OnWillRenderObject");
    }

    void OnPreCull()
    {
        long value = long.Parse(System.DateTime.Now.ToString("ss")) * 100000 + long.Parse(System.DateTime.Now.ToString("fffff"));
        if (!hasFinishRecord)
    //        writer.WriteLine("OnPreCull;" + value);
        Debug.Log("OnPreCull");
    }

    void OnBecameVisible()
    {
        long value = long.Parse(System.DateTime.Now.ToString("ss")) * 100000 + long.Parse(System.DateTime.Now.ToString("fffff"));
        if (!hasFinishRecord)
      //      writer.WriteLine("OnWillRenderObject;" + value);
        Debug.Log("OnBecameVisible");
    }

    void OnBecameInvisible()
    {
        long value = long.Parse(System.DateTime.Now.ToString("ss")) * 100000 + long.Parse(System.DateTime.Now.ToString("fffff"));
        if (!hasFinishRecord)
         //   writer.WriteLine("OnBecameInvisible;" + value);
        Debug.Log("OnBecameInvisibl");
    }

    void OnPreRender()
    {
        long value = long.Parse(System.DateTime.Now.ToString("ss")) * 100000 + long.Parse(System.DateTime.Now.ToString("fffff"));
        if (!hasFinishRecord)
       //     writer.WriteLine("OnPreRender;" + value);
        Debug.Log("OnPreRender");
    }

    void OnRenderObject()
    {
        long value = long.Parse(System.DateTime.Now.ToString("ss")) * 100000 + long.Parse(System.DateTime.Now.ToString("fffff"));
        if (!hasFinishRecord)
        //    writer.WriteLine("OnRenderObject;" + value);
        Debug.Log("OnRenderObject");
    }

    void OnPostRender()
    {
        long value = long.Parse(System.DateTime.Now.ToString("ss")) * 100000 + long.Parse(System.DateTime.Now.ToString("fffff"));
        if (!hasFinishRecord)
        //    writer.WriteLine("OnPostRender;" + value);
        Debug.Log("OnPostRender");
    }

    IEnumerator WaitForEndOfFrame()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            long value = long.Parse(System.DateTime.Now.ToString("ss")) * 100000 + long.Parse(System.DateTime.Now.ToString("fffff"));
            if (!hasFinishRecord)
                writer.WriteLine("WaitForNextFrame;" +value);
            Debug.Log("WaitForNextFrame");
        }
    }
}
