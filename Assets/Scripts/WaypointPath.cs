using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointPath : MonoBehaviour
{
    public bool comeBack = false;

    public Transform GetWaypoint(int waypointIndex)
    {
        return transform.GetChild(waypointIndex);
    }

    public int GetNextWaypointIndex(int currentWaypointIndex)
    {
        int nextWaypointIndex = currentWaypointIndex + 1;

        if (comeBack == true)

        {

            if (nextWaypointIndex == transform.childCount)
            {
                nextWaypointIndex = 0;
            }

        } else
        {
            if (nextWaypointIndex == transform.childCount)
            {
                nextWaypointIndex = nextWaypointIndex-1;
            }
        }

        return nextWaypointIndex;
    }
}
