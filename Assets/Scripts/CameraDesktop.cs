using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Camera))]
public class CameraDesktop : MonoBehaviour
{
    #region Inspector
    #endregion Inspector

    #region Privates
    Camera mainCam;
    Camera thisCam;
    bool isFollowCam;

    #endregion Privates

    void Start()
    {
        mainCam = Camera.main;
        thisCam = GetComponent<Camera>();
    }

    void Update()
    {

        if (!The3DWorldManager.instance.hasModeExaminator)
        {
            if (thisCam.backgroundColor != mainCam.backgroundColor)
            {
                thisCam.backgroundColor = mainCam.backgroundColor;
            }

            if (transform.position != mainCam.transform.position)
            {
                transform.position = mainCam.transform.position;
            }

            if (transform.rotation != mainCam.transform.rotation)
            {
                transform.rotation = mainCam.transform.rotation;
            }
        }
        else
        {

        }
    }
 

    public void Setup()
    {
        transform.position = mainCam.transform.position;
        transform.rotation = mainCam.transform.rotation;
        if (thisCam.backgroundColor != mainCam.backgroundColor)
        {
            thisCam.backgroundColor = mainCam.backgroundColor;
        }
    }

    
}
