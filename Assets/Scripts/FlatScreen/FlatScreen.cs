using System.Collections;
using System.Collections.Generic;
using ViveSR.anipal.Eye;
using UnityEngine;
using System;
using System.Text;

using System.IO;

public class FlatScreen : MonoBehaviour
{
    #region Inspector
    public string filenamePrefix;
    public PTVR.Stimuli.Objects.MetaData metaData;
    public int currentSceneNb = 0;
    public bool isGetDataEyes;
    public bool isGetDataHead;
    #endregion Inspector

    #region Privates
    bool isEditor = false;
    StreamWriter outputFile = null;
    Camera mainCamera;
    Vector2 projectedCam = new Vector2();
    Vector2 projectedLeftEye = new Vector2();
    Vector2 projectedRightEye = new Vector2();
    Vector2 projectedCombinedEye = new Vector2();
    Vector3 positionCam = new Vector3();
    Vector3 positionLeftEye = new Vector3();
    Vector3 positionRightEye = new Vector3();
    Vector3 positionCombinedEye = new Vector3();
    DateTime eyeTime;
    bool hasProjection;
    string startDate;
    string fixedUpdateDate;

    #endregion Privates

    #region UnityCall
    private void Awake()
    {
        startDate = System.DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");
#if UNITY_EDITOR
    isEditor = true;
#endif
    }

    void Start()
    {
        mainCamera = Camera.main;
        Setup();
    }

    public void Update()
    {
        fixedUpdateDate = System.DateTime.Now.ToString("yyyy-M-dd--HH-mm-ss.fff");
        hasProjection = false;
        projectedCam = Vector3.zero;
        projectedLeftEye = Vector3.zero;
        projectedRightEye = Vector3.zero;

        DataBank.Instance.SetData("elasped_frames", Time.frameCount.ToString());

        if(isGetDataEyes )
            DoEyeProjectionsAndWrite();
        if (isGetDataHead)
            DoHeadsetProjectionsAndWrite();
  
    }
    #endregion UnityCall

    void Setup()
    {
 
        if (isGetDataEyes || isGetDataHead)
        {
            StringBuilder data = new StringBuilder();
            data.Append("trial;")
                .Append("scene_id;")
                .Append("time_stamp_computer;");

            if (isGetDataHead)
            {
                StringBuilder dataHead = new StringBuilder();
                dataHead.Append(data.ToString())
                        .Append("time_stamp_headset;")
                        .Append("x_headset_pos;")
                        .Append("y_headset_pos;")
                        .Append("z_headset_pos;")
                        .Append("x_headset_forward_direction_projection_on_screen;")
                        .Append("y_headset_forward_direction_projection_on_screen;")
                        .Append("thetax_headset_forward_direction;")
                        .Append("thetay_headset_forward_direction;")
                        .Append("ecc_headset_forward_direction;")
                        .Append("hm_headset_forward_direction");
                ExporterManager.instance.Setup(filenamePrefix+"_headset",dataHead.ToString());
            }
            if (isGetDataEyes)
            {
                StringBuilder dataGaze = new StringBuilder();
                dataGaze.Append(data.ToString())
                        .Append("time_stamp_gaze;")
                        .Append("current_cs_origin_x;")
                        .Append("current_cs_origin_y;")
                        .Append("current_cs_origin_z;")
                        .Append("screen_origin_global_x;")
                        .Append("screen_origin_global_y;")
                        .Append("screen_origin_global_z;")
                        .Append("left_eye_openness_validity;")
                        .Append("left_eye_openness;")
                        .Append("left_eye_wide;")
                        .Append("left_eye_squeeze;")
                        .Append("left_eye_frown;")
                        .Append("left_eye_pupil_position_in_sensor_area_validity;")
                        .Append("left_eye_position_in_sensor;")
                        .Append("left_eye_pupil_diameter_validity;")
                        .Append("left_eye_pupil_diameter_in_mm;")
                        .Append("left_eye_gaze_normalized_vector_validity;")
                        .Append("left_eye_gaze_normalized_vector;")
                        .Append("left_eye_gaze_origin_validity;")
                        .Append("left_eye_gaze_origin_in_mm;")
                        .Append("left_eye_gaze_position_ts_space_x;")
                        .Append("left_eye_gaze_position_ts_space_y;")
                        .Append("right_eye_openness_validity;")
                        .Append("right_eye_openness;")
                        .Append("right_eye_wide;")
                        .Append("right_eye_squeeze;")
                        .Append("right_eye_frown;")
                        .Append("right_eye_pupil_position_in_sensor_area_validity;")
                        .Append("right_eye_position_in_sensor;")
                        .Append("right_eye_pupil_diameter_validity;")
                        .Append("right_eye_pupil_diameter_in_mm;")
                        .Append("right_eye_gaze_normalized_vector_validity;")
                        .Append("right_eye_gaze_normalized_vector;")
                        .Append("right_eye_gaze_origin_validity;")
                        .Append("right_eye_gaze_origin_in_mm;")
                        .Append("right_eye_gaze_position_ts_space_x;")
                        .Append("right_eye_gaze_position_ts_space_y;");
                       // .Append("x_combined_eye_forward_direction_projection_on_screen;")
                        //.Append("y_combined_eye_forward_direction_projection_on_screen;");
                  
                ExporterManager.instance.Setup(filenamePrefix+"_gaze",dataGaze.ToString());
            }
        }
    }

    void DoEyeProjectionsAndWrite()
    {
        int bufferSize = EyesTrackingManager.updateSyncEyeDataColl.Count;// EyesTrackingManager.updateSyncEyeGaze.Count ;
        for (int i = 0; i < bufferSize; i++)
        {
            Vector3 leftGazeOrigin = Utils.instance.RightCoordinatesToLeftCoordinates(EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.left.gaze_origin_mm * 0.001f);
            CastRay(leftGazeOrigin, EyesTrackingManager.instance.updateSyncLeftEyeGaze[i], out projectedLeftEye, out positionLeftEye);

            Vector3 rightGazeOrigin = Utils.instance.RightCoordinatesToLeftCoordinates(EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.right.gaze_origin_mm * 0.001f);
            CastRay(rightGazeOrigin, EyesTrackingManager.instance.updateSyncRightEyeGaze[i], out projectedRightEye, out positionRightEye);
            Vector3 combined = Vector3.zero;
            if ((EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.left.eye_openness + EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.right.eye_openness) > 0)
                combined = ((EyesTrackingManager.instance.updateSyncLeftEyeGaze[i] * EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.left.eye_openness + EyesTrackingManager.instance.updateSyncRightEyeGaze[i]) * EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.right.eye_openness) / (EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.left.eye_openness + EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.right.eye_openness);
            else
                combined = mainCamera.transform.forward;
            CastRay(Vector3.zero, combined, out projectedCombinedEye, out positionCombinedEye);
            EyeDataPTVR eyeDataPTVR = new EyeDataPTVR();
            eyeDataPTVR.eyes = EyesTrackingManager.updateSyncEyeDataColl[i];
            eyeDataPTVR.eyeLeftGazeDirectionValidity = EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.left.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_GAZE_DIRECTION_VALIDITY);
            eyeDataPTVR.eyeLeftGazeOriginValidity = EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.left.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_GAZE_ORIGIN_VALIDITY);
            eyeDataPTVR.eyeLeftOpenessValidity = EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.left.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_EYE_OPENNESS_VALIDITY);

            eyeDataPTVR.eyeLeftPupilPositionInSensorValidity = EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.left.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_PUPIL_POSITION_IN_SENSOR_AREA_VALIDITY);
            eyeDataPTVR.eyeLeftPupilDiameterValidity = EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.left.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_PUPIL_DIAMETER_VALIDITY);

            eyeDataPTVR.eyeRightGazeDirectionValidity = EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.right.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_GAZE_DIRECTION_VALIDITY);
            eyeDataPTVR.eyeRightGazeOriginValidity = EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.right.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_GAZE_ORIGIN_VALIDITY);
            eyeDataPTVR.eyeRightOpenessValidity = EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.right.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_EYE_OPENNESS_VALIDITY);
            eyeDataPTVR.eyeRightPupilPositionInSensorValidity = EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.right.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_PUPIL_POSITION_IN_SENSOR_AREA_VALIDITY);
            eyeDataPTVR.eyeRightPupilDiameterValidity = EyesTrackingManager.updateSyncEyeDataColl[i].verbose_data.right.GetValidity(SingleEyeDataValidity.SINGLE_EYE_DATA_PUPIL_DIAMETER_VALIDITY);

            if (hasProjection)
                WriteProjectionToFileGaze(eyeDataPTVR);
        }
    }

    void CastRay(Vector3 gazeOrigin, Vector3 gazeDirection,out Vector2 projectedEye, out Vector3 positionEye)
    {   // Bit shift the index of the layer (8) to get a bit mask
        int layerMask = 1 << 6;
        RaycastHit hit;
        if (Physics.Raycast(gazeOrigin + mainCamera.transform.position, gazeDirection, out hit, Mathf.Infinity, layerMask))
        {
            if (hit.transform.GetInstanceID() == transform.GetInstanceID())
            {
                hasProjection = true;
                projectedEye = transform.InverseTransformPoint(hit.point);
                projectedEye[0] *= transform.localScale[0];
                projectedEye[1] *= transform.localScale[1];
                positionEye = hit.point;
            }
            else
            {
                projectedEye = Vector2.zero;
                positionEye = Vector3.zero;
            }
        }
        else
        {
            projectedEye = Vector2.zero;
            positionEye = Vector3.zero;
        }
    }

    public void DoHeadsetProjectionsAndWrite()
    {
        CastRay(Vector3.zero, mainCamera.transform.forward, out projectedCam, out positionCam);
        if (projectedCam != Vector2.zero)
        {
            Vector2 camPerimetric = Utils.instance.XYZtoPerimetric(projectedCam.x, projectedCam.y, (transform.position - mainCamera.transform.position).magnitude);
            Vector2 camXYdeg = Utils.instance.XYZtoXYdeg(projectedCam.x, projectedCam.y, (transform.position - mainCamera.transform.position).magnitude);
            //TODO Separate DataBank Header when multiple tangentScreen Record can bug later seems work for the moment
            DataBank.Instance.SetData("time_stamp_headset", System.DateTime.Now.ToString("yyyy-M-dd--HH-mm-ss.fff"));
            int actualid = 0;
            for (int count = 0; count < The3DWorldManager.instance.curr3DWorld.id_scene.Count; count++)
            {
                if (The3DWorldManager.instance.curr3DWorld.id_scene[count] == The3DWorldManager.instance.currScene.id)
                {
                    actualid = count;
                }
            }

            DataBank.Instance.SetData("scene_id", actualid.ToString().ToString());
            DataBank.Instance.SetData("time_stamp_computer", DateTime.Now.ToString("yyyy-M-dd--HH-mm-ss.fff").Replace(",", "."));

            DataBank.Instance.SetData("x_headset_pos", mainCamera.transform.position.x.ToString());
            DataBank.Instance.SetData("y_headset_pos", mainCamera.transform.position.y.ToString());
            DataBank.Instance.SetData("z_headset_pos", mainCamera.transform.position.z.ToString());

            DataBank.Instance.SetData("x_headset_forward_direction_projection_on_screen", projectedCam.x.ToString());
            DataBank.Instance.SetData("y_headset_forward_direction_projection_on_screen", projectedCam.y.ToString());

            DataBank.Instance.SetData("thetax_headset_forward_direction", camXYdeg.x.ToString());
            DataBank.Instance.SetData("thetay_headset_forward_direction", camXYdeg.y.ToString());

            DataBank.Instance.SetData("ecc_headset_forward_direction", camPerimetric.x.ToString());
            DataBank.Instance.SetData("hm_headset_forward_direction", camPerimetric.y.ToString());

            if (isGetDataHead)
            { ExporterManager.instance.WriteFile(filenamePrefix + "_headset"); }
        }
        projectedCam = Vector3.zero;
        hasProjection = false;
    }
    public void WriteProjectionToFileGaze(EyeDataPTVR eyeData)
    {
        if (projectedLeftEye != Vector2.zero ||
        projectedRightEye != Vector2.zero)
        {
            //Gaze
            int actualid = 0;
            for (int count = 0; count < The3DWorldManager.instance.curr3DWorld.id_scene.Count; count++)
            {
                if (The3DWorldManager.instance.curr3DWorld.id_scene[count] == The3DWorldManager.instance.currScene.id)
                {
                    actualid = count;
                }
            }
        
            
            DataBank.Instance.SetData("scene_id", actualid.ToString().ToString());
            DataBank.Instance.SetData("time_stamp_computer", DateTime.Now.ToString("yyyy-M-dd--HH-mm-ss.fff").Replace(",", "."));
            DataBank.Instance.SetData("time_stamp_gaze", eyeData.eyes.timestamp.ToString().Replace(",","."));

            DataBank.Instance.SetData("current_cs_origin_x",transform.parent.GetComponent<PrefabController>().currentCS.pos.x.ToString().Replace(",", "."));
            DataBank.Instance.SetData("current_cs_origin_y", transform.parent.GetComponent<PrefabController>().currentCS.pos.y.ToString().Replace(",", "."));
            DataBank.Instance.SetData("current_cs_origin_z", transform.parent.GetComponent<PrefabController>().currentCS.pos.z.ToString().Replace(",", "."));

            DataBank.Instance.SetData("screen_origin_global_x", transform.position.x.ToString().Replace(",", "."));
            DataBank.Instance.SetData("screen_origin_global_y", transform.position.y.ToString().Replace(",", "."));
            DataBank.Instance.SetData("screen_origin_global_z", transform.position.z.ToString().Replace(",", "."));

            DataBank.Instance.SetData("left_eye_openness_validity", eyeData.eyeLeftOpenessValidity.ToString().Replace(",", "."));
            DataBank.Instance.SetData("left_eye_openness", eyeData.eyes.verbose_data.left.eye_openness.ToString().Replace(",","."));

            DataBank.Instance.SetData("left_eye_pupil_position_in_sensor_area_validity", eyeData.eyeLeftPupilPositionInSensorValidity.ToString().Replace(",", "."));
            DataBank.Instance.SetData("left_eye_position_in_sensor", eyeData.eyes.verbose_data.left.pupil_position_in_sensor_area.ToString("F12").Replace(",", "."));
  
            DataBank.Instance.SetData("left_eye_pupil_diameter_validity", eyeData.eyeLeftPupilDiameterValidity.ToString().Replace(",", ".") );
            DataBank.Instance.SetData("left_eye_pupil_diameter_in_mm", eyeData.eyes.verbose_data.left.pupil_diameter_mm.ToString().Replace(",", "."));

            DataBank.Instance.SetData("left_eye_wide", eyeData.eyes.expression_data.left.eye_wide.ToString("F12").Replace(",", "."));
            DataBank.Instance.SetData("left_eye_squeeze", eyeData.eyes.expression_data.left.eye_squeeze.ToString("F12").Replace(",", "."));
            DataBank.Instance.SetData("left_eye_frown", eyeData.eyes.expression_data.left.eye_frown.ToString("F12").Replace(",", "."));

            DataBank.Instance.SetData("left_eye_gaze_normalized_vector_validity", eyeData.eyeLeftGazeDirectionValidity.ToString().Replace(",", "."));
            DataBank.Instance.SetData("left_eye_gaze_normalized_vector", Utils.instance.RightCoordinatesToLeftCoordinates(eyeData.eyes.verbose_data.left.gaze_direction_normalized).ToString().Replace(",", "."));

            DataBank.Instance.SetData("left_eye_gaze_origin_validity", eyeData.eyeLeftGazeOriginValidity.ToString().Replace(",", "."));
            DataBank.Instance.SetData("left_eye_gaze_origin_in_mm", Utils.instance.RightCoordinatesToLeftCoordinates(Utils.instance.mmInM(eyeData.eyes.verbose_data.left.gaze_origin_mm)).ToString("F12").Replace(",", "."));
                    
            DataBank.Instance.SetData("left_eye_gaze_position_ts_space_x", projectedLeftEye.x.ToString());
            DataBank.Instance.SetData("left_eye_gaze_position_ts_space_y", projectedLeftEye.y.ToString());

            DataBank.Instance.SetData("right_eye_openness_validity", eyeData.eyeRightOpenessValidity.ToString().Replace(",", "."));
            DataBank.Instance.SetData("right_eye_openness", eyeData.eyes.verbose_data.right.eye_openness.ToString().Replace(",", "."));
       
            DataBank.Instance.SetData("right_eye_pupil_position_in_sensor_area_validity", eyeData.eyeRightPupilPositionInSensorValidity.ToString().Replace(",", "."));
            DataBank.Instance.SetData("right_eye_position_in_sensor", eyeData.eyes.verbose_data.right.pupil_position_in_sensor_area.ToString("F12").Replace(",", "."));

            DataBank.Instance.SetData("right_eye_pupil_diameter_validity", eyeData.eyeLeftPupilDiameterValidity.ToString().Replace(",", "."));
            DataBank.Instance.SetData("right_eye_pupil_diameter_in_mm", eyeData.eyes.verbose_data.left.pupil_diameter_mm.ToString().Replace(",", "."));

            DataBank.Instance.SetData("right_eye_wide", eyeData.eyes.expression_data.right.eye_wide.ToString("F12").Replace(",", "."));
            DataBank.Instance.SetData("right_eye_squeeze", eyeData.eyes.expression_data.right.eye_squeeze.ToString("F12").Replace(",", "."));
            DataBank.Instance.SetData("right_eye_frown", eyeData.eyes.expression_data.right.eye_frown.ToString("F12").Replace(",", "."));

            DataBank.Instance.SetData("right_eye_gaze_normalized_vector_validity", eyeData.eyeRightGazeDirectionValidity.ToString().Replace(",", "."));
            DataBank.Instance.SetData("right_eye_gaze_normalized_vector", Utils.instance.RightCoordinatesToLeftCoordinates(eyeData.eyes.verbose_data.right.gaze_direction_normalized).ToString().Replace(",", "."));

            DataBank.Instance.SetData("right_eye_gaze_origin_validity", eyeData.eyeRightGazeOriginValidity.ToString().Replace(",", "."));
            DataBank.Instance.SetData("right_eye_gaze_origin_in_mm", Utils.instance.RightCoordinatesToLeftCoordinates(Utils.instance.mmInM(eyeData.eyes.verbose_data.right.gaze_origin_mm)).ToString("F12").Replace(",", "."));

            DataBank.Instance.SetData("right_eye_gaze_position_ts_space_x", projectedRightEye.x.ToString());
            DataBank.Instance.SetData("right_eye_gaze_position_ts_space_y", projectedRightEye.y.ToString());

            /*
            DataBank.Instance.SetData("x_combined_eye_forward_direction_projection_on_screen", projectedCombinedEye.x.ToString());
            DataBank.Instance.SetData("y_combined_eye_forward_direction_projection_on_screen", projectedCombinedEye.y.ToString());
            */

            if (isGetDataEyes)
            { 
                ExporterManager.instance.WriteFile(filenamePrefix + "_gaze"); 
            }
        }
        projectedLeftEye = Vector3.zero;
        projectedRightEye = Vector3.zero;
        hasProjection = false;
    }
}


