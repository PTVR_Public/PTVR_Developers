using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "BasicUIObjectsData", menuName = "ScriptableObjects/BasicUIObjectsData", order = 1)]
public class BasicUIObjectsData : ScriptableObject
{
    public List<BasicObjects> basicUIObjects;
}
