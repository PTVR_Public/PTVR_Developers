using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
///  Create BasicObjects List in folder for VisualScene
/// </summary>

[CreateAssetMenu(fileName = "BasicObjectsData", menuName = "ScriptableObjects/BasicObjectsData", order = 1)]
public class BasicObjectsData : ScriptableObject
{
    public List <BasicObjects> basicObjects;
}

[Serializable]
public struct BasicObjects
{
    public GameObject prefabObject;
    public string typeObject;
}