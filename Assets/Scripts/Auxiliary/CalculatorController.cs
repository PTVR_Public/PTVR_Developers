using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalculatorController : MonoBehaviour
{
    public PTVR.Stimuli.Objects.Calculator calculatorData;

    public float ResultFloat ()
    {
        switch (calculatorData.calculation)
        {
            case "AngleAtOriginBetweenTwoVisualObjects":
                return AngleAtOriginBetweenTwoVisualObjects();
                break;
            case "RadialDistanceFromOrigin":
                return RadialDistanceFromOrigin();
                break;
            case "AngleAtHeadPOVBetweenTwoPoints":
                return AngleAtHeadPOVBetweenTwoPoints();
                break;
            case "RadialDistanceFromHeadPOV":
                return RadialDistanceFromHeadPOV();
                break;
            case "DistanceBetweenTwoVisualObjects":
                return DistanceBetweenTwoVisualObjects();
                break;
            case "AngleAtApexBetweenTwoVisualObjects":
                return AngleAtApexBetweenTwoVisualObjects();
                break; 
            default:
                Debug.LogError("Calculator:sdqf unknown operation : " + calculatorData.calculation);
                break;
        }
        return 0;
    }

    public Vector3 ResultVector3 ()
    {
        return Vector3.zero;
    }

    float AngleAtOriginBetweenTwoVisualObjects()
    {
        Vector3 origin = SetVisualSettings.instance.origin;
        Vector3 point1 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[0]].transform.position;
        Vector3 point2 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[1]].transform.position;

        point1 = point1 - origin;
        point2 = point2 - origin;

        return Vector3.Angle(point1, point2);
    }

    float RadialDistanceFromOrigin()
    {
        Vector3 origin = SetVisualSettings.instance.origin;
        //The point setup in json
        Vector3 point = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[0]].transform.position;
        //Vector3.Distance(origin,point) = (point  - origin).magnitude
        return (point - origin).magnitude;
    }
    float AngleAtHeadPOVBetweenTwoPoints()
    {
        
        Vector3 headPOV = Camera.main.transform.position;
        //The point1 setup in json
        Vector3 point1 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[0]].transform.position;
        //The point1 setup in json
        Vector3 point2 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[1]].transform.position;

        point1 = point1 - headPOV;
        point2 = point2 - headPOV;

        return Vector3.Angle(point1, point2);
    }

    float RadialDistanceFromHeadPOV()
    {
        Vector3 headPOV = Camera.main.transform.position;
        //The point setup in json
        Vector3 point = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[0]].transform.position;
        //Vector3.Distance(headPOV,point ) = (point  - headPOV).magnitude
        return (point - headPOV).magnitude;
    }

    float DistanceBetweenTwoVisualObjects()
    {
        //The point1 setup in json
        Vector3 point1 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[0]].transform.position;
        //The point2 setup in json
        Vector3 point2 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[1]].transform.position;
        //Vector3.Distance(point1 ,point2) = (point1 - point2).magnitude
        return (point1 - point2).magnitude;
    }


    float AngleAtApexBetweenTwoVisualObjects()
    {   
        //The apex setup in json
        Vector3 vVertex = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[0]].transform.position;
        //The point1 setup in json
        Vector3 point1 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[1]].transform.position;
        //The point2 setup in json
        Vector3 point2 = SetVisualSettings.instance.GameObjectCol[calculatorData.idsForCalculation[2]].transform.position;

        point1 = point1 - vVertex;
        point2 = point2 - vVertex;

        return Vector2.Angle(point1, point2);
    }
}
