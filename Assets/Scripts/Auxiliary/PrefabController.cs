﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PTVR.Stimuli;
using System;
using TMPro;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using System.Linq;

// This scripts is on all objects
public class PrefabController : MonoBehaviour
{
	#region Inspector
	//general data
	public PTVR.Stimuli.Objects.ColoratedObject coloratedObject =null;
	public PTVR.Stimuli.Objects.VirtualPoint objData = null;
	public long parentId;
	public List <Renderer> renderers = new List<Renderer>();
	public GameObject goToFacing;
	public bool isFacing = false;
	public bool onOppositeDirection = false;
	public long idObjectToFacing;

	public bool isFacingPOV = false;
	public bool isFacingPOVatCalibration = false;
	public bool isAutoRadialDistance = false;
	public List<float> perimetricCoordinates;
	public bool isKeepPerimetricAngles = false;
	public Vector3 specificScale = Vector3.one;

	public Vector2 angularSize2d = Vector2.zero;
	public bool angularSizeApprox = false;

	public CurrentCS currentCS = new CurrentCS();

	public float orientationDeg = 0;
	public float angularSize = 0;

	//for flat screen
	public PTVR.Stimuli.Objects.InfiniteCircularCone infiniteCone; //!< The center of visual cone on the screen. The object placed will
	public bool isTangentAngularCorrection = false; //!< Simple correction for the eccentricity positionning. The cone remains circular.

	//for the lines
	public Transform startPoint = null, endPoint = null;

	public PTVR.Stimuli.Objects.VirtualPoint posUI;

	#region Color
	public PTVR.Stimuli.Color actualColor = null;
	public PTVR.Stimuli.Color defaultColor = null;
	public UnityEngine.Color defaultOutlineColor;
	#endregion Color
	public UnityEngine.Color originOutlineColor;
    public float originOutlineWidth ;
	public bool defaultVisiblity;

	public bool isAlreadySetup;
	/// <summary>
	/// Text
	/// </summary>
	public float originFontSize; // For Text

	public Vector3 defaultScale;

	public Vector3 defaultScaledTransformSize;


	public float defaultSpeed=0;
	// Translation movement
	public float speed = 0;
	public bool isMoving = false;
	public bool forward_movement = false;
	public bool reverse_movement = false;
	public bool upward_movement = false;
	public bool downward_movement = false;
	public bool left_movement = false;
	public bool right_movement = false;

	// Rotation movement
	public bool left_rotate = false;
	public bool right_rotate = false;

	// Periodic movement
	public Vector3 start_periodic_position;
	public Vector3 end_periodic_position;
	public float period = 0;
	public float count_period = 0;
	public string periodic_function;
	public bool flag_periodic_movement;
	public bool flag_start_periodic_movement = false;
	public Vector3 periodic_movement;
	public Vector3 last_periodic_movement;
	public bool flag_periodic_function_up = false;
	public bool flag_periodic_function_down = false;
	public bool look_in_movement_direction = false;
	public string stop_mode = "reset";


	public bool isGrabable = false;
	public bool isGrabed = false;
	public bool isAttractor = false;
	public bool isAttractive = false;
	public bool isAttractortaken = false;
	public bool isInAttractor = false;
	public int AttractorIndex = 0;
	public bool ResetPos = false;
	public bool isStatic = false;
	public Vector3 attraction_distance;
	public string stringInAttractor = "";
	public bool isRead = false;
	public TMP_Text textRead;
	public long readsupportId = -1;
	public string readString = null;
	public string expectedString = null;
	public bool readingAttractor = false;
	public long readingAttractorID = -1;
	public Vector3 initialPos;
	public Vector3 initialPosForMovement;
	public Vector3 currentPosForMovement;
	public Quaternion initialRot;

	public List<Transform> AttractorsTransformList;
	public List<PrefabController> AttractorsPCList;
	public Vector3 object_size_in_meters;
	public bool first_staircase = true;
	public int current_staircase_id = 1;

	public Dictionary<string, Periodic_function> dict_periodic_functions;

	public string defaultText = "";
	#endregion Inspector

	#region Privates
	float currentColorDeltaTime = 0;
	double lastTime;
	IEnumerator dynaColorCoroutine = null;
	static Transform onScreenTool = null; 
	static Transform pointingTool = null;
	static Transform positionTool = null;
	TMP_Text textmeshpro;
	string originalText;


	#endregion Privates

	#region UnityCall
	void Awake()
	{

		if (onScreenTool == null)
			onScreenTool = new GameObject().transform;
		if (pointingTool == null)
			pointingTool = new GameObject().transform;
		if (positionTool == null)
			positionTool = new GameObject().transform;

	}

    private void OnEnable()
    {
		
		if (GetComponentInChildren<Renderer>() != null)
		{
			if (defaultColor != null)
			{
				GetComponentInChildren<Renderer>().material.color = defaultColor.ToUnityColor();
				GetComponentInChildren<Renderer>().enabled = defaultVisiblity;
				actualColor = defaultColor;
				GetComponentInChildren<Renderer>().material.EnableKeyword("_EMISSION");
				GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", defaultColor.ToUnityColor());
			}
		}
	}
    void OnDisable()
	{

		if (transform.Find("ScaledTransform"))
		{
			Outline[] outlines = transform.Find("ScaledTransform").GetComponentsInChildren<Outline>();

			foreach (Outline line in outlines)
			{
				line.OutlineColor = defaultOutlineColor;
				line.OutlineWidth = 0;
				line.GetComponent<MeshRenderer>().material.color = coloratedObject.color.ToUnityColor();
				line.GetComponent<MeshRenderer>().enabled = defaultVisiblity;
			}
		}
		else
		{
			if (GetComponent<Outline>() != null)
			{
				GetComponent<Outline>().OutlineColor = defaultOutlineColor;
				GetComponent<Outline>().OutlineWidth = 0;
			}
			if (renderers.Count > 0)
			{
				ResetColor();
				if (GetComponentInChildren<MeshRenderer>() != null)
				{
					GetComponentInChildren<MeshRenderer>().material.color = defaultColor.ToUnityColor();
					actualColor = defaultColor;
					GetComponentInChildren<MeshRenderer>().enabled = defaultVisiblity;
					GetComponentInChildren<MeshRenderer>().material.EnableKeyword("_EMISSION");
					GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", defaultColor.ToUnityColor());
				}
			}	
		}

		if (GetComponent<Image>() != null)
		{
			GetComponent<Image>().color = defaultColor.ToUnityColor();
		}
		
	}

	public void Setup()
    {

		periodic_movement = new Vector3(0, 0, 0);

		last_periodic_movement = new Vector3(0, 0, 0);

		dict_periodic_functions = new Dictionary<string, Periodic_function>();

		initialPos = new Vector3(transform.position[0], transform.position[1], transform.position[2]);

		initialPosForMovement = new Vector3(transform.position[0], transform.position[1], transform.position[2]);

		initialRot = new Quaternion(transform.rotation[0], transform.rotation[1], transform.rotation[2], transform.rotation[3]);

		currentPosForMovement = new Vector3(transform.position[0], transform.position[1], transform.position[2]);

		if (!isAlreadySetup)
        {
			isAlreadySetup = true;
        }
		if (coloratedObject != null)
		{
			defaultColor = coloratedObject.color;
			defaultColor.type = coloratedObject.color.type;
		}
		/*if (transform.GetChild(0) != null && transform.GetChild(0).GetComponentInChildren<Outline>()!=null)
		{
			originOutlineColor = transform.GetChild(0).GetComponentInChildren<Outline>().OutlineColor;
			originOutlineWidth = transform.GetChild(0).GetComponentInChildren<Outline>().OutlineWidth;
		}*/
		if (isFacing)
		{
			if (idObjectToFacing < 0)
			{
				GameObject go = goToFacing;

				PointToLookAt(go);
			}
			else
			{
				PointToLookAt(SetVisualSettings.instance.GameObjectCol[idObjectToFacing]);
			}
		}
		Update();
		if (coloratedObject != null)
		{
			actualColor = defaultColor;
			actualColor.type = defaultColor.type;
		}
		if (GetComponent<TMP_Text>() != null)
		{
			textmeshpro = GetComponent<TMP_Text>();
			defaultText = textmeshpro.text;
			//Debug.Log(defaultText);
			originFontSize = textmeshpro.fontSize;


		}
		if (renderers.Count > 0)
		{
			if (GetComponentInChildren<MeshRenderer>() != null)
			{
			//	defaultVisiblity = GetComponentInChildren<MeshRenderer>().enabled;

				/*Outline line = GetComponent<Outline>();
				line.OutlineWidth = 0;
				*/
			}
		}
		if(GetComponent<Outline>() !=null)
        {
			GetComponent<Outline>().OutlineWidth = 0;
		}
		if (transform.Find("ScaledTransform"))
		{
			Outline[] outlines = transform.Find("ScaledTransform").GetComponentsInChildren<Outline>();

			foreach (Outline line in outlines)
			{
				line.OutlineWidth = 0;
			
				defaultVisiblity = coloratedObject.isVisible;
			}
			
		}

	}

    public void Start()
	{



		if (isFacing)
		{
			if (idObjectToFacing < 0)
			{
				GameObject go = goToFacing;

				PointToLookAt(go);
			}
			else
			{
				PointToLookAt(SetVisualSettings.instance.GameObjectCol[idObjectToFacing]);
			}
		}
		//ResetColor();
		SetAngularSize2D();
		PrepareCAPOC();
		if (isAutoRadialDistance)
		{
			CalculatePerimetricPositionOnScreen(true);
		}
		if (GetComponent<TMP_Text>() != null)
	  	 {
			textmeshpro = GetComponent<TMP_Text>();
			originalText = textmeshpro.text;
			while (NewDoTextWorks()) ;
			while (DoTextWorks()) ;
		 }
		if (objData != null)
			DoCoordinatedWork();
		UpdateLineStartEndPoints();
		lastTime = System.DateTime.Now.Ticks / System.TimeSpan.TicksPerMillisecond;
		if (coloratedObject != null)
		{
			//Debug.Log(coloratedObject.isVisible);
			if (coloratedObject.isVisible == false)
			{
				if (GetComponentInChildren<MeshRenderer>() != null)
					GetComponentInChildren<MeshRenderer>().enabled = coloratedObject.isVisible;
			}
			else
			{
				if (GetComponentInChildren<MeshRenderer>() != null)
					GetComponentInChildren<MeshRenderer>().enabled = coloratedObject.isVisible;
			}
		}
	}

	void handle_movement()
    {
		if (isMoving)
		{
			//yourDictionary.Keys.ToArray()
			foreach (string key_dict in dict_periodic_functions.Keys.ToArray())
			//foreach (string key_dict in new List<string>{ "sinusoidal", "triangle", "square"})
            {
				//var key_dict_string = key_dict.Split(new[] { "_" }, StringSplitOptions.None)[0];

				//Debug.Log(key_dict_string);

				if (dict_periodic_functions.ContainsKey(key_dict))
                {

					currentPosForMovement = transform.position;

					if (dict_periodic_functions[key_dict].flag_periodic_movement)
					{
						if (dict_periodic_functions[key_dict].flag_start_periodic_movement)
						{
							dict_periodic_functions[key_dict].periodic_movement = dict_periodic_functions[key_dict].end_periodic_position - dict_periodic_functions[key_dict].start_periodic_position;

							dict_periodic_functions[key_dict].flag_start_periodic_movement = false;

							//Debug.Log(stop_mode);

							if (stop_mode.Equals("reset"))

							{
								//Debug.Log("reset movement");
								dict_periodic_functions[key_dict].count_period = 0;

								dict_periodic_functions[key_dict].last_periodic_movement = new Vector3(0, 0, 0);

								transform.position = initialPos;

							} else
                            {

								//transform.position = currentPosForMovement;
							}

						}

						var periodic_step = 0f;
						var movement_direction = new Vector3(0, 0, 0);

						if (key_dict.Contains("sinusoidal"))
						{

							periodic_step = 0.5f * (1.0f + Mathf.Sin(Mathf.PI * (2.0f * dict_periodic_functions[key_dict].count_period - 0.5f)));

							movement_direction = (dict_periodic_functions[key_dict].periodic_movement * periodic_step) - dict_periodic_functions[key_dict].last_periodic_movement;

							transform.position += (dict_periodic_functions[key_dict].periodic_movement * periodic_step) - dict_periodic_functions[key_dict].last_periodic_movement;

							dict_periodic_functions[key_dict].count_period += Time.deltaTime / dict_periodic_functions[key_dict].period;

						}

						if (key_dict.Contains("triangle"))
						//if (dict_periodic_functions[key_dict].periodic_function == "triangle")
                        {

                            periodic_step = 2 * dict_periodic_functions[key_dict].count_period;

                            movement_direction = (dict_periodic_functions[key_dict].periodic_movement * periodic_step) - dict_periodic_functions[key_dict].last_periodic_movement;

                            transform.position += (dict_periodic_functions[key_dict].periodic_movement * periodic_step) - dict_periodic_functions[key_dict].last_periodic_movement;


                            if (dict_periodic_functions[key_dict].flag_periodic_function_up)

                            {
                                if (dict_periodic_functions[key_dict].count_period >= 0.5)

                                {
                                    dict_periodic_functions[key_dict].flag_periodic_function_up = false;
                                }
                            }
                            else
                            {
                                if (dict_periodic_functions[key_dict].count_period <= 0.0)

                                {
                                    dict_periodic_functions[key_dict].flag_periodic_function_up = true;
                                }
                            }

                            if (dict_periodic_functions[key_dict].flag_periodic_function_up)
                            { dict_periodic_functions[key_dict].count_period += Time.deltaTime / dict_periodic_functions[key_dict].period; }
                            else
                            { dict_periodic_functions[key_dict].count_period -= Time.deltaTime / dict_periodic_functions[key_dict].period; }

                        }

						//triangle
						if (key_dict.Contains("square"))
						//if (dict_periodic_functions[key_dict].periodic_function == "square")
                        {

                            if (dict_periodic_functions[key_dict].count_period >= 0.5)

                            {
                                periodic_step = -1;
                                movement_direction = (dict_periodic_functions[key_dict].periodic_movement * periodic_step) - dict_periodic_functions[key_dict].last_periodic_movement;
                                transform.position += (dict_periodic_functions[key_dict].periodic_movement * periodic_step) - dict_periodic_functions[key_dict].last_periodic_movement;
                                dict_periodic_functions[key_dict].flag_periodic_function_up = false;

                            }

                            if (dict_periodic_functions[key_dict].count_period <= 0.0)

                            {
                                periodic_step = 1;
                                movement_direction = (dict_periodic_functions[key_dict].periodic_movement * periodic_step) - dict_periodic_functions[key_dict].last_periodic_movement;
                                transform.position += (dict_periodic_functions[key_dict].periodic_movement * periodic_step) - dict_periodic_functions[key_dict].last_periodic_movement;
                                dict_periodic_functions[key_dict].flag_periodic_function_up = true;
                            }

                            if (dict_periodic_functions[key_dict].flag_periodic_function_up)
                            {
                                dict_periodic_functions[key_dict].count_period += Time.deltaTime / dict_periodic_functions[key_dict].period;
                            }
                            else
                            { dict_periodic_functions[key_dict].count_period -= Time.deltaTime / dict_periodic_functions[key_dict].period; }

                        }

                        if (dict_periodic_functions[key_dict].look_in_movement_direction)
                        {
                            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movement_direction), 0.15F);
                        }

                        dict_periodic_functions[key_dict].last_periodic_movement = dict_periodic_functions[key_dict].periodic_movement * periodic_step;

					}
				}
            }

			// 
			if (forward_movement)
			{
				transform.position += transform.forward * Time.deltaTime * speed;
			}
			if (reverse_movement)
			{
				transform.position -= transform.forward * Time.deltaTime * speed;
			}

			if (upward_movement)
			{
				transform.position += new Vector3(0, 1, 0) * Time.deltaTime * speed;
			}
			if (downward_movement)
			{
				transform.position -= new Vector3(0, 1, 0) * Time.deltaTime * speed;
			}

			if (right_movement)
			{
				transform.position += transform.right * Time.deltaTime * speed;
			}
			if (left_movement)
			{
				transform.position -= transform.right * Time.deltaTime * speed;
			}

			if (left_rotate)
			{
				transform.Rotate(new Vector3(0, -0.75f, 0));
			}

			if (right_rotate)
			{
				transform.Rotate(new Vector3(0, 0.75f, 0));
			}
		}



	}

    private void LateUpdate()
    {

		if (isStatic)
		{
			transform.position = initialPos;
			transform.rotation = initialRot;
		}

		if (ResetPos) // Move back object to initial position
        {
			if (!isInAttractor)
            {
				transform.position = initialPos;
			}
			
		}

		//Debug.Log(transform.name);
		//Debug.Log(defaultScaledTransformSize);

		if (!defaultScaledTransformSize.Equals(new Vector3(0,0,0)))
        {
			Debug.Log("LateUpdate");
			Debug.Log(transform.name);
			Debug.Log(defaultScaledTransformSize);
			transform.localScale = defaultScaledTransformSize;

		}

	}

	void handle_attraction()
	{
		if (isAttractive)
        {
			if (!isGrabed)
            {
				if (!isInAttractor)
				{
					var distances = new List<float>();
					var distances_3d = new List<List<float>>();
					for (int i = 0; i < AttractorsTransformList.Count; i++)
					{
						if (AttractorsPCList[i].isAttractortaken == false)
						{
							var distance_to_attractor = Vector3.Distance(AttractorsTransformList[i].position, transform.position);
							distances.Add(distance_to_attractor);

							distances_3d.Add(new List<float>() { Math.Abs(AttractorsTransformList[i].position.x - transform.position.x),
																 Math.Abs(AttractorsTransformList[i].position.y - transform.position.y),
																 Math.Abs(AttractorsTransformList[i].position.z - transform.position.z)});
						}
						else
						{
							distances.Add(100.0f);
							distances_3d.Add(new List<float>() { 100, 100, 100 });
						}
					}

					var minimum_distance = distances.Min();
					var minimum_distance_3d = distances_3d[distances.IndexOf(minimum_distance)];

					// 3d distance for attraction
					if ((minimum_distance_3d[0] <= attraction_distance[0]) & 
						(minimum_distance_3d[1] <= attraction_distance[1]) & 
						(minimum_distance_3d[2] <= attraction_distance[2]))
					{
						AttractorIndex = distances.IndexOf(minimum_distance);
						transform.position = AttractorsTransformList[AttractorIndex].position + new Vector3(0, 0.05f, 0);
						AttractorsPCList[AttractorIndex].isAttractortaken = true;
						isInAttractor = true;

						// Retrieve letter landing on attractor
						AttractorsPCList[AttractorIndex].stringInAttractor = transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text;

						var string_in_attractors = "";

						if (isRead)
                        {
							// Read all the attractors and put it in the read_string
							for (int i = 0; i < AttractorsPCList.Count; i++)

								string_in_attractors += AttractorsPCList[i].stringInAttractor;

							SetVisualSettings.instance.GameObjectCol[readingAttractorID].GetComponent<PrefabController>().readString = string_in_attractors;

						}

						// Play sound when inAttractor
						transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<AudioSource>().Play();
					}
				}
			} 
			else
            {
				if (isInAttractor)
				{
					// remove string in attractor when letter is removed
					isInAttractor = false;
					AttractorsPCList[AttractorIndex].isAttractortaken = false;
					AttractorsPCList[AttractorIndex].stringInAttractor = "";//transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text;

					var string_in_attractors = "";

					if (isRead)
					{
						// Read all the attractors and put it in the read_string
						for (int i = 0; i < AttractorsPCList.Count; i++)

							string_in_attractors += AttractorsPCList[i].stringInAttractor;

						SetVisualSettings.instance.GameObjectCol[readingAttractorID].GetComponent<PrefabController>().readString = string_in_attractors;
					}
				}
			}
        }

		// if this attractor is reading other attractors letters
		if (readingAttractor)
        {

			// Display text in FlatScreen
			var match = expectedString.Equals(readString);

			textRead.text = "Mot écrit : " + readString + "\n" + "Mot attendu : " + expectedString + "\nMatch: " + match.ToString();

		}
	}

	void Update()
	{
		handle_movement();

		handle_attraction();



		if (isFacing)
        {
			if (idObjectToFacing < 0)
			{
				GameObject go = goToFacing;
				
				PointToLookAt(go);
			}
			else
			{
				PointToLookAt(SetVisualSettings.instance.GameObjectCol[idObjectToFacing]);
			}
        }

		if (isKeepPerimetricAngles)
			CalculatePerimetricPositionOnScreen();
		if (coloratedObject != null)
		{
			if (actualColor != null)
			{
				if (actualColor.type == "dynamicColor")
				{
					Debug.Log("yes");
					PTVR.Stimuli.DynamicColor dynamiColor = (PTVR.Stimuli.DynamicColor)actualColor;
					if (dynamiColor.colorCollection.Count != 0)
						NextDynamicColor(dynamiColor);
				}
                else
                {

					if (transform.GetComponent<TMP_Text>() != null)
					{
						transform.GetComponent<TMP_Text>().color = actualColor.ToUnityColor();
					}
					else if (transform.GetComponent<LineRenderer>() != null)
					{
						transform.GetComponent<LineRenderer>().startColor = actualColor.ToUnityColor();
						transform.GetComponent<LineRenderer>().endColor = actualColor.ToUnityColor();
					}
					else if (transform.GetComponent<SpriteRenderer>() != null)
					{
						transform.GetComponent<SpriteRenderer>().color = actualColor.ToUnityColor();

					}
					else if (transform.GetComponent<Image>() != null)
					{
						transform.GetComponent<Image>().color = actualColor.ToUnityColor();

					}
					else if (transform.GetComponent<RawImage>() != null)
					{
						transform.GetComponent<RawImage>().color = actualColor.ToUnityColor();
					}
					else
					{
						if (transform.GetChild(0).GetChild(0).GetComponent<Renderer>() != null)
						{
							transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.color = actualColor.ToUnityColor();
							transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
							transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.SetColor("_EmissionColor", actualColor.ToUnityColor());
						}

					}
			
                }
			}
		}	
			if (textmeshpro != null)
		{
			if(textmeshpro.text != originalText)
				textmeshpro.text = originalText;
			while (NewDoTextWorks()) ;
			while (DoTextWorks()) ;
		}
		if (objData != null)
			DoCoordinatedWork();
		UpdateLineStartEndPoints();
		if(posUI !=null)
			CheckPositionUI();
	}

	#endregion UnityCall

	public void ResetObject()
    {
		
    }

	/*TODO See if is relevant to developpe Scale and Position*/
    #region Scale
	public void SetupScale()
    {

    }

	#endregion Scale

	#region Position
	public void SetupPosition()
	{

	}
	#endregion Position

	#region Color
	public void CheckColor(PTVR.Stimuli.Color _color)
	{
		switch (_color.type)
		{
			case "rgbcolor":
				Debug.Log("rgbcolor");
				break;

			case "rgb255color":
				Debug.Log("rgb255color");
				break;

			case "hexcolor":
				Debug.Log("hexcolor");
				break;
			case "dynamicColor":
				Debug.Log("dynamicColor");
				break;
		}
	}

	void NextDynamicColor(DynamicColor _color)
	{
		double currentTime = System.DateTime.Now.Ticks / System.TimeSpan.TicksPerMillisecond;

		if (lastTime + currentColorDeltaTime > currentTime)
			return;
		KeyValuePair<float, PTVR.Stimuli.Color> nextColorPair = _color.GetNextTimeColor();

		if (_color.Counter == 1)
		{
			if (transform.GetComponent<TMP_Text>() != null)
			{
				transform.GetComponent<TMP_Text>().color = nextColorPair.Value.ToUnityColor();
			}
			else if (transform.GetComponent<LineRenderer>() != null)
			{
				transform.GetComponent<LineRenderer>().startColor = nextColorPair.Value.ToUnityColor();
				transform.GetComponent<LineRenderer>().endColor = nextColorPair.Value.ToUnityColor();
			}
			else if (transform.GetComponent<SpriteRenderer>() != null)
			{
				transform.GetComponent<SpriteRenderer>().color = nextColorPair.Value.ToUnityColor();

			}
			else if (transform.GetComponent<Image>() != null)
			{
				transform.GetComponent<Image>().color = nextColorPair.Value.ToUnityColor();

			}
			else if (transform.GetComponent<RawImage>() != null)
			{
				transform.GetComponent<RawImage>().color = nextColorPair.Value.ToUnityColor();
			}
			else
			{
				transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.color = nextColorPair.Value.ToUnityColor();
				transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
				transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material.SetColor("_EmissionColor", nextColorPair.Value.ToUnityColor());

			}
			lastTime = System.DateTime.Now.Ticks / System.TimeSpan.TicksPerMillisecond;

		}
		else
		{
			Debug.Log(nextColorPair.Value);
			dynaColorCoroutine = FluidColorChange(nextColorPair.Key - currentColorDeltaTime, nextColorPair.Value);
			StartCoroutine(dynaColorCoroutine);
			lastTime = System.DateTime.Now.Ticks / System.TimeSpan.TicksPerMillisecond;
		}
		currentColorDeltaTime = nextColorPair.Key;
	}

	public void StopDynamicColor(DynamicColor _color)
	{

		StopAllCoroutines();
		currentColorDeltaTime = 0;
		lastTime = System.DateTime.Now.Ticks / System.TimeSpan.TicksPerMillisecond;
		_color.Restart();

	}

	public IEnumerator FluidColorChange(float _nextTime, PTVR.Stimuli.Color _nextColor)
	{

		if (transform.GetComponent<TMP_Text>() != null)
		{
			TMP_Text rend = transform.GetComponent<TMP_Text>();
			Vector4 previousColor = rend.color;

			Vector4 diffColor = (Vector4)(_nextColor.ToUnityColor()) - previousColor;
			float localDelta = Time.deltaTime / _nextTime * 1000;

			for (var dt = 0.0f; dt < 1f; dt += localDelta)
			{
				previousColor = previousColor + diffColor * localDelta;
				rend.color = previousColor;
				yield return null;
			}
		}
		else if (transform.GetComponent<LineRenderer>() != null)
		{
			LineRenderer rend = transform.GetComponent<LineRenderer>();
			Vector4 previousColor = rend.startColor;

			Vector4 diffColor = (Vector4)(_nextColor.ToUnityColor()) - previousColor;
			float localDelta = Time.deltaTime / _nextTime * 1000;

			for (var dt = 0.0f; dt < 1f; dt += localDelta)
			{
				previousColor = previousColor + diffColor * localDelta;
				rend.startColor = previousColor;
				rend.endColor = previousColor;
				yield return null;
			}
		}
		else if (transform.GetComponent<SpriteRenderer>() != null)
		{
			SpriteRenderer rend = transform.GetComponent<SpriteRenderer>();
			Vector4 previousColor = rend.color;


			Vector4 diffColor = (Vector4)(_nextColor.ToUnityColor()) - previousColor;
			float localDelta = Time.deltaTime / _nextTime * 1000;

			for (var dt = 0.0f; dt < 1f; dt += localDelta)
			{
				previousColor = previousColor + diffColor * localDelta;
				rend.color = previousColor;
				yield return null;
			}


		}
		else if (transform.GetComponent<Image>() != null)
		{
			Image rend = transform.GetComponent<Image>();
			Vector4 previousColor = rend.color;


			Vector4 diffColor = (Vector4)(_nextColor.ToUnityColor()) - previousColor;
			float localDelta = Time.deltaTime / _nextTime * 1000;

			for (var dt = 0.0f; dt < 1f; dt += localDelta)
			{
				previousColor = previousColor + diffColor * localDelta;
				rend.color = previousColor;
				yield return null;
			}
		}
		else if (transform.GetComponent<RawImage>() != null)
		{
			RawImage rend = transform.GetComponent<RawImage>();
			Vector4 previousColor = rend.color;


			Vector4 diffColor = (Vector4)(_nextColor.ToUnityColor()) - previousColor;
			float localDelta = Time.deltaTime / _nextTime * 1000;

			for (var dt = 0.0f; dt < 1f; dt += localDelta)
			{
				previousColor = previousColor + diffColor * localDelta;
				rend.color = previousColor;
				yield return null;
			}
		}
		else
		{

			Renderer rend = transform.GetChild(0).GetChild(0).GetComponent<Renderer>();
			Vector4 previousColor = rend.material.color;

			//Vector4 diffColor = UnityEngine.Color.Lerp(_nextColor.ToUnityColor(), previousColor, 0);
			Vector4 diffColor = (Vector4)(_nextColor.ToUnityColor()) - previousColor;
			float localDelta = Time.deltaTime / _nextTime * 1000;

			for (var dt = 0.0f; dt < 1f; dt += localDelta)
			{
				previousColor = previousColor + diffColor * localDelta;
				rend.material.color = previousColor;
				rend.material.EnableKeyword("_EMISSION");
				rend.material.SetColor("_EmissionColor", _nextColor.ToUnityColor());
				yield return null;

			}
		}



	}

	public void SetupColor(PTVR.Stimuli.Color _color)
	{
		switch (_color.type)
		{
			case "rgbcolor":
				RGBColor rgbColor = (PTVR.Stimuli.RGBColor)_color;
				//defaultColor = rgbColor;
				break;
			case "rgb255color":
				RGB255Color rgb255Color = (PTVR.Stimuli.RGB255Color)_color;
				//defaultColor = rgb255Color;
				break;
			case "hexcolor":
				HexColor hexColor = (PTVR.Stimuli.HexColor)_color;
				//defaultColor = hexColor;
				break;
			case "dynamicColor":
				DynamicColor dynamicColor = (PTVR.Stimuli.DynamicColor)_color;
				//defaultColor = dynamicColor;
				break;
		}
	}
	void ResetColor()
    {
		if(isAlreadySetup)
		{ if (renderers.Count > 0)
			{
				if (actualColor.type == "dynamicColor" && actualColor != defaultColor)
				{
					PTVR.Stimuli.DynamicColor dynamiColor = (PTVR.Stimuli.DynamicColor)(actualColor);
					Debug.Log("Deactivate" + dynamiColor);

					StopDynamicColor(dynamiColor);
					actualColor = defaultColor;
					for (int count = 0; renderers.Count > count; count++)
					{
						renderers[count].material.color = actualColor.ToUnityColor();
						renderers[count].material.EnableKeyword("_EMISSION");
						renderers[count].material.SetColor("_EmissionColor", actualColor.ToUnityColor());
					}
				}
			}
		}
		
	}
	#endregion Color

	#region Text
	public void ChangeText(string _myNewText, float _myNewFontSize)
	{
		originalText = _myNewText;
		if (GetComponent<TextMeshPro>() != null)
		{
			GetComponent<TextMeshPro>().fontSize = _myNewFontSize;
		}
		if (GetComponent<TextMeshProUGUI>() != null)
		{
			GetComponent<TextMeshProUGUI>().fontSize = _myNewFontSize;
		}

		TMP_Text tmpText = GetComponent<TMP_Text>();
		if (tmpText != null)
		{
			tmpText.fontSize = _myNewFontSize;
			tmpText.text = _myNewText;
		}
	}

	//TODO: generalization instead of tmp.text
	bool DoTextWorks()
	{
		string textToProcess = textmeshpro.text;
		int indexofMarker = -1;
		//int indexofMarker = textToProcess.IndexOf("%");
		if (indexofMarker == -1)
			return false;
		textToProcess = textToProcess.Substring(indexofMarker + 1);
		string potentialId = Regex.Match(textToProcess, @"\d+").Value;

		long number;

		bool success = long.TryParse(potentialId, out number);

		if (!success)
			return false; ;


		GameObject calculator;
		if (!SetVisualSettings.instance.GameObjectCol.TryGetValue(number, out calculator))
			return false; ;

		textmeshpro.text = textmeshpro.text.Replace("%" + potentialId, calculator.GetComponent<CalculatorController>().ResultFloat().ToString("F4"));
		return true;

	}

	bool NewDoTextWorks()
	{

		string textToProcess = textmeshpro.text;
		int indexofMarker = -1;
		if (textToProcess != string.Empty)
		{
			//indexofMarker = textToProcess.IndexOf("%");
		}
		if (indexofMarker == -1)
			return false;
		textToProcess = textToProcess.Substring(indexofMarker + 1);
		bool isFreeze = true;
		string potentialId = Regex.Match(textToProcess, @"\d+i").Value;
		if (potentialId == String.Empty)
		{
			potentialId = Regex.Match(textToProcess, @"\d+").Value;
			isFreeze = false;
		}
		else
		{
			potentialId = potentialId.Remove(potentialId.Length - 1);
		}
		long number;

		bool success = long.TryParse(potentialId, out number);



		if (!success)
			return false;
		float result = The3DWorldManager.instance.curr3DWorld.calculatorManager.ResultFloat(number);
		if (result == float.MaxValue)
			return false;

		textmeshpro.text = textmeshpro.text.Replace("%" + potentialId + "i", result.ToString("F4"));
		textmeshpro.text = textmeshpro.text.Replace("%" + potentialId, result.ToString("F4"));
		if (isFreeze)
			originalText = textmeshpro.text;
		return true;
	}

	#endregion Text

	void CheckPositionUI()
    {
		if(GetComponent<RectTransform>().localPosition != SetUISettings.instance.CheckPositionUI(posUI))
        {
			GetComponent<RectTransform>().localPosition = SetUISettings.instance.CheckPositionUI(posUI);
		}
    }


	void CalculatePerimetricPositionOnScreen(bool isStaticPov = false)
	{
		if (transform.parent == null)
			return;
		if (infiniteCone == null)
		{
			CalculateOnScreen(isStaticPov);
			return;
		}

		if (infiniteCone.circularOrEllipseBase == "ellipse")
			CalculateOnScreenFromEllipse(isStaticPov);
		else
			CalculateOnScreenFromCircle(isStaticPov);
	}

	void CalculateOnScreen(bool isStaticPov = false)
	{
		Vector3 mainCamPos = Camera.main.transform.position;
		if (isStaticPov)
			mainCamPos = currentCS.pos;

		float radialDistance = (mainCamPos - transform.position).magnitude;
		
		radialDistance = radialDistance / (float)Math.Cos(perimetricCoordinates[0]);// radial distance projection on the screen (dependent on eccentricity)

		float z = -0.0005f * radialDistance;

		transform.localPosition = Utils.instance.PerimetricRDToXYZ(perimetricCoordinates[0], perimetricCoordinates[1], radialDistance);
	    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, z);
	}

	void CalculateOnScreenFromCircle(bool isStaticPov = false)
	{
		Vector3 coneCorrection = Vector3.zero;
		if (infiniteCone.tangentCorrection)
		{
			coneCorrection = infiniteCone.CAPOC.ToUnityPos();
		}

		Vector3 mainCamPos = Camera.main.transform.position;
		if (isStaticPov)
			mainCamPos = currentCS.pos;

		float radialDistance = (mainCamPos - transform.position - coneCorrection).magnitude;
		radialDistance = radialDistance / (float)Math.Cos(perimetricCoordinates[0]);

		float z = -0.0005f * radialDistance;

		float x = radialDistance * (float)Math.Cos(perimetricCoordinates[1]) * (float)Math.Sin(perimetricCoordinates[0])+ infiniteCone.CAPOC.position_current_CS[0];
		float y = radialDistance * (float)Math.Sin(perimetricCoordinates[1]) * (float)Math.Sin(perimetricCoordinates[0])+ infiniteCone.CAPOC.position_current_CS[1];
		

		transform.localPosition = new Vector3(x, y, z);
	}

	void CalculateOnScreenFromEllipse(bool isStaticPov = false)
	{
		if (transform.parent == null)
			return;

		Vector3 coneCorrection = Vector3.zero;

		Vector3 mainCamPos = Camera.main.transform.position;
		if (isStaticPov)
			mainCamPos = currentCS.pos;


		pointingTool.position = currentCS.pos+infiniteCone.apexPosition.ToUnityPos ()+ mainCamPos;

		onScreenTool.SetParent(transform.parent);

		onScreenTool.localPosition = infiniteCone.CAPOC.ToUnityPos();

		pointingTool.LookAt(onScreenTool);

		Vector3 directionVect = pointingTool.forward ;
		Vector3 upVect = pointingTool.up;
		pointingTool.transform.RotateAround(pointingTool.transform.position, upVect , perimetricCoordinates[0] / (float)Math.PI * 180);
		pointingTool.transform.RotateAround(pointingTool.transform.position, directionVect, perimetricCoordinates[1] / (float)Math.PI * 180);

		transform.position = Utils.instance.LineScreenIntersection(transform.parent, pointingTool.position, pointingTool.forward);
		return;
	}

	void PrepareCAPOC ()
    {
		if (infiniteCone == null)
			return;
		Transform parentTrans = transform.parent;
		if (parentTrans == null)
			return;
		Vector3 mainCamPos = currentCS.pos;

		float radialDistance = (mainCamPos - transform.position).magnitude;
	/*	radialDistance = radialDistance / (float)Math.Cos(infiniteCone.CAPOC.perimetricCoordinates[0]);

		infiniteCone.CAPOC.position_current_CS[0] = radialDistance * (float)Math.Cos(infiniteCone.CAPOC.perimetricCoordinates[1]) * (float)Math.Sin(infiniteCone.CAPOC.perimetricCoordinates[0]);
		infiniteCone.CAPOC.position_current_CS[1] = radialDistance * (float)Math.Sin(infiniteCone.CAPOC.perimetricCoordinates[1]) * (float)Math.Sin(infiniteCone.CAPOC.perimetricCoordinates[0]);
	*/
		}

	void ManageCalibFacing()
	{
		positionTool.position = 2*transform.position-currentCS.pos;
		transform.LookAt(positionTool);
	}

	void ManageFacing()
	{
		Camera mainCam = Camera.main;
		positionTool.position = mainCam.transform.position;
		transform.LookAt(positionTool);
	}
	public void PointToLookAt(GameObject _point)
    {
		if (onOppositeDirection)
		{
			transform.rotation = Quaternion.LookRotation((transform.position - _point.transform.position).normalized);
			
		}
		else
        {
			transform.LookAt(_point.transform);
		}

		/*
		//Si Forward inversé
		transform.rotation *= Quaternion.Euler(0,180,0);
		 */

	}

	public void SetAngularSize1D ()
    {

    }

	public void SetAngularSize2D ()
    {
		if (angularSize2d == Vector2.zero)
			return;

		Vector3 aPos = transform.position - currentCS.pos;
		float sizeX=0;
		float sizeY=0;
		if (!angularSizeApprox)
		{
			float xEcc = (float)Math.Atan(transform.position.x / aPos.magnitude);
			float yEcc = (float)Math.Atan(transform.position.y / aPos.magnitude);

			sizeX = ((float)Math.Tan(xEcc + angularSize2d[0] / 2) - (float)Math.Tan(xEcc - angularSize2d[0] / 2)) * aPos.magnitude * specificScale[0];
			sizeY = ((float)Math.Tan(yEcc + angularSize2d[1] / 2) - (float)Math.Tan(yEcc - angularSize2d[1] / 2)) * aPos.magnitude * specificScale[1];
		}
		else
		{

			sizeX = 2*(float)Math.Tan(angularSize2d[0]/2) * aPos.magnitude * specificScale[0];
			sizeY = 2*(float)Math.Tan(angularSize2d[1]/2) * aPos.magnitude * specificScale[1];
		}
		transform.localScale = new Vector3(sizeX, sizeY, transform.localScale[2]);
	}


	void UpdateLineStartEndPoints ()
    {
		if (startPoint == null || endPoint == null)
			return;

		LineRenderer lineRenderer = GetComponent<LineRenderer>();
		lineRenderer.SetPosition(0, startPoint.position);
		lineRenderer.SetPosition(1, endPoint.position);
	}

	void DoCoordinatedWork ()
    {
		if (objData.calculatedPosition == "")
			return;
		int indexofMarker = objData.calculatedPosition.IndexOf("%Pos");
		if (indexofMarker != -1)
		{
			DoPositionCalculus();
			return;
		}

		indexofMarker = objData.calculatedPosition.IndexOf("%");
		if (indexofMarker == -1)
			return;
		string textToProcess = objData.calculatedPosition.Substring(indexofMarker + 1);
		string potentialId = Regex.Match(textToProcess, @"\d+").Value;

		long number;

		bool success = long.TryParse(potentialId, out number);

		if (!success)
			return;

		transform.position = The3DWorldManager.instance.curr3DWorld.calculatorManager.ResultVector3(number);
	}
	void DoPositionCalculus ()
    {
		int indexofMarkerFirst = objData.calculatedPosition.IndexOf("%Pos")+3;

		string textToProcess = objData.calculatedPosition.Substring(indexofMarkerFirst + 1);
		string potentialId = Regex.Match(textToProcess, @"\d+").Value;

		int indexofOperator = textToProcess.IndexOf("+");
		string theOperator = textToProcess.Substring(indexofOperator, 1);

		textToProcess = textToProcess.Substring(indexofOperator);

		int indexofMarkerSecond = textToProcess.IndexOf("%Pos") + 3;
		string potentialId2 = Regex.Match(textToProcess, @"\d+").Value;
		long number1 = 0, number2 = 0;

		bool success = long.TryParse(potentialId, out number1) && long.TryParse(potentialId2, out number2);

		if (!success)
			return;

		transform.position = SetVisualSettings.instance.GameObjectCol[number1].transform.position
			+ SetVisualSettings.instance.GameObjectCol[number2].transform.position;

	}

}

public class Periodic_function
{
	public Vector3 start_periodic_position;
	public Vector3 end_periodic_position;
	public float period;
	public float count_period;
	public string periodic_function;
	public bool flag_periodic_movement;
	public bool flag_start_periodic_movement;
	public Vector3 periodic_movement;
	public Vector3 last_periodic_movement;
	public bool flag_periodic_function_up;
	public bool flag_periodic_function_down;
	public bool look_in_movement_direction;

	public Periodic_function()
    {
		period = 0;
		count_period = 0;
		periodic_movement = new Vector3(0, 0, 0);
		last_periodic_movement = new Vector3(0, 0, 0);
		flag_start_periodic_movement = false;
		flag_periodic_function_up = false;
		flag_periodic_function_down = false;
		look_in_movement_direction = false;
}
}
	
