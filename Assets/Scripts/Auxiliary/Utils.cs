using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections.Generic;


public class Utils : MonoBehaviour
{
    #region Inspector
    public static Utils instance;
    #endregion Inspector

    private static Transform pointingTool;
    private static Transform placementTool;

    #region UnityCall
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            Debug.LogWarning("Double Utils, destroy all exept one");
        }
        else
        {
            pointingTool = new GameObject().transform;
            placementTool = new GameObject().transform;
            instance = this;
        }
    }

    #endregion UnityCall
    public Vector3 mmInM(Vector3 mm)
    {
        Vector3 m = mm;
        m.x = mmInM(m.x);
        m.y = mmInM(m.y);
        m.z = mmInM(m.z);
        return m;
    }
    public float mmInM(float mm)
    {
        float m = mm * 0.001f;
        return m;
    }
    public Vector3 RightCoordinatesToLeftCoordinates(Vector3 rightCoordinates)
    {
        Vector3 leftCoordinates = rightCoordinates;
        leftCoordinates.x = - leftCoordinates.x;
        return leftCoordinates;
    }
    //Eccentricity , HalfMeridian  TODO: use CartesianToPerimetric
    public Vector2 XYZtoPerimetric(float X, float Y, float Z)
    {
        float half_meridian, eccentricity;

        float rho = (float)Math.Sqrt(X * X + Y * Y + Z * Z);

        half_meridian = (float)Math.Atan2(Y, X) * 180 / (float)Math.PI;

        if (half_meridian < 0)
        {
            half_meridian = 360 + half_meridian;
        }

        eccentricity = (float)Math.Acos(Z / rho) * 180 / (float)Math.PI;

        return new Vector2(eccentricity, half_meridian);
    }

    // Perimetric to XY


    //Eccentricity, HalfMeridian , RadialDistance Change to : CartesianToPerimetric
    public Vector3 XYZtoPerimetricRD(float X, float Y, float Z)
    {
        float half_meridian, eccentricity;

        float rho = (float)Math.Sqrt(X * X + Y * Y + Z * Z);

        half_meridian = (float)Math.Atan2(Y, X);

        eccentricity = (float)Math.Acos(Z / rho);

        return new Vector3(eccentricity, half_meridian, rho); //rho is radialDistance
    }

    //Position x, y ,z
    public Vector3 PerimetricRDToXYZ (float eccentricity, float halfMeridian, float radialDistance)
    {
        float x = radialDistance * Mathf.Cos(halfMeridian) * Mathf.Sin(eccentricity);
        float y = radialDistance * Mathf.Sin(halfMeridian) * Mathf.Sin(eccentricity);
        
        float z = radialDistance * Mathf.Cos(eccentricity);

        return new Vector3(x, y, z);
    }

    //Rotation depending of Eccentricity, HalfMeridian to Facing the direction?
    


    public Vector2 XYZtoXYdeg(float X, float Y, float Z)
    {
        return new Vector2(Mathf.Atan(X / Z), (float)Math.Atan(Y / Z));
    }

    public Vector4 CalculateScreenParameters (Transform plane)
    {
        placementTool.SetParent(plane);
        placementTool.transform.localPosition = new Vector3(0,0,1);
        Vector4 abcdParameters = new Vector4(0,0,0,0);
        abcdParameters[0] = placementTool.transform.position[0]
            - plane.transform.position[0];
        abcdParameters[1] = placementTool.transform.position[1]
            - plane.transform.position[1];
        abcdParameters[2] = placementTool.transform.position[2]
            - plane.transform.position[2];


        abcdParameters[3] = 
            - plane.transform.position[0] * abcdParameters[0] +
            - plane.transform.position[1] * abcdParameters[1] +
            - plane.transform.position[2] * abcdParameters[2];

        return abcdParameters;
    }

    public Vector3 LineScreenIntersection(Transform plane, Vector3 LineOrigin, Vector3 LineDirection)
    {
        Vector4 abcdParameters = CalculateScreenParameters(plane);

        /*float t = -(LineOrigin[0] + LineOrigin[1] + LineOrigin[2] + abcdParameters[3]) /
            (abcdParameters[0] * LineDirection[0] + abcdParameters[1] * LineDirection[1] + abcdParameters[2] * LineDirection[2]);
        */
        float t = -(LineOrigin[0]* abcdParameters[0] + LineOrigin[1]* abcdParameters[1] + LineOrigin[2]* abcdParameters[2] + abcdParameters[3]) /
            (abcdParameters[0] * LineDirection[0] + abcdParameters[1] * LineDirection[1] + abcdParameters[2] * LineDirection[2]);
        
        return (new Vector3(t * LineDirection[0], t * LineDirection[1], t * LineDirection[2]-0.00005f) +LineOrigin);

    }

    public Vector3 RotateVectorPerimetric (Vector3 toRotate, float eccentricity, float HalfMeridian)
    {

        pointingTool.position = Vector3.zero;
        placementTool.position = toRotate;
        pointingTool.LookAt(placementTool);

        Vector3 directionVect = new Vector3(pointingTool.forward[0], pointingTool.forward[1], pointingTool.forward[2]);
        Vector3 upVect = new Vector3(pointingTool.up[0], pointingTool.up[1], pointingTool.up[2]);

        pointingTool.transform.RotateAround(pointingTool.transform.position, upVect, eccentricity / (float)Math.PI * 180);
        pointingTool.transform.RotateAround(pointingTool.transform.position, directionVect, HalfMeridian / (float)Math.PI * 180);
        toRotate = pointingTool.forward;
        return toRotate;
    }

    public class InfiniteConeCalculator
    {

        /*
    ellipseCalculator functions


    You can also open it with spyder and exectute it directly, without PYTHONPATH configuration.

    This tool permits a calculation of the end points of the ellipse chord
    passing througth its angular center.
    It uses the cone equation instead of the vector method used in PTVR.
    Its goal is to cross check the endpoints positions.
    The parameter name follow the document (PTVR_Cone_and_Visual_Angles-3.pdf)



            */

        //The system XYZ, useful for calculation (don't change it)
        private Vector3 x = new Vector3(1, 0, 0);
        private Vector3 y = new Vector3(0, 1, 0);
        private Vector3 z = new Vector3(0, 0, 1);

        /*3d identity matrix  (don't change it)
        #1 0 0
        #0 1 0
        #0 0 1
        I = np.array( [[1, 0, 0], [0, 1, 0], [0, 0, 1] ] )
        */

        private Matrix4x4 I = Matrix4x4.identity;

        /*the vector pointing to the SO. Its norm is the distance
        for instance, if the vector is (0,0,5), the screen is on the z axis at 5 m distance*/
        public Vector3 p = new Vector3(10, 0, 10);

        /*the cone axis vector. It's direction is the direction of the cone axis.
        For easier use, it can be not normalized, never mind.*/
        public Vector3 g = new Vector3(0, 1, 1);

        //the position of the cone apex in the space
        public Vector3 O = new Vector3(0, 0, 0);

        //the cone apperture angle (angle at apex in degrees)
        public float gamma = 15;

        //the chord rotation
        public float theta = 90;

        //The calculated parameters:
        public Vector3 xPrime;
        public Vector3 t;
        public float d = 0;
        public Matrix4x4 R;
        public Vector3 K;

        public InfiniteConeCalculator()
        {
            I[3, 3] = 0; //a 4x4 matrix with the last column and line is equivalent to a 3x3 matrix
        }

        //# �tape 1 of the document, calculation of screen x axis, called xPrime
        public Vector3 calculateXprime()
        {
            if (d == 0)
                d = p.magnitude;
            p = p.normalized;
            xPrime = Vector3.Cross(y, p);
            return xPrime;
        }
        /*�tape 2 (3) of the document, calculation of the rotation R of xPrime
        #This rotation gives the same orientation as the desired chord*/
        public Matrix4x4 XprimeRotation() {
            p = p.normalized;
            float theta_rad = (float)Math.PI / 180 * theta;



            Matrix4x4 Q = new Matrix4x4();
            Q.SetRow(0, new Vector4(0, -p[2], p[1], 0));
            Q.SetRow(1, new Vector4(p[2], 0, -p[0], 0));
            Q.SetRow(2, new Vector4(-p[1], p[0], 0));
            Q.SetRow(3, new Vector4());

            Matrix4x4 QQ = Q * Q;

            for (int i=0; i<3; i++)
                R.SetRow(i, I.GetRow(i) + (float)Math.Sin(theta_rad) * Q.GetRow(i) + (float)(1 - Math.Cos(theta_rad)) * QQ.GetRow(i));

            return R;
        }
        /*#�tape 2 of the document, calculation of the rotated xPrime t
        #t is parralel to the desired chord, its origin is SO.*/
        public Vector3 tCalculator()
        {
            t = R * xPrime;
            return R * xPrime;
        }
        /*#�tape 3 of the document, calculation of the angular center of the ellipse
        #K*/
        public Vector3 kCalculator()
        {

            float alpha = d / Vector3.Dot(g, p);
            K = O + alpha * g;
            return O + alpha * g;
        }
        /*#�tape 4 of the document, calculation or the desired endpoints
        #E1 and E2*/
        public void getE1E2(out Vector3 E1, out Vector3 E2)
        {
            float gamma_rad = gamma / 180 * (float)Math.PI;

            float a = (float)Math.Pow(Vector3.Dot(t, g), 2) - (float)Math.Pow(Math.Cos(gamma_rad / 2), 2);
            float b = 2 * Vector3.Dot(K - O, g) * Vector3.Dot(t, g) - (float)Math.Pow(2 * Vector3.Dot(K - O, t) * Math.Cos(gamma_rad / 2), 2);
            float c = (float)Math.Pow(Vector3.Dot(K - O, g), 2) - (float)Math.Pow((K - O).magnitude * (float)Math.Cos(gamma_rad / 2), 2);

            float beta = (float)(-b - Math.Sqrt(b * b - 4 * a * c));
            E1 = K + beta * t;

            beta = (float)(-b + Math.Sqrt(b * b - 4 * a * c));
            E2 = K + beta * t;

            return;
        }

        /*#the main go througth each steps, giving the analytic result of each "�tape".
        def main() :
            xPrime=calculateXprime(p)
            print("xPrime, the screen local x axis:")
            print(xPrime)
            print()
            R = XprimeRotation(90, p)
            print("rotation matrix of xprime to get t:")
            print(R)
            print()
            t=tCalculator(R, xPrime)
            print("the parallel to the cord vector at SO t:")
            print(t)
            print()
            K = kCalculator(g, p)
            print("the pseudo center of the ellipse K:")
            print(K)
            print()
            E1,E2 = getE1E2(t, g, gamma, K)
            print("the two endpoints of the cord E1,E2:")
            print(E1, E2)



        main()*/
    }

}
