using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResponsiveUIFixe : MonoBehaviour
{
    public float positionX;
    public float positionY;

    float tempWidthScreen;
    float tempHeightScreen;
    bool isChanged;
    void Start()
    {
        tempHeightScreen = Screen.height;
        tempWidthScreen = Screen.width;
        isChanged = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(tempWidthScreen != Screen.width)
        {
            tempWidthScreen = Screen.width;
            isChanged = true;
        }
        if(tempHeightScreen != Screen.height)
        {
            tempHeightScreen = Screen.height;
            isChanged = true;
        }
        if (isChanged)
        {
            Vector3 temp = new Vector3(positionX,positionY);
            float y = temp.y * (tempHeightScreen / 2);
            float x = temp.x * (tempWidthScreen / 2);
            Vector3 pos = new Vector3(x, y);
            transform.position = pos;
            isChanged = false;
        }
    }
}
