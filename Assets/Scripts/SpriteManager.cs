﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteManager : MonoBehaviour {

    #region Inspector
    #endregion Inspector

    #region Privates
    Transform positionReference;
    Transform currentSpriteTrans;
    Camera cam;
    bool isPosition = false;
    bool isFacing = false;
    #endregion Privates

    #region UnityCall
    void Start()
    {
        cam = Camera.main;
        currentSpriteTrans = GetComponent<Transform>();
        UpdateOrientation();
        UpdatePosition();
    }

    void Update()
    {
        UpdateOrientation();
        UpdatePosition();
    }
    #endregion UnityCall

    void UpdateOrientation ()
    {
	if (!isFacing || currentSpriteTrans == null || cam == null)
	    return;
	currentSpriteTrans.transform.LookAt (cam.transform);
    }

    void UpdatePosition ()
    {
	if (!isPosition || currentSpriteTrans == null || positionReference == null)
	    return;
	currentSpriteTrans.transform.position = positionReference.position;
    }
}
