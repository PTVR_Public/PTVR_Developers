using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

/**
* \brief Class that calculates the appropriate x-height in points based on the viewing_distance_in_m and the visual_angle_of_centered_object of a text. Used in SetTrialConfiguration.cs
*/

public class TextHeightManager : MonoBehaviour
{
    #region Inspector
    public static TextHeightManager instance; //Singleton
    #endregion Inspector

    #region Privates
    double emPhysicalHeightInPt; //!< Text's size in points (correspond to font size / point size / body size)
    double emPhysicalHeightInM; //!< Text's size in meters
    #endregion Privates

    #region UnityCall
    void Awake()
    {
       
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    #endregion UnityCall

    /** Method that converts degrees to radians
     */
    float Deg2Rad(float _eccentricityInDeg)
    {
        return _eccentricityInDeg * Mathf.PI / 180;
    }

    /** Method that gives the size on tangent screen of the segment 
        from the point with an eccentricity of zero degree
        to the point with an eccentricity of "eccentricity_in_deg"
     */
    public float EccentricityToSizeOnTangentScreen(float _eccentricityInDeg, float _viewingDistanceInM)
    {
        return (float)_viewingDistanceInM * Mathf.Tan(Deg2Rad(_eccentricityInDeg));
    }

   /** Method that gives the size in meters on tangent screen of the object (centered on the 0° eccentricity axis)
    subtending a visual angle of 'visual_angle_of_object' degrees
     */
    public float VisualAngleToSizeOnTangentScreen(float _visualAngleOfCenteredObject, float _viewingDistanceInM)
    {
        return (float) ( 2 * EccentricityToSizeOnTangentScreen((_visualAngleOfCenteredObject / 2), _viewingDistanceInM) ) ;
    }

    /** Method that calculates the size in points that the text needs to have to display the x-height at a correct physical size.
     * @param textmeshpro is the text we want to get the x-height from.
     */
    public float CalculateXPhysicalHeightPt(float _viewingDistanceInM, float _visualAngleOfCenteredObject, TMP_Text _textmeshpro)
    {

        emPhysicalHeightInM = VisualAngleToSizeOnTangentScreen(_visualAngleOfCenteredObject, _viewingDistanceInM);
        emPhysicalHeightInPt = (emPhysicalHeightInM / (_textmeshpro.font.faceInfo.meanLine * 0.001) * _textmeshpro.font.faceInfo.pointSize * 0.01);

        return (float)emPhysicalHeightInPt;
    }

    public double VisualAngleInDegreesToLogmar(float _visual_angle_of_centered_object)
    {
        float minutes_of_arc = _visual_angle_of_centered_object / 60;
        float logMAR_acuity = - Mathf.Log10(1 / minutes_of_arc);
        return System.Math.Round(logMAR_acuity, 2);
    }



}
