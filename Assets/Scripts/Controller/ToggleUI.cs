using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

// Manage un Toggle seul

public class ToggleUI : MonoBehaviour
{
    public InputUnityEvent inputUnityEvent; // ce qui donne les infos (le bouton est enclenche ou non ) 
    Toggle toggle;
    bool isChecked;
    public InputUnityEvent SetupToggleUI()
    {
        toggle = GetComponent<Toggle>();

        inputUnityEvent = new InputUnityEvent(); // contient press hold et release 

        inputUnityEvent.OnInputPress = new CustomUnityEvent(); // permet de l'initialiser 
        inputUnityEvent.OnInputRelease = new CustomUnityEvent();

        return inputUnityEvent;
    }

    void Update()
    {
        if (toggle.isOn && !isChecked)
        {
            Debug.Log("Validate");
            isChecked = toggle.isOn;
            inputUnityEvent.OnInputPress.Invoke();
        }
        else if (!toggle.isOn && isChecked)
        {
            Debug.Log("No validate");
            isChecked = toggle.isOn;
            inputUnityEvent.OnInputRelease.Invoke();
        }
        
    }
    private void Start()
    {
        toggle = GetComponent<Toggle>();
        isChecked = toggle.isOn;

    }

}
