using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class DistanceFromPointController : MonoBehaviour
{
    public Dictionary<long, DistanceFromPointUnityEvent> distanceFromPoint = new Dictionary<long, DistanceFromPointUnityEvent>();
    public GameObject objectTracked;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        foreach(var dfp in distanceFromPoint)
        {
            Vector3 pointCoordinates = new Vector3(distanceFromPoint[dfp.Key].pointCoordinates[0], distanceFromPoint[dfp.Key].pointCoordinates[1], distanceFromPoint[dfp.Key].pointCoordinates[2]);

            if ((objectTracked.transform.position - pointCoordinates).magnitude < distanceFromPoint[dfp.Key].distanceFromPointInMeters )
            {
                if (distanceFromPoint[dfp.Key].isPress)
                {
                    DataBank.Instance.SetData("event_name", distanceFromPoint[dfp.Key].input.inputName);
                    DataBank.Instance.SetData("event", distanceFromPoint[dfp.Key].input.button);
                    DataBank.Instance.SetData("event_mode", "hold");
                    distanceFromPoint[dfp.Key].input.OnInputHold.Invoke();
                }
                else
                {
                    DataBank.Instance.SetData("event_name", distanceFromPoint[dfp.Key].input.inputName);
                    DataBank.Instance.SetData("event", distanceFromPoint[dfp.Key].input.button);
                    DataBank.Instance.SetData("event_mode", "press");
                    distanceFromPoint[dfp.Key].input.OnInputPress.Invoke();
                    distanceFromPoint[dfp.Key].isPress = true;
                }
                distanceFromPoint[dfp.Key].wasReleaseThisFrame = false;
            }
            else
            {
                if (!distanceFromPoint[dfp.Key].wasReleaseThisFrame)
                {
                    Debug.Log("-------------------DEBUG RELEASE---------------");
                    DataBank.Instance.SetData("event_name", distanceFromPoint[dfp.Key].input.inputName);
                    DataBank.Instance.SetData("event", distanceFromPoint[dfp.Key].input.button);
                    DataBank.Instance.SetData("event_mode", "release");
                    distanceFromPoint[dfp.Key].input.OnInputRelease.Invoke();
                    distanceFromPoint[dfp.Key].isPress = false;
                    distanceFromPoint[dfp.Key].wasReleaseThisFrame = true;
                }
            }
        }
       
    }

    public InputUnityEvent SetUp(long _id, string _objectTracked, List<float> _pointCoordinates, float _distanceFromPointInMeters,string _nameButton)
    {
        DistanceFromPointUnityEvent distanceFromPointTemp;
        if (!distanceFromPoint.ContainsKey(_id))
        {


            distanceFromPointTemp = new DistanceFromPointUnityEvent();

            //Step 1 Link Input

            distanceFromPointTemp.input = new InputUnityEvent();
            distanceFromPointTemp.input.OnInputRelease = new CustomUnityEvent();
            distanceFromPointTemp.input.OnInputHold = new CustomUnityEvent();
            distanceFromPointTemp.input.OnInputPress = new CustomUnityEvent();
            distanceFromPointTemp.pointCoordinates = _pointCoordinates;
            distanceFromPointTemp.distanceFromPointInMeters = _distanceFromPointInMeters;

            distanceFromPointTemp.input.button = _objectTracked;
            distanceFromPointTemp.input.inputName = _nameButton;
               distanceFromPoint[_id] = distanceFromPointTemp;
            }
            else
            {
            distanceFromPointTemp = distanceFromPoint[_id];
            }
        return distanceFromPointTemp.input;
    }



}
