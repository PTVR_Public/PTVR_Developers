using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using PTVR.Data.Event;
public class DistanceFromPointManager : MonoBehaviour
{
    public static DistanceFromPointManager instance;

    public List<DistanceFromPointController> controllers;
    private void Awake()
    {
        if (instance != null)
            Destroy(this);
        else
            instance = this;
    }

    public InputUnityEvent TakeDistanceFromPoint(DistanceFromPoint _distanceFromPoint)
    {
        switch (_distanceFromPoint.objectTracked) {
            case "headset":
                return controllers[0].SetUp(_distanceFromPoint.id, _distanceFromPoint.objectTracked, _distanceFromPoint.pointCoordinates, _distanceFromPoint.distanceFromPointInMeters,_distanceFromPoint.eventName);

            default:
                return null;
        }
    }

}
[Serializable]
public class DistanceFromPointUnityEvent
{
    public List<float> pointCoordinates;
    public bool isPress;
    public bool wasPressedThisFrame;
    public bool wasReleaseThisFrame;
    public float distanceFromPointInMeters;
    public string objectTracked;
    public InputUnityEvent input;
}

