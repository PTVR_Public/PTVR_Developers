using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Globalization;
using System;

public class GraphUIManager : MonoBehaviour
{
    [SerializeField]  private Sprite dot; // The dot material is set in the editor

    RectTransform graphContainer;
    Image graphContainerBackground;
    Image graphFrameBackground;
    RawImage xAxisImage;
    RawImage yAxisImage;

    RectTransform labelTemplateX;
    RectTransform labelTemplateY;

    RectTransform axisTemplateX;
    RectTransform axisTemplateY;

    TextMeshProUGUI axisNameX;
    TextMeshProUGUI axisNameY;

    List<float> xLabels;
    List<float> yLabels;

    RectTransform labelTemplateXUP;
    RectTransform labelTemplateYRIGHT;

    TextMeshProUGUI axisNameXUP;
    TextMeshProUGUI axisNameYRIGHT;

    public PTVR.Stimuli.Objects.GraphUI graphUI;
    public List<List<float>> valueList;
    float currentDotX;
    float currentDotY;

    float x_value;
    float y_value;

    float stopTimer;

    float graphHeight; // Height of the graph container 
    float graphWidth; // Width of the graph container 

    public GameObject currentDotGameObject;
    GameObject lastDotGameObject;
    GameObject beforeLastDotGameObject;
    List<GameObject> dots;
    bool lastDotDeleted;

    #region Inspector
    public static GraphUIManager instance;
    #endregion Inspector

    private void Awake() // s''execute en premier, verifie si l'instance est nulle ou pas
    {
        if (instance != null) // si elle existe d�j�, on la d�truit
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    public static float GetAngleFromVectorFloat(Vector3 direction)
    {
        direction = direction.normalized;
        float n = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        if (n < 0) n += 360;

        return n;
    }

    private void Start()
    {
        valueList = new List<List<float>>();
        dots = new List<GameObject>();
        lastDotDeleted = false;
        yLabels = new List<float>();
    }

    //Creation du template de base du graph (fond, axes, taille..)
    public void CreateGraph( GameObject prefabGraphUI, PTVR.Stimuli.Objects.GraphUI _graphUI)
    {
        stopTimer = 0;
        graphUI = _graphUI;


        graphContainer = prefabGraphUI.transform.Find("GraphContainer").GetComponent<RectTransform>();
        graphFrameBackground = prefabGraphUI.transform.Find("GraphFrame").GetComponent<Image>();

        labelTemplateX = graphContainer.Find("LabelTemplateX").GetComponent<RectTransform>();


        labelTemplateY = graphContainer.Find("LabelTemplateY").GetComponent<RectTransform>();

        axisTemplateX = graphContainer.Find("AxisTemplateX").GetComponent<RectTransform>();

        axisTemplateY = graphContainer.Find("AxisTemplateY").GetComponent<RectTransform>();

        axisNameX = graphContainer.Find("AxisNameX").GetComponent<TextMeshProUGUI>();
        axisNameY = graphContainer.Find("AxisNameY").GetComponent<TextMeshProUGUI>();
        axisNameX.text = graphUI.xAxisName;
        axisNameY.text = graphUI.yAxisName;
        axisNameX.color = graphUI.textColor.ToUnityColor();
        axisNameY.color = graphUI.textColor.ToUnityColor();
        axisNameX.fontSize = graphUI.fontSizeInPoints;
        axisNameY.fontSize = graphUI.fontSizeInPoints;
        lastDotGameObject = null;
        beforeLastDotGameObject = null;

        

        
        if (graphUI.secondXAxis)
        {
            labelTemplateXUP = graphContainer.Find("LabelTemplateXUP").GetComponent<RectTransform>();
            axisNameXUP = graphContainer.Find("AxisNameXUP").GetComponent<TextMeshProUGUI>();
            axisNameXUP.gameObject.SetActive(true);
            axisNameXUP.text = graphUI.xUpAxisName;
            axisNameXUP.color = graphUI.textColor.ToUnityColor();
            axisNameXUP.fontSize = graphUI.fontSizeInPoints;
        }
        
        if (graphUI.secondYAxis)
        {
            labelTemplateYRIGHT = graphContainer.Find("LabelTemplateYRIGHT").GetComponent<RectTransform>();
            axisNameYRIGHT = graphContainer.Find("AxisNameYRIGHT").GetComponent<TextMeshProUGUI>();
            axisNameYRIGHT.gameObject.SetActive(true);
            axisNameYRIGHT.text = graphUI.YRightAxisName;
            axisNameYRIGHT.color = graphUI.textColor.ToUnityColor();
            axisNameYRIGHT.fontSize = graphUI.fontSizeInPoints;
        }


        graphContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(prefabGraphUI.GetComponent<RectTransform>().sizeDelta[0] - 150 , prefabGraphUI.GetComponent<RectTransform>().sizeDelta[1] - 150);

        graphContainerBackground = graphContainer.transform.Find("ContainerBackground").GetComponent<Image>();
        PTVR.Stimuli.Color givenColor = graphUI.color;
        graphContainerBackground.color = givenColor.ToUnityColor();

        PTVR.Stimuli.Color givenFrameColor = graphUI.frameColor;
        graphFrameBackground.color = givenFrameColor.ToUnityColor();

        graphHeight = graphContainer.sizeDelta.y; // Height of the graph container 
        graphWidth = graphContainer.sizeDelta.x; // Height of the graph container 

        xAxisImage = graphContainer.Find("AxisTemplateX").GetComponent<RawImage>();
        yAxisImage = graphContainer.Find("AxisTemplateY").GetComponent<RawImage>();

        xAxisImage.color = graphUI.gridColor.ToUnityColor();
        yAxisImage.color = graphUI.gridColor.ToUnityColor();
        CreateGraphLabels();

    }

    // Creation des labels du graph
    private void CreateGraphLabels()
    {

        // Creation of the list of labels(or graduations) for the x and y axis 
        xLabels = new List<float>();
        int labelsXCount = (int)Math.Floor((graphUI.maximumXValue - graphUI.minimumXValue) / graphUI.graduationsXRange);
        float graduationX = graphUI.minimumXValue;
        for (int i = 0; i <= labelsXCount; i++)
        {
            xLabels.Add(graduationX);
            graduationX += graphUI.graduationsXRange;
        }

        yLabels.Clear();
        //yLabels = new List<float>();
        int labelsYCount = (int)Math.Floor((graphUI.maximumYValue - graphUI.minimumYValue) / graphUI.graduationsYRange);
        float graduationY = graphUI.minimumYValue;
        for (int i = 0; i <= labelsYCount; i++)
        {
            yLabels.Add(graduationY);
            graduationY += graphUI.graduationsYRange;
        }

        // Placement of labels on the Y axis

        for (int i = 0; i < yLabels.Count; i++)
        {
            GameObject oldTemplateY = GameObject.Find("labelTemplateY");
            Destroy(oldTemplateY);
            RectTransform labelY = Instantiate(labelTemplateY); // We  make the label visible. We create one label for each dot displayed

            labelY.SetParent(graphContainer);
            labelY.gameObject.SetActive(true);
            float normalizedValue = ((yLabels[i] + (0 - yLabels[0])) / (graphUI.maximumYValue - graphUI.minimumYValue)) * graphHeight;
            labelY.anchoredPosition = new Vector2(-25f, normalizedValue); // We multiply this value in order to place the label correctly depending on or graphHeight
            labelY.GetComponent<TextMeshProUGUI>().text = Math.Round(yLabels[i], 3).ToString();
            labelY.GetComponent<TextMeshProUGUI>().color = graphUI.textColor.ToUnityColor();
            labelY.GetComponent<TextMeshProUGUI>().fontSize = graphUI.fontSizeInPoints;
            labelY.localScale = new Vector3(1, 1, 1);



            if (graphUI.secondYAxis)
            {
                
                RectTransform labelYRIGHT = Instantiate(labelTemplateYRIGHT); // We  make the label visible. We create one label for each dot displayed
                labelYRIGHT.SetParent(graphContainer);
                labelYRIGHT.gameObject.SetActive(true);
                labelYRIGHT.anchoredPosition = new Vector2(25f, normalizedValue); // We multiply this value in order to place the label correctly depending on or graphHeight
                labelYRIGHT.GetComponent<TextMeshProUGUI>().text = Math.Round(graphUI.YRightLabels[i], 3).ToString();
                labelYRIGHT.GetComponent<TextMeshProUGUI>().color = graphUI.textColor.ToUnityColor();
                labelYRIGHT.GetComponent<TextMeshProUGUI>().fontSize = graphUI.fontSizeInPoints;
                labelYRIGHT.localScale = new Vector3(1, 1, 1);
            }

            if (graphUI.grid)
            {

                RectTransform axisX = Instantiate(axisTemplateX);
                //axisX.GetComponent<Image>().color = gridColor.ToUnityColor();
                axisX.SetParent(graphContainer);
                axisX.gameObject.SetActive(true);
                axisX.anchoredPosition = new Vector2(-5f, normalizedValue);
                axisX.localScale = new Vector3(1, 1, 1);
                axisX.sizeDelta = new Vector2(graphContainer.GetComponent<RectTransform>().sizeDelta[0], axisX.sizeDelta[1]);
            }

        }

        for (int i = 0; i < xLabels.Count; i++)
        {
            RectTransform labelX = Instantiate(labelTemplateX); // We  make the label visible. We create one label for each dot displayed
            labelX.SetParent(graphContainer);
            labelX.gameObject.SetActive(true);
            float normalizedValue = ((xLabels[i] + (0 - xLabels[0])) / (graphUI.maximumXValue - graphUI.minimumXValue)) * graphWidth;
            labelX.anchoredPosition = new Vector2(normalizedValue, -15f);
            labelX.GetComponent<TextMeshProUGUI>().text = Math.Round(xLabels[i], 3).ToString();
            labelX.GetComponent<TextMeshProUGUI>().color = graphUI.textColor.ToUnityColor();
            labelX.GetComponent<TextMeshProUGUI>().fontSize = graphUI.fontSizeInPoints;

            labelX.localScale = new Vector3(1, 1, 1);

            if (graphUI.secondXAxis)
            {
                
                RectTransform labelXUP = Instantiate(labelTemplateXUP); // We  make the label visible. We create one label for each dot displayed
                labelXUP.SetParent(graphContainer);
                labelXUP.gameObject.SetActive(true);

                labelXUP.anchoredPosition = new Vector2(normalizedValue, 25f);
                labelXUP.GetComponent<TextMeshProUGUI>().text = Math.Round(graphUI.XUpLabels[i], 3).ToString();
                labelXUP.GetComponent<TextMeshProUGUI>().color = graphUI.textColor.ToUnityColor();
                labelXUP.GetComponent<TextMeshProUGUI>().fontSize = graphUI.fontSizeInPoints;

                labelXUP.localScale = new Vector3(1, 1, 1);
            }

            if (graphUI.grid)
            {

                RectTransform axisY = Instantiate(axisTemplateY);
                axisY.SetParent(graphContainer);
                axisY.gameObject.SetActive(true);
                axisY.anchoredPosition = new Vector2(normalizedValue, 0);
                axisY.localScale = new Vector3(1, 1, 1);
                axisY.sizeDelta = new Vector2(graphContainer.GetComponent<RectTransform>().sizeDelta[1], axisY.sizeDelta[1]);

            }

        }
    }


    GameObject CreateDot(Vector2 anchoredPosition) // anchoredPosition is the position of the dot in pixels, relative to the graphContainer
    {
        GameObject gameObject = new GameObject("dot", typeof(Image));
        gameObject.transform.SetParent(graphContainer, false);
        gameObject.GetComponent<Image>().sprite = dot;

        PTVR.Stimuli.Color dotColor = graphUI.dotColor;
        gameObject.GetComponent<Image>().color = dotColor.ToUnityColor();
        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = anchoredPosition;
        rectTransform.sizeDelta = new Vector2(11, 11); // size of the dot
                                                       // To anchor it in the bottom left corner
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);

        return gameObject;

    }
    private void CreateDotConnection(Vector2 dotPositionA, Vector2 dotPositionB)
    {
        GameObject gameObject = new GameObject("dotConnection", typeof(Image));
        gameObject.transform.SetParent(graphContainer, false);

        PTVR.Stimuli.Color dotConnectionColor = graphUI.dotConnectionColor;
        gameObject.GetComponent<Image>().color = dotConnectionColor.ToUnityColor();
        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();

        Vector2 direction = (dotPositionB - dotPositionA).normalized; // direction from A to B
        float distance = Vector2.Distance(dotPositionA, dotPositionB);

        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);
        rectTransform.sizeDelta = new Vector2(distance, 3f); // to make the bar, the line connection

        rectTransform.anchoredPosition = dotPositionA + direction * distance * 0.5f; // We place the center of the line between A and B 
        rectTransform.localEulerAngles = new Vector3(0, 0, GetAngleFromVectorFloat(direction)); // Orientation of the line in order to connect the dots


    }

    public void addNewPoint()
    {
        
        if (graphUI.operationX != String.Empty) // Si on veut appliquer une opération sur les valeurs de x 
        {
            switch (graphUI.operationX)
            {

                default:
                    if (graphUI.xIsInvariantCulture)
                    {
                        x_value = float.Parse(DataBank.Instance.GetData(graphUI.xValuesName), CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        x_value = float.Parse(DataBank.Instance.GetData(graphUI.xValuesName));
                    }
                    break;
            }
        }
        else
        {
            if (graphUI.xIsInvariantCulture)
            {
                x_value = float.Parse(DataBank.Instance.GetData(graphUI.xValuesName), CultureInfo.InvariantCulture);
            }
            else
            {
                x_value = float.Parse(DataBank.Instance.GetData(graphUI.xValuesName));
            }
        }
        if (graphUI.operationY != String.Empty) // Si on veut appliquer une opération sur les valeurs de y 
        {
            switch (graphUI.operationY)
            {
                case "calculerReadingSpeed":
                    string misreadWords = DataBank.Instance.GetData(graphUI.yValuesName);
                    var misreadWordsSplit = misreadWords.Split(' ');
                    int misreadWordsCount = misreadWordsSplit.Length - 1;
                    float timing = (float)(The3DWorldManager.instance.elapsedTimeCurrentScene / 1000); // in seconds
                    stopTimer = timing;
                    if (misreadWordsCount > 10)
                    {
                        misreadWordsCount = 10;
                    }
                    y_value = 60 * (10 - misreadWordsCount) / timing;

                    break;
                case "count":
                    string data = DataBank.Instance.GetData(graphUI.yValuesName);
                    var dataSplit = data.Split(' ');
                    int dataCount = dataSplit.Length - 1;
                    y_value = dataCount;
                    break;
                default:
                    if (graphUI.yIsInvariantCulture)
                    {
                        y_value = float.Parse(DataBank.Instance.GetData(graphUI.yValuesName), CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        y_value = float.Parse(DataBank.Instance.GetData(graphUI.yValuesName));
                    }

                    break;
            }
        }
        else // si aucune opération n'est appliquée 
        {
            y_value = float.Parse(DataBank.Instance.GetData(graphUI.yValuesName));
            if (graphUI.yIsInvariantCulture)
            {
                y_value = float.Parse(DataBank.Instance.GetData(graphUI.yValuesName), CultureInfo.InvariantCulture);
            }
            else
            {
                y_value = float.Parse(DataBank.Instance.GetData(graphUI.yValuesName));
            }
        }

        // Gérer les points en dehors du graph
        
        if (y_value > graphUI.maximumYValue){
            y_value = graphUI.maximumYValue;
        }
        
        if (x_value > graphUI.maximumXValue){
            x_value = graphUI.maximumXValue;
        }

        currentDotX = x_value;
        currentDotY = y_value;

        double xPosition = ((currentDotX + (0 - xLabels[0])) / (graphUI.maximumXValue - graphUI.minimumXValue)) * graphWidth;
        double yPosition = ((currentDotY + (0 - yLabels[0])) / (graphUI.maximumYValue - graphUI.minimumYValue)) * graphHeight;

        currentDotGameObject = CreateDot(new Vector2((float)xPosition, (float)yPosition)); // nouveau point

    }


    public void UpdateLastPoint()
    {
        if (currentDotGameObject) // on n update pas le point sur le graph pendant la lecture
        {
            if (!lastDotDeleted)
            {
                float x_value = 0;
                float y_value = 0;

                if (graphUI.operationX != String.Empty) // Si on veut appliquer une opération sur les valeurs de x 
                {
                    switch (graphUI.operationX)
                    {
                        default:
                            if (graphUI.xIsInvariantCulture)
                            {
                                x_value = float.Parse(DataBank.Instance.GetData(graphUI.xValuesName), CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                x_value = float.Parse(DataBank.Instance.GetData(graphUI.xValuesName));
                            }
                            break;
                    }
                }
                else
                {
                    if (graphUI.xIsInvariantCulture)
                    {
                        x_value = float.Parse(DataBank.Instance.GetData(graphUI.xValuesName), CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        x_value = float.Parse(DataBank.Instance.GetData(graphUI.xValuesName));
                    }
                }
                if (graphUI.operationY != String.Empty) // Si on veut appliquer une opération sur les valeurs de y 
                {
                    switch (graphUI.operationY)
                    {
                        case "calculerReadingSpeed":
                            string misreadWords = DataBank.Instance.GetData(graphUI.yValuesName);
                            var misreadWordsSplit = misreadWords.Split(' ');
                            int misreadWordsCount = misreadWordsSplit.Length - 1;
                            if (misreadWordsCount > 10)
                            {
                                misreadWordsCount = 10;
                            }
                            y_value = 60 * (10 - misreadWordsCount) / stopTimer;
                            break;
                        default:
                            if (graphUI.yIsInvariantCulture)
                            {
                                y_value = float.Parse(DataBank.Instance.GetData(graphUI.yValuesName), CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                y_value = float.Parse(DataBank.Instance.GetData(graphUI.yValuesName));
                            }

                            break;
                    }
                }
                else // si aucune opération n'est appliquée 
                {
                    if (graphUI.yIsInvariantCulture)
                    {
                        y_value = float.Parse(DataBank.Instance.GetData(graphUI.yValuesName), CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        y_value = float.Parse(DataBank.Instance.GetData(graphUI.yValuesName));
                    }
                }
                if (y_value > graphUI.maximumYValue)
                {
                    y_value = graphUI.maximumYValue;
                }
                if (x_value > graphUI.maximumXValue)
                {
                    x_value = graphUI.maximumXValue;
                }
                float normalizedXValue = ((x_value + (0 - xLabels[0])) / (graphUI.maximumXValue - graphUI.minimumXValue)) * graphWidth;
                float normalizedYValue = ((y_value + (0 - xLabels[0])) / (graphUI.maximumYValue - graphUI.minimumYValue)) * graphHeight;

                currentDotX = x_value;
                currentDotY = y_value;

                currentDotGameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(normalizedXValue, normalizedYValue);


            }
        }
    }

    public void DeleteLastPoint()
    {
        if (currentDotGameObject)
        {
            currentDotGameObject.SetActive(false);
            lastDotDeleted = true;


        }
    }

    public void SaveLastPoint()
    {
        if (!lastDotDeleted)
        {
            List<float> couple = new List<float>();
            couple.Add(currentDotX);
            couple.Add(currentDotY);
            valueList.Add(couple);
        }
        else
        {
            lastDotDeleted = false;
        }
        
    }

    public void ShowAllPoints()
    {
        foreach (List<float> subList in valueList) // pour chaque couple de valeur 
        {


            double xPosition = ((subList[0] + (0 - xLabels[0])) / (graphUI.maximumXValue - graphUI.minimumXValue)) * graphWidth;
            double yPosition = ((subList[1] + (0 - yLabels[0])) / (graphUI.maximumYValue - graphUI.minimumYValue)) * graphHeight;

            GameObject dotGameObject = CreateDot(new Vector2((float)xPosition, (float)yPosition)); // nouveau point
            
            if (lastDotGameObject != null) // this is not the first dot
            {
                CreateDotConnection(lastDotGameObject.GetComponent<RectTransform>().anchoredPosition, dotGameObject.GetComponent<RectTransform>().anchoredPosition);
            }
            
            lastDotGameObject = dotGameObject;
            dots.Add(dotGameObject);
            

        }
    }



}

