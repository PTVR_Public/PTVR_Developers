using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Manage l'ensemble des toggles 
public class ToggleUIManager : MonoBehaviour 
{

    #region Inspector
    public GameObject prefabToggleUI;
    public static ToggleUIManager instance;
    #endregion Inspector

    #region Privates
    List<InputUnityEvent> inputUI;
    public List<GameObject> toggleUI;

    #endregion Privates

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(this);
        }
        inputUI = new List<InputUnityEvent>();
        toggleUI = new List<GameObject>();
    }

    public InputUnityEvent CreateToggleUI(PTVR.Data.Event.ToggleUI _toggleUI)
    {
        GameObject go = Instantiate(prefabToggleUI, transform);
        Text toggleLabel = go.transform.GetComponentInChildren<Text>();
        Toggle go_toggle = go.GetComponent<Toggle>();
        toggleLabel.text = _toggleUI.label;
        
        toggleUI.Add(go);
        inputUI.Add(go.GetComponent<ToggleUI>().SetupToggleUI());
        

        return go.GetComponent<ToggleUI>().inputUnityEvent;
    }

    public void CleanToggleUI()
    {
        for (int count = 0; count < toggleUI.Count; count++)
        {
            Destroy(toggleUI[count]);
        }
        inputUI.Clear();
        toggleUI.Clear();
    }
}
