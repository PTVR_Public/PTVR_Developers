using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ButtonUI : MonoBehaviour
{
    public InputUnityEvent inputUnityEvent;
    public string button;
   // public PTVR.Data.Input.ButtonUI buttonUI;
    bool isPress;
    bool isSetup;

    public InputUnityEvent SetupButtonUI(string _nameInput)
    {
        CheckSetup(_nameInput);
        return inputUnityEvent;
    }

    public void OnPointerDown(PointerEventData data)
    {
        Debug.Log("OnPointerDown called.");
        
        isPress = true;
        DataBank.Instance.SetData("event_name", inputUnityEvent.inputName);
        DataBank.Instance.SetData("event", inputUnityEvent.button);
        DataBank.Instance.SetData("event_mode", "press");
        inputUnityEvent.OnInputPress.Invoke();
        // Testing add word to a list when the button is pressed

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
      
        //Debug.Log("Mouse enter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
       
       //Debug.Log("Mouse exit");
    }
    void CheckSetup(string _nameInput)
    {
      
          //  Debug.Log("test");
            inputUnityEvent = new InputUnityEvent();
            inputUnityEvent.OnInputPress = new CustomUnityEvent();
            inputUnityEvent.OnInputRelease = new CustomUnityEvent();
            inputUnityEvent.OnInputHold = new CustomUnityEvent();


            EventTrigger trigger = GetComponent<EventTrigger>();
            EventTrigger.Entry entryDown = new EventTrigger.Entry();
            entryDown.eventID = EventTriggerType.PointerDown;
            entryDown.callback.AddListener((data) => { OnPointerDown((PointerEventData)data); });
            trigger.triggers.Add(entryDown);

            EventTrigger.Entry entryUp = new EventTrigger.Entry();
            entryUp.eventID = EventTriggerType.PointerUp;
            entryUp.callback.AddListener((data) => { OnPointerUp((PointerEventData)data); });
            trigger.triggers.Add(entryUp);
            inputUnityEvent.isSetup = true;
            inputUnityEvent.button = button;
            inputUnityEvent.inputName = _nameInput;
        
            EventTrigger.Entry entryEnter = new EventTrigger.Entry();
            entryEnter.eventID = EventTriggerType.PointerEnter;
            entryEnter.callback.AddListener((data) => { OnPointerEnter((PointerEventData)data); });
            trigger.triggers.Add(entryEnter);

            EventTrigger.Entry entryExit = new EventTrigger.Entry();
            entryExit.eventID = EventTriggerType.PointerExit;
            entryExit.callback.AddListener((data) => { OnPointerExit((PointerEventData)data); });
            trigger.triggers.Add(entryExit);
    }
    void Update()
    {
       
        
        if (isPress)
        {
            DataBank.Instance.SetData("event_name", inputUnityEvent.inputName);
            DataBank.Instance.SetData("event", inputUnityEvent.button);
            DataBank.Instance.SetData("event_mode", "hold");
            inputUnityEvent.OnInputHold.Invoke();
        }
        /*if(GetComponent<RectTransform>().localPosition!= SetUISettings.instance.CheckPositionUI(buttonUI.position))
            GetComponent<RectTransform>().localPosition = SetUISettings.instance.CheckPositionUI(buttonUI.position);*/
     
    }

    public void OnPointerUp(PointerEventData data)
    {
        Debug.Log("OnPointerUp called.");
        isPress=false;
        DataBank.Instance.SetData("event_name", inputUnityEvent.inputName);
        DataBank.Instance.SetData("event", inputUnityEvent.button);
        DataBank.Instance.SetData("event_mode", "release");
        inputUnityEvent.OnInputRelease.Invoke();
    }

}
