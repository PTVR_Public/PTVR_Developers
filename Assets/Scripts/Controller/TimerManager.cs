using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/// <summary>
///  1)Calculate Time
///  2)Add Timed
///  Launch event when alarm reach goal
/// </summary>
/// 
public class TimerManager : MonoBehaviour
{
    #region Inspector
    public static TimerManager instance;// singleton
    public List<long> alarm;
    public Dictionary<long,Alarm> alarms = new Dictionary<long,Alarm>();
    //Id 

    #endregion Inspector

    #region Privates

    public bool isTimingScene;
    DateTime lastTimestampScene;
    TimeSpan elapsedTimestampScene;
    public bool isTimingTrial;
    DateTime lastTimestampTrial;
    TimeSpan elaspedTimestampTrial;
    public bool isTimingExperiment;
    DateTime lastTimestampExperiment;
    TimeSpan elaspedTimestampExperiment;

    public int minuteElapsed;
    public int secondElapsed;
    public int microsecondsElapsed;
    public float monsterNumber;
    float lastValue;
    float actualValue;
    #endregion Privates
   
    /*void Minuteur()
    {
        secondElapsed = elapsedTimestamp.Seconds;
        minuteElapsed = elapsedTimestamp.Minutes;
        //monsterNumber = elapsedTimestamp.Minutes+(secondElapsed / 100.0f);
    }
    */
    #region UnityCall
    void Awake()
    {
        if(instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }


    void Update()
    {

        // actualValue = Time.realtimeSinceStartup;
        // elapsedTimestamp = actualTimestamp - lastTimestamp;

        // TODO see if suspendCurrentScene stop input timed to calaculate
        /*if (ExperimentManager.instance.state != SystemState.PAUSED)
        {
            /*ExperimentManager.instance.elaspedTimeCurrentScene = ExperimentManager.instance.elaspedTimeCurrentScene + System.DateTime.Now.Ticks;
            Debug.Log("TIme "+ExperimentManager.instance.elaspedTimeCurrentScene);*/

        if (isTimingScene)
        {
            elapsedTimestampScene = DateTime.Now - lastTimestampScene;
            The3DWorldManager.instance.elapsedTimeCurrentScene = elapsedTimestampScene.TotalMilliseconds;
            //print(The3DWorldManager.instance.elapsedTimeCurrentScene);
        }
        if(isTimingTrial)
        {
            elaspedTimestampTrial = DateTime.Now - lastTimestampTrial;
            The3DWorldManager.instance.elapsedTimeCurrentTrial = elaspedTimestampTrial.TotalMilliseconds;
        }
        if(isTimingExperiment)
        {
            elaspedTimestampExperiment = DateTime.Now - lastTimestampExperiment;
            The3DWorldManager.instance.elapsedTimeCurrentExperiment = elaspedTimestampExperiment.TotalMilliseconds;
        }

            //ExperimentManager.instance.elaspedTimeCurrentScene += System.DateTime.Now.Ticks;
    /*(actualValue - lastValue) * 1000;*/
         


            // }
            //Minuteur();
            // Debug.Log(ExperimentManager.instance.elaspedTimeCurrentScene.ToString());
            if (alarms.Count!=0)
            {       for (int count = 0; count < alarm.Count; count++)
                    {
                         switch(alarms[alarm[count]].whenStart)
                         {
                            case WhenStart.START_EXPERIMENT:
                            if (isTimingExperiment)
                            {
                
                                if (alarms[alarm[count]].startTime < The3DWorldManager.instance.elapsedTimeCurrentExperiment)
                                {
                                    CheckAlarm(count, The3DWorldManager.instance.elapsedTimeCurrentExperiment);
                                 //   Debug.Log(alarms[alarm[count]].startTime + " " + ExperimentManager.instance.elapsedTimeCurrentExperiment);
                                }
                              //  Debug.Log(alarms[alarm[count]].startTime + " " + ExperimentManager.instance.elapsedTimeCurrentExperiment);
                            }
                            break;
                            case WhenStart.START_SCENE:
                             if (isTimingScene)
                             {
                                 if (alarms[alarm[count]].startTime < The3DWorldManager.instance.elapsedTimeCurrentScene )
                                 {
                                  CheckAlarm(count, The3DWorldManager.instance.elapsedTimeCurrentScene);
                               
                                 }
                              
                             }
                         
                                break;
                            case WhenStart.START_TRIAL:
                             if (isTimingTrial)
                             {
                                 if (alarms[alarm[count]].startTime < The3DWorldManager.instance.elapsedTimeCurrentTrial)
                                 {
                                    CheckAlarm(count, The3DWorldManager.instance.elapsedTimeCurrentTrial);
                                 }
                             }
                            break;

                            case WhenStart.CURRENT_TIME:
                            if(alarms[alarm[count]] != null)
                                if (alarms[alarm[count]].startTime < (DateTime.Now-alarms[alarm[count]].tempStart).TotalMilliseconds )
                                {
                                    if (alarms[alarm[count]] != null)
                                         CheckAlarm(count, (DateTime.Now - alarms[alarm[count]].tempStart).TotalMilliseconds);
                                }
                                break;
                     
                         }
                 
                   }
            
            }
    }

    void DebugAlarm(int count,double _elapsed)
    {

        if (!alarms[alarm[count]].isTime&& alarms[alarm[count]].input.hasEventOnPress)
        {
            alarms[alarm[count]].isTime = true;
            DataBank.Instance.SetData("event_name", alarms[alarm[count]].input.inputName);
            DataBank.Instance.SetData("event", alarms[alarm[count]].input.button);
            DataBank.Instance.SetData("event_mode", "press");
            alarms[alarm[count]].input.OnInputPress.Invoke();
        }
        else if(alarms[alarm[count]].isTime)
        {
            if (_elapsed - alarms[alarm[count]].startTime <= alarms[alarm[count]].duration)
            {
                //Hold
                if (alarms[alarm[count]].input.hasEventOnHold)
                {
                    DataBank.Instance.SetData("event_name", alarms[alarm[count]].input.inputName);
                    DataBank.Instance.SetData("event", alarms[alarm[count]].input.button);
                    DataBank.Instance.SetData("event_mode", "hold");
                    alarms[alarm[count]].input.OnInputHold.Invoke();
                }
            }
            else
            {
                //Release
                if (alarms[alarm[count]].input.hasEventOnRelease)
                {
                    alarms[alarm[count]].isTime = false;
                    alarms[alarm[count]].isRelease = true;
                    alarms[alarm[count]].input.OnInputRelease.Invoke();
                    DataBank.Instance.SetData("event_name", alarms[alarm[count]].input.inputName);
                    DataBank.Instance.SetData("event", alarms[alarm[count]].input.button);
                    DataBank.Instance.SetData("event_mode", "release");
                }
            }
        }
 
    }

    void CheckAlarm(int count,double _elapsed)
    {
        if (!alarms[alarm[count]].isTime && alarms[alarm[count]].input.hasEventOnPress)
        {
            alarms[alarm[count]].isTime = true;
            DataBank.Instance.SetData("event_name", alarms[alarm[count]].input.inputName);
            DataBank.Instance.SetData("event", alarms[alarm[count]].input.button);
            DataBank.Instance.SetData("event_mode", "press");
            alarms[alarm[count]].input.OnInputPress.Invoke();
        }
        else if (alarms[alarm[count]].isTime)
        {
            if (_elapsed - alarms[alarm[count]].startTime <= alarms[alarm[count]].duration && alarms[alarm[count]].input.hasEventOnHold)
            {
                //Hold

                DataBank.Instance.SetData("event_name", alarms[alarm[count]].input.inputName);
                DataBank.Instance.SetData("event", alarms[alarm[count]].input.button);
                DataBank.Instance.SetData("event_mode", "hold");
                alarms[alarm[count]].input.OnInputHold.Invoke();
            }
            else
            {
                //Release
                if (alarms[alarm[count]].input.hasEventOnRelease)
                {
                    alarms[alarm[count]].isTime = false;
                    alarms[alarm[count]].isRelease = true;
                    alarms[alarm[count]].input.OnInputRelease.Invoke();
                    DataBank.Instance.SetData("event_name", alarms[alarm[count]].input.inputName);
                    DataBank.Instance.SetData("event", alarms[alarm[count]].input.button);
                    DataBank.Instance.SetData("event_mode", "release");
                }
            }
        }


    }

    #endregion UnityCall
    public void SetTimeScene()
    {
        The3DWorldManager.instance.elapsedTimeCurrentScene = 0;
        lastTimestampScene = System.DateTime.Now;
    }
    //Initialize Each new Scene the elaspedTimeCurrentScene to 0


    public void StopTimerScene()
    {
        isTimingScene = false;
        The3DWorldManager.instance.elapsedTimeCurrentScene = 0;
    }

    public void SetTimerExperiment()
    {
        The3DWorldManager.instance.elapsedTimeCurrentExperiment = 0;
        lastTimestampExperiment = System.DateTime.Now;
        isTimingExperiment = true;
    }

    public void SetTimerTrial()
    {
        The3DWorldManager.instance.elapsedTimeCurrentTrial = 0;
        lastTimestampTrial = System.DateTime.Now;
        isTimingTrial = true;
    }

    public Alarm CreateTimer(long _id, float _startTime,string _whenStartTimed,string _nameInput/*, float _duration*/)
    {
        if (!alarms.ContainsKey(_id))
        {
            alarms[_id] = new Alarm();
            alarms[_id].input = new InputUnityEvent();
            alarms[_id].input.OnInputPress = new CustomUnityEvent();
            alarms[_id].input.OnInputHold = new CustomUnityEvent();
            alarms[_id].input.OnInputRelease = new CustomUnityEvent();
            alarms[_id].input.button = "Timer";
            alarms[_id].isAlreadyTime = false;
            alarms[_id].input.inputName = _nameInput;
            alarms[_id].startTime = _startTime;
            switch (_whenStartTimed)
            {
                case "Start Timed":
                    alarms[_id].whenStart = WhenStart.START_SCENE;
                    break;
                case "Start Trial":
                    alarms[_id].whenStart = WhenStart.START_TRIAL;
                    break;
                case "Start Experiment":
                    alarms[_id].whenStart = WhenStart.START_EXPERIMENT;
                    break;
                case "Current Time":
                    alarms[_id].whenStart = WhenStart.CURRENT_TIME;
                    alarms[_id].tempStart = DateTime.Now;
                    break;
            }
            alarm.Add(_id);
        }
        else
        {
            alarms[_id].isAlreadyTime = false;
        }
        return alarms[_id];
        /*
        Alarm alarm = new Alarm();
      
        alarm.startTime = _startTime;
       // alarm.duration =  _duration;
        alarm.input = new InputUnityEvent();
        alarm.input.OnInputPress = new CustomUnityEvent();
        alarm.input.OnInputHold = new CustomUnityEvent();
        alarm.input.OnInputRelease = new CustomUnityEvent();
        alarm.input.button = "Timer";
        return alarm;
        */
    }
    public InputUnityEvent TakeTimer(long _id/*,float _duration*/)
    {
        if(alarms.ContainsKey(_id))
        {
            alarms[_id].isTime = false;
            return alarms[_id].input;
        }
        return null;
    }

    public void CleanSpecificTimed(long _id)
    {
        if (alarms.ContainsKey(_id))
        {
            alarms.Remove(_id);
            alarm.Remove(_id);
           
        }
    }
}

[Serializable]
public class Alarm
{
    public bool isTime;
    public bool isAlreadyTime;
    public bool isRelease;
    public float startTime;
    public float currentTime;
    public float duration;
    public WhenStart whenStart;
    public DateTime tempStart;
    public InputUnityEvent input;
}

public enum WhenStart
{
    START_SCENE,
    START_TRIAL, 
    START_EXPERIMENT,
    CURRENT_TIME
}