using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonUIManager : MonoBehaviour
{
    #region Inspector
    public static ButtonUIManager instance;
    public GameObject prefabButtonUI;

    #endregion Inspector

    #region Privates
    List<InputUnityEvent> inputUI;
    public Dictionary <long,GameObject> buttonUI;

    #endregion Privates

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(this);
        }
        inputUI = new List<InputUnityEvent>();
       
    }
    /*
    public void SetupButtonUI()
    {
        for (int count = 0; count < inputUI.Count; count++)
        {
            if (inputUI[count].hasEventOnPress)
            {
                inputUI[count].OnInputPress.AddListener(inputUI[count].actionPress);
            }
            if (inputUI[count].hasEventOnRelease)
            {
                inputUI[count].OnInputRelease.AddListener(inputUI[count].actionRelease);
            }
            if (inputUI[count].hasEventOnHold)
            {
                inputUI[count].OnInputHold.AddListener(inputUI[count].actionHold);
            }
        }
    }
    */
  /*  public GameObject SetupButton(PTVR.Data.Input.ButtonUI _buttonUI)
    {   GameObject go;
        go = Instantiate(prefabButtonUI, transform);
        ExperimentManager.instance.GameObjectCol[_buttonUI.id] = go;
 
        return go;
    }
  */
  
    public InputUnityEvent LinkButtonUI(PTVR.Data.Event.Button _button)
    {
        GameObject go =null;
        bool firstTime = false;
        //Debug.Log("Link");
        if (The3DWorldManager.instance.GameObjectCol.ContainsKey(_button.idObjectButton))
        {
            go = The3DWorldManager.instance.GameObjectCol[_button.idObjectButton];
           
            if(go.GetComponent<ButtonUI>().inputUnityEvent.isSetup == false)
            {
         
                //go.GetComponent<ButtonUI>().inputUnityEvent.isSetup = true;
                firstTime = true;

            }
           
            if (firstTime)
            {

                inputUI.Add(go.GetComponent<ButtonUI>().SetupButtonUI(_button.eventName));

            }
        }
     
        return go.GetComponent<ButtonUI>().inputUnityEvent;
    }
    /*
    public void CleanButtonUI()
    {
        for(int count = 0; count < buttonUI.Count; count++)
        {
            Destroy(buttonUI[count]);
        }
        inputUI.Clear();
        buttonUI.Clear();
    }
  */
}
