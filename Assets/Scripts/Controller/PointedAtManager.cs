using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class PointedAtManager : MonoBehaviour
{
	#region Inspector
	public static PointedAtManager instance;
	/*
	public List<PointedAtUnityEvent> pointedAt;
	*/
	public GameObject prefabPointedAt;
	public Dictionary<long,GameObject> pointedAtCone = new Dictionary<long,GameObject>();
	public Dictionary<long,PointedAtUnityEvent> pointedAt = new Dictionary<long, PointedAtUnityEvent>();
	
	#endregion Inspector

	#region Privates
	static Transform rotationTransform = null;
	
	#endregion Privates

	#region UnityCall
	void Awake()
    {
		if (instance != null)
		{
			Destroy(this);
		}
		else
		{
			instance = this;
			rotationTransform = new GameObject().transform;
		}
	}
	#endregion UnityCall
	

	public InputUnityEvent TakePointedAt(PTVR.Data.Event.PointedAt _pointedAt)
	{
		if (pointedAt.ContainsKey(_pointedAt.id))
        {
			return pointedAt[_pointedAt.id].input;
		}
		return null;
	}
	public float eccentricity = 0f;
	public float halfMeridian = 0f;
	public PointedAtUnityEvent SetPointed(PTVR.Data.Event.PointedAt _pointedAt)
    {
		PointedAtUnityEvent pointed;
		if (!pointedAt.ContainsKey(_pointedAt.id))
		{ pointed = new PointedAtUnityEvent();

			//Step 1 Link Input
			pointed.pointed = _pointedAt;
			pointed.input = new InputUnityEvent();
			pointed.input.OnInputRelease = new CustomUnityEvent();
			pointed.input.OnInputHold = new CustomUnityEvent();
			pointed.input.OnInputPress = new CustomUnityEvent();
			pointed.input.button = "PointedAt";
			pointed.input.inputName = _pointedAt.eventName;
			
			if (pointed.pointed.targetId != -1)
				pointed.pointed.InitWithGO(The3DWorldManager.instance.GameObjectCol[pointed.pointed.targetId]);
			pointedAt[_pointedAt.id]=pointed ;
		}
		else
        {
			pointed = pointedAt[_pointedAt.id];
		}
		if (!pointedAtCone.ContainsKey(_pointedAt.coneHash))
		{ 
			Transform baseParent = null;
			//TO DO Switch
			switch(pointed.pointed.coneOriginid)
            {
				case -1:
					//HandLeft
					pointedAtCone[_pointedAt.coneHash] = (Instantiate(new GameObject(), SetVisualSettings.instance.controllerModels[0].transform.parent.transform));
					baseParent = SetVisualSettings.instance.controllerModels[0].transform.parent.transform;
					break;
				case -2:
					//HandRight
					pointedAtCone[_pointedAt.coneHash] = (Instantiate(new GameObject(), SetVisualSettings.instance.controllerModels[1].transform.parent.transform));
					baseParent = SetVisualSettings.instance.controllerModels[1].transform.parent.transform;
					break;
				case -3:
					//HeadSet
					pointedAtCone[_pointedAt.coneHash] = (Instantiate(new GameObject(), Camera.main.gameObject.transform));
					baseParent = Camera.main.gameObject.transform;
					break;
				case -4:
					//CurrentCS
					pointedAtCone[_pointedAt.coneHash] = (Instantiate(new GameObject(), SetVisualSettings.instance.current_cs.gameObject.transform));
					baseParent = SetVisualSettings.instance.current_cs.gameObject.transform;
					break;
				case -5:
					//Global
					pointedAtCone[_pointedAt.coneHash] = (Instantiate(new GameObject(), SetVisualSettings.instance.gameObject.transform));
					baseParent = SetVisualSettings.instance.gameObject.transform;
					break;
					// Left Eye
				case -6:
					pointedAtCone[_pointedAt.coneHash] = (Instantiate(new GameObject(), EyesTrackingManager.instance.eyeOrbiteLeft.transform));
					baseParent = EyesTrackingManager.instance.eyeOrbiteLeft.transform;
					break;
					// Right Eye
				case -7:
					pointedAtCone[_pointedAt.coneHash] = (Instantiate(new GameObject(), EyesTrackingManager.instance.eyeOrbiteRight.transform));
					baseParent = EyesTrackingManager.instance.eyeOrbiteRight.transform;
					break;
					// Combined Eye
				case -8:
					pointedAtCone[_pointedAt.coneHash] = (Instantiate(new GameObject(), EyesTrackingManager.instance.eyeOrbiteCombine.transform));
					baseParent = EyesTrackingManager.instance.eyeOrbiteCombine.transform;
					break;
					// Basic Object
				default:
					pointedAtCone[_pointedAt.coneHash] = (Instantiate(new GameObject(), SetVisualSettings.instance.GameObjectCol[_pointedAt.coneOriginid].transform));
					baseParent = SetVisualSettings.instance.GameObjectCol[_pointedAt.coneOriginid].transform;
					break;
		    }
			pointed.pointed.originIs = pointedAtCone[_pointedAt.coneHash];
			//Step 2 : Solution Create Cone with Radis and Height/Depth  
			//Set Orientation
			// Apply Eccentricity and HalfMeridian
			Vector3 direction = Utils.instance.PerimetricRDToXYZ(pointed.pointed.eccentricity, pointed.pointed.halfMeridian, _pointedAt.pointingConeDepth);


			//direction = pointedAtCone[_pointedAt.coneHash].transform.TransformDirection(direction);
			//pointedAtCone[_pointedAt.coneHash].transform.localRotation = Quaternion.LookRotation(direction);
			//pointedAtCone[_pointedAt.coneHash].transform.LookAt(pointedAtCone[_pointedAt.coneHash].transform.position + direction);

			pointedAtCone[_pointedAt.coneHash].transform.RotateAround(pointedAtCone[_pointedAt.coneHash].transform.position, baseParent.transform.up, pointed.pointed.eccentricity /*/ Mathf.PI * 180*/);
			pointedAtCone[_pointedAt.coneHash].transform.RotateAround(pointedAtCone[_pointedAt.coneHash].transform.position, baseParent.transform.forward, pointed.pointed.halfMeridian/* / Mathf.PI * 180*/);
			pointedAtCone[_pointedAt.coneHash].transform.localRotation *= Quaternion.Euler(-90, 0, 0);
			//return baseDirectionTool.transform.forward;
			/*
			pointedAtCone[_pointedAt.coneHash].transform.Rotate(
			  -90f,
				0f,
				0f
				);
			*/
			// Create Cone Setup Interaction Collision
			GameObject go = Instantiate(new GameObject(), pointedAtCone[_pointedAt.coneHash].transform);
		    go.AddComponent<Rigidbody>().useGravity = false;
			go.GetComponent<Rigidbody>().isKinematic = true;
			go.AddComponent<Corn>();

			//Radius must be set in degr�? TODO
			go.GetComponent<Corn>().radius = (_pointedAt.pointingConeDepth * Mathf.Tan(_pointedAt.pointingConeRadiusDeg * Mathf.PI / 180));
			//Depth Cone
			go.GetComponent<Corn>().height = _pointedAt.pointingConeDepth;

			go.GetComponent<Corn>().Setup(pointed);
			go.GetComponent<Corn>().SetupTarget(pointed);
			pointedAt[_pointedAt.id] = pointed;
		}
		else
		{
			//else
			pointed.pointed.originIs = pointedAtCone[_pointedAt.coneHash];
			pointedAtCone[_pointedAt.coneHash].GetComponentInChildren<Corn>().SetupTarget(pointed);
		}
	
		return pointed;
    }
}


[Serializable]
public class PointedAtUnityEvent
{
	[Tooltip("true for inside, false for outside, internal")]
	public bool isInside;
	public PTVR.Data.Event.PointedAt pointed;
	public InputUnityEvent input;
}
