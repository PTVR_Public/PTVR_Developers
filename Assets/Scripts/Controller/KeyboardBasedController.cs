using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using System;

using UnityEngine.InputSystem.Controls;
using JetBrains.Annotations;

public class KeyboardBasedController : MonoBehaviour
{
    #region Inspector
    public static KeyboardBasedController instance;
    public Dictionary<Key, KeyboardUnityEvent> keyboardKeys = new Dictionary<Key, KeyboardUnityEvent>();
    public List <Key> keys = new List<Key>();
    #endregion Inspector

    private void Awake()
    {
        if (instance != null)
            Destroy(this);
        else
            instance = this;

        /*
        for(int count = 0; count < keyboardKey.Count; count++)
        {
            keyboardKey[count].input.OnInputPress = new CustomUnityEvent();
            keyboardKey[count].input.OnInputHold= new CustomUnityEvent();
            keyboardKey[count].input.OnInputRelease = new CustomUnityEvent();
        }
        */

    }
    private void Update()
    {
        for(int count = 0; count < keys.Count;count++)
        {
            if (Keyboard.current[keyboardKeys[keys[count]].key].wasReleasedThisFrame)
            {
                    DataBank.Instance.SetData("event_name", keyboardKeys[keyboardKeys[keys[count]].key].input.inputName);
                    DataBank.Instance.SetData("event", keyboardKeys[keyboardKeys[keys[count]].key].input.button);
                    DataBank.Instance.SetData("event_mode", "release");
                    keyboardKeys[keyboardKeys[keys[count]].key].input.OnInputRelease.Invoke();
                    keyboardKeys[keyboardKeys[keys[count]].key].isPress = false;
                    Debug.Log("release");
                
            }
            else if (Keyboard.current[keyboardKeys[keys[count]].key].wasPressedThisFrame)
            {
                DataBank.Instance.SetData("event_name", keyboardKeys[keyboardKeys[keys[count]].key].input.inputName);
                DataBank.Instance.SetData("event", keyboardKeys[keyboardKeys[keys[count]].key].input.button);
                DataBank.Instance.SetData("event_mode", "press");
                keyboardKeys[keyboardKeys[keys[count]].key].input.OnInputPress.Invoke();
                keyboardKeys[keyboardKeys[keys[count]].key].isPress = true;
            }
            else  if (keyboardKeys[keyboardKeys[keys[count]].key].isPress)
            {
                    DataBank.Instance.SetData("event_name", keyboardKeys[keyboardKeys[keys[count]].key].input.inputName);
                    DataBank.Instance.SetData("event", keyboardKeys[keyboardKeys[keys[count]].key].input.button);
                    DataBank.Instance.SetData("event_mode", "hold");
                    keyboardKeys[keyboardKeys[keys[count]].key].input.OnInputHold.Invoke();

            }

        }
    
    }
        


    public void SetupKeyCap()
    {
        /*
        foreach (KeyValuePair<Key, KeyboardUnityEvent> entry in keyboardKeys)
        {
            if (keyboardKeys[entry.Key].input.hasEventOnPress)
            {
                keyboardKeys[entry.Key].input.OnInputPress.RemoveAllListeners();
                keyboardKeys[entry.Key].input.OnInputPress.AddListener(keyboardKeys[entry.Key].input.actionPress);
            }
            if (keyboardKeys[entry.Key].input.hasEventOnHold)
            {
                keyboardKeys[entry.Key].input.OnInputHold.RemoveAllListeners();
                keyboardKeys[entry.Key].input.OnInputHold.AddListener(keyboardKeys[entry.Key].input.actionHold);
            }
            if (keyboardKeys[entry.Key].input.hasEventOnRelease)
            {
                keyboardKeys[entry.Key].input.OnInputRelease.RemoveAllListeners();
                keyboardKeys[entry.Key].input.OnInputRelease.AddListener(keyboardKeys[entry.Key].input.actionRelease);
            }
        }
        */
       
    }


    public InputUnityEvent TakeKey(Key _key,string _keyName,string _inputName)
    {
        if (!keyboardKeys.ContainsKey(_key))
        {
           keyboardKeys[_key] = new KeyboardUnityEvent();
           keyboardKeys[_key].key = _key;
           keyboardKeys[_key].input = new InputUnityEvent();
           keyboardKeys[_key].input.button= _keyName;
           keyboardKeys[_key].input.inputName = _inputName;
           keyboardKeys[_key].input.OnInputPress = new CustomUnityEvent();
           keyboardKeys[_key].input.OnInputHold = new CustomUnityEvent();
           keyboardKeys[_key].input.OnInputRelease = new CustomUnityEvent();
           keys.Add(_key);
           //Ecrit plusieur fois a corriger
        }
        return keyboardKeys[_key].input;
        /*for (int count = 0; count < keyboardKey.Count; count++)
        {
            if(_key == keyboardKey[count].key)
            {
                //keyboardKey[count].input.button = _key.ToString();
                return keyboardKey[count].input;

            }
        }
        return null;*/
    }
}

[SerializeField]
public class InputEventPair
{
    public enum InputType { KEYBOARD, VR_CONTROLLER, OTHER}
    public InputType inputType;
}

[Serializable]
public class KeyboardUnityEvent : InputEventPair
{
    public Key key;
    public bool isPress;
    public InputUnityEvent input;
}

