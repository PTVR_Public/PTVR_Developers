using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerVRRenderer : MonoBehaviour
{
    GameObject goButtonSystem;
    public Outline buttonSystem;

    GameObject goButtonMenu;
    public Outline buttonMenu;

    GameObject goButtonRGrip;
    public Outline buttonRGrip;

    GameObject goButtonTrackpad;
    public Outline buttonTrackpad;

    GameObject goButtonLGrip;
    public Outline buttonLGrip;

    GameObject goButtonTrigger;
    public Outline buttonTrigger;


    void Update()
    {
        if(goButtonSystem == null && transform.Find("sys_button") !=null)
        {
            goButtonSystem = transform.Find("sys_button").gameObject;
            if (goButtonSystem.GetComponent<Outline>() == null)
            {
                goButtonSystem.AddComponent<Outline>();
                buttonSystem = goButtonSystem.GetComponent<Outline>();
                buttonSystem.GetComponent<Outline>().OutlineWidth = 0;
            }
        }
        if (goButtonMenu == null && transform.Find("button") != null)
        {
            goButtonMenu = transform.Find("button").gameObject;
            if (goButtonMenu.GetComponent<Outline>() == null)
            {
                goButtonMenu.AddComponent<Outline>();
                buttonMenu = goButtonSystem.GetComponent<Outline>();
                buttonMenu.GetComponent<Outline>().OutlineWidth = 0;
            }      
        }
        if (goButtonRGrip == null && transform.Find("rgrip") != null)
        {
            goButtonRGrip = transform.Find("rgrip").gameObject;
            if (goButtonRGrip.GetComponent<Outline>() == null)
            {
                goButtonRGrip.AddComponent<Outline>();
                buttonRGrip = goButtonRGrip.GetComponent<Outline>();
                buttonRGrip.GetComponent<Outline>().OutlineWidth = 0;
            }
        }
        if (goButtonLGrip == null && transform.Find("lgrip") != null)
        {
            goButtonLGrip = transform.Find("lgrip").gameObject;
            if (goButtonLGrip.GetComponent<Outline>() == null)
            {
                goButtonLGrip.AddComponent<Outline>();
                buttonLGrip = goButtonLGrip.GetComponent<Outline>();
                buttonLGrip.GetComponent<Outline>().OutlineWidth = 0;
            }
        }
        if (goButtonTrackpad == null && transform.Find("trackpad") != null)
        {
            goButtonTrackpad = transform.Find("trackpad").gameObject;
            if (goButtonTrackpad.GetComponent<Outline>() == null)
            {
                goButtonTrackpad.AddComponent<Outline>();
                buttonTrackpad = goButtonTrackpad.GetComponent<Outline>();
                buttonTrackpad.GetComponent<Outline>().OutlineWidth = 0;
            }
        }
        if (goButtonTrigger == null && transform.Find("trigger") != null)
        {
            goButtonTrigger = transform.Find("trigger").gameObject;
            if (goButtonTrigger.GetComponent<Outline>() == null)
            {
                goButtonTrigger.AddComponent<Outline>();
                buttonTrigger = goButtonTrigger.GetComponent<Outline>();
                buttonTrigger.OutlineWidth = 0;
            }
        }
    }
}
