using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using Valve.VR;
using UnityEngine.Events;
using System.Collections.Generic;
using System;

public class SteamVRBasedController : XRBaseController
{
    #region Inspector
    // SteamVR Tracking
    [Header("SteamVR Tracking")]
    public SteamVR_Input_Sources inputSource = SteamVR_Input_Sources.Any;
    public SteamVR_Action_Pose poseAction = null;

    // SteamVR Input
    public List<HandControllerUnityEvent> vrInput;
    [Header("SteamVR Input")]
    public SteamVR_Action_Boolean  gripAction = null;

    public SteamVR_Action_Boolean triggerAction = null;

    public SteamVR_Action_Boolean touchpadClickAction = null;

    public SteamVR_Action_Boolean touchpadTouchAction = null;

    public SteamVR_Action_Vector2 touchpadPos = null;
    public float  touchPadPositionX;
    public float touchPadPositionY;

    public SteamVR_Action_Single triggerFloat;

    public Animator animator;
    public float speed;
    #endregion Inspector

    #region Privates
    bool isPressGrip;
    bool isPressTrigger;
    bool isPressTouch;
    bool isPressClick;
    #endregion Privates

    private void Start()
    {
  
        // Start SteamVR
        SteamVR.Initialize();

        vrInput.Add(new HandControllerUnityEvent());
        // added new unknown type
        vrInput[vrInput.Count - 1].type = "right_touch_press";

        for (int count = 0; count < vrInput.Count; count++)
        {
            vrInput[count].input = new InputUnityEvent();
            vrInput[count].input.OnInputPress = new CustomUnityEvent();
            vrInput[count].input.OnInputRelease = new CustomUnityEvent();
            vrInput[count].input.OnInputHold = new CustomUnityEvent();
        }
    }
    protected override void UpdateTrackingInput(XRControllerState controllerState)
    {
        if(controllerState != null)
        {   // Get position from pose action
            Vector3 position = poseAction[inputSource].localPosition;
            controllerState.position = position;

            // Get rotation from pose action
            Quaternion rotation = poseAction[inputSource].localRotation;
            controllerState.rotation = rotation;
        }
    }

    protected override void UpdateInput(XRControllerState controllerState)
    {
        if (controllerState != null)
        {
            // Reset all of the input values
            controllerState.ResetFrameDependentStates();

            // Check select action, apply to controller
            SetInteractionState(ref controllerState.selectInteractionState, gripAction[inputSource]);

            // Check activate action, apply to controller
            SetInteractionState(ref controllerState.activateInteractionState, triggerAction[inputSource]);
            for (int count = 0; count < vrInput.Count; count++)
            {

                switch (vrInput[count].type)
                {

                    case "right_touch_press":
                        if (touchpadClickAction[inputSource].stateDown)
                        {
                            if (!isPressClick)
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "press");
                                vrInput[count].input.OnInputPress.Invoke();
                                Debug.Log("Right Trigger is Press");
                                isPressClick = true;
                            }
                            else
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "hold");
                                vrInput[count].input.OnInputHold.Invoke();
                            }
                        }
                        else if (touchpadClickAction[inputSource].stateUp)
                        {

                            DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                            DataBank.Instance.SetData("event", vrInput[count].type);
                            DataBank.Instance.SetData("event_mode", "release");
                            Debug.Log("Right Trigger is Release");
                            isPressClick = false;
                            vrInput[count].input.OnInputRelease.Invoke();

                        }
                        break;
                    case "right_trigger":
                        if (triggerFloat[inputSource].axis == 1)
                        {
                            if (!isPressTrigger && triggerFloat[inputSource].lastAxis == 1)
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "press");
                                vrInput[count].input.OnInputPress.Invoke();
                                Debug.Log("Right Trigger is Press");
                                isPressTrigger = true;
                            }
                            else
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "hold");
                                vrInput[count].input.OnInputHold.Invoke();
                            }
                        }
                        else if (isPressTrigger)
                        {
                            
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "release");
                                Debug.Log("Right Trigger is Release");
                                isPressTrigger = false;
                                vrInput[count].input.OnInputRelease.Invoke();
                            
                        }
                        break;
                    case "right_grip":
                        if (gripAction[inputSource].lastStateUp != gripAction[inputSource].stateUp && gripAction[inputSource].stateUp)
                        {
                                animator.SetFloat("Grip", 0);
                          
                                isPressGrip = false;
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "release");
                                vrInput[count].input.OnInputRelease.Invoke();
                                Debug.Log("Right Grip is Release");
                            
                        }
                        else if (gripAction[inputSource].stateDown)
                        {
                            if (!isPressGrip)
                            {
                                animator.SetFloat("Grip", 1);
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "press");
                                vrInput[count].input.OnInputPress.Invoke();
                                Debug.Log("Right Grip is Press");
                                isPressGrip = true;
                            }
                            else
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "hold");
                                vrInput[count].input.OnInputHold.Invoke();
                            }
                        }
                        break;
                    case "right_touch":
                        if (touchpadTouchAction[inputSource].lastStateUp != touchpadTouchAction[inputSource].stateUp && touchpadTouchAction[inputSource].stateUp)
                        { 
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "release");
                                vrInput[count].input.OnInputRelease.Invoke();

                                isPressTouch = false;
                                Debug.Log("Right TouchPad is Release");
                            
                        }
                        else if (touchpadTouchAction[inputSource].stateDown)
                        {
                            if (!isPressTouch)
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "press");
                                vrInput[count].input.OnInputPress.Invoke();
                                Debug.Log("Right TouchPad is Touch");
                                isPressTouch = true;
                            }
                            else
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "hold");
                                vrInput[count].input.OnInputHold.Invoke();
                            }
                        }
                        break;
                    case "left_trigger":
                        if (triggerFloat[inputSource].axis == 1)
                        {
                            if (!isPressTrigger && triggerFloat[inputSource].lastAxis == 1)
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "press");
                                vrInput[count].input.OnInputPress.Invoke();
                                Debug.Log("Left Trigger is Press");
                                isPressTrigger = true;
                            }
                            else
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "hold");
                                vrInput[count].input.OnInputHold.Invoke();
                            }
                        }
                        else if (isPressTrigger)
                        {
                            
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "release");
                                isPressTrigger = false;
                                Debug.Log("Left Trigger is Release");
                                vrInput[count].input.OnInputRelease.Invoke();
                            
                        }
                        break;
                    case "left_grip":
                        if   (gripAction[inputSource].lastStateUp != gripAction[inputSource].stateUp && gripAction[inputSource].stateUp)
                        {
                           
                            animator.SetFloat("Grip", 0);

                               isPressGrip = false;
                                DataBank.Instance.SetData("input_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("input", vrInput[count].type);
                                DataBank.Instance.SetData("input_mode", "release");
                                Debug.Log("Left Grip is Release");
                                vrInput[count].input.OnInputRelease.Invoke();
                            
                        }
                        else if (gripAction[inputSource].stateDown)
                        {
                            if (!isPressGrip)
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "press");
                                animator.SetFloat("Grip", 1);
                                vrInput[count].input.OnInputPress.Invoke();
                                Debug.Log("Left Grip is Press");
                                isPressGrip = true;
                            }
                            else
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "hold");
                                vrInput[count].input.OnInputHold.Invoke();
                            }
                        }
                        break;
                    case "left_touch":

                        if (touchpadTouchAction[inputSource].lastStateUp != touchpadTouchAction[inputSource].stateUp && touchpadTouchAction[inputSource].stateUp)
                        {
                            
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "release"); 
                                vrInput[count].input.OnInputRelease.Invoke();
                                Debug.Log("Left TouchPad is Release");
                                isPressTouch = false;
                        }
                        else if (touchpadTouchAction[inputSource].stateDown)
                        {
                            if (!isPressTouch)
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "press");
                                vrInput[count].input.OnInputPress.Invoke();
                                Debug.Log("Left TouchPad is Touch");
                                isPressTouch = true;
                            }
                            else
                            {
                                DataBank.Instance.SetData("event_name", vrInput[count].input.inputName);
                                DataBank.Instance.SetData("event", vrInput[count].type);
                                DataBank.Instance.SetData("event_mode", "hold");
                                vrInput[count].input.OnInputHold.Invoke();
                            }
                        }
                        break;
                }

            }
            //if (touchpadClickAction[inputSource].stateDown)
            //{
              
            //    if (!isPressClick)
            //    {

                   
            //    }
            //    else
            //    {
                   
            //    }
            //}
            //if (touchpadClickAction[inputSource].stateUp)
            //{
            //    isPressClick = false;
                
            //}

            touchPadPositionX = touchpadPos[inputSource].axis.x;
            touchPadPositionY = touchpadPos[inputSource].axis.y;

            if (animator != null && animator.isActiveAndEnabled)
            {
                animator.SetFloat("Trigger", triggerFloat[inputSource].lastAxis);
            }

        }
    }

    private void SetInteractionState(ref InteractionState interactionState, SteamVR_Action_Boolean_Source action)
    {
        // Pressed this frame
        interactionState.activatedThisFrame = action.stateDown;

        // Released this frame
        interactionState.deactivatedThisFrame = action.stateUp;

        // Is pressed
        interactionState.active = action.state;
    }

    public InputUnityEvent TakeHandController(string _input)
    {
        
        for (int count = 0; count < vrInput.Count; count++)
        {
            if (vrInput[count].type == _input)
            {
                return vrInput[count].input;
            }
        }
        return null;
    }

}

[Serializable]
public class HandControllerUnityEvent
{
    public string type;
    public InputUnityEvent input;

}