using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionManager : MonoBehaviour
{

	#region Inspector
	public static PositionManager instance;//Singleton

	public long SaveDelayms = 500;
	public bool hasSaveCameraPosRot = false;
	#endregion Inspector

	#region Privates
	long LastSaveTime;
	long id;
	#endregion Privates

	#region UnityCall
	private void Awake()
	{
		if (instance != null)
		{
			Destroy(this);
			Debug.LogWarning("Double PositionManager.cs");
		}
		else
		{
			instance = this;
		}
	}

	void Start()
	{
		LastSaveTime = System.DateTime.Now.Ticks / System.TimeSpan.TicksPerMillisecond;
	}

	public void Update()
	{

		if (The3DWorldManager.instance.state == SystemState.END_EXPERIMENT)
			return;
		long CurrentTime = System.DateTime.Now.Ticks / System.TimeSpan.TicksPerMillisecond;

		if (hasSaveCameraPosRot && (CurrentTime - LastSaveTime) > SaveDelayms)
		{
			LastSaveTime = CurrentTime;
			SaveCameraPositionRotation();
		}


	}
	#endregion UnityCall

	public void SetSaveCameraPosRot(bool isSave)
    {
		hasSaveCameraPosRot = true;
		ExporterManager.instance.Setup("headset_data", "trial;scene_id;elapsed_frames;date_of_exp_run;timestamp;timestamp_ms;headset_x_global;headset_y_global;headset_z_global;headset_x_rot;headset_y_rot;headset_z_rot;ecc_headset_forward_direction;hm_headset_forward_direction;");
	}

	public void SetId(long _id)
	{
		id = _id;
	}

	public void SaveCameraPositionRotation(string aDate = "")
	{
		Transform camTrans = Camera.main.transform;
		Vector3 camPos = camTrans.position; // local position before
		Vector3 camRot = camTrans.eulerAngles; // local euler angles before
		DataBank.Instance.SetData("elapsed_frames", Time.frameCount.ToString());
		DataBank.Instance.SetData("date_of_exp_run", ExporterManager.instance.startDate);

		if (aDate == "")
		{
			System.DateTime var_date = System.DateTime.Now;
			DataBank.Instance.SetData("timestamp", var_date.ToString("yyyy-M-dd--HH-mm-ss.fff"));
			DataBank.Instance.SetData("timestamp_ms", format_date_time(var_date).ToString());
		}
		else
		{
			System.DateTime var_date = System.DateTime.Parse(aDate);
			DataBank.Instance.SetData("timestamp", aDate);
			DataBank.Instance.SetData("timestamp_ms", format_date_time(var_date).ToString());
		}

		DataBank.Instance.SetData("headset_x_global", camPos.x.ToString());
		DataBank.Instance.SetData("headset_y_global", camPos.y.ToString());
		DataBank.Instance.SetData("headset_z_global", camPos.z.ToString());

		DataBank.Instance.SetData("headset_x_rot", camRot.x.ToString());
		DataBank.Instance.SetData("headset_y_rot", camRot.y.ToString());
		DataBank.Instance.SetData("headset_z_rot", camRot.z.ToString());

		Vector2 camperim = Utils.instance.XYZtoPerimetric(camTrans.forward[0], camTrans.forward[1], camTrans.forward[2]);
		DataBank.Instance.SetData("ecc_headset_forward_direction", camperim[0].ToString());
		DataBank.Instance.SetData("hm_headset_forward_direction", camperim[1].ToString());

		//Vector2 camdeg = Utils.instance.XYZtoXYdeg(camTrans.forward[0], camTrans.forward[1], camTrans.forward[2]);
		//DataBank.Instance.SetData("thetax_headset_forward_direction", camdeg[0].ToString());
		//DataBank.Instance.SetData("thetay_headset_forward_direction", camdeg[1].ToString());

		ExporterManager.instance.WriteFile("headset_data");
	}

	public int format_date_time(System.DateTime date)
	{
		return date.Hour * 3600000 + date.Minute * 60000 + date.Second * 1000 + date.Millisecond;
	}
}
