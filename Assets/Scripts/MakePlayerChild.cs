using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakePlayerChild : MonoBehaviour
{
    Transform player;
    //Transform text;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("OpenXRPlayer").transform;

        //text = GameObject.Find("TextMeshPro(Clone)").transform;

        player.parent = transform;

        //text.parent = transform;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
