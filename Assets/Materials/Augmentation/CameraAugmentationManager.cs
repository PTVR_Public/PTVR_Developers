using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAugmentationManager : MonoBehaviour
{
    Camera cam;
    public Transform reticle;
    public Transform renderROI;
    public Vector3 offset;
    public float value;
    public float offsetdistancecam;
    void Start()
    {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        cam.fieldOfView = value;
        //transform.position = new Vector3( reticle.position.x, reticle.position.y, Camera.main.transform.position.z) ;
       
        transform.LookAt(reticle);
        renderROI.LookAt(transform);
        reticle.LookAt(transform);
        renderROI.transform.Rotate(0, 180, 0);
        renderROI.transform.position = reticle.position + offset;
    }
}
