#!/bin/bash

# Author: jtm
# Date: 05/08/2022

if [ X$1 = X ]
then
        echo "**"
        echo "** CopyPrivateToPublic.sh"
        echo "**"
        echo "** This script is intended to help you for copy file from privates to publics"
        echo "**"
        echo "** Usage: CopyPrivateToPublic privates_path_researchers , publics_path_researchers , private_path_developpers, publics_path_developpers" 
        echo "**"
        echo "** Todo:  indicate where is your privates PTVR_Researchers and where is your publics PTVR_Researchers"
        echo "**                       is your privates PTVR_Developpers and where is your publics PTVR_Developpers"
        echo "** Note: "
        echo "** Warning: This script works if file names don't have space"
        echo "**"
        echo "** Example: ./CopyPrivateToPublic.sh privates_path_researchers  publics_path_researchers  private_path_developpers publics_path_developpers"
        exit
fi

# Variables
exclude_researchers = --exclude JSON_Files/ --exclude Results/  --exclude Debugs/ --exclude Experiments/ --exclude conda/ --exclude .gitignore --exclude .gitlab-ci.yml --exclude *.txt --exclude .git/
privates_path_researchers=$1
publics_path_researchers=$2

exclude_developpers = --exclude PTVR_Researchers/ --exclude conda/ --exclude .gitignore --exclude .gitlab-ci.yml --exclude *.txt --exclude .git/
privates_path_developpers=$3
publics_path_developpers=$4

rsync -a $privates_path_researchers $publics_path_researchers exclude_researchers
rsync -a $privates_path_developpers $publics_path_developpers exclude_researchers



printf '#!/bin/bash\n'
printf '\n# Copy COMMANDS\n'
printf '# COPY: Researchers %s -----> %s\n' "$privates_path_researchers" "$publics_path_researchers"
printf '# COPY: Developpers %s -----> %s\n' "$privates_path_developpers" "$publics_path_developpers"
printf '# DATE: %s' $(date +'%m/%d/%Y')
printf '\n'




