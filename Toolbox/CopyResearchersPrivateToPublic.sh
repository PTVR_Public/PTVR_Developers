#!/bin/bash

# Author: jtm
# Date: 05/08/2022

if [ X$1 = X ]
then
        echo "**"
        echo "** CopyResearchersPrivateToPublic.sh"
        echo "**"
        echo "** This script is intended to help you for copy file from privates to publics"
        echo "**"
        echo "** Usage: CopyResearchersPrivateToPublic privates_path , publics_path"
        echo "**"
        echo "** Todo:  indicate where is your privates PTVR_Researchers and where is your publics PTVR_Researchers"
        echo "**"
        echo "** Note: "
        echo "** Warning: This script works if file names don't have space"
        echo "**"
        echo "** Example: ./CopyResearchersPrivateToPublic.sh privates_path , publics_path
        exit
fi

# Variables
privates_path=$1
publics_path=$2
rsync -a $privates_path $publics_path --exclude JSON_Files/ --exclude Results/  --exclude Debugs/ --exclude Experiments/ --exclude conda/ --exclude .gitignore --exclude .gitlab-ci.yml --exclude *.txt --exclude .git/



printf '#!/bin/bash\n'
printf '\n# Copy COMMANDS\n'
printf '# COPY: %s -----> %s\n' "$privates_path" "$publics_path"
printf '# DATE: %s' $(date +'%m/%d/%Y')
printf '\n'




