#!/bin/bash

# RENAMING COMMANDS
# GOAL: graphId -----> graph_id
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/GraphUI/graph_UI_demo.py, at line 124
# I propose to replace graphId by graph_id in :     eventAddNewPoint = event.AddNewPoint( graphId=graphUI.id)
sed -i "124s/graphId/graph_id/" ../PTVR_Researchers/Python_Scripts/Demos/GraphUI/graph_UI_demo.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/GraphUI/graph_UI_demo.py, at line 126
# I propose to replace graphId by graph_id in :     eventShowAllPoints = event.ShowGraphPoints(graphId=graphUI.id)
sed -i "126s/graphId/graph_id/" ../PTVR_Researchers/Python_Scripts/Demos/GraphUI/graph_UI_demo.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/mrs_cps_test.py, at line 87
# I propose to replace graphId by graph_id in :     eventAddNewPoint = event.AddNewPoint( graphId=graphUI.id, isInvariantCulture=True)
sed -i "87s/graphId/graph_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/mrs_cps_test.py, at line 131
# I propose to replace graphId by graph_id in :     eventShowGraphPoints = event.ShowGraphPoints(graphId=graphUI.id)
sed -i "131s/graphId/graph_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_graph.py, at line 54
# I propose to replace graphId by graph_id in :     eventUpdateGraph = event.UpdateGraph(graphId=graph.id,parameter1=parameter1, parameter2=parameter2)
sed -i "54s/graphId/graph_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_graph.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 173
# I propose to replace graphId by graph_id in :     eventShowGraphPoints = event.AddNewPoint(graphId = graphUI.id, isInvariantCulture = True)
sed -i "173s/graphId/graph_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 174
# I propose to replace graphId by graph_id in :     eventShowAllPoints = event.ShowGraphPoints(graphId = graphUI.id)
sed -i "174s/graphId/graph_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 175
# I propose to replace graphId by graph_id in :     eventDeleteLastPoint = event.DeleteLastPoint(graphId = graphUI.id)
sed -i "175s/graphId/graph_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 177
# I propose to replace graphId by graph_id in :     #eventAddMisreadWords = event.AddNewPoint(graphId = misread_words_graph.id,isInvariantCulture = True)
sed -i "177s/graphId/graph_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 509
# I propose to replace graphId by graph_id in :     eventShowGraphPoints = event.ShowGraphPoints(graphId=graphUI_created_id)
sed -i "509s/graphId/graph_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py
