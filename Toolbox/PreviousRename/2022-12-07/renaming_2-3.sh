#!/bin/bash

# RENAMING COMMANDS
# GOAL: Experiment -----> The3DWorld
# DATE: 12/07/2022

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 893
# I propose to replace Experiment by The3DWorld in :     PTVR.SystemUtils.LaunchExperiment (username)
sed -i.bak "893s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 170
# I propose to replace Experiment by The3DWorld in : exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "170s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 555
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "555s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 560
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "560s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Vestibular/vestibular.py, at line 57
# I propose to replace Experiment by The3DWorld in :     PTVR.SystemUtils.LaunchExperiment (posner.username)
sed -i.bak "57s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Vestibular/vestibular.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 3
# I propose to replace Experiment by The3DWorld in : ...\PTVR_Researchers\Python_Scripts\Experiments\Saroi\saroi.py
sed -i.bak "3s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 1100
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file 
sed -i.bak "1100s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 1108
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "1108s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 171
# I propose to replace Experiment by The3DWorld in :     PTVR.SystemUtils.LaunchExperiment("subject") 
sed -i.bak "171s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 190
# I propose to replace Experiment by The3DWorld in :     PTVR.SystemUtils.LaunchExperiment("subject") 
sed -i.bak "190s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 686
# I propose to replace Experiment by The3DWorld in :     execute_post_treatment_script = event.ExecutePythonScript(script_path="Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment",
sed -i.bak "686s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 263
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment(parameters.subject_name) 
sed -i.bak "263s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py, at line 78
# I propose to replace Experiment by The3DWorld in :     # Write the Experiment to file
sed -i.bak "78s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py, at line 80
# I propose to replace Experiment by The3DWorld in :     print("The Experiment has been written.")
sed -i.bak "80s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py, at line 85
# I propose to replace Experiment by The3DWorld in :         #PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "85s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py, at line 96
# I propose to replace Experiment by The3DWorld in :     # Write the Experiment to file
sed -i.bak "96s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py, at line 98
# I propose to replace Experiment by The3DWorld in :     print("The Experiment has been written.")
sed -i.bak "98s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py, at line 103
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "103s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 33
# I propose to replace Experiment by The3DWorld in : detailsExperiment="Typography Demo to visualize the difference of fontsize between differents fonts when they have the same physical height."
sed -i.bak "33s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 132
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "132s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 171
# I propose to replace Experiment by The3DWorld in :     PTVR.SystemUtils.LaunchExperiment(username="demo") 
sed -i.bak "171s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py, at line 65
# I propose to replace Experiment by The3DWorld in :     # Write the Experiment to file
sed -i.bak "65s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py, at line 67
# I propose to replace Experiment by The3DWorld in :     print("The Experiment has been written.")
sed -i.bak "67s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py, at line 72
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "72s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 35
# I propose to replace Experiment by The3DWorld in : detailsExperiment = "Demo which displays one letter and one cube. Both of them have the same size. Demo used to proove internal consistency in Unity. "
sed -i.bak "35s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 76
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment = detailsExperiment)
sed -i.bak "76s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 89
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment( username)
sed -i.bak "89s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 34
# I propose to replace Experiment by The3DWorld in : detailsExperiment= "This demo displays one phrase following MNRead's standards."
sed -i.bak "34s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 75
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment = detailsExperiment)
sed -i.bak "75s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 92
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "92s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 146
# I propose to replace Experiment by The3DWorld in :     # Write the Experiment to file
sed -i.bak "146s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 149
# I propose to replace Experiment by The3DWorld in :     print("The Experiment has been written.")
sed -i.bak "149s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 154
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "154s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 33
# I propose to replace Experiment by The3DWorld in : detailsExperiment = "This demo recreate the MNRead Carboard, by displaying 3 sentences at 3 different sizes."
sed -i.bak "33s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 81
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=detailsExperiment)
sed -i.bak "81s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 94
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "94s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 166
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment(username) 
sed -i.bak "166s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 273
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment("Jo") 
sed -i.bak "273s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py, at line 45
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "45s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py, at line 50
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchThe3DWorld(username) # Launch the Experiment with PTVR.
sed -i.bak "50s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 46
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "46s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 64
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "64s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 69
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "69s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py, at line 41
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "41s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py, at line 49
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "49s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py, at line 54
# I propose to replace Experiment by The3DWorld in :             #PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "54s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 70
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "70s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 125
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "125s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 130
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "130s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 90
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "90s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 144
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "144s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 149
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "149s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 52
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "52s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 83
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "83s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 88
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "88s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 72
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "72s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 152
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "152s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 157
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "157s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 142
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "142s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 147
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchThe3DWorld() # Launch the Experiment with PTVR.
sed -i.bak "147s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 57
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "57s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 87
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "87s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 92
# I propose to replace Experiment by The3DWorld in :             #PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "92s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 68
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "68s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 140
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "140s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 145
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "145s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 47
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "47s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 73
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "73s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 78
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "78s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 66
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "66s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 127
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "127s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 132
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "132s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 71
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "71s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 133
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "133s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 138
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "138s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 59
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "59s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 77
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "77s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 82
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "82s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 142
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "142s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 188
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "188s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 193
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "193s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 70
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "70s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 152
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "152s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 157
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "157s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 143
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "143s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 363
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "363s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 368
# I propose to replace Experiment by The3DWorld in :             #PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "368s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 67
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "67s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 125
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "125s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 130
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "130s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 138
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "138s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 176
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "176s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 181
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "181s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py, at line 50
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "50s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py, at line 66
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "66s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py, at line 71
# I propose to replace Experiment by The3DWorld in :             #PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "71s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 67
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment = text_description)
sed -i.bak "67s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 95
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "95s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 100
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "100s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py, at line 80
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "80s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 74
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "74s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 106
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "106s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 111
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "111s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 83
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "83s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 153
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "153s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 158
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "158s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 70
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "70s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 133
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "133s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 138
# I propose to replace Experiment by The3DWorld in :             #PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "138s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 67
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "67s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 143
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "143s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 148
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "148s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py, at line 45
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "45s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py, at line 84
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "84s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py, at line 90
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "90s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py, at line 62
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "62s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 39
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "39s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 58
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "58s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 63
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "63s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 38
# I propose to replace Experiment by The3DWorld in : """The scene while change """ + str(experiment_duration_in_ms) + " in ms since the start Experiment change the background color to Red"""
sed -i.bak "38s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 58
# I propose to replace Experiment by The3DWorld in :    my_input_timer_experiment = input.Timer (delay_in_ms=experiment_duration_in_ms,input_name="experiment",timer_start="Start Experiment")
sed -i.bak "58s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 77
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "77s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 82
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "82s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 87
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "87s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py, at line 116
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (name_of_subject=username, detailsExperiment=text_description )
sed -i.bak "116s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py, at line 183
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "183s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py, at line 38
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "38s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py, at line 65
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "65s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py, at line 70
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "70s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py, at line 40
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "40s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py, at line 76
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "76s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py, at line 81
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "81s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py, at line 43
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "43s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py, at line 76
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "76s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py, at line 80
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "80s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py, at line 70
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "70s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py, at line 74
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "74s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py, at line 79
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "79s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py, at line 119
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (name_of_subject=username, detailsExperiment=text_description )
sed -i.bak "119s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py, at line 176
# I propose to replace Experiment by The3DWorld in :             #PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "176s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 57
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "57s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 181
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "181s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 188
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "188s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py, at line 63
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "63s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py, at line 66
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "66s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py, at line 71
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "71s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py, at line 40
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "40s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py, at line 61
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "61s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py, at line 66
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "66s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py, at line 42
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=textDescription)
sed -i.bak "42s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py, at line 52
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "52s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py, at line 57
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "57s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 41
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "41s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 52
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "52s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 57
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "57s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py, at line 50
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "50s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py, at line 54
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "54s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 98
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld ( detailsExperiment=text_description)
sed -i.bak "98s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 149
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "149s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py, at line 104
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.          
sed -i.bak "104s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py, at line 120
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.          
sed -i.bak "120s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py, at line 135
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld ( detailsExperiment=text_description)
sed -i.bak "135s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py, at line 193
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "193s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 95
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (detailsExperiment=text_description)
sed -i.bak "95s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 175
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "175s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py, at line 138
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld ( detailsExperiment=text_description)
sed -i.bak "138s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py, at line 187
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "187s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py, at line 69
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "69s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py, at line 101
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "101s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py, at line 106
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "106s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py, at line 62
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "62s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py, at line 85
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "85s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py, at line 90
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "90s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py, at line 101
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "101s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py, at line 172
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "172s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py, at line 136
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld ( detailsExperiment=text_description)
sed -i.bak "136s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py, at line 187
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "187s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 171
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (detailsExperiment=text_description)
sed -i.bak "171s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 173
# I propose to replace Experiment by The3DWorld in :     # Set ExperimentalCoordinatesSystem
sed -i.bak "173s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 193
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "193s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 94
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "94s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 117
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "117s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 122
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()# Launch the Experiment with PTVR.
sed -i.bak "122s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 97
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "97s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 117
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "117s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 122
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()# Launch the Experiment with PTVR.
sed -i.bak "122s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py, at line 121
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(parameters.subject_name) 
sed -i.bak "121s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 167
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "167s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 264
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(parameters.subject_name) 
sed -i.bak "264s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 222
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "222s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 222
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "222s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py, at line 38
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "38s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py, at line 41
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "41s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py, at line 47
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "47s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 69
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "69s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 122
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "122s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 127
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "127s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 17
# I propose to replace Experiment by The3DWorld in : if you do not change the internal default "jsonFileCategory" parameter when calling the object Experiment,
sed -i.bak "17s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 40
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (detailsExperiment=text_description)
sed -i.bak "40s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 50
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to a .json file
sed -i.bak "50s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 54
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment ()
sed -i.bak "54s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 78
# I propose to replace Experiment by The3DWorld in : The location where you save your JSON files depends on the "jsonFileCategory" field/parameter of the object Experiment.
sed -i.bak "78s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 80
# I propose to replace Experiment by The3DWorld in : When you do not pass the parameter "jsonFileCategory" to the object Experiment, this parameter
sed -i.bak "80s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 84
# I propose to replace Experiment by The3DWorld in :                             "PTVR_ROOT.../Tests/", "PTVR_ROOT.../Experiments/", or "PTVR_ROOT.../Demos/" 
sed -i.bak "84s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 94
# I propose to replace Experiment by The3DWorld in :                             "PTVR_ROOT.../Tests/", "PTVR_ROOT.../Experiments/", or "PTVR_ROOT.../Demos/" 
sed -i.bak "94s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 105
# I propose to replace Experiment by The3DWorld in : you will have to pass your own "jsonFileCategory" parameter to the object Experiment, in which case
sed -i.bak "105s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 22
# I propose to replace Experiment by The3DWorld in : if you do  change the internal default "jsonFileCategory" parameter when calling the object Experiment,
sed -i.bak "22s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 45
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (detailsExperiment=text_description)
sed -i.bak "45s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 52
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to a .json file
sed -i.bak "52s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 56
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment (jsonFileCategory=my_json_file_category)
sed -i.bak "56s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py, at line 86
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment="")
sed -i.bak "86s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py, at line 90
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "90s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py, at line 95
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "95s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py, at line 35
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "35s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py, at line 76
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "76s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py, at line 81
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "81s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py, at line 37
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "37s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py, at line 79
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "79s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py, at line 84
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "84s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py, at line 39
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "39s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py, at line 65
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "65s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py, at line 70
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "70s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py, at line 56
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "56s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py, at line 76
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "76s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py, at line 81
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "81s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py, at line 52
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "52s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py, at line 71
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "71s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py, at line 76
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "76s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py, at line 36
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "36s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py, at line 119
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "119s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py, at line 125
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "125s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py, at line 37
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "37s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py, at line 51
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "51s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py, at line 55
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "55s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py, at line 35
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "35s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py, at line 63
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "63s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py, at line 67
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "67s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py, at line 40
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "40s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py, at line 54
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "54s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py, at line 58
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "58s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 66
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "66s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 71
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "71s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 76
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "76s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py, at line 33
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "33s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py, at line 47
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "47s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py, at line 52
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "52s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py, at line 53
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "53s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py, at line 71
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "71s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py, at line 76
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "76s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py, at line 36
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description, operatorMode=True)
sed -i.bak "36s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py, at line 85
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "85s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py, at line 91
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username) # Launch the Experiment with PTVR.
sed -i.bak "91s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py, at line 33
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "33s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py, at line 46
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "46s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py, at line 51
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "51s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py, at line 69
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "69s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py, at line 90
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "90s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py, at line 95
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "95s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py, at line 54
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "54s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py, at line 76
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "76s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py, at line 81
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "81s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py, at line 130
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "130s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py, at line 155
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "155s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py, at line 160
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "160s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py, at line 32
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "32s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py, at line 46
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "46s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py, at line 51
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "51s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py, at line 48
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(detailsExperiment=text_description)
sed -i.bak "48s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py, at line 70
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "70s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py, at line 75
# I propose to replace Experiment by The3DWorld in :             #PTVR.SystemUtils.LaunchExperiment(username)
sed -i.bak "75s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py, at line 47
# I propose to replace Experiment by The3DWorld in :                                         detailsExperiment = text_description )
sed -i.bak "47s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py, at line 127
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "127s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py, at line 132
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "132s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py, at line 38
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (name_of_subject = username, detailsExperiment=text_description)
sed -i.bak "38s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py, at line 54
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "54s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py, at line 59
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "59s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py, at line 41
# I propose to replace Experiment by The3DWorld in :    exp = visual.The3DWorld (name_of_subject = username, detailsExperiment=text_description)
sed -i.bak "41s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py, at line 53
# I propose to replace Experiment by The3DWorld in :    exp.write() # Write the Experiment to .json file
sed -i.bak "53s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py, at line 58
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "58s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py, at line 49
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (name_of_subject = username, detailsExperiment=text_description)
sed -i.bak "49s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py, at line 54
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "54s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py, at line 59
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "59s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 75
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (name_of_subject = username, detailsExperiment=text_description)
sed -i.bak "75s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 79
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "79s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 84
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "84s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 78
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "78s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 85
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment ( )
sed -i.bak "85s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py, at line 116
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "116s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py, at line 78
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "78s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.1_create_object_in_cartesian_coord_+_object_rotation.py, at line 53
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "53s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.1_create_object_in_cartesian_coord_+_object_rotation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.0_create_object_in_cartesian_coord.py, at line 48
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "48s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.0_create_object_in_cartesian_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/3_rotation_animated_about_one_axis.py, at line 48
# I propose to replace Experiment by The3DWorld in :     PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "48s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/3_rotation_animated_about_one_axis.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py, at line 50
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "50s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py, at line 65
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "65s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py, at line 89
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment( )
sed -i.bak "89s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py, at line 80
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "80s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py, at line 71
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "71s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py, at line 65
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "65s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py, at line 72
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "72s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 3
# I propose to replace Experiment by The3DWorld in : ...\PTVR_Researchers\Python_Scripts\Demos\Experiment_building\4_succession_of_blocks_w_trials.py
sed -i.bak "3s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 108
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file    
sed -i.bak "108s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 114
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment() # Launch the Experiment with PTVR.
sed -i.bak "114s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py, at line 3
# I propose to replace Experiment by The3DWorld in : ...\Demos\Experiment_building\loop_of_trials_w_several_scenes_per_trial.py
sed -i.bak "3s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py, at line 28
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld ( name_of_subject = "toto", detailsExperiment= "you can describe your experiment in this string" )
sed -i.bak "28s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py, at line 50
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "50s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py, at line 55
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment ()
sed -i.bak "55s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py, at line 20
# I propose to replace Experiment by The3DWorld in : change the internal default "resultsPath" parameter when calling the object Experiment,
sed -i.bak "20s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py, at line 43
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (name_of_subject = username, detailsExperiment = text_description,resultsPath = my_results_path )
sed -i.bak "43s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py, at line 58
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment ()
sed -i.bak "58s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 20
# I propose to replace Experiment by The3DWorld in : the internal default "resultsPath" parameter when calling the object Experiment,
sed -i.bak "20s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 25
# I propose to replace Experiment by The3DWorld in : you will have to pass your own "resultsPath" to the object Experiment, in which case
sed -i.bak "25s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 39
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (name_of_subject = username, detailsExperiment=text_description)
sed -i.bak "39s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 59
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment ()
sed -i.bak "59s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 86
# I propose to replace Experiment by The3DWorld in : The location where you save your results files depends on the "resultsPath" field/parameter of the object Experiment
sed -i.bak "86s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 88
# I propose to replace Experiment by The3DWorld in : When you do not pass the parameter "resultsPath" to the object Experiment, this parameter
sed -i.bak "88s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 92
# I propose to replace Experiment by The3DWorld in :                             "PTVR_ROOT.../Tests/", "PTVR_ROOT.../Experiments/", or "PTVR_ROOT.../Demos/" 
sed -i.bak "92s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 105
# I propose to replace Experiment by The3DWorld in :                             "PTVR_ROOT.../Tests/", "PTVR_ROOT.../Experiments/", or "PTVR_ROOT.../Demos/" 
sed -i.bak "105s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 113
# I propose to replace Experiment by The3DWorld in : you will have to pass your own "resultsPath" parameter to the object Experiment, in which case
sed -i.bak "113s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py, at line 3
# I propose to replace Experiment by The3DWorld in : ...\Demos\Experiment_building\1_loop_of_trials_w_one_scene_per_trial.py
sed -i.bak "3s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py, at line 28
# I propose to replace Experiment by The3DWorld in :                                         detailsExperiment= "you can describe your experiment in this string" )
sed -i.bak "28s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py, at line 49
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "49s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py, at line 54
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment ()
sed -i.bak "54s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py, at line 7
# I propose to replace Experiment by The3DWorld in :     ...\Demos\Experiment_building\1_loop_of_trials_w_one_scene_per_trial.py
sed -i.bak "7s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py, at line 56
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "56s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py, at line 60
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment ()
sed -i.bak "60s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 117
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "117s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 94
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "94s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 133
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "133s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.2_create_one_rotated_flat_screen.py, at line 47
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "47s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.2_create_one_rotated_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 95
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "95s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 120
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "120s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.2_impossible_to_rotate_tangent_screen.py, at line 49
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "49s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.2_impossible_to_rotate_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 103
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "103s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 105
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "105s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 220
# I propose to replace Experiment by The3DWorld in :             #PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "220s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 224
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "224s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 202
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "202s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py, at line 83
# I propose to replace Experiment by The3DWorld in :     # Write the Experiment to file
sed -i.bak "83s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py, at line 85
# I propose to replace Experiment by The3DWorld in :     print("The Experiment has been written.")
sed -i.bak "85s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py, at line 90
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "90s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 4
# I propose to replace Experiment by The3DWorld in : # based on file in \...Experiments\MNRead\MNREAD_Related\typography_comparison.py
sed -i.bak "4s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 7
# I propose to replace Experiment by The3DWorld in : # Nextcloud2\DEVISE\Devise PIs\Python files to keep aside\Python Scripts before cleaning \Demos\Experiments\MNRead\MNREAD_Related\typography_comparison.py
sed -i.bak "7s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 272
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "272s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/1_hello_world.py, at line 27
# I propose to replace Experiment by The3DWorld in :         PTVR.SystemUtils.LaunchExperiment() 
sed -i.bak "27s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/1_hello_world.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py, at line 81
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "81s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py, at line 86
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "86s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py, at line 115
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (name_of_subject=username, detailsExperiment=text_description )
sed -i.bak "115s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py, at line 182
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "182s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py, at line 50
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(name_of_subject = username, detailsExperiment=text_description)
sed -i.bak "50s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py, at line 100
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "100s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py, at line 105
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "105s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py, at line 35
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld (name_of_subject = username, detailsExperiment=text_description)
sed -i.bak "35s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py, at line 47
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "47s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py, at line 52
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "52s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py, at line 45
# I propose to replace Experiment by The3DWorld in :     exp = visual.The3DWorld(name_of_subject = username, detailsExperiment=text_description)
sed -i.bak "45s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py, at line 91
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "91s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py, at line 95
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "95s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 67
# I propose to replace Experiment by The3DWorld in :                                         detailsExperiment = description_of_experiment )
sed -i.bak "67s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 150
# I propose to replace Experiment by The3DWorld in :     exp.write() # Write the Experiment to .json file
sed -i.bak "150s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 155
# I propose to replace Experiment by The3DWorld in :             PTVR.SystemUtils.LaunchExperiment()
sed -i.bak "155s/Experiment/The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py
