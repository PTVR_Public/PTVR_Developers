#!/bin/bash

# RENAMING COMMANDS
# GOAL: Write the experiment -----> Write The3DWorld
# DATE: 12/07/2022

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 635
# I propose to replace Write the experiment by Write The3DWorld in :     # Write the experiment to .json file
sed -i.bak "635s/Write the experiment/Write The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 251
# I propose to replace Write the experiment by Write The3DWorld in :     # Write the experiment to file
sed -i.bak "251s/Write the experiment/Write The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 125
# I propose to replace Write the experiment by Write The3DWorld in :     # Write the experiment to file
sed -i.bak "125s/Write the experiment/Write The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 161
# I propose to replace Write the experiment by Write The3DWorld in :     # Write the experiment to file
sed -i.bak "161s/Write the experiment/Write The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 82
# I propose to replace Write the experiment by Write The3DWorld in :     # Write the experiment to file
sed -i.bak "82s/Write the experiment/Write The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 84
# I propose to replace Write the experiment by Write The3DWorld in :     # Write the experiment to file
sed -i.bak "84s/Write the experiment/Write The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 86
# I propose to replace Write the experiment by Write The3DWorld in :     # Write the experiment to file
sed -i.bak "86s/Write the experiment/Write The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 156
# I propose to replace Write the experiment by Write The3DWorld in :     # Write the experiment to file
sed -i.bak "156s/Write the experiment/Write The3DWorld/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py
