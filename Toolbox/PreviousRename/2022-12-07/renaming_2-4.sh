#!/bin/bash

# RENAMING COMMANDS
# GOAL: exp -----> my_world
# DATE: 12/07/2022


# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 140
# I propose to replace exp by my_world in : exp = visual.The3DWorld(output_headset=True, output_gaze=True, 
sed -i.bak "140s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 279
# I propose to replace exp by my_world in :     exp.add_scene(PauseVisScene)
sed -i.bak "279s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 531
# I propose to replace exp by my_world in : def SetDefaultObjectToTheScene(exp,scene,CueOn=False,StimOn=False,MaskOn=False,
sed -i.bak "531s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 595
# I propose to replace exp by my_world in :     exp.add_scene(scene)
sed -i.bak "595s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 602
# I propose to replace exp by my_world in :     exp.set_coordinate_system_from_a_virtual_point(origin)
sed -i.bak "602s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 608
# I propose to replace exp by my_world in :         exp.add_scene(fixscene)
sed -i.bak "608s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 633
# I propose to replace exp by my_world in :         run_one_block(exp, current_block,newOrigin)
sed -i.bak "633s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 636
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "636s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 640
# I propose to replace exp by my_world in : def run_one_block(exp, current_block,origin)
sed -i.bak "640s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 677
# I propose to replace exp by my_world in :     exp.set_coordinate_system_from_a_virtual_point(origin)
sed -i.bak "677s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 684
# I propose to replace exp by my_world in :     SetDefaultObjectToTheScene(exp=exp,scene=s0VisScene)
sed -i.bak "684s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 770
# I propose to replace exp by my_world in :         exp.set_coordinate_system_from_a_virtual_point(origin)
sed -i.bak "770s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 772
# I propose to replace exp by my_world in :         SetDefaultObjectToTheScene(exp=exp,scene=s1VisScene)
sed -i.bak "772s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 795
# I propose to replace exp by my_world in :         exp.add_scene(s1VisScene)
sed -i.bak "795s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 802
# I propose to replace exp by my_world in :         exp.set_coordinate_system_from_a_virtual_point(origin)
sed -i.bak "802s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 811
# I propose to replace exp by my_world in :         SetDefaultObjectToTheScene(exp=exp,scene=s2VisScene,CueOn=cues_are_displayed,cueRot=cueRot,stimRot=stimRot,scueside=scueside,cueSide=cueSide,maskRot=maskRot)
sed -i.bak "811s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 817
# I propose to replace exp by my_world in :         exp.set_coordinate_system_from_a_virtual_point(origin)
sed -i.bak "817s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 831
# I propose to replace exp by my_world in :         SetDefaultObjectToTheScene(exp=exp,scene=s3VisScene,CueOn=cues_are_displayed,StimOn= stims_are_displayed,cueRot=cueRot,stimRot=stimRot,scueside=scueside,cueSide=cueSide,maskRot=maskRot)
sed -i.bak "831s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 837
# I propose to replace exp by my_world in :         exp.set_coordinate_system_from_a_virtual_point(origin)
sed -i.bak "837s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 851
# I propose to replace exp by my_world in :         SetDefaultObjectToTheScene(exp=exp,scene=s3_bis_VisScene,CueOn=cues_are_displayed,MaskOn=masks_are_displayed,cueRot=cueRot,stimRot=stimRot,scueside=scueside,cueSide=cueSide,maskRot=maskRot)
sed -i.bak "851s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 855
# I propose to replace exp by my_world in :         exp.set_coordinate_system_from_a_virtual_point(origin)
sed -i.bak "855s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 868
# I propose to replace exp by my_world in :         SetDefaultObjectToTheScene(exp=exp,scene=s4VisScene)
sed -i.bak "868s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 876
# I propose to replace exp by my_world in :     exp.set_coordinate_system_from_a_virtual_point(origin)
sed -i.bak "876s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 887
# I propose to replace exp by my_world in :     SetDefaultObjectToTheScene(exp=exp,scene=s5VisScene)
sed -i.bak "887s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 42
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "42s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 170
# I propose to replace exp by my_world in : exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "170s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 237
# I propose to replace exp by my_world in : exp.translate_coordinate_system_along_current(translation=np.array([0.0,headset_pos_y,0.0]))
sed -i.bak "237s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 356
# I propose to replace exp by my_world in :     exp.add_scene (calibration_scene)
sed -i.bak "356s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 367
# I propose to replace exp by my_world in :         my_angle_calc= exp.calculatorManager.AddAngleAtOriginBetweenTwoVisualObjects(visualObject1= reference_point_debug, visualObject2=fixation_point)
sed -i.bak "367s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 400
# I propose to replace exp by my_world in :     exp.add_scene (block_launch_scene)
sed -i.bak "400s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 424
# I propose to replace exp by my_world in :     exp.add_scene(trial_cue_scene)
sed -i.bak "424s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 433
# I propose to replace exp by my_world in :         my_angle_calc= exp.calculatorManager.AddAngleAtOriginBetweenTwoVisualObjects(visualObject1=fixation_point, visualObject2=stimulus)
sed -i.bak "433s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 461
# I propose to replace exp by my_world in :     exp.add_scene(trial_cue_stimuli_scene)
sed -i.bak "461s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 482
# I propose to replace exp by my_world in :     exp.add_scene(trial_mask_scene)
sed -i.bak "482s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 504
# I propose to replace exp by my_world in :     exp.add_scene(trial_end_scene)
sed -i.bak "504s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 534
# I propose to replace exp by my_world in :     exp.add_scene (block_end_scene)
sed -i.bak "534s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 555
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "555s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Vestibular/vestibular.py, at line 36
# I propose to replace exp by my_world in : posner.exp.outputGaze = True
sed -i.bak "36s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Vestibular/vestibular.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Vestibular/vestibular.py, at line 49
# I propose to replace exp by my_world in :     for aScene in posner.exp.scenes.values()
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Vestibular/vestibular.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Vestibular/vestibular.py, at line 56
# I propose to replace exp by my_world in :     posner.exp.write()
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Vestibular/vestibular.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Vestibular/vestibular.py, at line 62
# I propose to replace exp by my_world in : #	exp\.add_scene\([a-zA-Z_0-9]+\)
sed -i.bak "62s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Vestibular/vestibular.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 294
# I propose to replace exp by my_world in : exp = visual.The3DWorld( name_of_subject= username,
sed -i.bak "294s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 297
# I propose to replace exp by my_world in : exp.translate_coordinate_system_along_global ( np.array ( [ 0.0, head_pos_y, 0.0] ) )
sed -i.bak "297s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 746
# I propose to replace exp by my_world in :     exp.add_scene (calibration_scene)
sed -i.bak "746s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 799
# I propose to replace exp by my_world in :     exp.add_scene ( block_launch_scene_step_1 )
sed -i.bak "799s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 849
# I propose to replace exp by my_world in :     exp.add_scene(block_launch_scene_step_2)
sed -i.bak "849s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 910
# I propose to replace exp by my_world in :     exp.add_scene (trial_launch_scene)
sed -i.bak "910s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 1042
# I propose to replace exp by my_world in :     exp.add_scene(trial_scene)
sed -i.bak "1042s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 1045
# I propose to replace exp by my_world in :     exp.add_scene (after_trial_pos_feedback_scene)
sed -i.bak "1045s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 1046
# I propose to replace exp by my_world in :     exp.add_scene (after_trial_neg_feedback_scene) 
sed -i.bak "1046s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 1076
# I propose to replace exp by my_world in :     exp.add_scene (after_trial_scene)
sed -i.bak "1076s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 1100
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file 
sed -i.bak "1100s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 58
# I propose to replace exp by my_world in : def create_scene(exp,tangent_screen_id, sentence,screen_width,screen_height,logmar,distance)
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 61
# I propose to replace exp by my_world in :     exp.add_scene(scene)
sed -i.bak "61s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 155
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject=parameters.subject_name)
sed -i.bak "155s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 156
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(current_coordinate_system)
sed -i.bak "156s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 161
# I propose to replace exp by my_world in :         create_scene(exp,  
sed -i.bak "161s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 169
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "169s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 174
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject=parameters.subject_name)
sed -i.bak "174s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 175
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(current_coordinate_system)
sed -i.bak "175s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 180
# I propose to replace exp by my_world in :         create_scene(exp,  
sed -i.bak "180s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 188
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "188s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 72
# I propose to replace exp by my_world in :     distance_head_text = exp.calculatorManager.AddRadialDistanceFromHeadPOV(empty_screen)
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 122
# I propose to replace exp by my_world in :     experiment.sentences_sequence_mnread(exp)
sed -i.bak "122s/experiment/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 122
# I propose to replace exp by my_world in :     experiment.sentences_sequence_mnread(exp)
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 126
# I propose to replace exp by my_world in : def create_main_UI(scene,exp,phrase,current_tangent_screen,empty_screen,logmar,degrees)
sed -i.bak "126s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 174
# I propose to replace exp by my_world in :     timer_calculator = exp.calculatorManager.AddElaspedTimeCurrentScene()
sed -i.bak "174s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 193
# I propose to replace exp by my_world in :     distance_head_text = exp.calculatorManager.AddRadialDistanceFromHeadPOV(empty_screen)
sed -i.bak "193s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 230
# I propose to replace exp by my_world in :     outside_given_limit_input =  PTVR.Data.Input.DistanceFromPoint(activation_area_center_point=experiment.current_coordinate_system ,
sed -i.bak "230s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 234
# I propose to replace exp by my_world in :     inside_given_limit_input =  PTVR.Data.Input.DistanceFromPoint(activation_area_center_point=experiment.current_coordinate_system,
sed -i.bak "234s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 473
# I propose to replace exp by my_world in : def create_words_UI(scene,exp,phrase) 
sed -i.bak "473s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 623
# I propose to replace exp by my_world in : def end_experiment(exp,scene,smallest_logmar) 
sed -i.bak "623s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 637
# I propose to replace exp by my_world in :     end_experiment_text = interface.end_experiment_text()
sed -i.bak "637s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 699
# I propose to replace exp by my_world in :     scene.placeUI(end_experiment_text)
sed -i.bak "699s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_parameters.py, at line 8
# I propose to replace exp by my_world in : Parameters used in mnread_vr_experiment """
sed -i.bak "8s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_parameters.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 22
# I propose to replace exp by my_world in : goal 
sed -i.bak "22s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 24
# I propose to replace exp by my_world in : how to use 
sed -i.bak "24s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 26
# I propose to replace exp by my_world in : predictions 
sed -i.bak "26s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 27
# I propose to replace exp by my_world in : PC screen to interact with the experiment 
sed -i.bak "27s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 107
# I propose to replace exp by my_world in :                  scene,exp,phrase_to_write= "",tag = "",
sed -i.bak "107s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 156
# I propose to replace exp by my_world in :     scene.place(tangent_screen,exp)
sed -i.bak "156s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 157
# I propose to replace exp by my_world in :     scene.place(empty_tangent_screen,exp)
sed -i.bak "157s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 158
# I propose to replace exp by my_world in :     scene.place(text,exp)
sed -i.bak "158s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 162
# I propose to replace exp by my_world in :     ## Add the scene to the experiment 
sed -i.bak "162s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 163
# I propose to replace exp by my_world in :     exp.add_scene(scene)
sed -i.bak "163s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 167
# I propose to replace exp by my_world in : def sentences_sequence_mnread(exp) 
sed -i.bak "167s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 201
# I propose to replace exp by my_world in :                      exp = exp,
sed -i.bak "201s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 209
# I propose to replace exp by my_world in :         interactions.create_words_UI(t1,exp,list_cutted_phrases_from_mnread_phrase_cutting)
sed -i.bak "209s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 210
# I propose to replace exp by my_world in :         ## Creation of main button UI to interact with the experiment
sed -i.bak "210s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 211
# I propose to replace exp by my_world in :         interactions.create_main_UI(t1,exp, list_cutted_phrases_from_mnread_phrase_cutting,current_tangent_screen[0],empty_screen[0], logmar_value,current_visual_angle)
sed -i.bak "211s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 228
# I propose to replace exp by my_world in :     interactions.end_experiment(exp,end_scene, smallest_logmar)
sed -i.bak "228s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 229
# I propose to replace exp by my_world in :     exp.add_scene(end_scene)
sed -i.bak "229s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 234
# I propose to replace exp by my_world in :     # Set-up the experiment file
sed -i.bak "234s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 235
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(operatorMode=True,name_of_subject=parameters.subject_name)
sed -i.bak "235s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 236
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(current_coordinate_system)
sed -i.bak "236s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 238
# I propose to replace exp by my_world in :     exp.add_scene(start_scene)
sed -i.bak "238s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 245
# I propose to replace exp by my_world in :     start_scene.place(first_empty_screen,exp)
sed -i.bak "245s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 251
# I propose to replace exp by my_world in :     # Write the experiment to file
sed -i.bak "251s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 253
# I propose to replace exp by my_world in :         exp.scenes[exp.id_scene[i]].interaction[-1].events[0].SetNextScene(id_next_scene=exp.id_scene[-1])
sed -i.bak "253s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 254
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "254s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 255
# I propose to replace exp by my_world in :     print("The experiment has been written.")
sed -i.bak "255s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_csv_post_traitement.R, at line 14
# I propose to replace exp by my_world in : setwd("C
sed -i.bak "14s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_csv_post_traitement.R

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_csv_post_traitement.R, at line 17
# I propose to replace exp by my_world in : filename <- "mnread_vr_experiment__Main__DummyUser_2022-12-10--16-18-42.csv_cleaned"
sed -i.bak "17s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_csv_post_traitement.R

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py, at line 22
# I propose to replace exp by my_world in : def create_scene (exp,scene)
sed -i.bak "22s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py, at line 63
# I propose to replace exp by my_world in :     exp.add_scene(scene)
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py, at line 71
# I propose to replace exp by my_world in :     exp = visual.The3DWorld()
sed -i.bak "71s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py, at line 72
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(current_coordinate_system)
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py, at line 76
# I propose to replace exp by my_world in :     create_scene (exp=exp,scene = scene)
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py, at line 79
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "79s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py, at line 48
# I propose to replace exp by my_world in :     exp = visual.The3DWorld()
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py, at line 49
# I propose to replace exp by my_world in :     exp.set_coordinate_system_from_a_virtual_point (POVatCalibration) # new origin is now at POVatCalibration 
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py, at line 51
# I propose to replace exp by my_world in :     visScene  = PTVR.Stimuli.World.VisualScene (experiment=exp) 
sed -i.bak "51s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py, at line 59
# I propose to replace exp by my_world in :     exp.add_scene(visScene)
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py, at line 97
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 37
# I propose to replace exp by my_world in : def typography_comparison(exp) 
sed -i.bak "37s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 60
# I propose to replace exp by my_world in :     t1 = PTVR.Stimuli.World.VisualScene(experiment=exp)
sed -i.bak "60s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 115
# I propose to replace exp by my_world in :     exp.add_scene(t1)
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 121
# I propose to replace exp by my_world in :     # Set-up the experiment file
sed -i.bak "121s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 122
# I propose to replace exp by my_world in :     exp = visual.The3DWorld()
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 123
# I propose to replace exp by my_world in :     typography_comparison(exp)
sed -i.bak "123s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 125
# I propose to replace exp by my_world in :     # Write the experiment to file
sed -i.bak "125s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 126
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "126s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 127
# I propose to replace exp by my_world in :     print("The experiment has been written.")
sed -i.bak "127s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 143
# I propose to replace exp by my_world in :     # Set-up the experiment file
sed -i.bak "143s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 144
# I propose to replace exp by my_world in :     exp = visual.The3DWorld() 
sed -i.bak "144s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 145
# I propose to replace exp by my_world in :     #exp.translate_coordinate_system ( )
sed -i.bak "145s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 151
# I propose to replace exp by my_world in :         exp.add_scene(scene)
sed -i.bak "151s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 159
# I propose to replace exp by my_world in :     exp.add_scene(endscene)
sed -i.bak "159s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 161
# I propose to replace exp by my_world in :     # Write the experiment to file
sed -i.bak "161s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 162
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "162s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 163
# I propose to replace exp by my_world in : print("The experiment has been written.")
sed -i.bak "163s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py, at line 34
# I propose to replace exp by my_world in :     exp = visual.The3DWorld()
sed -i.bak "34s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py, at line 35
# I propose to replace exp by my_world in :     exp.set_coordinate_system_from_a_virtual_point (POVatCalibration)
sed -i.bak "35s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py, at line 37
# I propose to replace exp by my_world in :     scene  = PTVR.Stimuli.World.VisualScene (experiment=exp,background_color=color.RGBColor(0.5,0.5,0.5,1)) 
sed -i.bak "37s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py, at line 38
# I propose to replace exp by my_world in :     exp.add_scene(scene)
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py, at line 66
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 37
# I propose to replace exp by my_world in : def single_letter_demo(exp) 
sed -i.bak "37s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 41
# I propose to replace exp by my_world in :     t1 = PTVR.Stimuli.World.VisualScene(experiment=exp)
sed -i.bak "41s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 75
# I propose to replace exp by my_world in :     # Set-up the experiment file
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 76
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld = detailsExperiment)
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 79
# I propose to replace exp by my_world in :     # Add this scene to the experiment
sed -i.bak "79s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 80
# I propose to replace exp by my_world in :     exp.add_scene(single_letter_demo(exp))
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 82
# I propose to replace exp by my_world in :     # Write the experiment to file
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 83
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 84
# I propose to replace exp by my_world in :     print("The experiment has been written.")
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 38
# I propose to replace exp by my_world in : def single_phrase_mnread(exp) 
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 47
# I propose to replace exp by my_world in :     t1 = PTVR.Stimuli.World.VisualScene(experiment=exp)
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 74
# I propose to replace exp by my_world in :     # Set-up the experiment file
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 75
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld = detailsExperiment)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 80
# I propose to replace exp by my_world in :     # Add this scene to the experiment
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 82
# I propose to replace exp by my_world in :     exp.add_scene(single_phrase_mnread(exp))
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 84
# I propose to replace exp by my_world in :     # Write the experiment to file
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 85
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 86
# I propose to replace exp by my_world in :     print("The experiment has been written.")
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 36
# I propose to replace exp by my_world in :     exp = visual.The3DWorld()
sed -i.bak "36s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 37
# I propose to replace exp by my_world in :     exp.set_coordinate_system_from_a_virtual_point (POVatCalibration) # new origin is now at POVatCalibration 
sed -i.bak "37s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 39
# I propose to replace exp by my_world in :     scene  = PTVR.Stimuli.World.VisualScene (experiment=exp,background_color=color.RGBColor(0.5,0.5,0.5,1)) 
sed -i.bak "39s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 40
# I propose to replace exp by my_world in :     exp.add_scene(scene)
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 148
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "148s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 38
# I propose to replace exp by my_world in : def cardboard_mnread(exp) 
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 42
# I propose to replace exp by my_world in :     exp.set_coordinate_system_from_a_virtual_point (POVatCalibration)
sed -i.bak "42s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 43
# I propose to replace exp by my_world in :     t1 = PTVR.Stimuli.World.VisualScene(experiment=exp)
sed -i.bak "43s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 75
# I propose to replace exp by my_world in :     exp.add_scene(t1)  
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 80
# I propose to replace exp by my_world in :     # Set-up the experiment file
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 81
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=detailsExperiment)
sed -i.bak "81s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 84
# I propose to replace exp by my_world in :     cardboard_mnread(exp)
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 86
# I propose to replace exp by my_world in :     # Write the experiment to file
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 87
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 88
# I propose to replace exp by my_world in :     print("The experiment has been written.")
sed -i.bak "88s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 31
# I propose to replace exp by my_world in : def create_scene(phrase_to_display, visual_angle_of_centered_object, scene,exp,phrase_to_write= "",tag_duration = "",logmar_value = 0, is_mnread_sentence = False) 
sed -i.bak "31s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 76
# I propose to replace exp by my_world in :     ## Add the scene to the experiment 
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 78
# I propose to replace exp by my_world in :     exp.add_scene(scene)
sed -i.bak "78s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 86
# I propose to replace exp by my_world in : def sentences_sequence_mnread(exp) 
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 113
# I propose to replace exp by my_world in :         t1 = PTVR.Stimuli.World.VisualScene(experiment=exp)
sed -i.bak "113s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 118
# I propose to replace exp by my_world in :                      exp = exp,
sed -i.bak "118s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 124
# I propose to replace exp by my_world in :         #interface.create_MainButtonsUI(t1,exp, list_cutted_phrases_from_mnread_phrase_cutting, logmar_value)
sed -i.bak "124s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 125
# I propose to replace exp by my_world in :         #interface.create_UIWords(t1,exp,list_cutted_phrases_from_mnread_phrase_cutting)
sed -i.bak "125s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 128
# I propose to replace exp by my_world in :         t2 = PTVR.Stimuli.World.VisualScene(experiment=exp)
sed -i.bak "128s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 132
# I propose to replace exp by my_world in :         create_scene("Pause. \n Remettez-vous en position de confort.",parameters.visual_angle_of_centered_object,t2,exp,phrase_to_display,"end")
sed -i.bak "132s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 139
# I propose to replace exp by my_world in :     #interface.end_experiment(exp)
sed -i.bak "139s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 148
# I propose to replace exp by my_world in :     # Set-up the experiment file
sed -i.bak "148s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 150
# I propose to replace exp by my_world in :     exp = visual.The3DWorld()
sed -i.bak "150s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 151
# I propose to replace exp by my_world in :     exp.set_coordinate_system_from_a_virtual_point (POVatCalibration)
sed -i.bak "151s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 154
# I propose to replace exp by my_world in :     sentences_sequence_mnread(exp)
sed -i.bak "154s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 156
# I propose to replace exp by my_world in :     # Write the experiment to file
sed -i.bak "156s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 157
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "157s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 158
# I propose to replace exp by my_world in :     print("The experiment has been written.")
sed -i.bak "158s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 111
# I propose to replace exp by my_world in : def create_words_UI(scene,exp) 
sed -i.bak "111s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 258
# I propose to replace exp by my_world in :     # Set-up the experiment file
sed -i.bak "258s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 259
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(operatorMode=True,name_of_subject="Jo")
sed -i.bak "259s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 261
# I propose to replace exp by my_world in :     exp.add_scene(scene)
sed -i.bak "261s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 263
# I propose to replace exp by my_world in :     create_words_UI(scene, exp)
sed -i.bak "263s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 264
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "264s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 265
# I propose to replace exp by my_world in :     print("The experiment has been written.")
sed -i.bak "265s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py, at line 20
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "20s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py, at line 40
# I propose to replace exp by my_world in :     exp = PTVR.Visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py, at line 44
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py, at line 45
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/buttonUI_Management.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 19
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "19s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 46
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 48
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(current_rotate_z)
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 49
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(current_rotate_x)
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 50
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(current_rotate_y)
sed -i.bak "50s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 51
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([current_pos_x,current_pos_y,current_pos_z]))
sed -i.bak "51s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 53
# I propose to replace exp by my_world in :     myScene.place(my_cube,exp)
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 55
# I propose to replace exp by my_world in :     exp.reset_coordinate_system() # Reset Sequence
sed -i.bak "55s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 57
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([current_pos_x,current_pos_y,current_pos_z]))
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 58
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(current_rotate_z)
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 59
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(current_rotate_x)
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 60
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(current_rotate_y)
sed -i.bak "60s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 62
# I propose to replace exp by my_world in :     myScene.place(my_cube,exp)
sed -i.bak "62s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 63
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 64
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py, at line 22
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "22s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py, at line 41
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "41s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py, at line 46
# I propose to replace exp by my_world in :     myScene.place(parent,exp)
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py, at line 47
# I propose to replace exp by my_world in :     myScene.place(line,exp)
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py, at line 48
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py, at line 49
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 58
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 70
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 73
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([0.0,1.2,1.0]))
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 75
# I propose to replace exp by my_world in :     myScene.place(text_info,exp)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 81
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "81s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 82
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 83
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 101
# I propose to replace exp by my_world in :     myScene.place(sphere_black,exp)
sed -i.bak "101s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 102
# I propose to replace exp by my_world in :     myScene.place(axis_x,exp)
sed -i.bak "102s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 103
# I propose to replace exp by my_world in :     myScene.place(axis_y,exp)
sed -i.bak "103s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 104
# I propose to replace exp by my_world in :     myScene.place(axis_z,exp)
sed -i.bak "104s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 107
# I propose to replace exp by my_world in :     myScene.place(cube_red,exp)
sed -i.bak "107s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 110
# I propose to replace exp by my_world in :     myScene.place(cube_green,exp)
sed -i.bak "110s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 113
# I propose to replace exp by my_world in :     myScene.place(cube_blue,exp)
sed -i.bak "113s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 116
# I propose to replace exp by my_world in :     myScene.place(cube_yellow,exp)
sed -i.bak "116s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 119
# I propose to replace exp by my_world in :     myScene.place(cube_orange,exp)
sed -i.bak "119s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 122
# I propose to replace exp by my_world in :     myScene.place(cube_purple,exp)
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 124
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "124s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 125
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "125s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 47
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 65
# I propose to replace exp by my_world in : def create_capoc_and_segment(my_scene,exp,tangent_screen,value_eccentricity_capoc_deg)
sed -i.bak "65s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 74
# I propose to replace exp by my_world in :     my_scene.place(linetool,exp)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 78
# I propose to replace exp by my_world in :     my_scene.place(sphere_1,exp)
sed -i.bak "78s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 82
# I propose to replace exp by my_world in :     my_scene.place(sphere_2,exp)
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 87
# I propose to replace exp by my_world in :     my_scene.place(capoc,exp)
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 90
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "90s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 93
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([current_x_pos,current_y_pos,current_z_pos ]))
sed -i.bak "93s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 94
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "94s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 95
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "95s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 96
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "96s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 97
# I propose to replace exp by my_world in :     current_pos_x, current_pos_y,current_pos_z, current_cs_axis_X,current_cs_axis_Y,current_cs_axis_Z = exp.get_coordinate_system()
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 104
# I propose to replace exp by my_world in :     myScene.place(axis_x_current,exp)
sed -i.bak "104s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 105
# I propose to replace exp by my_world in :     myScene.place(axis_y_current,exp)
sed -i.bak "105s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 106
# I propose to replace exp by my_world in :     myScene.place(axis_z_current,exp)
sed -i.bak "106s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 111
# I propose to replace exp by my_world in :     myScene.place(tangent_screen,exp)
sed -i.bak "111s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 127
# I propose to replace exp by my_world in :     myScene.place(axis_x,exp)
sed -i.bak "127s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 128
# I propose to replace exp by my_world in :     myScene.place(axis_y,exp)
sed -i.bak "128s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 129
# I propose to replace exp by my_world in :     myScene.place(axis_z,exp)
sed -i.bak "129s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 133
# I propose to replace exp by my_world in :         create_capoc_and_segment(myScene,exp,tangent_screen,i)
sed -i.bak "133s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 143
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "143s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 144
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "144s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 24
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "24s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 52
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "52s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 53
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 54
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 55
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "55s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 62
# I propose to replace exp by my_world in :         myScene.place(axis_x,exp)
sed -i.bak "62s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 63
# I propose to replace exp by my_world in :         myScene.place(axis_y,exp)
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 64
# I propose to replace exp by my_world in :         myScene.place(axis_z,exp)
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 77
# I propose to replace exp by my_world in :         myScene.place(cube_red,exp)
sed -i.bak "77s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 78
# I propose to replace exp by my_world in :         myScene.place(cube_green,exp)
sed -i.bak "78s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 79
# I propose to replace exp by my_world in :         myScene.place(cube_blue,exp)
sed -i.bak "79s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 80
# I propose to replace exp by my_world in :         myScene.place(my_text_information,exp)
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 81
# I propose to replace exp by my_world in :         exp.add_scene(myScene)
sed -i.bak "81s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 83
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 60
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "60s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 72
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 75
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([0.0,1.2,1.0]))
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 82
# I propose to replace exp by my_world in :     myScene.place(text_info,exp)
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 84
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 85
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 86
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 104
# I propose to replace exp by my_world in :     myScene.place(sphere_black,exp)
sed -i.bak "104s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 105
# I propose to replace exp by my_world in :     myScene.place(axis_x,exp)
sed -i.bak "105s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 106
# I propose to replace exp by my_world in :     myScene.place(axis_y,exp)
sed -i.bak "106s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 107
# I propose to replace exp by my_world in :     myScene.place(axis_z,exp)
sed -i.bak "107s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 114
# I propose to replace exp by my_world in :     myScene.place(cube_red,exp)
sed -i.bak "114s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 121
# I propose to replace exp by my_world in :     myScene.place(cube_green,exp)
sed -i.bak "121s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 128
# I propose to replace exp by my_world in :     myScene.place(cube_blue,exp)
sed -i.bak "128s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 135
# I propose to replace exp by my_world in :     myScene.place(cube_yellow,exp)
sed -i.bak "135s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 142
# I propose to replace exp by my_world in :     myScene.place(cube_orange,exp)
sed -i.bak "142s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 149
# I propose to replace exp by my_world in :     myScene.place(cube_purple,exp)
sed -i.bak "149s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 151
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "151s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 152
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "152s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 56
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 68
# I propose to replace exp by my_world in :     exp = PTVR.Visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "68s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 73
# I propose to replace exp by my_world in :     myScene.place(axis_global_z,exp)
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 74
# I propose to replace exp by my_world in :     myScene.place(axis_global_x,exp)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 75
# I propose to replace exp by my_world in :     myScene.place(axis_global_y,exp)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 78
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([0.0,1.2,1.0]))
sed -i.bak "78s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 80
# I propose to replace exp by my_world in :     myScene.place(text_info,exp)
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 82
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 83
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 84
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 93
# I propose to replace exp by my_world in :     myScene.place(axis_group,exp)
sed -i.bak "93s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 94
# I propose to replace exp by my_world in :     myScene.place(axis_x,exp)
sed -i.bak "94s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 95
# I propose to replace exp by my_world in :     myScene.place(axis_y,exp)
sed -i.bak "95s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 96
# I propose to replace exp by my_world in :     myScene.place(axis_z,exp)
sed -i.bak "96s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 109
# I propose to replace exp by my_world in :     myScene.place(cube_red,exp)
sed -i.bak "109s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 115
# I propose to replace exp by my_world in :     myScene.place(cube_green,exp)
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 121
# I propose to replace exp by my_world in :     myScene.place(cube_blue,exp)
sed -i.bak "121s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 127
# I propose to replace exp by my_world in :     myScene.place(cube_yellow,exp)
sed -i.bak "127s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 133
# I propose to replace exp by my_world in :     myScene.place(cube_orange,exp)
sed -i.bak "133s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 139
# I propose to replace exp by my_world in :     myScene.place(cube_purple,exp)
sed -i.bak "139s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 141
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "141s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py, at line 142
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "142s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 29
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "29s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 57
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 59
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 60
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "60s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 61
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "61s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 66
# I propose to replace exp by my_world in :         myScene.place(axis_x,exp)
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 67
# I propose to replace exp by my_world in :         myScene.place(axis_y,exp)
sed -i.bak "67s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 68
# I propose to replace exp by my_world in :         myScene.place(axis_z,exp)
sed -i.bak "68s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 81
# I propose to replace exp by my_world in :         myScene.place(cube_red,exp)
sed -i.bak "81s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 82
# I propose to replace exp by my_world in :         myScene.place(cube_green,exp)
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 83
# I propose to replace exp by my_world in :         myScene.place(cube_blue,exp)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 84
# I propose to replace exp by my_world in :         myScene.place(my_text_information,exp)
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 85
# I propose to replace exp by my_world in :         exp.add_scene(myScene)
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 87
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 47
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 68
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "68s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 71
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([current_x_pos,current_y_pos,current_z_pos ]))
sed -i.bak "71s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 72
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 73
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 74
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 75
# I propose to replace exp by my_world in :     current_pos_x, current_pos_y,current_pos_z, current_cs_axis_X,current_cs_axis_Y,current_cs_axis_Z = exp.get_coordinate_system()
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 82
# I propose to replace exp by my_world in :     myScene.place(axis_x_current,exp)
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 83
# I propose to replace exp by my_world in :     myScene.place(axis_y_current,exp)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 84
# I propose to replace exp by my_world in :     myScene.place(axis_z_current,exp)
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 89
# I propose to replace exp by my_world in :     myScene.place(tangent_screen,exp)
sed -i.bak "89s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 105
# I propose to replace exp by my_world in :     myScene.place(linetool,exp)
sed -i.bak "105s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 109
# I propose to replace exp by my_world in :     myScene.place(sphere_1,exp)
sed -i.bak "109s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 113
# I propose to replace exp by my_world in :     myScene.place(sphere_2,exp)
sed -i.bak "113s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 118
# I propose to replace exp by my_world in :     myScene.place(capoc,exp)
sed -i.bak "118s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 127
# I propose to replace exp by my_world in :     myScene.place(axis_x,exp)
sed -i.bak "127s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 128
# I propose to replace exp by my_world in :     myScene.place(axis_y,exp)
sed -i.bak "128s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 129
# I propose to replace exp by my_world in :     myScene.place(axis_z,exp)
sed -i.bak "129s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 139
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "139s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 140
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "140s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 27
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "27s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 47
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 49
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 56
# I propose to replace exp by my_world in :     # flat_screen.rotate_to_look_at_in_opposite_direction(id_object_to_lookAt=exp.idOriginCurrentCS)#idOriginCurrentCS = -4
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 58
# I propose to replace exp by my_world in :     myScene.place(my_text_information,exp)
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 64
# I propose to replace exp by my_world in :     flatscreen.rotate_to_look_at(exp.idOriginCurrentCS)
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 68
# I propose to replace exp by my_world in :     myScene.place(tangent_screen,exp)
sed -i.bak "68s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 69
# I propose to replace exp by my_world in :     myScene.place(my_text_opposite,exp)
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 70
# I propose to replace exp by my_world in :     myScene.place(flatscreen,exp)
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 71
# I propose to replace exp by my_world in :     myScene.place(my_text_lookAt,exp)
sed -i.bak "71s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 73
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 54
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 66
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 74
# I propose to replace exp by my_world in :     myScene.place(axis_x,exp)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 75
# I propose to replace exp by my_world in :     myScene.place(axis_y,exp)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 76
# I propose to replace exp by my_world in :     myScene.place(axis_z,exp)
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 79
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([0.0,1.2,1.0]))
sed -i.bak "79s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 81
# I propose to replace exp by my_world in :     myScene.place(text_info,exp)
sed -i.bak "81s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 83
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 84
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 85
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 94
# I propose to replace exp by my_world in :     myScene.place(axis_group,exp)
sed -i.bak "94s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 95
# I propose to replace exp by my_world in :     myScene.place(axis_x_current,exp)
sed -i.bak "95s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 96
# I propose to replace exp by my_world in :     myScene.place(axis_y_current,exp)
sed -i.bak "96s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 97
# I propose to replace exp by my_world in :     myScene.place(axis_z_current,exp)
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 109
# I propose to replace exp by my_world in :     myScene.place(cube_red,exp)
sed -i.bak "109s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 112
# I propose to replace exp by my_world in :     myScene.place(cube_green,exp)
sed -i.bak "112s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 115
# I propose to replace exp by my_world in :     myScene.place(cube_blue,exp)
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 118
# I propose to replace exp by my_world in :     myScene.place(cube_yellow,exp)
sed -i.bak "118s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 121
# I propose to replace exp by my_world in :     myScene.place(cube_orange,exp)
sed -i.bak "121s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 124
# I propose to replace exp by my_world in :     myScene.place(cube_purple,exp)
sed -i.bak "124s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 126
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "126s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 127
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "127s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 58
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 71
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "71s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 75
# I propose to replace exp by my_world in :     myScene.place(text_info,exp)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 77
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([0.0,1.2,1.0]))
sed -i.bak "77s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 82
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 83
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 84
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 103
# I propose to replace exp by my_world in :     myScene.place(sphere_black,exp)
sed -i.bak "103s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 104
# I propose to replace exp by my_world in :     myScene.place(axis_x,exp)
sed -i.bak "104s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 105
# I propose to replace exp by my_world in :     myScene.place(axis_y,exp)
sed -i.bak "105s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 106
# I propose to replace exp by my_world in :     myScene.place(axis_z,exp)
sed -i.bak "106s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 110
# I propose to replace exp by my_world in :     myScene.place(cube_red,exp)
sed -i.bak "110s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 114
# I propose to replace exp by my_world in :     myScene.place(cube_green,exp)
sed -i.bak "114s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 118
# I propose to replace exp by my_world in :     myScene.place(cube_blue,exp)
sed -i.bak "118s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 122
# I propose to replace exp by my_world in :     myScene.place(cube_yellow,exp)
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 126
# I propose to replace exp by my_world in :     myScene.place(cube_orange,exp)
sed -i.bak "126s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 130
# I propose to replace exp by my_world in :     myScene.place(cube_purple,exp)
sed -i.bak "130s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 132
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "132s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 133
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "133s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 19
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "19s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 59
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 63
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(np.array([0, 0, 10]))  
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 64
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(90)
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 66
# I propose to replace exp by my_world in :     myScene.place(my_cube,exp)
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 69
# I propose to replace exp by my_world in :     exp.reset_coordinate_system()
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 70
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(90)
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 71
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(np.array([0, 0, 10]))
sed -i.bak "71s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 74
# I propose to replace exp by my_world in :     myScene.place(my_sphere,exp)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 76
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 77
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "77s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 91
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "91s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 101
# I propose to replace exp by my_world in : def placing_objects_on_screen(exp,scene,tangent_screen)
sed -i.bak "101s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 102
# I propose to replace exp by my_world in :     scene.place(tangent_screen,exp)
sed -i.bak "102s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 120
# I propose to replace exp by my_world in :     scene.place(cube_red,exp)
sed -i.bak "120s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 123
# I propose to replace exp by my_world in :     scene.place(cube_green,exp)
sed -i.bak "123s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 126
# I propose to replace exp by my_world in :     scene.place(cube_blue,exp)
sed -i.bak "126s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 129
# I propose to replace exp by my_world in :     scene.place(cube_yellow,exp)
sed -i.bak "129s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 132
# I propose to replace exp by my_world in :     scene.place(cube_orange,exp)
sed -i.bak "132s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 135
# I propose to replace exp by my_world in :     scene.place(cube_purple,exp)
sed -i.bak "135s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 137
# I propose to replace exp by my_world in :     scene.place(axis_x,exp)
sed -i.bak "137s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 138
# I propose to replace exp by my_world in :     scene.place(axis_y,exp)
sed -i.bak "138s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 139
# I propose to replace exp by my_world in :     scene.place(axis_z,exp)
sed -i.bak "139s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 142
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "142s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 148
# I propose to replace exp by my_world in :     myScene.place(text_info,exp)
sed -i.bak "148s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 149
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([current_x_pos,current_y_pos,current_z_pos]))
sed -i.bak "149s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 151
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "151s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 152
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "152s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 153
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "153s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 159
# I propose to replace exp by my_world in :     myScene.place(axis_x_current,exp)
sed -i.bak "159s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 160
# I propose to replace exp by my_world in :     myScene.place(axis_y_current,exp)
sed -i.bak "160s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 161
# I propose to replace exp by my_world in :     myScene.place(axis_z_current,exp)
sed -i.bak "161s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 165
# I propose to replace exp by my_world in :     placing_objects_on_screen(exp, myScene,  tangent_screen_1)
sed -i.bak "165s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 169
# I propose to replace exp by my_world in :     placing_objects_on_screen(exp, myScene,  tangent_screen_2)
sed -i.bak "169s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 173
# I propose to replace exp by my_world in :     placing_objects_on_screen(exp, myScene,  tangent_screen_3)
sed -i.bak "173s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 177
# I propose to replace exp by my_world in :     placing_objects_on_screen(exp, myScene,  tangent_screen_4)
sed -i.bak "177s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 181
# I propose to replace exp by my_world in :     placing_objects_on_screen(exp, myScene,  tangent_screen_5)
sed -i.bak "181s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 185
# I propose to replace exp by my_world in :     placing_objects_on_screen(exp, myScene,  tangent_screen_6)
sed -i.bak "185s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 187
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "187s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 188
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "188s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 58
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 70
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 77
# I propose to replace exp by my_world in :     myScene.place(axis_x_global,exp)
sed -i.bak "77s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 78
# I propose to replace exp by my_world in :     myScene.place(axis_y_global,exp)
sed -i.bak "78s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 79
# I propose to replace exp by my_world in :     myScene.place(axis_z_global,exp)
sed -i.bak "79s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 80
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([0.0,1.2,1.0]))
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 83
# I propose to replace exp by my_world in :     myScene.place(text_info,exp)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 85
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 86
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 87
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 96
# I propose to replace exp by my_world in :     myScene.place(axis_group,exp)
sed -i.bak "96s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 97
# I propose to replace exp by my_world in :     myScene.place(axis_x_current,exp)
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 98
# I propose to replace exp by my_world in :     myScene.place(axis_y_current,exp)
sed -i.bak "98s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 99
# I propose to replace exp by my_world in :     myScene.place(axis_z_current,exp)
sed -i.bak "99s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 122
# I propose to replace exp by my_world in :     myScene.place(sphere_black,exp)
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 123
# I propose to replace exp by my_world in :     myScene.place(axis_x,exp)
sed -i.bak "123s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 124
# I propose to replace exp by my_world in :     myScene.place(axis_y,exp)
sed -i.bak "124s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 125
# I propose to replace exp by my_world in :     myScene.place(axis_z,exp)
sed -i.bak "125s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 129
# I propose to replace exp by my_world in :     myScene.place(cube_red,exp)
sed -i.bak "129s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 133
# I propose to replace exp by my_world in :     myScene.place(cube_green,exp)
sed -i.bak "133s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 137
# I propose to replace exp by my_world in :     myScene.place(cube_blue,exp)
sed -i.bak "137s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 141
# I propose to replace exp by my_world in :     myScene.place(cube_yellow,exp)
sed -i.bak "141s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 145
# I propose to replace exp by my_world in :     myScene.place(cube_orange,exp)
sed -i.bak "145s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 149
# I propose to replace exp by my_world in :     myScene.place(cube_purple,exp)
sed -i.bak "149s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 151
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "151s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 152
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "152s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 57
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 76
# I propose to replace exp by my_world in : def create_tangent_screen(exp,my_scene,radial_distance,eccentricity_deg,half_meridian_deg,is_visible)
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 82
# I propose to replace exp by my_world in :     my_scene.place(tangent_screen,exp)
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 85
# I propose to replace exp by my_world in : def create_segment_on_tangent_screen(exp,my_scene,tangent_screen,capoc_eccentricity_deg,capoc_half_meridian_deg,segment_orientation_value_deg,segment_orientation_type,linetool_color_value)
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 98
# I propose to replace exp by my_world in :     my_scene.place(segment_main,exp)
sed -i.bak "98s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 105
# I propose to replace exp by my_world in :     my_scene.place(sphere_1,exp)
sed -i.bak "105s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 112
# I propose to replace exp by my_world in :     my_scene.place(sphere_2,exp)
sed -i.bak "112s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 115
# I propose to replace exp by my_world in : def create_axis_tangent_screen(exp,my_scene,tangent_screen)
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 120
# I propose to replace exp by my_world in :     place_axis(exp,my_scene,axis_x,axis_y,axis_z)
sed -i.bak "120s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 137
# I propose to replace exp by my_world in : def place_axis(exp,my_scene,axis_X,axis_Y,axis_Z)
sed -i.bak "137s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 138
# I propose to replace exp by my_world in :     my_scene.place(axis_X,exp)
sed -i.bak "138s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 139
# I propose to replace exp by my_world in :     my_scene.place(axis_Y,exp)
sed -i.bak "139s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 140
# I propose to replace exp by my_world in :     my_scene.place(axis_Z,exp)
sed -i.bak "140s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 143
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "143s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 162
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([current_x_pos,current_y_pos,current_z_pos]))
sed -i.bak "162s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 163
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "163s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 164
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "164s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 165
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "165s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 167
# I propose to replace exp by my_world in :     current_pos_x, current_pos_y,current_pos_z, current_cs_axis_X,current_cs_axis_Y,current_cs_axis_Z = exp.get_coordinate_system()
sed -i.bak "167s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 172
# I propose to replace exp by my_world in :     place_axis(exp, my_scene, current_axis_X, current_axis_Y, current_axis_Z)
sed -i.bak "172s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 175
# I propose to replace exp by my_world in :     main_screen = create_tangent_screen(exp, 
sed -i.bak "175s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 218
# I propose to replace exp by my_world in :     #create_axis_tangent_screen(exp,my_scene,tangent_screen)
sed -i.bak "218s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 221
# I propose to replace exp by my_world in :     create_segment_on_tangent_screen(exp,
sed -i.bak "221s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 239
# I propose to replace exp by my_world in :     my_scene.place(capoc,exp)
sed -i.bak "239s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 256
# I propose to replace exp by my_world in :     my_scene.place(text_main,exp)
sed -i.bak "256s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 263
# I propose to replace exp by my_world in :         projection_screen = create_tangent_screen(exp,
sed -i.bak "263s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 272
# I propose to replace exp by my_world in :         create_axis_tangent_screen(exp,my_scene,projection_screen)
sed -i.bak "272s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 277
# I propose to replace exp by my_world in :         my_scene.place(capoc_projected,exp)
sed -i.bak "277s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 307
# I propose to replace exp by my_world in :         create_segment_on_tangent_screen(exp = exp, my_scene = my_scene, 
sed -i.bak "307s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 315
# I propose to replace exp by my_world in :         exp.reset_coordinate_system()
sed -i.bak "315s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 346
# I propose to replace exp by my_world in :         my_scene.place(text_projected,exp)
sed -i.bak "346s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 356
# I propose to replace exp by my_world in :         my_scene.place(ray_direction_A,exp)
sed -i.bak "356s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 357
# I propose to replace exp by my_world in :         my_scene.place(ray_direction_B,exp)
sed -i.bak "357s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 358
# I propose to replace exp by my_world in :         my_scene.place(ray_direction_Capoc,exp)
sed -i.bak "358s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 362
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "362s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 363
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "363s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 54
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 67
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "67s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 72
# I propose to replace exp by my_world in :     myScene.place(axis_x_global,exp)
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 73
# I propose to replace exp by my_world in :     myScene.place(axis_y_global,exp)
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 74
# I propose to replace exp by my_world in :     myScene.place(axis_z_global,exp)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 77
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([0.0,1.2,1.0]))
sed -i.bak "77s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 80
# I propose to replace exp by my_world in :     myScene.place(text_info,exp)
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 82
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 83
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 84
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 94
# I propose to replace exp by my_world in :     myScene.place(axis_group,exp)
sed -i.bak "94s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 95
# I propose to replace exp by my_world in :     myScene.place(axis_x,exp)
sed -i.bak "95s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 96
# I propose to replace exp by my_world in :     myScene.place(axis_y,exp)
sed -i.bak "96s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 97
# I propose to replace exp by my_world in :     myScene.place(axis_z,exp)
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 107
# I propose to replace exp by my_world in :     myScene.place(cube_red,exp)
sed -i.bak "107s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 110
# I propose to replace exp by my_world in :     myScene.place(cube_green,exp)
sed -i.bak "110s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 113
# I propose to replace exp by my_world in :     myScene.place(cube_blue,exp)
sed -i.bak "113s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 116
# I propose to replace exp by my_world in :     myScene.place(cube_yellow,exp)
sed -i.bak "116s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 119
# I propose to replace exp by my_world in :     myScene.place(cube_orange,exp)
sed -i.bak "119s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 122
# I propose to replace exp by my_world in :     myScene.place(cube_purple,exp)
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 124
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "124s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 125
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "125s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 90
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "90s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 100
# I propose to replace exp by my_world in : def placing_objects_on_screen(exp,scene,tangent_screen)
sed -i.bak "100s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 114
# I propose to replace exp by my_world in :     scene.place(tangent_screen,exp)
sed -i.bak "114s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 116
# I propose to replace exp by my_world in :     scene.place(cube_red,exp)
sed -i.bak "116s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 119
# I propose to replace exp by my_world in :     scene.place(cube_green,exp)
sed -i.bak "119s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 122
# I propose to replace exp by my_world in :     scene.place(cube_blue,exp)
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 125
# I propose to replace exp by my_world in :     scene.place(cube_yellow,exp)
sed -i.bak "125s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 128
# I propose to replace exp by my_world in :     scene.place(cube_orange,exp)
sed -i.bak "128s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 131
# I propose to replace exp by my_world in :     scene.place(cube_purple,exp)
sed -i.bak "131s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 133
# I propose to replace exp by my_world in :     scene.place(axis_x,exp)
sed -i.bak "133s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 134
# I propose to replace exp by my_world in :     scene.place(axis_y,exp)
sed -i.bak "134s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 135
# I propose to replace exp by my_world in :     scene.place(axis_z,exp)
sed -i.bak "135s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 138
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "138s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 144
# I propose to replace exp by my_world in :     myScene.place(text_info,exp)
sed -i.bak "144s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 145
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([0.0,1.2,1.0]))
sed -i.bak "145s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 147
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "147s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 148
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "148s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 149
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "149s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 153
# I propose to replace exp by my_world in :     placing_objects_on_screen(exp, myScene,  tangent_screen_1)
sed -i.bak "153s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 157
# I propose to replace exp by my_world in :     placing_objects_on_screen(exp, myScene,  tangent_screen_2)
sed -i.bak "157s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 161
# I propose to replace exp by my_world in :     placing_objects_on_screen(exp, myScene,  tangent_screen_3)
sed -i.bak "161s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 165
# I propose to replace exp by my_world in :     placing_objects_on_screen(exp, myScene,  tangent_screen_4)
sed -i.bak "165s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 169
# I propose to replace exp by my_world in :     placing_objects_on_screen(exp, myScene,  tangent_screen_5)
sed -i.bak "169s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 173
# I propose to replace exp by my_world in :     placing_objects_on_screen(exp, myScene,  tangent_screen_6)
sed -i.bak "173s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 175
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "175s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 176
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "176s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py, at line 19
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "19s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py, at line 50
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "50s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py, at line 54
# I propose to replace exp by my_world in :     myScene.place(tangent_screen,exp)
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py, at line 65
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "65s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py, at line 66
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 44
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 67
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld = text_description)
sed -i.bak "67s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 71
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation = np.array([current_x_pos,current_y_pos,current_z_pos ]))
sed -i.bak "71s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 72
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z_deg)
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 73
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x_deg)
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 74
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y_deg)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 82
# I propose to replace exp by my_world in :     my_scene.place(my_parent,exp)
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 92
# I propose to replace exp by my_world in :     my_scene.place(my_child,exp)
sed -i.bak "92s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 94
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "94s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 95
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "95s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py, at line 48
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py, at line 49
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(np.array([0, height_of_head_viewpoint, 0]))  
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py, at line 72
# I propose to replace exp by my_world in :             vis_scene.place (my_sphere,exp) 
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py, at line 73
# I propose to replace exp by my_world in :             vis_scene.place (inner_sphere,exp) 
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py, at line 75
# I propose to replace exp by my_world in :     exp.add_scene(vis_scene)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py, at line 76
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 36
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "36s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 74
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 75
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 76
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 77
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "77s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 81
# I propose to replace exp by my_world in :         myScene.place(sphere_black,exp)
sed -i.bak "81s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 85
# I propose to replace exp by my_world in :         myScene.place(axis_x,exp)
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 86
# I propose to replace exp by my_world in :         myScene.place(axis_y,exp)
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 87
# I propose to replace exp by my_world in :         myScene.place(axis_z,exp)
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 100
# I propose to replace exp by my_world in :         myScene.place(cube_red,exp)
sed -i.bak "100s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 101
# I propose to replace exp by my_world in :         myScene.place(cube_green,exp)
sed -i.bak "101s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 102
# I propose to replace exp by my_world in :         myScene.place(cube_blue,exp)
sed -i.bak "102s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 103
# I propose to replace exp by my_world in :         myScene.place(my_text_information,exp)
sed -i.bak "103s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 104
# I propose to replace exp by my_world in :         exp.add_scene(myScene)
sed -i.bak "104s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 106
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "106s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 47
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 65
# I propose to replace exp by my_world in : def create_segment_on_tangent_screen(exp,myScene,tangent_screen,new_segment_orientation_deg)
sed -i.bak "65s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 72
# I propose to replace exp by my_world in :     myScene.place(linetool,exp)
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 76
# I propose to replace exp by my_world in :     myScene.place(sphere_1,exp)
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 80
# I propose to replace exp by my_world in :     myScene.place(sphere_2,exp)
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 83
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 86
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([current_x_pos,current_y_pos,current_z_pos ]))
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 87
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 88
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "88s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 89
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "89s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 90
# I propose to replace exp by my_world in :     current_pos_x, current_pos_y,current_pos_z, current_cs_axis_X,current_cs_axis_Y,current_cs_axis_Z = exp.get_coordinate_system()
sed -i.bak "90s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 97
# I propose to replace exp by my_world in :     myScene.place(axis_x_current,exp)
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 98
# I propose to replace exp by my_world in :     myScene.place(axis_y_current,exp)
sed -i.bak "98s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 99
# I propose to replace exp by my_world in :     myScene.place(axis_z_current,exp)
sed -i.bak "99s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 104
# I propose to replace exp by my_world in :     myScene.place(tangent_screen,exp)
sed -i.bak "104s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 114
# I propose to replace exp by my_world in :         create_segment_on_tangent_screen(exp,myScene,tangent_screen,i)
sed -i.bak "114s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 118
# I propose to replace exp by my_world in :     myScene.place(capoc,exp)
sed -i.bak "118s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 127
# I propose to replace exp by my_world in :     myScene.place(axis_x,exp)
sed -i.bak "127s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 128
# I propose to replace exp by my_world in :     myScene.place(axis_y,exp)
sed -i.bak "128s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 129
# I propose to replace exp by my_world in :     myScene.place(axis_z,exp)
sed -i.bak "129s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 142
# I propose to replace exp by my_world in :     myScene.place(lineTool2,exp)
sed -i.bak "142s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 143
# I propose to replace exp by my_world in :     exp.reset_coordinate_system()
sed -i.bak "143s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 148
# I propose to replace exp by my_world in :     myScene.place(capoc_global,exp)
sed -i.bak "148s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 152
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "152s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 153
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "153s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 37
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "37s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 64
# I propose to replace exp by my_world in : def place_axis(exp,myScene,axis_X_current,axis_Y_current,axis_Z_current)
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 65
# I propose to replace exp by my_world in :     myScene.place(axis_X_current,exp)
sed -i.bak "65s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 66
# I propose to replace exp by my_world in :     myScene.place(axis_Y_current,exp)
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 67
# I propose to replace exp by my_world in :     myScene.place(axis_Z_current,exp)
sed -i.bak "67s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 70
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 73
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([current_x_pos,current_y_pos,current_z_pos ]))
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 74
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 75
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 76
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 77
# I propose to replace exp by my_world in :     current_pos_x, current_pos_y,current_pos_z, current_cs_axis_X,current_cs_axis_Y,current_cs_axis_Z = exp.get_coordinate_system()
sed -i.bak "77s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 82
# I propose to replace exp by my_world in :     place_axis(exp, my_scene, axis_X_current, axis_Y_current, axis_Z_current)
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 87
# I propose to replace exp by my_world in :     my_scene.place(tangent_screen,exp)
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 102
# I propose to replace exp by my_world in :     my_scene.place(sphere,exp)
sed -i.bak "102s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 109
# I propose to replace exp by my_world in :     exp.reset_coordinate_system()
sed -i.bak "109s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 115
# I propose to replace exp by my_world in :     my_scene.place(cube,exp) 
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 120
# I propose to replace exp by my_world in :     my_scene.place(lineTool,exp) 
sed -i.bak "120s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 122
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([current_x_pos,current_y_pos,current_z_pos ]))
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 123
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "123s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 124
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "124s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 125
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "125s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 131
# I propose to replace exp by my_world in :     my_scene.place(cylinder,exp)
sed -i.bak "131s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 132
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "132s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 133
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "133s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 55
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "55s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 67
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "67s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 73
# I propose to replace exp by my_world in :     myScene.place(axis_global_x,exp)
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 74
# I propose to replace exp by my_world in :     myScene.place(axis_global_y,exp)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 75
# I propose to replace exp by my_world in :     myScene.place(axis_global_z,exp)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 78
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([0.0,1.2,1.0]))
sed -i.bak "78s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 81
# I propose to replace exp by my_world in :     myScene.place(text_info,exp)
sed -i.bak "81s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 83
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_z)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 84
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_x)
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 85
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 94
# I propose to replace exp by my_world in :     myScene.place(axis_group,exp)
sed -i.bak "94s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 95
# I propose to replace exp by my_world in :     myScene.place(axis_x,exp)
sed -i.bak "95s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 96
# I propose to replace exp by my_world in :     myScene.place(axis_y,exp)
sed -i.bak "96s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 97
# I propose to replace exp by my_world in :     myScene.place(axis_z,exp)
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 110
# I propose to replace exp by my_world in :     myScene.place(cube_red,exp)
sed -i.bak "110s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 116
# I propose to replace exp by my_world in :     myScene.place(cube_green,exp)
sed -i.bak "116s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 122
# I propose to replace exp by my_world in :     myScene.place(cube_blue,exp)
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 128
# I propose to replace exp by my_world in :     myScene.place(cube_yellow,exp)
sed -i.bak "128s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 134
# I propose to replace exp by my_world in :     myScene.place(cube_orange,exp)
sed -i.bak "134s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 140
# I propose to replace exp by my_world in :     myScene.place(cube_purple,exp)
sed -i.bak "140s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 142
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "142s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 143
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "143s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py, at line 45
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py, at line 47
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py, at line 60
# I propose to replace exp by my_world in :     timer_calculator = exp.calculatorManager.AddElaspedTimeCurrentScene()
sed -i.bak "60s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py, at line 84
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py, at line 21
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "21s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py, at line 40
# I propose to replace exp by my_world in :     exp = visual.The3DWorld ()
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py, at line 48
# I propose to replace exp by my_world in :     PosCalc=exp.calculatorManager.AddPositionCalculation(target,target2)
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py, at line 52
# I propose to replace exp by my_world in :     visScene.place(target,exp) # Place the target in the Visual Scene
sed -i.bak "52s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py, at line 53
# I propose to replace exp by my_world in :     visScene.place(target2,exp) # Place the target in the Visual Scene
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py, at line 54
# I propose to replace exp by my_world in :     visScene.place(target3,exp) # Place the target in the Visual Scene
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py, at line 56
# I propose to replace exp by my_world in :     exp.add_scene(visScene)              
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py, at line 58
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 19
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "19s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 39
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "39s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 48
# I propose to replace exp by my_world in :     #my_angle_calc= exp.calculatorManager.AddAngleAtHeadPOVBetweenTwoVisualObjects(visualObject1= my_sphere_1, visualObject2 =my_sphere_2)
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 49
# I propose to replace exp by my_world in :     my_angle_calc= exp.calculatorManager.AddAngleAtOriginBetweenTwoVisualObjects(visualObject1= my_sphere_1, visualObject2 =my_sphere_2)
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 53
# I propose to replace exp by my_world in :     myScene.place(my_sphere_1,exp)
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 54
# I propose to replace exp by my_world in :     myScene.place(my_sphere_2,exp)
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 55
# I propose to replace exp by my_world in :     myScene.place(my_text,exp)
sed -i.bak "55s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 57
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 58
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 22
# I propose to replace exp by my_world in : experiment_duration_in_ms = 12500 # in Ms
sed -i.bak "22s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 38
# I propose to replace exp by my_world in : """The scene while change """ + str(experiment_duration_in_ms) + " in ms since the start The3DWorld change the background color to Red"""
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 54
# I propose to replace exp by my_world in : def NewScene(exp, id_trial ,id_scene)
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 58
# I propose to replace exp by my_world in :    my_input_timer_experiment = input.Timer (delay_in_ms=experiment_duration_in_ms,input_name="experiment",timer_start="Start The3DWorld")
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 70
# I propose to replace exp by my_world in :    my_scene.AddInteraction(inputs = [my_input_timer_experiment],events=[my_event_save_scene])
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 73
# I propose to replace exp by my_world in :    exp.add_scene(my_scene)
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 77
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "77s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 80
# I propose to replace exp by my_world in :             NewScene(exp= exp,id_trial=j,id_scene=i)
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 82
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py, at line 116
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject=username, detailsThe3DWorld=text_description )
sed -i.bak "116s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py, at line 117
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(translation=np.array ([0, height_of_static_pov, 0]) )
sed -i.bak "117s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py, at line 124
# I propose to replace exp by my_world in :     my_scene.place (my_text,exp)
sed -i.bak "124s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py, at line 137
# I propose to replace exp by my_world in :     my_scene.place (sphere_target,exp)
sed -i.bak "137s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py, at line 140
# I propose to replace exp by my_world in :     my_scene.place ( target_point,exp )
sed -i.bak "140s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py, at line 176
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "176s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py, at line 177
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "177s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py, at line 38
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py, at line 42
# I propose to replace exp by my_world in :     my_scene.place(my_text,exp)
sed -i.bak "42s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py, at line 64
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py, at line 65
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "65s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py, at line 40
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py, at line 45
# I propose to replace exp by my_world in :     my_scene.place(my_text,exp)
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py, at line 73
# I propose to replace exp by my_world in :     my_scene.place (my_text_always_on,exp)
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py, at line 75
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py, at line 76
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py, at line 43
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "43s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py, at line 74
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py, at line 76
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py, at line 47
# I propose to replace exp by my_world in : def NewScene(exp, id_scene)
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py, at line 53
# I propose to replace exp by my_world in :    my_scene.place(my_text,exp)
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py, at line 59
# I propose to replace exp by my_world in :        my_event.SetNextScene(id_next_scene=exp.id_scene[0])    
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py, at line 64
# I propose to replace exp by my_world in :    my_scene.place (my_text_always_on,exp)
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py, at line 66
# I propose to replace exp by my_world in :    exp.add_scene(my_scene)
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py, at line 70
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py, at line 72
# I propose to replace exp by my_world in :         NewScene(exp= exp,id_scene=i)
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py, at line 74
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py, at line 119
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject=username, detailsThe3DWorld=text_description )
sed -i.bak "119s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py, at line 120
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(translation=np.array ([0, height_of_static_pov, 0]) )
sed -i.bak "120s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py, at line 127
# I propose to replace exp by my_world in :     my_scene.place (my_text,exp)
sed -i.bak "127s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py, at line 131
# I propose to replace exp by my_world in :     my_scene.place (sphere_target,exp)
sed -i.bak "131s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py, at line 134
# I propose to replace exp by my_world in :     my_scene.place (target_point,exp)
sed -i.bak "134s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py, at line 169
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "169s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py, at line 170
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "170s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 57
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 172
# I propose to replace exp by my_world in :     myScene.place(left_sphere,exp)
sed -i.bak "172s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 173
# I propose to replace exp by my_world in :     myScene.place(mid_sphere,exp)
sed -i.bak "173s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 174
# I propose to replace exp by my_world in :     myScene.place(right_sphere,exp)
sed -i.bak "174s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 176
# I propose to replace exp by my_world in :     myScene.place(left_text,exp)
sed -i.bak "176s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 177
# I propose to replace exp by my_world in :     myScene.place(mid_text,exp)
sed -i.bak "177s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 178
# I propose to replace exp by my_world in :     myScene.place(right_text,exp)
sed -i.bak "178s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 180
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "180s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 181
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "181s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py, at line 41
# I propose to replace exp by my_world in : def NewScene(exp)
sed -i.bak "41s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py, at line 57
# I propose to replace exp by my_world in :    my_scene.place (my_text_always_on,exp)
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py, at line 59
# I propose to replace exp by my_world in :    exp.add_scene(my_scene)
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py, at line 63
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py, at line 64
# I propose to replace exp by my_world in :     NewScene(exp= exp)
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py, at line 66
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py, at line 19
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "19s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py, at line 40
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py, at line 59
# I propose to replace exp by my_world in :         myScene.place(my_object,exp)
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py, at line 60
# I propose to replace exp by my_world in :         exp.add_scene(myScene)
sed -i.bak "60s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py, at line 61
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "61s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py, at line 15
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "15s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py, at line 42
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=textDescription)
sed -i.bak "42s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py, at line 46
# I propose to replace exp by my_world in :     myScene.place(audio,exp)
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py, at line 49
# I propose to replace exp by my_world in :     myScene.place(myTextLive,exp)  
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py, at line 51
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "51s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py, at line 52
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "52s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 19
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "19s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 41
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "41s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 44
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([-2,0.0,0.0]))
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 45
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotation_deg=45)
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 48
# I propose to replace exp by my_world in :     my_scene.place(my_sphere,exp)
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 49
# I propose to replace exp by my_world in :     my_scene.place(my_line,exp)
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 51
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "51s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 52
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "52s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py, at line 14
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "14s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py, at line 34
# I propose to replace exp by my_world in :     exp = visual.The3DWorld()
sed -i.bak "34s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py, at line 38
# I propose to replace exp by my_world in :     myScene1.place(myLineX,exp)
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py, at line 42
# I propose to replace exp by my_world in :     myScene1.place(myLineY,exp)
sed -i.bak "42s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py, at line 46
# I propose to replace exp by my_world in :     myScene1.place(myLineZ,exp)
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py, at line 48
# I propose to replace exp by my_world in :     exp.add_scene(myScene1)
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py, at line 50
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "50s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 98
# I propose to replace exp by my_world in :     exp = visual.The3DWorld ( detailsThe3DWorld=text_description)
sed -i.bak "98s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 99
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(translation=np.array([0.0, height_of_static_pov, 0.0]))
sed -i.bak "99s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 108
# I propose to replace exp by my_world in :         my_scene.place (my_tangent_screen,exp)
sed -i.bak "108s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 115
# I propose to replace exp by my_world in :         my_scene.place(my_text_scene,exp)
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 119
# I propose to replace exp by my_world in :         my_scene.place(my_text_info,exp)
sed -i.bak "119s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 127
# I propose to replace exp by my_world in :             my_scene.place(my_object,exp)
sed -i.bak "127s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 128
# I propose to replace exp by my_world in :             my_scene.place(my_text,exp)
sed -i.bak "128s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 134
# I propose to replace exp by my_world in :             my_event_next_scene.SetNextScene(id_next_scene=exp.id_scene[0])
sed -i.bak "134s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 143
# I propose to replace exp by my_world in :         exp.add_scene(my_scene)
sed -i.bak "143s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 145
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "145s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py, at line 52
# I propose to replace exp by my_world in : # the simplest experiment and scene
sed -i.bak "52s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py, at line 53
# I propose to replace exp by my_world in : exp = visual.The3DWorld (  )
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py, at line 61
# I propose to replace exp by my_world in :     # the simplest experiment and scene
sed -i.bak "61s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py, at line 62
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (  )
sed -i.bak "62s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py, at line 95
# I propose to replace exp by my_world in :     myScene.place(sphere1,exp)
sed -i.bak "95s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py, at line 96
# I propose to replace exp by my_world in :     myScene.place(sphere2,exp)
sed -i.bak "96s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py, at line 97
# I propose to replace exp by my_world in :     myScene.place(sphere3,exp)
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py, at line 99
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "99s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py, at line 100
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "100s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py, at line 63
# I propose to replace exp by my_world in : # the simplest experiment and scene
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py, at line 64
# I propose to replace exp by my_world in : exp = visual.The3DWorld (  )
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py, at line 111
# I propose to replace exp by my_world in :     myScene.place(sphere1,exp)
sed -i.bak "111s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py, at line 112
# I propose to replace exp by my_world in :     myScene.place(sphere2,exp)
sed -i.bak "112s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py, at line 113
# I propose to replace exp by my_world in :     myScene.place(sphere3,exp)
sed -i.bak "113s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py, at line 115
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py, at line 116
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "116s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py, at line 16
# I propose to replace exp by my_world in :     Warning! experimental, use at your own risks
sed -i.bak "16s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py, at line 57
# I propose to replace exp by my_world in :     warning! experimental, use at your own risks.
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py, at line 135
# I propose to replace exp by my_world in :     exp = visual.The3DWorld ( detailsThe3DWorld=text_description)
sed -i.bak "135s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py, at line 149
# I propose to replace exp by my_world in :             my_event_next_scene.SetNextScene(exp.id_scene[len(exp.id_scene)-1])
sed -i.bak "149s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py, at line 185
# I propose to replace exp by my_world in :         exp.add_scene(my_scene)
sed -i.bak "185s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py, at line 189
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "189s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 95
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (detailsThe3DWorld=text_description)
sed -i.bak "95s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 96
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(translation=np.array([0.0, height_of_static_pov, 0.0]))
sed -i.bak "96s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 105
# I propose to replace exp by my_world in :         my_scene.place (my_tangent_screen,exp)
sed -i.bak "105s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 114
# I propose to replace exp by my_world in :         my_scene.place(my_text_scene,exp)
sed -i.bak "114s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 148
# I propose to replace exp by my_world in :             my_scene.place(my_object,exp)
sed -i.bak "148s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 149
# I propose to replace exp by my_world in :             my_scene.place(my_text_letter,exp)
sed -i.bak "149s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 150
# I propose to replace exp by my_world in :             my_scene.place(my_text_distance,exp)
sed -i.bak "150s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 163
# I propose to replace exp by my_world in :             my_event_next_scene.SetNextScene(exp.id_scene[0])
sed -i.bak "163s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 169
# I propose to replace exp by my_world in :         exp.add_scene(my_scene)   
sed -i.bak "169s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 171
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "171s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py, at line 16
# I propose to replace exp by my_world in :     Warning! experimental, use at your own risks
sed -i.bak "16s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py, at line 57
# I propose to replace exp by my_world in :     warning! experimental, use at your own risks.
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py, at line 138
# I propose to replace exp by my_world in :     exp = visual.The3DWorld ( detailsThe3DWorld=text_description)
sed -i.bak "138s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py, at line 149
# I propose to replace exp by my_world in :             my_event_end_current_scene.SetNextScene(exp.id_scene[0])
sed -i.bak "149s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py, at line 176
# I propose to replace exp by my_world in :                 my_scene.place(sphere,exp)
sed -i.bak "176s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py, at line 179
# I propose to replace exp by my_world in :         exp.add_scene(my_scene)
sed -i.bak "179s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py, at line 183
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "183s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py, at line 69
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py, at line 100
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "100s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py, at line 101
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "101s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py, at line 12
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "12s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py, at line 13
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "13s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py, at line 28
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "28s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py, at line 62
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "62s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py, at line 83
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py, at line 85
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py, at line 101
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "101s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py, at line 103
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(translation=np.array([0.0, height_of_static_pov, 0.0]))
sed -i.bak "103s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py, at line 112
# I propose to replace exp by my_world in :         my_scene.place (my_tangent_screen,exp)
sed -i.bak "112s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py, at line 119
# I propose to replace exp by my_world in :         my_scene.place(my_text_scene,exp)
sed -i.bak "119s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py, at line 122
# I propose to replace exp by my_world in :         my_scene.place(my_text_info,exp)
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py, at line 138
# I propose to replace exp by my_world in :             my_scene.place(my_object,exp)
sed -i.bak "138s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py, at line 162
# I propose to replace exp by my_world in :             my_event_end_current_scene.SetNextScene(exp.id_scene[0])
sed -i.bak "162s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py, at line 166
# I propose to replace exp by my_world in :         exp.add_scene(my_scene)
sed -i.bak "166s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py, at line 168
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "168s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py, at line 16
# I propose to replace exp by my_world in :     Warning! experimental, use at your own risks
sed -i.bak "16s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py, at line 57
# I propose to replace exp by my_world in :     warning! experimental, use at your own risks.
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py, at line 136
# I propose to replace exp by my_world in :     exp = visual.The3DWorld ( detailsThe3DWorld=text_description)
sed -i.bak "136s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py, at line 180
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "180s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py, at line 183
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "183s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 22
# I propose to replace exp by my_world in : In this experiment move your head towards the targets (sphere).
sed -i.bak "22s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 68
# I propose to replace exp by my_world in : # the simplest experiment and scene
sed -i.bak "68s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 69
# I propose to replace exp by my_world in : exp = visual.The3DWorld ()
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 97
# I propose to replace exp by my_world in : def scene_with_target (exp,ecc,hm)
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 104
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen,exp)
sed -i.bak "104s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 105
# I propose to replace exp by my_world in :     my_scene.place(my_target,exp)
sed -i.bak "105s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 163
# I propose to replace exp by my_world in :     my_scene.fill_in_results_file_column(column_name="distance_exp", value=distance_xp)
sed -i.bak "163s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 167
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "167s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 171
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (detailsThe3DWorld=text_description)
sed -i.bak "171s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 174
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(translation=np.array([0.0, height_of_static_pov, 0.0]))
sed -i.bak "174s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 182
# I propose to replace exp by my_world in :         scene_with_target(exp=exp, ecc=0, hm=0)
sed -i.bak "182s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 185
# I propose to replace exp by my_world in :         scene_with_target(exp=exp, ecc=eccentricity, hm=hm_step*count)
sed -i.bak "185s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 187
# I propose to replace exp by my_world in :     scene_with_target(exp=exp, ecc=0, hm=0)
sed -i.bak "187s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 188
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "188s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 14
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "14s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 47
# I propose to replace exp by my_world in :     #Event for Continu experiment
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 60
# I propose to replace exp by my_world in :     #Event for Continu experiment
sed -i.bak "60s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 83
# I propose to replace exp by my_world in :     #Event for Continu experiment
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 94
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "94s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 102
# I propose to replace exp by my_world in :             TrialScene.place(InformationText,exp)
sed -i.bak "102s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 105
# I propose to replace exp by my_world in :             TrialScene.place(myText,exp)
sed -i.bak "105s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 115
# I propose to replace exp by my_world in :             exp.add_scene(TrialScene)
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 117
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "117s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 14
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "14s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 50
# I propose to replace exp by my_world in :     #Event for Continu experiment
sed -i.bak "50s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 63
# I propose to replace exp by my_world in :     #Event for Continu experiment
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 86
# I propose to replace exp by my_world in :     #Event for Continu experiment
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 97
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 104
# I propose to replace exp by my_world in :             TrialScene.place(InformationText,exp)
sed -i.bak "104s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 107
# I propose to replace exp by my_world in :             TrialScene.place(myText,exp)
sed -i.bak "107s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 115
# I propose to replace exp by my_world in :             exp.add_scene(TrialScene)
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 117
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "117s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py, at line 45
# I propose to replace exp by my_world in : result_file = 'mnread_vr_experiment_gaze_data_DummyUser_2022-24-10--14-01-15.csv'
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py, at line 57
# I propose to replace exp by my_world in : def create_scene(exp,resultfile, sentence,data)
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py, at line 60
# I propose to replace exp by my_world in :     exp.add_scene(scene)
sed -i.bak "60s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py, at line 110
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(operatorMode=True,name_of_subject=parameters.subject_name)
sed -i.bak "110s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py, at line 111
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(current_coordinate_system)
sed -i.bak "111s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py, at line 115
# I propose to replace exp by my_world in :     create_scene(exp, result_file,sentences[0],data)
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py, at line 117
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "117s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py, at line 118
# I propose to replace exp by my_world in :     print("The experiment has been written.")
sed -i.bak "118s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 68
# I propose to replace exp by my_world in : # the simplest experiment and scene
sed -i.bak "68s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 69
# I propose to replace exp by my_world in : exp = visual.The3DWorld ()
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 98
# I propose to replace exp by my_world in : def scene_with_target (exp, ecc, hm, isTakePicture, scene_rank)
sed -i.bak "98s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 139
# I propose to replace exp by my_world in :     my_scene.fill_in_results_file_column(column_name="distance_exp", value=distance_xp)
sed -i.bak "139s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 143
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "143s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 146
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "146s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 147
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (translation = np.array([0.0, height_of_static_pov, 0.0]))
sed -i.bak "147s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 155
# I propose to replace exp by my_world in :         scene_with_target (exp = exp, ecc=0, hm=0, isTakePicture = False, scene_rank = count)
sed -i.bak "155s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 158
# I propose to replace exp by my_world in :         scene_with_target (exp = exp, ecc = eccentricity, hm = hm_step * count, isTakePicture = False, scene_rank = count )
sed -i.bak "158s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 161
# I propose to replace exp by my_world in :     scene_with_target (exp=exp, ecc=0, hm=0, isTakePicture = True, scene_rank = count + 1)
sed -i.bak "161s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 162
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "162s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 48
# I propose to replace exp by my_world in : # result_files = ['mnread_vr_experiment_tangent_screen694408715725260416_gaze_Johanna_2022-24-10--13-44-11.csv',
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 49
# I propose to replace exp by my_world in : #                 'mnread_vr_experiment_tangent_screen724736310865579648_gaze_Johanna_2022-24-10--13-44-11.csv',
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 50
# I propose to replace exp by my_world in : #                 'mnread_vr_experiment_tangent_screen415187326085863936_gaze_Johanna_2022-24-10--13-44-11.csv',
sed -i.bak "50s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 51
# I propose to replace exp by my_world in : #                 'mnread_vr_experiment_tangent_screen352090057368449472_gaze_Johanna_2022-24-10--13-44-11.csv']
sed -i.bak "51s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 55
# I propose to replace exp by my_world in : # result_files = ['mnread_vr_experiment_tangent_screen363451513119490176_gaze_Johanna_2022-25-10--09-36-13.csv',
sed -i.bak "55s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 56
# I propose to replace exp by my_world in : #                 'mnread_vr_experiment_tangent_screen611556093199102720_gaze_Johanna_2022-25-10--09-36-13.csv',
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 57
# I propose to replace exp by my_world in : #                 'mnread_vr_experiment_tangent_screen31971105964140988_gaze_Johanna_2022-25-10--09-36-13.csv',
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 58
# I propose to replace exp by my_world in : #                 'mnread_vr_experiment_tangent_screen381044559497066368_gaze_Johanna_2022-25-10--09-36-13.csv']
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 62
# I propose to replace exp by my_world in : # result_files = ['mnread_vr_experiment_tangent_screen564538369099832000_gaze_Johanna_2022-25-10--14-22-14.csv', # centre
sed -i.bak "62s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 63
# I propose to replace exp by my_world in : #                 'mnread_vr_experiment_tangent_screen358808534241466432_gaze_Johanna_2022-25-10--14-22-14.csv', # haut
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 64
# I propose to replace exp by my_world in : #                 'mnread_vr_experiment_tangent_screen279947777064336608_gaze_Johanna_2022-25-10--14-22-14.csv', # droite
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 69
# I propose to replace exp by my_world in : result_files = ['mnread_vr_experiment_tangent_screen958559519000970880_gaze_Johanna_2022-26-10--09-54-43.csv', # droite
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 84
# I propose to replace exp by my_world in : def create_scene(exp,tangent_screen_id, sentence,screen_width,screen_height)
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 87
# I propose to replace exp by my_world in :     exp.add_scene(scene)
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 251
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(operatorMode=True,name_of_subject=parameters.subject_name)
sed -i.bak "251s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 252
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(current_coordinate_system)
sed -i.bak "252s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 257
# I propose to replace exp by my_world in :         create_scene(exp,  data_cleaned['tangent_screen_id'][i],data_cleaned['sentence_displayed'][i],screen_width = data_cleaned['tangent_screen_width_in_m'][i],
sed -i.bak "257s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 260
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "260s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 261
# I propose to replace exp by my_world in :     print("The experiment has been written.")
sed -i.bak "261s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 90
# I propose to replace exp by my_world in : # the simplest experiment and scene
sed -i.bak "90s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 91
# I propose to replace exp by my_world in : exp = visual.The3DWorld ()
sed -i.bak "91s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 119
# I propose to replace exp by my_world in : def scene_with_target (exp, ecc, hm, isTakePicture, scene_rank)
sed -i.bak "119s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 128
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen,exp)
sed -i.bak "128s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 129
# I propose to replace exp by my_world in :     my_scene.place(my_target,exp)
sed -i.bak "129s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 194
# I propose to replace exp by my_world in :     my_scene.fill_in_results_file_column(column_name="distance_exp", value=distance_xp)
sed -i.bak "194s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 198
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "198s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 201
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "201s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 202
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (translation = np.array([0.0, height_of_static_pov, 0.0]))
sed -i.bak "202s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 210
# I propose to replace exp by my_world in :         scene_with_target (exp = exp, ecc=0, hm=0, isTakePicture = False, scene_rank = count)
sed -i.bak "210s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 213
# I propose to replace exp by my_world in :         scene_with_target (exp = exp, ecc = eccentricity, hm = hm_step * count, isTakePicture = False, scene_rank = count )
sed -i.bak "213s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 216
# I propose to replace exp by my_world in :     scene_with_target (exp=exp, ecc=0, hm=0, isTakePicture = True, scene_rank = count + 1)
sed -i.bak "216s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 217
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "217s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 88
# I propose to replace exp by my_world in : # the simplest experiment and scene
sed -i.bak "88s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 89
# I propose to replace exp by my_world in : exp = visual.The3DWorld ()
sed -i.bak "89s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 118
# I propose to replace exp by my_world in : def scene_with_target (exp, ecc, hm, isTakePicture, scene_rank)
sed -i.bak "118s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 194
# I propose to replace exp by my_world in :     my_scene.fill_in_results_file_column(column_name="distance_exp", value=distance_xp)
sed -i.bak "194s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 198
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "198s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 201
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "201s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 202
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (translation = np.array([0.0, height_of_static_pov, 0.0]))
sed -i.bak "202s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 210
# I propose to replace exp by my_world in :         scene_with_target (exp = exp, ecc=0, hm=0, isTakePicture = False, scene_rank = count)
sed -i.bak "210s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 213
# I propose to replace exp by my_world in :         scene_with_target (exp = exp, ecc = eccentricity, hm = hm_step * count, isTakePicture = False, scene_rank = count )
sed -i.bak "213s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 216
# I propose to replace exp by my_world in :     scene_with_target (exp=exp, ecc=0, hm=0, isTakePicture = True, scene_rank = count + 1)
sed -i.bak "216s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 217
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "217s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py, at line 38
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py, at line 40
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py, at line 41
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "41s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 47
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 69
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 72
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_current(translation=np.array([0.0,1.2,1.0]))
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 78
# I propose to replace exp by my_world in :     myScene.place(text_info,exp)
sed -i.bak "78s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 80
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_z(rotate_current_z)
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 81
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_x(rotate_current_x)
sed -i.bak "81s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 82
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_current_y(rotate_current_y)
sed -i.bak "82s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 109
# I propose to replace exp by my_world in :     myScene.place(tangent_screen_black,exp)
sed -i.bak "109s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 110
# I propose to replace exp by my_world in :     myScene.place(axis_x,exp)
sed -i.bak "110s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 111
# I propose to replace exp by my_world in :     myScene.place(axis_y,exp)
sed -i.bak "111s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 112
# I propose to replace exp by my_world in :     myScene.place(axis_z,exp)
sed -i.bak "112s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 116
# I propose to replace exp by my_world in :     myScene.place(cube_red,exp)
sed -i.bak "116s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 119
# I propose to replace exp by my_world in :     myScene.place(cube_green,exp)
sed -i.bak "119s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 121
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "121s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 122
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 22
# I propose to replace exp by my_world in : that will be passed to Unity to run your experiment (or demo or test ...). 
sed -i.bak "22s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 28
# I propose to replace exp by my_world in : Please see detailed explanations at the end of the present script
sed -i.bak "28s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 40
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (detailsThe3DWorld=text_description)
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 47
# I propose to replace exp by my_world in :     my_scene.place(my_text,exp)   
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 48
# I propose to replace exp by my_world in :     exp.add_scene(my_scene) # must be after the lines of code with my_scene.place (object)
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 50
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to a .json file
sed -i.bak "50s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 27
# I propose to replace exp by my_world in : that will be passed to Unity to run your experiment (or demo or test ...). 
sed -i.bak "27s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 33
# I propose to replace exp by my_world in : Please see detailed explanations at the end of the present script
sed -i.bak "33s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 45
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (detailsThe3DWorld=text_description)
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 49
# I propose to replace exp by my_world in :     my_scene.place(my_text,exp)   
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 50
# I propose to replace exp by my_world in :     exp.add_scene(my_scene) # must be after the lines of code with my_scene.place (object)
sed -i.bak "50s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 52
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to a .json file
sed -i.bak "52s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 66
# I propose to replace exp by my_world in : exp = visual.The3DWorld ( jsonFileCategory = myJsonFileCategory )
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 72
# I propose to replace exp by my_world in : exp = visual.The3DWorld ( jsonFileCategory = myJsonFileCategory )
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 78
# I propose to replace exp by my_world in : exp = visual.The3DWorld ( jsonFileCategory = myJsonFileCategory )
sed -i.bak "78s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py, at line 19
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "19s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py, at line 38
# I propose to replace exp by my_world in : def createScenes (exp)
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py, at line 44
# I propose to replace exp by my_world in :             myScene = PTVR.Stimuli.World.VisualScene (experiment = exp)
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py, at line 79
# I propose to replace exp by my_world in :             exp.add_scene(myScene)
sed -i.bak "79s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py, at line 86
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld="")
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py, at line 87
# I propose to replace exp by my_world in :     exp.set_coordinate_system_from_a_virtual_point(myStaticPOV)
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py, at line 89
# I propose to replace exp by my_world in :     createScenes(exp= exp)
sed -i.bak "89s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py, at line 90
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "90s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py, at line 17
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "17s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py, at line 35
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "35s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py, at line 75
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py, at line 76
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py, at line 22
# I propose to replace exp by my_world in : #the description of the experiment. It'll be integrated when the PTVR GUI will be functional.
sed -i.bak "22s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py, at line 37
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "37s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py, at line 49
# I propose to replace exp by my_world in :             # if the trial is the last of the experiment, start again the loop of trials
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py, at line 53
# I propose to replace exp by my_world in :                     event_end_current_scene.SetNextScene (id_next_scene = exp.id_scene[0]) # go the the first scene of the loop 
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py, at line 70
# I propose to replace exp by my_world in :             trial_scene.place (text_for_each_scene,exp)
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py, at line 75
# I propose to replace exp by my_world in :             trial_scene.place (my_text_always_on,exp)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py, at line 77
# I propose to replace exp by my_world in :             exp.add_scene(trial_scene)
sed -i.bak "77s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py, at line 79
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "79s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py, at line 17
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "17s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py, at line 39
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "39s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py, at line 64
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py, at line 65
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "65s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py, at line 15
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "15s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py, at line 56
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py, at line 61
# I propose to replace exp by my_world in :     my_scene.place(my_text,exp)
sed -i.bak "61s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py, at line 74
# I propose to replace exp by my_world in :     my_scene.place (my_text_always_on,exp)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py, at line 75
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py, at line 76
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py, at line 52
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "52s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py, at line 68
# I propose to replace exp by my_world in :     my_scene.place (my_text_always_on,exp)
sed -i.bak "68s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py, at line 70
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py, at line 71
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "71s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py, at line 36
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "36s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py, at line 38
# I propose to replace exp by my_world in :     exp.add_scene(scene)
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py, at line 119
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "119s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py, at line 21
# I propose to replace exp by my_world in :     When you press space you get a png in the result folder of your experiment
sed -i.bak "21s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py, at line 37
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "37s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py, at line 48
# I propose to replace exp by my_world in :     my_scene.place(my_text_information,exp)
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py, at line 49
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py, at line 51
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "51s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py, at line 35
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "35s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py, at line 57
# I propose to replace exp by my_world in :     my_scene.place (my_target_disc,exp)
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py, at line 58
# I propose to replace exp by my_world in :     my_scene.place(my_target_point,exp)
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py, at line 60
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "60s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py, at line 62
# I propose to replace exp by my_world in :     exp.add_scene(my_scene_2)
sed -i.bak "62s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py, at line 63
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py, at line 19
# I propose to replace exp by my_world in : Using this Event will be more useful for operators who are running an experiment and have a subject who fail a Trial.
sed -i.bak "19s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py, at line 40
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py, at line 52
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "52s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py, at line 54
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 16
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "16s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 43
# I propose to replace exp by my_world in : def NewScene (exp, counter)
sed -i.bak "43s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 51
# I propose to replace exp by my_world in :         my_event.SetNextScene(id_next_scene = exp.id_scene[0]) # go the the first scene of the loop 
sed -i.bak "51s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 56
# I propose to replace exp by my_world in :     my_scene.place(my_text,exp)
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 59
# I propose to replace exp by my_world in :     my_scene.place (my_text_always_on,exp)
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 60
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "60s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 66
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 69
# I propose to replace exp by my_world in :        NewScene (exp=exp, counter = i)
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 71
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "71s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py, at line 14
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "14s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py, at line 33
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "33s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py, at line 44
# I propose to replace exp by my_world in :     my_scene.place (my_text,exp)
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py, at line 46
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py, at line 47
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py, at line 53
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py, at line 70
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py, at line 71
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "71s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py, at line 2
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "2s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py, at line 3
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py, at line 36
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description, operatorMode=True)
sed -i.bak "36s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py, at line 38
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py, at line 85
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py, at line 33
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "33s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py, at line 43
# I propose to replace exp by my_world in :     my_scene.place(my_text_press,exp)
sed -i.bak "43s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py, at line 45
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py, at line 46
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py, at line 69
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py, at line 74
# I propose to replace exp by my_world in :     my_scene.place(my_object,exp)
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py, at line 87
# I propose to replace exp by my_world in :     my_scene.place (mytext_always_on,exp)
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py, at line 89
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "89s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py, at line 90
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "90s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py, at line 54
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py, at line 59
# I propose to replace exp by my_world in :     my_scene.place(my_object,exp)
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py, at line 73
# I propose to replace exp by my_world in :     my_scene.place (my_text_always_on,exp)
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py, at line 75
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py, at line 76
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py, at line 130
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "130s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py, at line 135
# I propose to replace exp by my_world in :         my_scene.place(my_object,exp)   
sed -i.bak "135s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py, at line 152
# I propose to replace exp by my_world in :     my_scene.place (my_text_always_on,exp)
sed -i.bak "152s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py, at line 154
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "154s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py, at line 155
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "155s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py, at line 32
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "32s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py, at line 42
# I propose to replace exp by my_world in :     my_scene.place(my_text_press,exp)
sed -i.bak "42s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py, at line 45
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py, at line 46
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py, at line 48
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(detailsThe3DWorld=text_description)
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py, at line 55
# I propose to replace exp by my_world in :     my_scene.place(my_object,exp)
sed -i.bak "55s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py, at line 67
# I propose to replace exp by my_world in :     my_scene.place (my_text_always_on,exp)
sed -i.bak "67s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py, at line 69
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py, at line 70
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py, at line 14
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "14s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py, at line 38
# I propose to replace exp by my_world in : # the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py, at line 46
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (  name_of_subject = username,
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py, at line 87
# I propose to replace exp by my_world in :     my_scene.place (my_text_pausable,exp)
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py, at line 123
# I propose to replace exp by my_world in :     my_scene.place (text_that_cannot_be_changed,exp)
sed -i.bak "123s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py, at line 126
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "126s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py, at line 127
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "127s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py, at line 22
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "22s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py, at line 38
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username, detailsThe3DWorld=text_description)
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py, at line 46
# I propose to replace exp by my_world in :     my_scene.place(my_text,exp)
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py, at line 53
# I propose to replace exp by my_world in :     exp.add_scene (my_scene)
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py, at line 54
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py, at line 23
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "23s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py, at line 41
# I propose to replace exp by my_world in :    exp = visual.The3DWorld (name_of_subject = username, detailsThe3DWorld=text_description)
sed -i.bak "41s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py, at line 49
# I propose to replace exp by my_world in :    my_scene.place(my_text,exp)
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py, at line 52
# I propose to replace exp by my_world in :    exp.add_scene (my_scene)
sed -i.bak "52s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py, at line 53
# I propose to replace exp by my_world in :    exp.write() # Write the The3DWorld to .json file
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py, at line 30
# I propose to replace exp by my_world in : def new_scene(exp,  id_scene)
sed -i.bak "30s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py, at line 37
# I propose to replace exp by my_world in :    my_scene.place(my_text,exp)
sed -i.bak "37s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py, at line 41
# I propose to replace exp by my_world in :    # that will be presented will the first scene of the experiment (here exp.id_scene[0]),
sed -i.bak "41s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py, at line 44
# I propose to replace exp by my_world in :        my_scene.SetNextScene ( id_next_scene = exp.id_scene [0] )
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py, at line 45
# I propose to replace exp by my_world in :    exp.add_scene(my_scene) 
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py, at line 49
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username, detailsThe3DWorld=text_description)
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py, at line 52
# I propose to replace exp by my_world in :        new_scene (exp = exp, id_scene = i)     
sed -i.bak "52s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py, at line 54
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 47
# I propose to replace exp by my_world in : def NewScene (exp, id_scene)
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 53
# I propose to replace exp by my_world in :    my_scene.place (my_text,exp)
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 60
# I propose to replace exp by my_world in :    # that will be presented will the first scene of the experiment (here exp.id_scene[0]),
sed -i.bak "60s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 63
# I propose to replace exp by my_world in :        my_event.SetNextScene ( id_next_scene = exp.id_scene[0] )    
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 69
# I propose to replace exp by my_world in :    my_scene.place (my_text_always_on,exp)
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 71
# I propose to replace exp by my_world in :    exp.add_scene(my_scene)
sed -i.bak "71s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 75
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username, detailsThe3DWorld=text_description)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 77
# I propose to replace exp by my_world in :         NewScene(exp= exp, id_scene= i )
sed -i.bak "77s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 79
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "79s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 33
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = "toto")
sed -i.bak "33s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 35
# I propose to replace exp by my_world in :     draw_CS.draw_current_cartesian_CS (exp,my_scene, axis_length=10, CS_axes_text=" Global ")  # draw GLOBAL Axes
sed -i.bak "35s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 43
# I propose to replace exp by my_world in :     my_scene.place (my_pink_point_P,exp)  
sed -i.bak "43s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 47
# I propose to replace exp by my_world in :     my_scene.place (my_pink_point_P_text,exp)
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 53
# I propose to replace exp by my_world in :     my_scene.place (my_orange_point,exp) 
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 57
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global( global_coordinates_of_orange_point)
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 58
# I propose to replace exp by my_world in :     draw_CS.draw_current_cartesian_CS (exp,my_scene, axis_length=1.2,  CS_axes_text="   Current ")    
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 69
# I propose to replace exp by my_world in :     my_scene.place (my_cylinder_M,exp)  
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 73
# I propose to replace exp by my_world in :     my_scene.place (my_black_cylinder_M_text,exp)     
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 77
# I propose to replace exp by my_world in :     exp.add_scene (my_scene)
sed -i.bak "77s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 78
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "78s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 37
# I propose to replace exp by my_world in : def draw_current_cartesian_CS (exp,scene, axis_length, CS_axes_text)
sed -i.bak "37s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 59
# I propose to replace exp by my_world in :     scene.place (XaxisPlus,exp) 
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 67
# I propose to replace exp by my_world in :         scene.place (myText_X_2m,exp) 
sed -i.bak "67s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 74
# I propose to replace exp by my_world in :         scene.place (myText_4m,exp)     
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 80
# I propose to replace exp by my_world in :     scene.place (X_axis_label,exp) 
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 86
# I propose to replace exp by my_world in :     scene.place (YaxisPlus,exp)     
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 91
# I propose to replace exp by my_world in :         scene.place (myText_Y_2m,exp)         
sed -i.bak "91s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 96
# I propose to replace exp by my_world in :     scene.place (Y_axis_label,exp) 
sed -i.bak "96s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 102
# I propose to replace exp by my_world in :     scene.place (ZaxisPlus,exp) 
sed -i.bak "102s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 108
# I propose to replace exp by my_world in :         scene.place (myText_Z_2m,exp) 
sed -i.bak "108s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 112
# I propose to replace exp by my_world in :         scene.place (myText_Z_4m,exp) 
sed -i.bak "112s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 116
# I propose to replace exp by my_world in :         scene.place (myText_Z_10m,exp) 
sed -i.bak "116s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 122
# I propose to replace exp by my_world in :     scene.place (Z_axis_label,exp) 
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py, at line 10
# I propose to replace exp by my_world in : + explications
sed -i.bak "10s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py, at line 48
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject = username)
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py, at line 51
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (head_viewpoint_position)
sed -i.bak "51s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py, at line 52
# I propose to replace exp by my_world in :     #exp.rotate_coordinate_system_about_global_y (90)
sed -i.bak "52s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py, at line 59
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen,exp)  
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py, at line 67
# I propose to replace exp by my_world in :     my_scene.place(my_center_of_TS,exp)
sed -i.bak "67s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py, at line 86
# I propose to replace exp by my_world in :     my_scene.place(my_object,exp)
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py, at line 101
# I propose to replace exp by my_world in :     my_scene.place (my_object_set_in_spherical,exp) 
sed -i.bak "101s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py, at line 111
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "111s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py, at line 112
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "112s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py, at line 26
# I propose to replace exp by my_world in : # an experiment (either seated or standing)
sed -i.bak "26s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py, at line 33
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = user_name)
sed -i.bak "33s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py, at line 37
# I propose to replace exp by my_world in :     CSdraw.draw_current_cartesian_CS (exp,my_scene, axis_length= 10, CS_axes_text= "Global") # in coordinate_system_drawing_utils.py
sed -i.bak "37s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py, at line 48
# I propose to replace exp by my_world in :     my_scene.place (my_text_welcome,exp)  
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py, at line 59
# I propose to replace exp by my_world in :     my_scene.place (my_text_left_handed,exp) 
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py, at line 68
# I propose to replace exp by my_world in :     my_scene.place (my_text_origin,exp)   
sed -i.bak "68s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py, at line 71
# I propose to replace exp by my_world in :     exp.add_scene (my_scene)
sed -i.bak "71s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py, at line 72
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.1_create_object_in_cartesian_coord_+_object_rotation.py, at line 31
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "31s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.1_create_object_in_cartesian_coord_+_object_rotation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.1_create_object_in_cartesian_coord_+_object_rotation.py, at line 46
# I propose to replace exp by my_world in :     my_scene.place (my_cube,exp)
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.1_create_object_in_cartesian_coord_+_object_rotation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.1_create_object_in_cartesian_coord_+_object_rotation.py, at line 47
# I propose to replace exp by my_world in :     exp.add_scene (my_scene)   
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.1_create_object_in_cartesian_coord_+_object_rotation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.1_create_object_in_cartesian_coord_+_object_rotation.py, at line 49
# I propose to replace exp by my_world in :     exp.write () 
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.1_create_object_in_cartesian_coord_+_object_rotation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.0_create_object_in_cartesian_coord.py, at line 29
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "29s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.0_create_object_in_cartesian_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.0_create_object_in_cartesian_coord.py, at line 41
# I propose to replace exp by my_world in :     my_scene.place (my_cube,exp)
sed -i.bak "41s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.0_create_object_in_cartesian_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.0_create_object_in_cartesian_coord.py, at line 42
# I propose to replace exp by my_world in :     exp.add_scene (my_scene)   
sed -i.bak "42s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.0_create_object_in_cartesian_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.0_create_object_in_cartesian_coord.py, at line 44
# I propose to replace exp by my_world in :     exp.write () 
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.0_create_object_in_cartesian_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/3_rotation_animated_about_one_axis.py, at line 31
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject = username)
sed -i.bak "31s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/3_rotation_animated_about_one_axis.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/3_rotation_animated_about_one_axis.py, at line 40
# I propose to replace exp by my_world in :         my_scene.place (my_cube,exp)
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/3_rotation_animated_about_one_axis.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/3_rotation_animated_about_one_axis.py, at line 41
# I propose to replace exp by my_world in :         exp.add_scene (my_scene)
sed -i.bak "41s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/3_rotation_animated_about_one_axis.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/3_rotation_animated_about_one_axis.py, at line 43
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "43s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/3_rotation_animated_about_one_axis.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py, at line 24
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "24s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py, at line 42
# I propose to replace exp by my_world in :     my_scene.place (my_cube_1,exp)
sed -i.bak "42s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py, at line 43
# I propose to replace exp by my_world in :     my_scene.place (my_cube_2,exp)
sed -i.bak "43s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py, at line 44
# I propose to replace exp by my_world in :     exp.add_scene (my_scene)   
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py, at line 46
# I propose to replace exp by my_world in :     exp.write () 
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py, at line 33
# I propose to replace exp by my_world in : # an experiment (either seated or standing)
sed -i.bak "33s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py, at line 42
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "42s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py, at line 43
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global ( translation = np.array ([0, height_of_head_viewpoint, 0]))    
sed -i.bak "43s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py, at line 55
# I propose to replace exp by my_world in :             my_cube.rotate_to_look_at(exp.idHeadset)
sed -i.bak "55s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py, at line 56
# I propose to replace exp by my_world in :             vis_scene.place (my_cube,exp)
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py, at line 58
# I propose to replace exp by my_world in :     exp.add_scene(vis_scene)
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py, at line 59
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py, at line 58
# I propose to replace exp by my_world in :     exp = visual.The3DWorld ( name_of_subject = username )
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py, at line 59
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global ( translation = np.array ( [0, height_of_head_viewpoint, 0 ] ) ) 
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py, at line 65
# I propose to replace exp by my_world in :     vis_scene.place (my_sphere,exp)
sed -i.bak "65s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py, at line 81
# I propose to replace exp by my_world in :             vis_scene.place (my_flat_screen,exp)  
sed -i.bak "81s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py, at line 83
# I propose to replace exp by my_world in :     exp.add_scene(vis_scene)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py, at line 84
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py, at line 38
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py, at line 41
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global ( translation = np.array ( [0, height_of_head_viewpoint, 0]) )  
sed -i.bak "41s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py, at line 47
# I propose to replace exp by my_world in :     vis_scene.place (my_cube,exp)  
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py, at line 70
# I propose to replace exp by my_world in :             my_sphere.rotate_to_look_at(id_object_to_lookAt=exp.idOriginCurrentCS)  
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py, at line 72
# I propose to replace exp by my_world in :             vis_scene.place (my_sphere,exp) 
sed -i.bak "72s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py, at line 73
# I propose to replace exp by my_world in :             vis_scene.place (inner_sphere,exp) 
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py, at line 75
# I propose to replace exp by my_world in :     exp.add_scene(vis_scene)
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py, at line 76
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "76s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py, at line 36
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "36s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py, at line 37
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(np.array([0, height_of_head_viewpoint, 0]))  
sed -i.bak "37s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py, at line 59
# I propose to replace exp by my_world in :             my_sphere.rotate_to_look_at(id_object_to_lookAt=exp.idHeadset)        
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py, at line 63
# I propose to replace exp by my_world in :             vis_scene.place (my_sphere,exp) 
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py, at line 64
# I propose to replace exp by my_world in :             vis_scene.place (inner_sphere,exp) 
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py, at line 66
# I propose to replace exp by my_world in :     exp.add_scene(vis_scene)
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py, at line 67
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "67s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py, at line 29
# I propose to replace exp by my_world in : # an experiment (either seated or standing)
sed -i.bak "29s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py, at line 38
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py, at line 41
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global ( translation = np.array ([-2, height_of_head_viewpoint, 2]))    
sed -i.bak "41s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py, at line 47
# I propose to replace exp by my_world in :     vis_scene.place (my_cube,exp)  
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py, at line 55
# I propose to replace exp by my_world in :             my_cube.rotate_to_look_at(exp.idOriginCurrentCS)
sed -i.bak "55s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py, at line 56
# I propose to replace exp by my_world in :             vis_scene.place (my_cube,exp)
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py, at line 58
# I propose to replace exp by my_world in :     exp.add_scene(vis_scene)
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py, at line 59
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "59s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py, at line 19
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (
sed -i.bak "19s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py, at line 39
# I propose to replace exp by my_world in : # an experiment (either seated or standing)
sed -i.bak "39s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py, at line 48
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)    
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py, at line 49
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py, at line 56
# I propose to replace exp by my_world in :     vis_scene.place (my_cube,exp)  
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py, at line 63
# I propose to replace exp by my_world in :             vis_scene.place (my_cube,exp)
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py, at line 65
# I propose to replace exp by my_world in :     exp.add_scene (vis_scene)
sed -i.bak "65s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py, at line 66
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 9
# I propose to replace exp by my_world in : NOte
sed -i.bak "9s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 14
# I propose to replace exp by my_world in : import PTVR.Visual as visual # Used to create the experiment
sed -i.bak "14s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 15
# I propose to replace exp by my_world in : import PTVR.SystemUtils # Used to launch the experiment
sed -i.bak "15s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 31
# I propose to replace exp by my_world in : exp = visual.The3DWorld()
sed -i.bak "31s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 67
# I propose to replace exp by my_world in :     block_launch_scene.place (block_text,exp) 
sed -i.bak "67s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 73
# I propose to replace exp by my_world in :     exp.add_scene ( block_launch_scene) 
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 86
# I propose to replace exp by my_world in :     trial_scene.place (block_text,exp)
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 92
# I propose to replace exp by my_world in :     exp.add_scene (trial_scene)
sed -i.bak "92s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 108
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file    
sed -i.bak "108s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py, at line 9
# I propose to replace exp by my_world in : or you can get this path in the code with
sed -i.bak "9s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py, at line 28
# I propose to replace exp by my_world in :     exp = visual.The3DWorld ( name_of_subject = "toto", detailsThe3DWorld= "you can describe your experiment in this string" )
sed -i.bak "28s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py, at line 46
# I propose to replace exp by my_world in :             trial_scene.place ( trial_text,exp)
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py, at line 48
# I propose to replace exp by my_world in :             exp.add_scene ( trial_scene )
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py, at line 50
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "50s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py, at line 43
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username, detailsThe3DWorld = text_description,resultsPath = my_results_path )
sed -i.bak "43s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py, at line 44
# I propose to replace exp by my_world in :     my_actual_results_path = exp.GetResultsPath()
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py, at line 52
# I propose to replace exp by my_world in :     exp.add_scene(my_scene) # must be after the lines of code with my_scene.place (object)
sed -i.bak "52s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py, at line 54
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py, at line 68
# I propose to replace exp by my_world in : exp = visual.The3DWorld (resultsPath = my_results_path)
sed -i.bak "68s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 29
# I propose to replace exp by my_world in : Please see detailed explanations at the end of the present script
sed -i.bak "29s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 39
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username, detailsThe3DWorld=text_description)
sed -i.bak "39s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 40
# I propose to replace exp by my_world in :     my_actual_results_path = exp.GetResultsPath()
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 53
# I propose to replace exp by my_world in :     exp.add_scene(my_scene) # must be after the lines of code with my_scene.place (object)
sed -i.bak "53s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 55
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "55s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 69
# I propose to replace exp by my_world in : exp = visual.The3DWorld ()
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py, at line 10
# I propose to replace exp by my_world in : or you can get this path in the code with
sed -i.bak "10s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py, at line 27
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (  name_of_subject = "toto", 
sed -i.bak "27s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py, at line 28
# I propose to replace exp by my_world in :                                         detailsThe3DWorld= "you can describe your experiment in this string" )
sed -i.bak "28s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py, at line 45
# I propose to replace exp by my_world in :         trial_scene.place ( trial_text,exp )
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py, at line 47
# I propose to replace exp by my_world in :         exp.add_scene ( trial_scene )
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py, at line 49
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py, at line 3
# I propose to replace exp by my_world in : ...\Demos\saving_results\3_simplest_yes_no_experiment.py
sed -i.bak "3s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py, at line 30
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = "toto" )
sed -i.bak "30s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py, at line 48
# I propose to replace exp by my_world in :         trial_scene.place ( trial_text,exp)
sed -i.bak "48s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py, at line 54
# I propose to replace exp by my_world in :         exp.add_scene ( trial_scene )
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py, at line 56
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 40
# I propose to replace exp by my_world in : # created in main () with  exp.translate_coordinate_system_along_global (viewpoint_position)
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 42
# I propose to replace exp by my_world in : # created in main () with  exp.translate_coordinate_system_along_global (viewpoint_position)
sed -i.bak "42s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 91
# I propose to replace exp by my_world in :     myScene.place(myTangentScreen,exp)       
sed -i.bak "91s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 92
# I propose to replace exp by my_world in :     myScene.place(fixationPoint,exp)     
sed -i.bak "92s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 93
# I propose to replace exp by my_world in :     myScene.place(myObject,exp)
sed -i.bak "93s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 95
# I propose to replace exp by my_world in :     exp.add_scene(myScene)
sed -i.bak "95s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 98
# I propose to replace exp by my_world in : exp = visual.The3DWorld(name_of_subject = username)
sed -i.bak "98s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 101
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (viewpoint_position)
sed -i.bak "101s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 102
# I propose to replace exp by my_world in :     #exp.rotate_coordinate_system_about_global_y (90)
sed -i.bak "102s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 111
# I propose to replace exp by my_world in :     myScene.SetNextScene(exp.id_scene[0])
sed -i.bak "111s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 113
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "113s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 17
# I propose to replace exp by my_world in :        exp.rotate_coordinate_system_about_current_x ( rotation_deg = ... )
sed -i.bak "17s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 19
# I propose to replace exp by my_world in :         exp.rotate_coordinate_system_about_current_y () or
sed -i.bak "19s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 20
# I propose to replace exp by my_world in :         exp.rotate_coordinate_system_about_current_z() )
sed -i.bak "20s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 49
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject = username)
sed -i.bak "49s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 54
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global ( translation = viewpoint_position)
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 55
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_global_x  ( rotation_deg = 0 )
sed -i.bak "55s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 56
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_global_y  ( rotation_deg = 0 )
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 57
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_global_z  ( rotation_deg = 45 )    
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 83
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_white,exp)
sed -i.bak "83s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 84
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_yellow,exp)
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 85
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_orange,exp)
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 86
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_green,exp)
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 87
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_blue,exp)
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 89
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "89s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 90
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "90s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 44
# I propose to replace exp by my_world in : def place_objects_w_cartesian_CS (exp,my_scene, my_screen)
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 50
# I propose to replace exp by my_world in :         my_scene.place (my_object,exp)
sed -i.bak "50s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 54
# I propose to replace exp by my_world in : def place_objects_w_perimetric_CS (exp,my_scene, my_tangent_screen, my_ecc)
sed -i.bak "54s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 63
# I propose to replace exp by my_world in :         my_scene.place (my_object,exp)
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 67
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject = username)
sed -i.bak "67s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 70
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (head_viewpoint_position)
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 71
# I propose to replace exp by my_world in :     #exp.rotate_coordinate_system_about_global_y (90)
sed -i.bak "71s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 104
# I propose to replace exp by my_world in :     my_scene.place (my_viewpoint,exp)
sed -i.bak "104s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 106
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_white,exp)  
sed -i.bak "106s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 107
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_yellow,exp)
sed -i.bak "107s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 108
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_orange,exp)
sed -i.bak "108s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 109
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_green,exp)
sed -i.bak "109s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 110
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_blue,exp)
sed -i.bak "110s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 113
# I propose to replace exp by my_world in :     place_objects_w_cartesian_CS (exp = exp, my_scene = my_scene, my_screen = my_tangent_screen_white)
sed -i.bak "113s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 114
# I propose to replace exp by my_world in :     place_objects_w_cartesian_CS (exp = exp, my_scene = my_scene, my_screen = my_tangent_screen_yellow)
sed -i.bak "114s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 115
# I propose to replace exp by my_world in :     place_objects_w_cartesian_CS (exp = exp, my_scene = my_scene, my_screen = my_tangent_screen_orange)
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 116
# I propose to replace exp by my_world in :     place_objects_w_cartesian_CS (exp = exp, my_scene = my_scene, my_screen = my_tangent_screen_green)
sed -i.bak "116s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 117
# I propose to replace exp by my_world in :     place_objects_w_cartesian_CS (exp = exp, my_scene = my_scene, my_screen = my_tangent_screen_blue)
sed -i.bak "117s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 120
# I propose to replace exp by my_world in :     place_objects_w_perimetric_CS (exp = exp, my_scene = my_scene, my_tangent_screen = my_tangent_screen_white, my_ecc = 9.0)    
sed -i.bak "120s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 121
# I propose to replace exp by my_world in :     place_objects_w_perimetric_CS (exp = exp, my_scene = my_scene, my_tangent_screen = my_tangent_screen_yellow, my_ecc = 9.0)
sed -i.bak "121s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 122
# I propose to replace exp by my_world in :     place_objects_w_perimetric_CS (exp = exp, my_scene = my_scene, my_tangent_screen = my_tangent_screen_orange, my_ecc = 9.0)
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 123
# I propose to replace exp by my_world in :     place_objects_w_perimetric_CS (exp = exp, my_scene = my_scene, my_tangent_screen = my_tangent_screen_green, my_ecc = 9.0)
sed -i.bak "123s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 124
# I propose to replace exp by my_world in :     place_objects_w_perimetric_CS (exp = exp, my_scene = my_scene, my_tangent_screen = my_tangent_screen_blue, my_ecc = 9.0)
sed -i.bak "124s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 128
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "128s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 129
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "129s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.2_create_one_rotated_flat_screen.py, at line 27
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject = username)
sed -i.bak "27s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.2_create_one_rotated_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.2_create_one_rotated_flat_screen.py, at line 40
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_white,exp)
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.2_create_one_rotated_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.2_create_one_rotated_flat_screen.py, at line 42
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "42s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.2_create_one_rotated_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.2_create_one_rotated_flat_screen.py, at line 43
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "43s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.2_create_one_rotated_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 18
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global ( translation = viewpoint_position)
sed -i.bak "18s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 50
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject = username)
sed -i.bak "50s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 55
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global ( translation = viewpoint_position) 
sed -i.bak "55s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 56
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global ( translation =  np.array ( [-1, 0, 0] ) )  
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 84
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_white,exp)
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 85
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_yellow,exp)
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 86
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_orange,exp)
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 87
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_green,exp)
sed -i.bak "87s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 88
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_blue,exp)
sed -i.bak "88s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 90
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "90s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 91
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "91s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 44
# I propose to replace exp by my_world in : exp = visual.The3DWorld(name_of_subject = username)
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 56
# I propose to replace exp by my_world in :         my_scene.place (my_object,exp)
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 68
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (viewpoint_position)
sed -i.bak "68s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 69
# I propose to replace exp by my_world in :     #exp.rotate_coordinate_system_about_global_y (90)
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 70
# I propose to replace exp by my_world in :     #exp.rotate_coordinate_system_about_current_y (90)    
sed -i.bak "70s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 100
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_white,exp)  
sed -i.bak "100s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 101
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_yellow,exp)
sed -i.bak "101s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 102
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_orange,exp)
sed -i.bak "102s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 103
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_green,exp)
sed -i.bak "103s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 104
# I propose to replace exp by my_world in :     my_scene.place (my_flat_screen_blue,exp)
sed -i.bak "104s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 115
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 116
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "116s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.2_impossible_to_rotate_tangent_screen.py, at line 28
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "28s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.2_impossible_to_rotate_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.2_impossible_to_rotate_tangent_screen.py, at line 42
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_white,exp)
sed -i.bak "42s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.2_impossible_to_rotate_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.2_impossible_to_rotate_tangent_screen.py, at line 44
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.2_impossible_to_rotate_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.2_impossible_to_rotate_tangent_screen.py, at line 45
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.2_impossible_to_rotate_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 24
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global ( translation = viewpoint_position)
sed -i.bak "24s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 56
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject = username)
sed -i.bak "56s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 61
# I propose to replace exp by my_world in :     #exp.translate_coordinate_system_along_global ( translation = viewpoint_position ) 
sed -i.bak "61s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 62
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global ( translation =  np.array ( [1, 1.65, 0] ) )  
sed -i.bak "62s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 91
# I propose to replace exp by my_world in :     my_scene.place (my_origin,exp)
sed -i.bak "91s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 92
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_white,exp)
sed -i.bak "92s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 93
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_yellow,exp)
sed -i.bak "93s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 94
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_orange,exp)
sed -i.bak "94s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 95
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_green,exp)
sed -i.bak "95s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 96
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_blue,exp)
sed -i.bak "96s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 98
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "98s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 99
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "99s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 23
# I propose to replace exp by my_world in :        exp.rotate_coordinate_system_about_current_x ( rotation_deg = ... )
sed -i.bak "23s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 25
# I propose to replace exp by my_world in :         exp.rotate_coordinate_system_about_current_y () or
sed -i.bak "25s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 26
# I propose to replace exp by my_world in :         exp.rotate_coordinate_system_about_current_z() )
sed -i.bak "26s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 55
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject = username)
sed -i.bak "55s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 60
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global ( translation = viewpoint_position)
sed -i.bak "60s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 61
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global ( translation = np.array([-2, 0, 0] ) )
sed -i.bak "61s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 63
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_global_x  ( rotation_deg = 0 )
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 64
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_global_y  ( rotation_deg = -45 )
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 65
# I propose to replace exp by my_world in :     exp.rotate_coordinate_system_about_global_z  ( rotation_deg = 0 )    
sed -i.bak "65s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 93
# I propose to replace exp by my_world in :     my_scene.place (my_origin,exp)
sed -i.bak "93s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 94
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_white,exp)
sed -i.bak "94s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 95
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_yellow,exp)
sed -i.bak "95s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 96
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_orange,exp)
sed -i.bak "96s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 97
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_green,exp)
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 98
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen_blue,exp)
sed -i.bak "98s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 100
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "100s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 101
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "101s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 90
# I propose to replace exp by my_world in : # the simplest experiment and scene
sed -i.bak "90s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 91
# I propose to replace exp by my_world in : exp = visual.The3DWorld ()
sed -i.bak "91s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 119
# I propose to replace exp by my_world in : def scene_with_target (exp, ecc, hm, isTakePicture, scene_rank)
sed -i.bak "119s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 127
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen,exp)
sed -i.bak "127s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 128
# I propose to replace exp by my_world in :     my_scene.place(my_target,exp)
sed -i.bak "128s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 192
# I propose to replace exp by my_world in :     my_scene.fill_in_results_file_column(column_name="distance_exp", value=distance_xp)
sed -i.bak "192s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 196
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "196s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 199
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "199s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 200
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (translation = np.array([0.0, height_of_static_pov, 0.0]))
sed -i.bak "200s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 208
# I propose to replace exp by my_world in :         scene_with_target (exp = exp, ecc=0, hm=0, isTakePicture = False, scene_rank = count)
sed -i.bak "208s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 211
# I propose to replace exp by my_world in :         scene_with_target (exp = exp, ecc = eccentricity, hm = hm_step * count, isTakePicture = False, scene_rank = count )
sed -i.bak "211s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 214
# I propose to replace exp by my_world in :     scene_with_target (exp=exp, ecc=0, hm=0, isTakePicture = True, scene_rank = count + 1)
sed -i.bak "214s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 215
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "215s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 88
# I propose to replace exp by my_world in : # the simplest experiment and scene
sed -i.bak "88s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 89
# I propose to replace exp by my_world in : exp = visual.The3DWorld ()
sed -i.bak "89s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 118
# I propose to replace exp by my_world in : def scene_with_target (exp, ecc, hm, isTakePicture, scene_rank)
sed -i.bak "118s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 119
# I propose to replace exp by my_world in :     if scene_rank == 1 
sed -i.bak "119s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 129
# I propose to replace exp by my_world in :     my_scene.place (my_tangent_screen,exp)
sed -i.bak "129s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 130
# I propose to replace exp by my_world in :     my_scene.place(my_target,exp)
sed -i.bak "130s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 197
# I propose to replace exp by my_world in :     my_scene.fill_in_results_file_column(column_name="distance_exp", value=distance_xp)
sed -i.bak "197s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 201
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "201s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 204
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "204s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 205
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (translation = np.array([0.0, height_of_static_pov, 0.0]))
sed -i.bak "205s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 213
# I propose to replace exp by my_world in :         scene_with_target (exp = exp, ecc=0, hm=0, isTakePicture = False, scene_rank = count)
sed -i.bak "213s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 215
# I propose to replace exp by my_world in :         scene_with_target (exp = exp, ecc = eccentricity, hm = hm_step * count, isTakePicture = False, scene_rank = count )
sed -i.bak "215s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 218
# I propose to replace exp by my_world in :     scene_with_target (exp=exp, ecc=0, hm=0, isTakePicture = True, scene_rank = count + 1)
sed -i.bak "218s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 219
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "219s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 73
# I propose to replace exp by my_world in : # the simplest experiment and scene
sed -i.bak "73s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 74
# I propose to replace exp by my_world in : exp = visual.The3DWorld ()
sed -i.bak "74s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 102
# I propose to replace exp by my_world in : def scene_with_target (exp, ecc, hm, isTakePicture, scene_rank)
sed -i.bak "102s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 174
# I propose to replace exp by my_world in :     my_scene.fill_in_results_file_column(column_name="distance_exp", value=distance_xp)
sed -i.bak "174s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 178
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "178s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 181
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username)
sed -i.bak "181s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 182
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (translation = np.array([0.0, height_of_static_pov, 0.0]))
sed -i.bak "182s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 190
# I propose to replace exp by my_world in :         scene_with_target (exp = exp, ecc=0, hm=0, isTakePicture = False, scene_rank = count)
sed -i.bak "190s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 193
# I propose to replace exp by my_world in :         scene_with_target (exp = exp, ecc = eccentricity, hm = hm_step * count, isTakePicture = False, scene_rank = count )
sed -i.bak "193s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 196
# I propose to replace exp by my_world in :     scene_with_target (exp=exp, ecc=0, hm=0, isTakePicture = True, scene_rank = count + 1)
sed -i.bak "196s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 197
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "197s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py, at line 25
# I propose to replace exp by my_world in : # an experiment (either seated or standing)
sed -i.bak "25s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py, at line 37
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject=username)
sed -i.bak "37s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py, at line 38
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (translation = head_viewpoint_position)
sed -i.bak "38s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py, at line 41
# I propose to replace exp by my_world in :     exp.add_scene(scene)
sed -i.bak "41s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py, at line 75
# I propose to replace exp by my_world in :         scene.place(myTangentScreen,exp)       
sed -i.bak "75s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py, at line 81
# I propose to replace exp by my_world in :         scene.place(text_object,exp)
sed -i.bak "81s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py, at line 84
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "84s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 50
# I propose to replace exp by my_world in : # an experiment (either seated or standing)
sed -i.bak "50s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 63
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject = username)
sed -i.bak "63s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 64
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global (translation = head_viewpoint_position)
sed -i.bak "64s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 65
# I propose to replace exp by my_world in :     current_cs_pos_x,current_cs_pos_y,current_cs_pos_z, current_cs_axis_x,current_cs_axis_y,current_cs_axis_z = exp.get_coordinate_system()
sed -i.bak "65s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 69
# I propose to replace exp by my_world in :     myScene.place(my_origin,exp)
sed -i.bak "69s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 81
# I propose to replace exp by my_world in :     myScene.place (my_near_text_forward,exp)   
sed -i.bak "81s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 90
# I propose to replace exp by my_world in :     myScene.place (my_near_cube_forward,exp)
sed -i.bak "90s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 97
# I propose to replace exp by my_world in :     myScene.place (my_far_text_forward,exp) 
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 104
# I propose to replace exp by my_world in :     myScene.place (my_far_cube_forward,exp)
sed -i.bak "104s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 113
# I propose to replace exp by my_world in :     myScene.place (myLegendStraightAhead,exp)
sed -i.bak "113s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 123
# I propose to replace exp by my_world in :     myScene.place (myLegendAbove,exp)
sed -i.bak "123s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 138
# I propose to replace exp by my_world in :     my_near_text_left.rotate_to_look_at(exp.idOriginCurrentCS)
sed -i.bak "138s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 141
# I propose to replace exp by my_world in :     myScene.place (my_near_text_left,exp)   
sed -i.bak "141s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 150
# I propose to replace exp by my_world in :     my_near_cube_left.rotate_to_look_at(exp.idHeadset)
sed -i.bak "150s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 153
# I propose to replace exp by my_world in :     myScene.place (my_near_cube_left,exp)
sed -i.bak "153s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 164
# I propose to replace exp by my_world in :     my_far_text_left.rotate_to_look_at(exp.idOriginCurrentCS)# easier than next commented line
sed -i.bak "164s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 166
# I propose to replace exp by my_world in :     myScene.place (my_far_text_left,exp) 
sed -i.bak "166s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 177
# I propose to replace exp by my_world in :     my_far_cube_left.rotate_to_look_at(exp.idOriginCurrentCS)# easier than next commented line
sed -i.bak "177s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 179
# I propose to replace exp by my_world in :     myScene.place (my_far_cube_left,exp)   
sed -i.bak "179s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 183
# I propose to replace exp by my_world in :     myScene.place(sphere_test,exp )
sed -i.bak "183s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 191
# I propose to replace exp by my_world in :     my_legend_to_the_left.rotate_to_look_at_in_opposite_direction(exp.idOriginCurrentCS)# easier than next commented line
sed -i.bak "191s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 193
# I propose to replace exp by my_world in :     myScene.place (my_legend_to_the_left,exp)
sed -i.bak "193s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 212
# I propose to replace exp by my_world in :     myScene.place (my_near_text_right,exp)   
sed -i.bak "212s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 229
# I propose to replace exp by my_world in :     my_near_cube_right.rotate_to_look_at(exp.idHeadset)# easier than next commented line
sed -i.bak "229s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 231
# I propose to replace exp by my_world in :     myScene.place (my_near_cube_right,exp)
sed -i.bak "231s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 243
# I propose to replace exp by my_world in :     myScene.place (my_far_text_right,exp) 
sed -i.bak "243s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 251
# I propose to replace exp by my_world in :     myScene.place (my_far_cube_right,exp)   
sed -i.bak "251s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 262
# I propose to replace exp by my_world in :     myScene.place (my_legend_to_the_right,exp)
sed -i.bak "262s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 265
# I propose to replace exp by my_world in :     exp.add_scene (myScene)
sed -i.bak "265s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 266
# I propose to replace exp by my_world in :     exp.write() 
sed -i.bak "266s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/1_hello_world.py, at line 12
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject="Jane Doe")
sed -i.bak "12s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/1_hello_world.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/1_hello_world.py, at line 19
# I propose to replace exp by my_world in :     my_scene.place (my_text,exp)
sed -i.bak "19s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/1_hello_world.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/1_hello_world.py, at line 20
# I propose to replace exp by my_world in :     exp.add_scene (my_scene)
sed -i.bak "20s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/1_hello_world.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/1_hello_world.py, at line 22
# I propose to replace exp by my_world in :     exp.write () # writes to .json file
sed -i.bak "22s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/1_hello_world.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py, at line 40
# I propose to replace exp by my_world in :     exp = visual.The3DWorld ( name_of_subject = "toto" )
sed -i.bak "40s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py, at line 45
# I propose to replace exp by my_world in :     my_scene.place (my_text,exp)
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py, at line 78
# I propose to replace exp by my_world in :     my_scene.place (my_text_always_on,exp)
sed -i.bak "78s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py, at line 80
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py, at line 81
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "81s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py, at line 115
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject=username, detailsThe3DWorld=text_description )
sed -i.bak "115s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py, at line 116
# I propose to replace exp by my_world in :     exp.translate_coordinate_system_along_global(translation=np.array ([0, height_of_static_pov, 0]) )
sed -i.bak "116s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py, at line 123
# I propose to replace exp by my_world in :     my_scene.place (my_text,exp)
sed -i.bak "123s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py, at line 136
# I propose to replace exp by my_world in :     my_scene.place (sphere_target,exp)
sed -i.bak "136s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py, at line 139
# I propose to replace exp by my_world in :     my_scene.place ( target_point,exp )
sed -i.bak "139s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py, at line 175
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "175s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py, at line 176
# I propose to replace exp by my_world in :     exp.write()
sed -i.bak "176s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py, at line 50
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject = username, detailsThe3DWorld=text_description)
sed -i.bak "50s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py, at line 57
# I propose to replace exp by my_world in :     my_scene.place(my_text,exp)
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py, at line 97
# I propose to replace exp by my_world in :     my_scene.place (my_text_always_on,exp)
sed -i.bak "97s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py, at line 99
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "99s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py, at line 100
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "100s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py, at line 18
# I propose to replace exp by my_world in : #the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "18s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py, at line 35
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (name_of_subject = username, detailsThe3DWorld=text_description)
sed -i.bak "35s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py, at line 44
# I propose to replace exp by my_world in :     my_scene.place (my_text_press,exp)
sed -i.bak "44s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py, at line 46
# I propose to replace exp by my_world in :     exp.add_scene (my_scene)
sed -i.bak "46s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py, at line 47
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "47s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py, at line 15
# I propose to replace exp by my_world in : when_start_timed indique quand le timed débute (début d'une scene , début de l'expérience , 
sed -i.bak "15s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py, at line 45
# I propose to replace exp by my_world in :     exp = visual.The3DWorld(name_of_subject = username, detailsThe3DWorld=text_description)
sed -i.bak "45s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py, at line 85
# I propose to replace exp by my_world in :     my_scene.place (my_target_disc,exp)
sed -i.bak "85s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py, at line 86
# I propose to replace exp by my_world in :     my_scene.place(my_target_point,exp)
sed -i.bak "86s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py, at line 88
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "88s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py, at line 90
# I propose to replace exp by my_world in :     exp.add_scene(my_scene_2)
sed -i.bak "90s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py, at line 91
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "91s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 57
# I propose to replace exp by my_world in : # the description of the experiment, what it does. It'll be integrated in the graphical interface.
sed -i.bak "57s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 58
# I propose to replace exp by my_world in : description_of_experiment = """
sed -i.bak "58s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 66
# I propose to replace exp by my_world in :     exp = visual.The3DWorld (  name_of_subject = "toto",
sed -i.bak "66s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 67
# I propose to replace exp by my_world in :                                         detailsThe3DWorld = description_of_experiment )
sed -i.bak "67s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 80
# I propose to replace exp by my_world in :     my_scene.place (text_that_cannot_be_changed,exp) 
sed -i.bak "80s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 93
# I propose to replace exp by my_world in :     my_scene.place (display_state_of_current_scene,exp)
sed -i.bak "93s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 122
# I propose to replace exp by my_world in :     my_scene.place (blue_text,exp)
sed -i.bak "122s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 149
# I propose to replace exp by my_world in :     exp.add_scene(my_scene)
sed -i.bak "149s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 150
# I propose to replace exp by my_world in :     exp.write() # Write the The3DWorld to .json file
sed -i.bak "150s/exp/my_world/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py
