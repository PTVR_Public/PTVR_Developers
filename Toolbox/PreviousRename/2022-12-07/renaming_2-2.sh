#!/bin/bash

# RENAMING COMMANDS
# GOAL: PTVR.Experiment -----> PTVR.Visual as visual
# DATE: 12/07/2022

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/renaming_2-1.sh, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : # GOAL
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/renaming_2-1.sh

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py, at line 123
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "123s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py, at line 42
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "42s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Posner/posner-diagram.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py, at line 30
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "30s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/Saroi/saroi.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py, at line 15
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "15s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/gaze_path_visualization.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py, at line 6
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "6s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py, at line 12
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "12s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/new_typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py, at line 19
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "19s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnreadformat_only_if_set_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py, at line 19
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "19s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_comparison.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 15
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "15s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py, at line 15
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "15s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py, at line 26
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "26s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/typography_x_height_angular_size.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py, at line 24
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "24s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_single_phrase.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 15
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "15s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py, at line 23
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "23s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_cardboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 16
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "16s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 10
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "10s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Placement_Current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/LineTool_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_on_parent.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_capoc_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_current_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_global.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/rotation_lookAt_and_lookAt_in_opposite_direction.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Different_translation_rotation_sequence.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_perimetric_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_local_cartesian.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projection_segment_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_current_CS_spherical.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_cartesian_on_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/projected_to_orientation_segment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/get_global_debug_with_parenting.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py, at line 17
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "17s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/array_of_eye_watching.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Rotation_cheatsheet_local.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/multiple_segment_on_screen_using_classical_segment_orientation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Global_to_Local_and_vice_versa.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Coordinate_systems/Position_cheatsheet_global_translate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_position.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_angle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timed_when.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py, at line 11
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "11s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_hand_controller.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_backend_simplifiedTimedScene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py, at line 11
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "11s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_pointed_at_obj.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_distance_from_point.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_timer_duration_solution.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/test_multiple_game.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/audio_file_tester.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/line_change_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Objects/axis_linetool.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_on_object_with_or_without_smoothing.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/custom_reticle_radius_checker.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_constant.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_with_scotome.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/ChangePointingCursorVisibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py, at line 13
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "13s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/reticle_generator_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/pointing_cursor_with_pointed_at_target_visible_invisible.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/selection_pointing_cursor_find_sphere.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/PointingCursor/head_gaze_cursor_pointing_with_recording_and_graph_doublon.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py, at line 15
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "15s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file_exp_gaze_output.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py, at line 25
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "25s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_recording.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py, at line 15
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "15s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/eye_tracking_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py, at line 25
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "25s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/debug_crash_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py, at line 23
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "23s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Eye_Tracking/head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Boilerplate/boilerplate.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Screen/Screen_result.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Json_Files/save_json_files_category.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Adaptative/prototype_1_adaptative.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_save_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py, at line 11
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "11s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_fill_in_results_file_column.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_text.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_background_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_take_picture.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_print_result_file_line.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_end_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_quit.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_visibility.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py, at line 2
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "2s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_enable.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_outline.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_restart_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_audio_settings.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py, at line 29
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "29s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py, at line 12
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "12s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/2_one_scene_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py, at line 12
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "12s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/1_one_scene_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py, at line 14
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "14s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/3_cycle_of_scenes_w_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py, at line 11
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "11s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Scenes/4_cycle_of_scenes_w_NON_simplified_duration.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py, at line 22
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "22s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/translation_vector_specified_in_GLOBAL_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py, at line 31
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "31s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/coordinate_system_drawing_utils.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py, at line 22
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "22s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/get_coordinates_of_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py, at line 17
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "17s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/1_global_cartesian_coordinate_system.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.1_create_object_in_cartesian_coord_+_object_rotation.py, at line 25
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "25s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.1_create_object_in_cartesian_coord_+_object_rotation.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.0_create_object_in_cartesian_coord.py, at line 22
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "22s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/1.0_create_object_in_cartesian_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/3_rotation_animated_about_one_axis.py, at line 25
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "25s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/3_rotation_animated_about_one_axis.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py, at line 18
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "18s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Objects/2_create_object_in_perimetric_coord.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py, at line 23
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "23s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_you.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py, at line 30
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "30s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Flat_Screens_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py, at line 19
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "19s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py, at line 17
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "17s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Eyes_Watching_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py, at line 18
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "18s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_array_of_cubes_watching_the_origin.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py, at line 29
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "29s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Arrays_of/spherical_Array_Of_Objects_Around_You.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py, at line 14
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment # Used to create the experiment
sed -i.bak "14s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/4_succession_of_blocks_w_trials.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py, at line 18
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "18s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/2_loop_of_trials_w_several_scenes_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py, at line 6
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "6s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/2_where_else_can_I_save_my_results_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py, at line 4
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "4s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/Saving_results/1_where_is_my_results_file_by_default.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py, at line 18
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "18s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/1_loop_of_trials_w_one_scene_per_trial.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py, at line 17
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "17s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Experiment_building/3_simplest_yes_no_experiment.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py, at line 24
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "24s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/3_moving_tangent_screen_with_moving_object_on_it.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py, at line 28
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "28s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.1_create_flat_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py, at line 24
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "24s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.3_place_objects_on_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.2_create_one_rotated_flat_screen.py, at line 15
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "15s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.2_create_one_rotated_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py, at line 28
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "28s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.0_create_flat_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py, at line 24
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "24s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/1.3_place_objects_on_flat_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.2_impossible_to_rotate_tangent_screen.py, at line 16
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "16s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.2_impossible_to_rotate_tangent_screen.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py, at line 34
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "34s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.0_create_tangent_screens_w_translation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py, at line 34
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "34s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Screens/2.1_create_tangent_screens_w_translation_and_rotation_of_CS.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py, at line 25
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "25s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/Asimov_head_gaze_cursor_pointing_with_record_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py, at line 23
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "23s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py, at line 25
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "25s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/eye_tracking/head_gaze_cursor_pointing_with_recording_and_graph_old_ec.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py, at line 11
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "11s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/Stroop_effect_with_tangent_screens.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 13
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "13s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/1_hello_world.py, at line 8
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "8s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Text/1_hello_world.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py, at line 21
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "21s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/1_keyboard_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py, at line 11
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "11s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/pointed_at_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py, at line 11
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "11s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/hand_controller_input.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py, at line 8
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "8s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/quit_PTVR.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py, at line 3
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "3s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/event_add_interaction_passed_to_event.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py, at line 40
# I propose to replace PTVR.Experiment by PTVR.Visual as visual in : import PTVR.Experiment
sed -i.bak "40s/PTVR.Experiment/PTVR.Visual as visual/" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Demos/Interactions_inputs_events/Events/pause_current_scene.py
