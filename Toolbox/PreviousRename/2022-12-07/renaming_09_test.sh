#!/bin/bash

# RENAMING COMMANDS
# Generated by createRenamingSuggestionList.py
# GOAL : import PTVR.Stimuli.World --> import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects
# DATE : 2022-12-07 14:38:10.837233


# In the file /Users/pkornp/Desktop/test_renaming/Debugs/Input/input_pointed_at.py, at line 15
# I propose to replace [import PTVR.Stimuli.World] by [import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects] in string ---> import PTVR.Stimuli.World as w
sed -i.bak "15s/import PTVR.Stimuli.World/import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects/g" /Users/pkornp/Desktop/test_renaming/Debugs/Input/input_pointed_at.py

# In the file /Users/pkornp/Desktop/test_renaming/Debugs/Input/input_pointed_at_obj.py, at line 15
# I propose to replace [import PTVR.Stimuli.World] by [import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects] in string ---> import PTVR.Stimuli.World as w
sed -i.bak "15s/import PTVR.Stimuli.World/import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects/g" /Users/pkornp/Desktop/test_renaming/Debugs/Input/input_pointed_at_obj.py

# In the file /Users/pkornp/Desktop/test_renaming/Debugs/Input/input_timer_duration_solution.pyOLD, at line 4
# I propose to replace [import PTVR.Stimuli.World] by [import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects] in string ---> import PTVR.Stimuli.World
sed -i.bak "4s/import PTVR.Stimuli.World/import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects/g" /Users/pkornp/Desktop/test_renaming/Debugs/Input/input_timer_duration_solution.pyOLD

# In the file /Users/pkornp/Desktop/test_renaming/Debugs/Input/input_timer_duration_solution.py, at line 4
# I propose to replace [import PTVR.Stimuli.World] by [import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects] in string ---> import PTVR.Stimuli.World
sed -i.bak "4s/import PTVR.Stimuli.World/import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects/g" /Users/pkornp/Desktop/test_renaming/Debugs/Input/input_timer_duration_solution.py

# In the file /Users/pkornp/Desktop/test_renaming/Debugs/Event/event_pause_current_scene.py, at line 32
# I propose to replace [import PTVR.Stimuli.World] by [import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects] in string ---> import PTVR.Stimuli.World
sed -i.bak "32s/import PTVR.Stimuli.World/import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects/g" /Users/pkornp/Desktop/test_renaming/Debugs/Event/event_pause_current_scene.py

# In the file /Users/pkornp/Desktop/test_renaming/Demos/Interactions_inputs_events/pointed_at_input.py, at line 15
# I propose to replace [import PTVR.Stimuli.World] by [import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects] in string ---> import PTVR.Stimuli.World as w
sed -i.bak "15s/import PTVR.Stimuli.World/import PTVR.Stimuli.Scenes\nimport PTVR.Stimuli.Objects\nimport PTVR.Stimuli.UIObjects/g" /Users/pkornp/Desktop/test_renaming/Demos/Interactions_inputs_events/pointed_at_input.py



# Cleaning up (removing .bak files)
find /Users/pkornp/Desktop/test_renaming -name "*.bak" -type f -delete
