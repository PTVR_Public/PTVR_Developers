#!/bin/bash

# RENAMING COMMANDS
# Generated by createRenamingSuggestionList.py
# GOAL : PTVR.Stimuli.World.TextUI --> PTVR.Stimuli.UIObjects.TextUI
# DATE : 2022-12-07 14:30:07.001250


# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 149
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     all_good_text = PTVR.Stimuli.World.TextUI(text="All good",font_name = font_name, fontsize_in_points = font_size_content)
sed -i.bak "149s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 159
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     all_wrong_text = PTVR.Stimuli.World.TextUI(text="All wrong",font_name = font_name,fontsize_in_points = font_size_content)
sed -i.bak "159s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 169
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     good_text = PTVR.Stimuli.World.TextUI(text=word,font_name = font_name,fontsize_in_points = font_size_content, align_vertical="Capline")
sed -i.bak "169s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 180
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     wrong_text = PTVR.Stimuli.World.TextUI(text=word,font_name = font_name,fontsize_in_points = font_size_content)
sed -i.bak "180s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 199
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     start_button_text = PTVR.Stimuli.World.TextUI(text="Start",font_name = font_name,fontsize_in_points=font_size_content)
sed -i.bak "199s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 210
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     quit_button_text = PTVR.Stimuli.World.TextUI(text="Quit",font_name = font_name,fontsize_in_points=font_size_content)
sed -i.bak "210s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 221
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     quit_button_inactive_text = PTVR.Stimuli.World.TextUI(text="Quit",font_name = font_name,fontsize_in_points=font_size_content)
sed -i.bak "221s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 235
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     ignore_button_text = PTVR.Stimuli.World.TextUI(text="Ignore Trial",font_name = font_name,fontsize_in_points=font_size_content)
sed -i.bak "235s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 247
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     ignore_button_inactive_text = PTVR.Stimuli.World.TextUI(text="Ignore Trial",font_name = font_name,fontsize_in_points=font_size_content)
sed -i.bak "247s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 261
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     finish_test_button_text = PTVR.Stimuli.World.TextUI(text="Finish Test",font_name = font_name,fontsize_in_points=font_size_content)
sed -i.bak "261s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 273
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     finish_test_button_inactive_text = PTVR.Stimuli.World.TextUI(text="Finish Test",font_name = font_name,fontsize_in_points=font_size_content)
sed -i.bak "273s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 286
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     yes_button_text = PTVR.Stimuli.World.TextUI(text="Yes",font_name = font_name,fontsize_in_points=font_size_content)
sed -i.bak "286s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 299
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     no_button_text = PTVR.Stimuli.World.TextUI(text="No",font_name = font_name,fontsize_in_points=font_size_content)
sed -i.bak "299s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 310
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     stop_button_text = PTVR.Stimuli.World.TextUI(text="Stop",font_name = font_name,fontsize_in_points=font_size_content)
sed -i.bak "310s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 322
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     continue_button_text = PTVR.Stimuli.World.TextUI(text="Continue",font_name = font_name,fontsize_in_points=font_size_content)
sed -i.bak "322s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 340
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     quit_button_description = PTVR.Stimuli.World.TextUI(text="Quit the experiment",
sed -i.bak "340s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 353
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     quit_button_description = PTVR.Stimuli.World.TextUI(text="End the experiment and go to the end screen",
sed -i.bak "353s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 366
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     ignore_button_description = PTVR.Stimuli.World.TextUI(text="Ignore the result of this trial",
sed -i.bak "366s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 379
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     start_button_description = PTVR.Stimuli.World.TextUI(text="Start the experiment",
sed -i.bak "379s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 392
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     stop_button_description = PTVR.Stimuli.World.TextUI(text="Stop the timer once the patient has finished reading",
sed -i.bak "392s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 405
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     continue_button_description = PTVR.Stimuli.World.TextUI(text="Launch the next sentence",
sed -i.bak "405s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 419
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     text_ignore_printed = PTVR.Stimuli.World.TextUI(text="Mark this trial as non-valid ? \n The current sentence will be indicated as ignored in the result file and the test will not be displayed on the graph",
sed -i.bak "419s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 430
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     same_size_description = PTVR.Stimuli.World.TextUI(text="Do you want to display the next sentence at the same size as the current sentence? \n Clicking yes will automatically launch the next sentence. ",
sed -i.bak "430s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 440
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     text_quit_printed = PTVR.Stimuli.World.TextUI(text="Do you want to quit the experiment ? \n The application will close and you will not have access to the end of experience statistics",
sed -i.bak "440s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 451
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     validate_end_quit_text = PTVR.Stimuli.World.TextUI(text="Do you want to close the application ?",
sed -i.bak "451s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 462
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     validate_finish_text = PTVR.Stimuli.World.TextUI(text="Do you want to end the experiment ? \n You will be redirected to the end screen",
sed -i.bak "462s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 474
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     informations_content_update = PTVR.Stimuli.World.TextUI(text="Subject : "+ parameters.subject_name + "\n" +
sed -i.bak "474s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 488
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     number_sentences = PTVR.Stimuli.World.TextUI(text=str(counter_sentences) +" / "+ str(parameters.number_of_sentences_to_use)
sed -i.bak "488s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 499
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     number_sentences_description = PTVR.Stimuli.World.TextUI(text=
sed -i.bak "499s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 514
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     logmar = PTVR.Stimuli.World.TextUI(text=str(logmar) + " logmar",
sed -i.bak "514s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 524
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     logmar_description = PTVR.Stimuli.World.TextUI(text= str(degrees) + " degrees",
sed -i.bak "524s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 538
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     timer = PTVR.Stimuli.World.TextUI(text= timer_calculator + " s.",
sed -i.bak "538s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 547
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     timer_description = PTVR.Stimuli.World.TextUI(text= "Reading time",
sed -i.bak "547s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 558
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     headset_screen_distance_content = PTVR.Stimuli.World.TextUI(text= distance_head_text + " m.",
sed -i.bak "558s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 568
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     headset_screen_distance_description = PTVR.Stimuli.World.TextUI(text="Headset-text distance",
sed -i.bak "568s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 579
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     distance_warning = PTVR.Stimuli.World.TextUI(text= "Distance not respected ",
sed -i.bak "579s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 590
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     text_end_experiment = PTVR.Stimuli.World.TextUI(text="End of the experiment",
sed -i.bak "590s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 54
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     showDot_button_text = PTVR.Stimuli.World.TextUI(text="Show dot")
sed -i.bak "54s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 63
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     nextScene_button_text = PTVR.Stimuli.World.TextUI(text="Next Scene")
sed -i.bak "63s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 114
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     stats = PTVR.Stimuli.World.TextUI(text="ACC : \n"+
sed -i.bak "114s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py, at line 126
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     result_button_text = PTVR.Stimuli.World.TextUI(text="Result")
sed -i.bak "126s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py, at line 58
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     button_text = PTVR.Stimuli.World.TextUI(text="Update")
sed -i.bak "58s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_graph.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 52
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     all_good.placeUIElement( PTVR.Stimuli.World.TextUI(text="All good"))
sed -i.bak "52s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 58
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     all_wrong.placeUIElement( PTVR.Stimuli.World.TextUI(text="All wrong"))
sed -i.bak "58s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 82
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     printButton.placeUIElement( PTVR.Stimuli.World.TextUI(text="PRINT"))
sed -i.bak "82s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 114
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->             UI_BUTTON_GOOD.placeUIElement( PTVR.Stimuli.World.TextUI(text=words[i]))
sed -i.bak "114s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py, at line 117
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->             UI_BUTTON_WRONG.placeUIElement( PTVR.Stimuli.World.TextUI(text=words[i]))
sed -i.bak "117s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Experiments/MNRead/MNREAD_Related/UI_false_words.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 76
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     all_good_text = PTVR.Stimuli.World.TextUI(text="All good",font_name = font_name, fontsize_in_points = font_size_content)
sed -i.bak "76s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 86
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     all_wrong_text = PTVR.Stimuli.World.TextUI(text="All wrong",font_name = font_name,fontsize_in_points = font_size_content)
sed -i.bak "86s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 96
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     good_text = PTVR.Stimuli.World.TextUI(text=word,font_name = font_name,fontsize_in_points = font_size_content, align_vertical="Capline")
sed -i.bak "96s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py, at line 107
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     wrong_text = PTVR.Stimuli.World.TextUI(text=word,font_name = font_name,fontsize_in_points = font_size_content)
sed -i.bak "107s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/UI/simple_mnread_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py, at line 57
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     stop_button_text = PTVR.Stimuli.World.TextUI(text="Stop timer and print result",fontsize_in_points=20)
sed -i.bak "57s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py, at line 64
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     timer = PTVR.Stimuli.World.TextUI(text= timer_calculator_current + " s.",
sed -i.bak "64s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Calculator/calculator_write_in_result_file.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py, at line 61
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     my_text_always_on = PTVR.Stimuli.World.TextUI (text=text_always_on,position_current_CS=np.array([0.0, 0.0, 0.3]),fontsize_in_points=25,textbox_width=700)
sed -i.bak "61s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_keyboard.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py, at line 48
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     my_text_ui = PTVR.Stimuli.World.TextUI(text="Off",position_current_CS=np.array([0.0, 0.0, 0.0]))
sed -i.bak "48s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py, at line 71
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     my_text_always_on = PTVR.Stimuli.World.TextUI (text=text_always_on,position_current_CS=np.array([0.0, 0.35, 0.3]),fontsize_in_points=25,textbox_width=700)
sed -i.bak "71s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Input/input_button_ui.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 71
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     myTextUiContinu = PTVR.Stimuli.World.TextUI(text="NextTrial",position_current_CS=np.array([-0.25, 0.0, 0.0]))
sed -i.bak "71s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py, at line 76
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     myTextUiCancel = PTVR.Stimuli.World.TextUI(text="CancelTrial",position_current_CS=np.array([0.25, 0.0, 0.0]))
sed -i.bak "76s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_1.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 74
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     myTextUiContinu = PTVR.Stimuli.World.TextUI(text="NextTrial",position_current_CS=np.array([-0.25, 0.0, 0.0]))
sed -i.bak "74s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py, at line 79
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     myTextUiCancel = PTVR.Stimuli.World.TextUI(text="CancelTrial",position_current_CS=np.array([0.25, 0.0, 0.0]))
sed -i.bak "79s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Cancel_Trial/cancel_trial_solution_3.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py, at line 88
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     button_info_text = PTVR.Stimuli.World.TextUI(text="Ajouter les data dans le fichier de résultat")
sed -i.bak "88s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py, at line 97
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     button_text = PTVR.Stimuli.World.TextUI(text="Trier le script")
sed -i.bak "97s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_execute_python_script.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py, at line 48
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     all_good_text = PTVR.Stimuli.World.TextUI(text="Save All")
sed -i.bak "48s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py, at line 59
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     le_button_text = PTVR.Stimuli.World.TextUI(text="Unsave Le")
sed -i.bak "59s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/debug_event_fill.py

# In the file /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py, at line 108
# I propose to replace [PTVR.Stimuli.World.TextUI] by [PTVR.Stimuli.UIObjects.TextUI] in string --->     my_object = PTVR.Stimuli.World.TextUI(text="test",position_current_CS=np.array([-0.25,0.0,0.0]),color=default_color_obj)
sed -i.bak "108s/PTVR.Stimuli.World.TextUI/PTVR.Stimuli.UIObjects.TextUI/g" /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts/Debugs/Event/event_change_object_color.py



# Cleaning up (removing .bak files)
find /Users/pkornp/Work/code/PTVR/PTVR_Researchers/Python_Scripts -name "*.bak" -type f -delete
