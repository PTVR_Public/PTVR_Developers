#!/bin/bash

# RENAMING COMMANDS
# GOAL: objectId -----> object_id
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectColor.py, at line 47
# I propose to replace objectId by object_id in :     my_event_activate = event.ChangeObjectColor(evtColor=color.RGBColor(0.0, 1.0, 0.0, 1),objectId= my_object.id,effect = "activate")
sed -i "47s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectColor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectColor.py, at line 51
# I propose to replace objectId by object_id in :     my_event_deactivate = event.ChangeObjectColor(objectId= my_object.id,effect = "deactivate")
sed -i "51s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectColor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectEnable.py, at line 48
# I propose to replace objectId by object_id in :     my_event_activate = event.ChangeObjectEnable(objectId=my_object.id,effect = "activate")
sed -i "48s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectEnable.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectEnable.py, at line 52
# I propose to replace objectId by object_id in :     my_event_deactivate = event.ChangeObjectEnable(objectId=my_object.id,effect = "deactivate")
sed -i "52s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectEnable.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputButtonUI.py, at line 63
# I propose to replace objectId by object_id in :     my_event_change_object_color_activate = event.ChangeObjectColor(evtColor=color.RGBColor(g=1.0), objectId=my_button.id, effect="activate")
sed -i "63s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputButtonUI.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputButtonUI.py, at line 64
# I propose to replace objectId by object_id in :     my_event_change_object_color_deactivate  = event.ChangeObjectColor(evtColor=color.RGBColor(g=1.0), objectId=my_button.id, effect="deactivate")
sed -i "64s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputButtonUI.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_false_words.py, at line 124
# I propose to replace objectId by object_id in :             green_visible = PTVR.Data.Event.ChangeObjectEnable(objectId=UI_BUTTON_GOOD.id, effect="activate")
sed -i "124s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_false_words.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_false_words.py, at line 125
# I propose to replace objectId by object_id in :             green_invisible = PTVR.Data.Event.ChangeObjectEnable(objectId=UI_BUTTON_GOOD.id, effect="deactivate")
sed -i "125s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_false_words.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_false_words.py, at line 127
# I propose to replace objectId by object_id in :             red_visible = PTVR.Data.Event.ChangeObjectEnable(objectId=UI_BUTTON_WRONG.id, effect="activate")
sed -i "127s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_false_words.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_false_words.py, at line 128
# I propose to replace objectId by object_id in :             red_invisible = PTVR.Data.Event.ChangeObjectEnable(objectId=UI_BUTTON_WRONG.id, effect="deactivate")
sed -i "128s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_false_words.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_graph.py, at line 55
# I propose to replace objectId by object_id in :     change = event.ChangeObjectEnable(objectId=graph.id, effect="deactivate")
sed -i "55s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_graph.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Objects/buttonUI.py, at line 71
# I propose to replace objectId by object_id in :     myEventChangeObjectColorActivate = event.ChangeObjectColor(evtColor=color.RGBColor(g=1.0), objectId=my_button_press.id, effect="activate")
sed -i "71s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/Objects/buttonUI.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Objects/buttonUI.py, at line 72
# I propose to replace objectId by object_id in :     myEventChangeObjectColorDeactivate  = event.ChangeObjectColor(evtColor=color.RGBColor(g=1.0), objectId=my_button_release.id, effect="deactivate")
sed -i "72s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Demos/Objects/buttonUI.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 780
# I propose to replace objectId by object_id in :     ChangeObjectColorEventDeactivate = PTVR.Data.Event.ChangeObjectColor(evtColor=dynColor, objectId=theSphere.id, effect="deactivate")
sed -i "780s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 790
# I propose to replace objectId by object_id in :     ChangeObjectColorEventActivate = PTVR.Data.Event.ChangeObjectColor(evtColor=dynColor, objectId=theSphere.id, effect="activate")
sed -i "790s/objectId/object_id/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py
