#!/bin/bash

# RENAMING COMMANDS
# GOAL: italic -----> is_italic
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectText.py, at line 38
# I propose to replace italic by is_italic in : text_activate_is_italic = False
sed -i "38s/italic/is_italic/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectText.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectText.py, at line 53
# I propose to replace italic by is_italic in :     my_event_activate = event.ChangeObjectText(text_object_id=my_text.id,text=text_activate,is_bold=text_activate_is_bold, is_italic=text_activate_is_italic, effect = "activate")
sed -i "53s/italic/is_italic/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectText.py
