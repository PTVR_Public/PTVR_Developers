#!/bin/bash

# RENAMING COMMANDS
# GOAL: SetOrigin -----> set_origin_of_room_calibration_coordinate_system_virtual_point
# DATE: 03/25/2022

# In the file ../PTVR_Researchers/doc/UserManual/5_coordinate_transformations/Suggested usage.md, at line 62
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :    exp.SetOrigin (staticPOV) # New Origin is now at static POV position. 
sed -i "62s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/doc/UserManual/5_coordinate_transformations/Suggested usage.md

# In the file ../PTVR_Researchers/doc/UserManual/8_when_is_the_viewpoint_concept_useful/2_static_viewpoint.md, at line 108
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :    exp.SetOrigin (staticPOV) # New Origin is now at static POV position. 
sed -i "108s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/doc/UserManual/8_when_is_the_viewpoint_concept_useful/2_static_viewpoint.md

# In the file ../PTVR_Researchers/doc/UserManual/Text/PositioningTextboxIn3D.md, at line 35
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in : - If you want your Textbox to face the origin, you can do it with **PTVR.Stimuli.Utils.VirtualPoint.isFacingOrigin**. Note that the origin is defined by default at position [0.0,0.0,0.0] in the 3D world, but that can be changed with **PTVR.Experiment.Experiment.SetOrigin**.
sed -i "35s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/doc/UserManual/Text/PositioningTextboxIn3D.md

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Flat_Screens/objects_placed_on_flat_screens.py, at line 67
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(myStaticPOV)
sed -i "67s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Flat_Screens/objects_placed_on_flat_screens.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/GraphUI/graph_UI_demo.py, at line 110
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(staticPOVposition )
sed -i "110s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/GraphUI/graph_UI_demo.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputPointedAt.py, at line 40
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in : # BUG 
sed -i "40s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputPointedAt.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputPointedAt.py, at line 50
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "50s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputPointedAt.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputPointedAtTarget.py, at line 41
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in : # BUG 
sed -i "41s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputPointedAtTarget.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputPointedAtTarget.py, at line 74
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "74s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputPointedAtTarget.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/mnread_cardboard.py, at line 42
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration)
sed -i "42s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/mnread_cardboard.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/mnread_vr_Clean.py, at line 151
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration)
sed -i "151s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/mnread_vr_Clean.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/mnread_vr_experiment.py, at line 145
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(staticPOVposition )
sed -i "145s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/mnread_vr_experiment.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/new_typography_comparison.py, at line 85
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration)
sed -i "85s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/new_typography_comparison.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/UI_false_words.py, at line 37
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # new origin is now at POVatCalibration 
sed -i "37s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/UI_false_words.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/UI_graph.py, at line 35
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration)
sed -i "35s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/UI_graph.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Plan_Tangent_et_Repère_Local.py, at line 60
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(staticPOVposition )
sed -i "60s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Plan_Tangent_et_Repère_Local.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_1_Pointing_Cursor_Constant.py, at line 85
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "85s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_1_Pointing_Cursor_Constant.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_2 _Pointing_Cusor _Calibration_based_on_collision.py, at line 87
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "87s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_2 _Pointing_Cusor _Calibration_based_on_collision.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_3_Poiting_Cursor_Verification_Head.py, at line 109
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "109s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_3_Poiting_Cursor_Verification_Head.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_4_ Pointing_Cursor_Verification_Gaze.py, at line 95
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "95s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_4_ Pointing_Cursor_Verification_Gaze.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_5_ Pointing_Cursor_Verification_HeadAndGaze.py, at line 117
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "117s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_5_ Pointing_Cursor_Verification_HeadAndGaze.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_6 _Pointing_Cusor _PointedTo.py, at line 88
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "88s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_6 _Pointing_Cusor _PointedTo.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursors_farcursor.py, at line 48
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "48s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursors_farcursor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/position_calculation.py, at line 40
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # New Origin is now the object "static POV"! 
sed -i "40s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/position_calculation.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Random_Array_Of_Objects_for_Visual_Search.py, at line 40
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV) # New Origin is now the object "static POV"! 
sed -i "40s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Random_Array_Of_Objects_for_Visual_Search.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Scene/CalibrationScene.py, at line 28
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :    #myScene.SetOrigin(myOrigin)
sed -i "28s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Scene/CalibrationScene.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Scene/cyclic_simplified_timing_scene.py, at line 29
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :    myScene.SetOrigin(myOrigin)
sed -i "29s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Scene/cyclic_simplified_timing_scene.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Scene/SetOriginMultiple.py, at line 28
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :    myScene.SetOrigin(myOrigin)
sed -i "28s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Scene/SetOriginMultiple.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Scene/SimplifiedTimedScene.py, at line 29
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :    myScene.SetOrigin(myOrigin)
sed -i "29s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Scene/SimplifiedTimedScene.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Spherical/Array_Of/spherical_Array_Of_Eyes_Watching_You.py, at line 35
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(staticPOV) # New Origin is now the object staticPOV ! 
sed -i "35s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Spherical/Array_Of/spherical_Array_Of_Eyes_Watching_You.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Spherical/Array_Of/spherical_Array_Of_Flat_Screens_Around_You.py, at line 53
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # New Origin is now the object POVatCalibration ! 
sed -i "53s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Spherical/Array_Of/spherical_Array_Of_Flat_Screens_Around_You.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Spherical/Array_Of/spherical_Array_Of_Objects_Around_You.py, at line 40
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "40s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Spherical/Array_Of/spherical_Array_Of_Objects_Around_You.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/moving_tangent_screen_with_moving_object_on_it.py, at line 59
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(myStaticPOV)
sed -i "59s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/moving_tangent_screen_with_moving_object_on_it.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/objects_placed_on_tangent_screens.py, at line 69
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (myStaticPOV)
sed -i "69s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/objects_placed_on_tangent_screens.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/spherical_Array_Of_Tangent_Screens_Around_You.py, at line 55
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (myStaticPOV) # New Origin is now  myStaticPOV ! 
sed -i "55s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/spherical_Array_Of_Tangent_Screens_Around_You.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_On_Tangent_Screen.py, at line 59
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(myOrigin)  
sed -i "59s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_On_Tangent_Screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_On_Tangent_Screen_x_height_centering.py, at line 60
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # grâce à ça, la nouvelle Origine est à la position POVatCalibration ! Super !
sed -i "60s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_On_Tangent_Screen_x_height_centering.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_placed_on_tangent_screens.py, at line 61
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (myStaticPOV)
sed -i "61s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_placed_on_tangent_screens.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_tangent.py, at line 41
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # new origin is now at POVatCalibration 
sed -i "41s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_tangent.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 51
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (myStaticPOV)
sed -i "51s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/Cone_test.py, at line 39
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(myStaticPOV)
sed -i "39s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/Cone_test.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/infinite_circular_cone_16deg_eccentricity_10deg.py, at line 120
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(myStaticPOV)
sed -i "120s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/infinite_circular_cone_16deg_eccentricity_10deg.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/infinite_circular_cone_ellipse_on_screen.py, at line 36
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in : exp.SetOrigin(myStaticPOV)
sed -i "36s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/infinite_circular_cone_ellipse_on_screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/text_showing_angle.py, at line 44
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(myOrigin)    
sed -i "44s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/text_showing_angle.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center.py, at line 38
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(myStaticPOV)
sed -i "38s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center_animTemps.py, at line 39
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(myStaticPOV)
sed -i "39s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center_animTemps.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_8deg_eccentricity_10deg.py, at line 39
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(myStaticPOV)
sed -i "39s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_8deg_eccentricity_10deg.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 599
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(origin)
sed -i "599s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 672
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     s0VisScene.SetOrigin(origin)
sed -i "672s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 765
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :         s1VisScene.SetOrigin(origin)
sed -i "765s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 797
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :         s2VisScene.SetOrigin(origin)
sed -i "797s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 812
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :         s3VisScene.SetOrigin(origin)
sed -i "812s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 832
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :         s3_bis_VisScene.SetOrigin(origin)
sed -i "832s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 850
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :         s4VisScene.SetOrigin(origin)
sed -i "850s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 871
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     s5VisScene.SetOrigin(origin)
sed -i "871s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/adaptative.py, at line 77
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(myStaticPOV)
sed -i "77s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/adaptative.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/static_POV.py, at line 6
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in : Goal
sed -i "6s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/static_POV.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/static_POV.py, at line 35
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin(myStaticPOV)
sed -i "35s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/static_POV.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/mnreadformat_only_if_SetXheightAngularSize.py, at line 49
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # new origin is now at POVatCalibration 
sed -i "49s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/Text/mnreadformat_only_if_SetXheightAngularSize.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py, at line 49
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # new origin is now at POVatCalibration 
sed -i "49s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_alignment.py, at line 90
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # new origin is now at POVatCalibration 
sed -i "90s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_alignment.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_alignment_ascender_descender.py, at line 90
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # new origin is now at POVatCalibration 
sed -i "90s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_alignment_ascender_descender.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_alignment_vertical_middle_different_fonts.py, at line 52
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # new origin is now at POVatCalibration 
sed -i "52s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_alignment_vertical_middle_different_fonts.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_demo.py, at line 40
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # new origin is now at POVatCalibration 
sed -i "40s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_demo.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_fontsize_in_points.py, at line 41
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # new origin is now at POVatCalibration 
sed -i "41s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_fontsize_in_points.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_On_Flat_Screen.py, at line 62
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # new origin is now at POVatCalibration 
sed -i "62s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_On_Flat_Screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_On_Flat_Screen_x_height_centering.py, at line 64
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # grâce à ça, la nouvelle Origine est à la position POVatCalibration ! Super !
sed -i "64s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_On_Flat_Screen_x_height_centering.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_orientation.py, at line 36
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (POVatCalibration) # new origin is now at POVatCalibration 
sed -i "36s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_orientation.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py, at line 38
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (myStaticPOV) # new origin is now at myStaticPOV 
sed -i "38s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_with_perimetric_coordinates.py, at line 51
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (myStaticPOV) # new origin is now at myStaticPOV 
sed -i "51s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_with_perimetric_coordinates.py
