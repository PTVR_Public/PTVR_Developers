#!/bin/bash

# RENAMING COMMANDS
# GOAL: TangentScreenIsVisible -----> tangent_screen_is_visible
# DATE: 04/28/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Scene/CalibrationScene.py, at line 27
# I propose to replace TangentScreenIsVisible by tangent_screen_is_visible in :    my_scene = PTVR.Stimuli.World.CalibrationScene (TangentScreenIsVisible=False)
sed -i "27s/TangentScreenIsVisible/tangent_screen_is_visible/" ../PTVR_Researchers/Python_Scripts/Demos/Scene/CalibrationScene.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 178
# I propose to replace TangentScreenIsVisible by tangent_screen_is_visible in : TangentScreenIsVisible = False
sed -i "178s/TangentScreenIsVisible/tangent_screen_is_visible/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 275
# I propose to replace TangentScreenIsVisible by tangent_screen_is_visible in :     PauseVisScene = PTVR.Stimuli.World.CalibrationScene(trial=0, has_controllers_visibility=False,TangentScreenIsVisible =TangentScreenIsVisible) 
sed -i "275s/TangentScreenIsVisible/tangent_screen_is_visible/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 539
# I propose to replace TangentScreenIsVisible by tangent_screen_is_visible in :     myTangentScreen = PTVR.Stimuli.World.FlatScreen(is_visible=TangentScreenIsVisible,size_in_meters=distance_xp/2)
sed -i "539s/TangentScreenIsVisible/tangent_screen_is_visible/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py
