#!/bin/bash

# RENAMING COMMANDS
# GOAL: controllers_visibility -----> has_controllers_visibility
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Flat_Screens/objects_placed_on_flat_screens.py, at line 72
# I propose to replace controllers_visibility by has_controllers_visibility in :             controllers_visibility=False)
sed -i "72s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Demos/Flat_Screens/objects_placed_on_flat_screens.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputControllerVR.py, at line 41
# I propose to replace controllers_visibility by has_controllers_visibility in :     my_scene = PTVR.Stimuli.World.VisualScene(controllers_visibility=True)
sed -i "41s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputControllerVR.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/spriteOrientation.py, at line 29
# I propose to replace controllers_visibility by has_controllers_visibility in :     #         display=input.TimedDisplay(ms=1000), background_color=bgColor, controllers_visibility=False)
sed -i "29s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Demos/spriteOrientation.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py, at line 68
# I propose to replace controllers_visibility by has_controllers_visibility in :     #         controllers_visibility=False)
sed -i "68s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py, at line 87
# I propose to replace controllers_visibility by has_controllers_visibility in :     #         controllers_visibility=False)
sed -i "87s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/text_showing_angle.py, at line 50
# I propose to replace controllers_visibility by has_controllers_visibility in :             controllers_visibility=False)
sed -i "50s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/text_showing_angle.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 275
# I propose to replace controllers_visibility by has_controllers_visibility in :     PauseVisScene = PTVR.Stimuli.World.CalibrationScene(trial=0, controllers_visibility=False,TangentScreenIsVisible =TangentScreenIsVisible) 
sed -i "275s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 673
# I propose to replace controllers_visibility by has_controllers_visibility in :         background_color=bgColor, controllers_visibility=False)
sed -i "673s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 766
# I propose to replace controllers_visibility by has_controllers_visibility in :                    background_color=bgColor, controllers_visibility=False)
sed -i "766s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 798
# I propose to replace controllers_visibility by has_controllers_visibility in :                                                     background_color=bgColor, controllers_visibility=False)
sed -i "798s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 813
# I propose to replace controllers_visibility by has_controllers_visibility in :                                                 background_color=bgColor, controllers_visibility=False)
sed -i "813s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 833
# I propose to replace controllers_visibility by has_controllers_visibility in :                                                 background_color=bgColor, controllers_visibility=False)
sed -i "833s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 851
# I propose to replace controllers_visibility by has_controllers_visibility in :         s4VisScene = PTVR.Stimuli.World.VisualScene(trial=num,background_color=bgColor,controllers_visibility=False)
sed -i "851s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 873
# I propose to replace controllers_visibility by has_controllers_visibility in :     s5VisScene = PTVR.Stimuli.World.VisualScene(trial=num, controllers_visibility=False,background_color=bgColor)
sed -i "873s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 915
# I propose to replace controllers_visibility by has_controllers_visibility in :     s0 = PTVR.Stimuli.World.VisualScene(experiment =exp,trial=trial_i, controllers_visibility=False, background_color=bgColor)    
sed -i "915s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 971
# I propose to replace controllers_visibility by has_controllers_visibility in :             controllers_visibility=False, background_color=bgColor)
sed -i "971s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 999
# I propose to replace controllers_visibility by has_controllers_visibility in :             #        background_color=bgColor, controllers_visibility=False)
sed -i "999s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 1001
# I propose to replace controllers_visibility by has_controllers_visibility in :                     sceneDurationInMs= timeout_ms, background_color=bgColor, controllers_visibility=False)
sed -i "1001s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py, at line 67
# I propose to replace controllers_visibility by has_controllers_visibility in :    #                                (valid_responses=["left_trigger", "right_trigger"])), controllers_visibility=False)
sed -i "67s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py, at line 108
# I propose to replace controllers_visibility by has_controllers_visibility in :                                display=input.UserOptionDisplay(events=input.KeyboardEvent(valid_responses=['y'])), controllers_visibility=False)
sed -i "108s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py, at line 131
# I propose to replace controllers_visibility by has_controllers_visibility in :                                    display = input.UserOptionDisplay(events=input.KeyboardEvent(valid_responses=['y'])), controllers_visibility=False)
sed -i "131s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/miminal_test_for_ec.py, at line 64
# I propose to replace controllers_visibility by has_controllers_visibility in :                                    (valid_responses=["left_trigger", "right_trigger"])), controllers_visibility=False)
sed -i "64s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Tests/miminal_test_for_ec.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/tangent_screen_hides_too_small_point.py, at line 71
# I propose to replace controllers_visibility by has_controllers_visibility in :             controllers_visibility=False)
sed -i "71s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Tests/tangent_screen_hides_too_small_point.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Test_Warning_XYangularSize.py, at line 30
# I propose to replace controllers_visibility by has_controllers_visibility in :             controllers_visibility=False)
sed -i "30s/controllers_visibility/has_controllers_visibility/" ../PTVR_Researchers/Python_Scripts/Tests/Test_Warning_XYangularSize.py
