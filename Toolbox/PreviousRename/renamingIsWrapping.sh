#!/bin/bash

# RENAMING COMMANDS
# GOAL: isWrapping -----> is_wrapping
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py, at line 65
# I propose to replace isWrapping by is_wrapping in :     text1.SetTextBoxParameters(textboxWidth_m = 1, textboxHeight_m=0.5, isWrapping= False)
sed -i "65s/isWrapping/is_wrapping/" ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py, at line 68
# I propose to replace isWrapping by is_wrapping in :     text2.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= False) 
sed -i "68s/isWrapping/is_wrapping/" ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py, at line 49
# I propose to replace isWrapping by is_wrapping in :     text1.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
sed -i "49s/isWrapping/is_wrapping/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py, at line 55
# I propose to replace isWrapping by is_wrapping in :     text2.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
sed -i "55s/isWrapping/is_wrapping/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py, at line 60
# I propose to replace isWrapping by is_wrapping in :     text3.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
sed -i "60s/isWrapping/is_wrapping/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py
