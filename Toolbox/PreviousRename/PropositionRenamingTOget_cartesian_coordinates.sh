#!/bin/bash

# RENAMING COMMANDS
# GOAL: GetCartesian -----> get_cartesian_coordinates
# DATE: 03/25/2022

# In the file ../PTVR_Researchers/doc/UserManual/3_placing_objects_w_3_coordinates/different_CS/different coordinate systems to draw the same figure.md, at line 55
# I propose to replace GetCartesian by get_cartesian_coordinates in :     a = mySphere.GetCartesian() # rename to "GetCartesianCoordinates ()
sed -i "55s/GetCartesian/get_cartesian_coordinates/" ../PTVR_Researchers/doc/UserManual/3_placing_objects_w_3_coordinates/different_CS/different coordinate systems to draw the same figure.md

# In the file ../PTVR_Researchers/PTVR/Stimuli/Utils.py, at line 165
# I propose to replace GetCartesian by get_cartesian_coordinates in :         return self.GetCartesian (system="leftHand_Zforward")
sed -i "165s/GetCartesian/get_cartesian_coordinates/" ../PTVR_Researchers/PTVR/Stimuli/Utils.py

# In the file ../PTVR_Researchers/PTVR/Stimuli/Utils.py, at line 167
# I propose to replace GetCartesian by get_cartesian_coordinates in :     def GetCartesian (self, system="leftHand_Zforward")
sed -i "167s/GetCartesian/get_cartesian_coordinates/" ../PTVR_Researchers/PTVR/Stimuli/Utils.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Objects/create_object.py, at line 35
# I propose to replace GetCartesian by get_cartesian_coordinates in :     a = myCube.GetCartesian() # rename to "GetCartesianCoordinates ()
sed -i "35s/GetCartesian/get_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/Objects/create_object.py
