#!/bin/bash

# RENAMING COMMANDS
# GOAL: textboxHeight_m -----> textbox_height
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py, at line 65
# I propose to replace textboxHeight_m by textbox_height in :     text1.SetTextBoxParameters(textboxWidth_m = 1, textboxHeight_m=0.5, isWrapping= False)
sed -i "65s/textboxHeight_m/textbox_height/" ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py, at line 68
# I propose to replace textboxHeight_m by textbox_height in :     text2.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= False) 
sed -i "68s/textboxHeight_m/textbox_height/" ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py, at line 49
# I propose to replace textboxHeight_m by textbox_height in :     text1.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
sed -i "49s/textboxHeight_m/textbox_height/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py, at line 55
# I propose to replace textboxHeight_m by textbox_height in :     text2.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
sed -i "55s/textboxHeight_m/textbox_height/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py, at line 60
# I propose to replace textboxHeight_m by textbox_height in :     text3.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
sed -i "60s/textboxHeight_m/textbox_height/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py
