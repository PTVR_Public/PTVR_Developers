#!/bin/bash

# RENAMING COMMANDS
# GOAL: SetOrigin -----> set_origin_of_room_calibration_coordinate_system_virtual_point
# DATE: 03/25/2022

# In the file ../PTVR_Researchers/doc/UserManual/5_coordinate_transformations/Suggested usage.md, at line 62
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :    exp.SetOrigin (staticPOV) # New Origin is now at static POV position. 
sed -i "62s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/doc/UserManual/5_coordinate_transformations/Suggested usage.md

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_2_Pointing_Cursor_Calibration_based_on_collision.py, at line 87
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "87s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_2_Pointing_Cursor_Calibration_based_on_collision.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_4_Pointing_Cursor_Verification_Gaze.py, at line 95
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "95s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_4_Pointing_Cursor_Verification_Gaze.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_5_Pointing_Cursor_Verification_HeadAndGaze.py, at line 117
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "117s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_5_Pointing_Cursor_Verification_HeadAndGaze.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_6_Pointing_Cusor_PointedTo.py, at line 88
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :     exp.SetOrigin (staticPOV )
sed -i "88s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Demo_6_Pointing_Cusor_PointedTo.py
