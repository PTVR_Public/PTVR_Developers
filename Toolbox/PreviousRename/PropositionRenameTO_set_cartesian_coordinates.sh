#!/bin/bash

# RENAMING COMMANDS
# GOAL: SetCartesian -----> set_cartesian_coordinates
# DATE: 03/24/2022

# In the file ../PTVR_Researchers/doc/UserManual/3 placing_objects_w_3_coordinates/in_3D_cartesian_coordinate_system.md, at line 36
# I propose to replace SetCartesian by set_cartesian_coordinates in : mySphere.SetCartesian (x = 0, y = 1.2, z = 50) </code>
sed -i "36s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/doc/UserManual/3 placing_objects_w_3_coordinates/in_3D_cartesian_coordinate_system.md

# In the file ../PTVR_Researchers/doc/UserManual/Text/PositioningTextboxIn3D.md, at line 13
# I propose to replace SetCartesian by set_cartesian_coordinates in : or with the method ``SetCartesian()``
sed -i "13s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/doc/UserManual/Text/PositioningTextboxIn3D.md

# In the file ../PTVR_Researchers/doc/UserManual/Text/PositioningTextboxIn3D.md, at line 17
# I propose to replace SetCartesian by set_cartesian_coordinates in : text_object.SetCartesian(x=0.0, y=0.0, z=3.0) 
sed -i "17s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/doc/UserManual/Text/PositioningTextboxIn3D.md

# In the file ../PTVR_Researchers/doc/UserManual/Text/PositioningTextboxIn3D.md, at line 60
# I propose to replace SetCartesian by set_cartesian_coordinates in : - PTVR.Stimuli.Utils.VirtualPoint.set_cartesian_coordinates()
sed -i "60s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/doc/UserManual/Text/PositioningTextboxIn3D.md

# In the file ../PTVR_Researchers/PTVR/Stimuli/Utils.py, at line 89
# I propose to replace SetCartesian by set_cartesian_coordinates in :         self.SetCartesian (position[0],position[1],position[2])
sed -i "89s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/PTVR/Stimuli/Utils.py

# In the file ../PTVR_Researchers/PTVR/Stimuli/Utils.py, at line 135
# I propose to replace SetCartesian by set_cartesian_coordinates in :         Log.SetWarning("SetXYZ is deprecated. Consider using SetCartesian instead.")
sed -i "135s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/PTVR/Stimuli/Utils.py

# In the file ../PTVR_Researchers/PTVR/Stimuli/Utils.py, at line 136
# I propose to replace SetCartesian by set_cartesian_coordinates in :         self.SetCartesian (x,y,z, system="leftHand_Zforward")
sed -i "136s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/PTVR/Stimuli/Utils.py

# In the file ../PTVR_Researchers/PTVR/Stimuli/Utils.py, at line 138
# I propose to replace SetCartesian by set_cartesian_coordinates in :     def SetCartesian (self, x=0.0,y=0.0,z=0.0, system="leftHand_Zforward", isNeedConvertionToOtherCS=True)
sed -i "138s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/PTVR/Stimuli/Utils.py

# In the file ../PTVR_Researchers/PTVR/Stimuli/Utils.py, at line 164
# I propose to replace SetCartesian by set_cartesian_coordinates in :         Log.SetWarning("GetXYZ is deprecated. Consider using SetCartesian instead.") #ansi escape character
sed -i "164s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/PTVR/Stimuli/Utils.py

# In the file ../PTVR_Researchers/PTVR/Stimuli/Utils.py, at line 210
# I propose to replace SetCartesian by set_cartesian_coordinates in :         self.SetCartesian(x, y, z, False) #Creates an error if eccentricity == 0. 
sed -i "210s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/PTVR/Stimuli/Utils.py

# In the file ../PTVR_Researchers/PTVR/Stimuli/Utils.py, at line 223
# I propose to replace SetCartesian by set_cartesian_coordinates in :         self.SetCartesian(x, y, z)
sed -i "223s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/PTVR/Stimuli/Utils.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/cartesian_coordinate_systems.py, at line 64
# I propose to replace SetCartesian by set_cartesian_coordinates in :     myCube.SetCartesian(x = 2*radius_sphere_in_meters, y = 0.0, z=radialDistance_m, system = "unity") 
sed -i "64s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/cartesian_coordinate_systems.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/cartesian_coordinate_systems.py, at line 71
# I propose to replace SetCartesian by set_cartesian_coordinates in :     myCylinder.SetCartesian(x = -0.2, y = 0.0, z = radialDistance_m)
sed -i "71s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/cartesian_coordinate_systems.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/cartesian_coordinate_systems.py, at line 84
# I propose to replace SetCartesian by set_cartesian_coordinates in :     myCube2.SetCartesian(x = 0.5, y = 0.5, z = 1.5, system = "direct")
sed -i "84s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/cartesian_coordinate_systems.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputPointedAt.py, at line 65
# I propose to replace SetCartesian by set_cartesian_coordinates in :     myText.SetCartesian ( x = 0, y = -1.1 * sphere_diameter/2, z = viewingDistance ) 
sed -i "65s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputPointedAt.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputPointedAtTarget.py, at line 65
# I propose to replace SetCartesian by set_cartesian_coordinates in :     myText.SetCartesian ( x = 0, y = -1.1 * sphere_diameter/2, z = viewingDistance ) 
sed -i "65s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputPointedAtTarget.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/UI_false_words.py, at line 49
# I propose to replace SetCartesian by set_cartesian_coordinates in :     virtualPoint.SetCartesian(x=-0.2,y=-1)
sed -i "49s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/UI_false_words.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/UI_false_words.py, at line 55
# I propose to replace SetCartesian by set_cartesian_coordinates in :     virtualPoint.SetCartesian(x=0.2,y=-1)
sed -i "55s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/UI_false_words.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/UI_false_words.py, at line 78
# I propose to replace SetCartesian by set_cartesian_coordinates in :     virtualPoint.SetCartesian(x=0,y=-1)
sed -i "78s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/UI_false_words.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/UI_false_words.py, at line 110
# I propose to replace SetCartesian by set_cartesian_coordinates in :             virtualPoint1.SetCartesian(x=horizontal_position,y=vertical_position)
sed -i "110s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/UI_false_words.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Objects/create_object.py, at line 32
# I propose to replace SetCartesian by set_cartesian_coordinates in :     # mySphere.SetCartesian (x = 0.0, y = 1.2, z = 50) # viewing distance is now 50 meters
sed -i "32s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/Objects/create_object.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursors_farcursor.py, at line 62
# I propose to replace SetCartesian by set_cartesian_coordinates in :     myTextr.SetCartesian ( x = 0, y = -1.2 * sphere_diameter/2, z = 1 ) 
sed -i "62s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursors_farcursor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursors_farcursor.py, at line 63
# I propose to replace SetCartesian by set_cartesian_coordinates in :     myTextg.SetCartesian ( x = 0, y = 1-1.2 * sphere_diameter/2, z = 3 ) 
sed -i "63s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursors_farcursor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursors_farcursor.py, at line 64
# I propose to replace SetCartesian by set_cartesian_coordinates in :     myTextb.SetCartesian ( x = 1, y = -1.2 * sphere_diameter/2, z = 7 ) 
sed -i "64s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursors_farcursor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Spherical/Array_Of/spherical_Array_Of_Eyes_Watching_You.py, at line 48
# I propose to replace SetCartesian by set_cartesian_coordinates in :             innerSphere.SetCartesian (x=0, y=0, z= 0.5)
sed -i "48s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Demos/Spherical/Array_Of/spherical_Array_Of_Eyes_Watching_You.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Textessai.py, at line 35
# I propose to replace SetCartesian by set_cartesian_coordinates in :    # text.SetCartesian(x=0, y=5, z=2)
sed -i "35s/SetCartesian/set_cartesian_coordinates/" ../PTVR_Researchers/Python_Scripts/Tests/Textessai.py
