#!/bin/bash

# RENAMING COMMANDS
# GOAL: isInvariantCulture -----> is_invariant_culture
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/mrs_cps_test.py, at line 87
# I propose to replace isInvariantCulture by is_invariant_culture in :     eventAddNewPoint = event.AddNewPoint( graphId=graphUI.id, isInvariantCulture=True)
sed -i "87s/isInvariantCulture/is_invariant_culture/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/mrs_cps_test.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 173
# I propose to replace isInvariantCulture by is_invariant_culture in :     eventShowGraphPoints = event.AddNewPoint(graphId = graphUI.id, isInvariantCulture = True)
sed -i "173s/isInvariantCulture/is_invariant_culture/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 177
# I propose to replace isInvariantCulture by is_invariant_culture in :     #eventAddMisreadWords = event.AddNewPoint(graphId = misread_words_graph.id,isInvariantCulture = True)
sed -i "177s/isInvariantCulture/is_invariant_culture/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py
