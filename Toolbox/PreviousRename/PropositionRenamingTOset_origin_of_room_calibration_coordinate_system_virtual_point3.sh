#!/bin/bash

# RENAMING COMMANDS
# GOAL: SetOrigin -----> set_origin_of_room_calibration_coordinate_system_virtual_point
# DATE: 03/25/2022

# In the file ../PTVR_Researchers/doc/UserManual/5_coordinate_transformations/Suggested_usage.md, at line 62
# I propose to replace SetOrigin by set_origin_of_room_calibration_coordinate_system_virtual_point in :    exp.SetOrigin (staticPOV) # New Origin is now at static POV position. 
sed -i "62s/SetOrigin/set_origin_of_room_calibration_coordinate_system_virtual_point/" ../PTVR_Researchers/doc/UserManual/5_coordinate_transformations/Suggested_usage.md
