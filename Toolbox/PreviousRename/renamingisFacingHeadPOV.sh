#!/bin/bash

# RENAMING COMMANDS
# GOAL: isFacingHeadPOV -----> is_facing_head_pov
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/new_typography_comparison.py, at line 42
# I propose to replace isFacingHeadPOV by is_facing_head_pov in :     legend.SetOrientation(isFacingHeadPOV = False, isFacingOrigin = True)
sed -i "42s/isFacingHeadPOV/is_facing_head_pov/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/new_typography_comparison.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Spherical/Array_Of/spherical_Array_Of_Eyes_Watching_You.py, at line 57
# I propose to replace isFacingHeadPOV by is_facing_head_pov in :             my_sphere.isFacingHeadPOV = True
sed -i "57s/isFacingHeadPOV/is_facing_head_pov/" ../PTVR_Researchers/Python_Scripts/Demos/Spherical/Array_Of/spherical_Array_Of_Eyes_Watching_You.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_orientation.py, at line 42
# I propose to replace isFacingHeadPOV by is_facing_head_pov in :     text1.SetOrientation(isFacingHeadPOV = False, isFacingOrigin = True)
sed -i "42s/isFacingHeadPOV/is_facing_head_pov/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_orientation.py
