#!/bin/bash

# RENAMING COMMANDS
# GOAL: newDurationSceneInMs -----> new_duration_scene_in_ms
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/rotations_global_local_animated.py, at line 114
# I propose to replace newDurationSceneInMs by new_duration_scene_in_ms in :     myScene.SetDuration(newDurationSceneInMs=time_of_scene)
sed -i "114s/newDurationSceneInMs/new_duration_scene_in_ms/" ../PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/rotations_global_local_animated.py
