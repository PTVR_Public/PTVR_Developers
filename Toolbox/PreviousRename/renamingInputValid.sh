#!/bin/bash

# RENAMING COMMANDS
# GOAL: validResponses -----> valid_responses
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Center_object.py, at line 24
# I propose to replace validResponses by valid_responses in :     myInput1 = input.Keyboard(validResponses=['y'])
sed -i "24s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Center_object.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Center_object.py, at line 35
# I propose to replace validResponses by valid_responses in :     myInput2 = input.Keyboard(validResponses=['y'])
sed -i "35s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Center_object.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeBackgroundColor.py, at line 44
# I propose to replace validResponses by valid_responses in :     my_input_true = input.ControllerVR(validResponses=['right_trigger'],mode="press")
sed -i "44s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeBackgroundColor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeBackgroundColor.py, at line 49
# I propose to replace validResponses by valid_responses in :     my_input_false = input.ControllerVR(validResponses=['right_trigger'],mode="release")
sed -i "49s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeBackgroundColor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectColor.py, at line 48
# I propose to replace validResponses by valid_responses in :     my_input_true = input.ControllerVR(validResponses=['right_trigger'],mode="press")
sed -i "48s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectColor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectColor.py, at line 52
# I propose to replace validResponses by valid_responses in :     my_input_false = input.ControllerVR(validResponses=['right_trigger'],mode="release")
sed -i "52s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectColor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectEnable.py, at line 49
# I propose to replace validResponses by valid_responses in :     my_input_true = input.ControllerVR(validResponses=['right_trigger'],mode="release")
sed -i "49s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectEnable.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectEnable.py, at line 53
# I propose to replace validResponses by valid_responses in :     my_input_false = input.ControllerVR(validResponses=['right_trigger'],mode="press")
sed -i "53s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectEnable.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectOutline.py, at line 56
# I propose to replace validResponses by valid_responses in :     my_input_true = input.ControllerVR (validResponses=['right_trigger'],mode="press")
sed -i "56s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectOutline.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectOutline.py, at line 62
# I propose to replace validResponses by valid_responses in :     my_input_false = input.ControllerVR (validResponses=['right_trigger'],mode="release")
sed -i "62s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectOutline.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectText.py, at line 54
# I propose to replace validResponses by valid_responses in :     my_input_true = input.ControllerVR(validResponses=['right_trigger'],mode="press")
sed -i "54s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectText.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectText.py, at line 59
# I propose to replace validResponses by valid_responses in :     my_input_false = input.ControllerVR(validResponses=['right_trigger'],mode="release")
sed -i "59s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectText.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectVisibility.py, at line 49
# I propose to replace validResponses by valid_responses in :     my_input_true = input.ControllerVR(validResponses=['right_trigger'],mode="release")
sed -i "49s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectVisibility.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectVisibility.py, at line 53
# I propose to replace validResponses by valid_responses in :     my_input_false = input.ControllerVR(validResponses=['right_trigger'],mode="press")
sed -i "53s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectVisibility.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventEndCurrentTrial.py, at line 47
# I propose to replace validResponses by valid_responses in :             my_input_next_scene = input.ControllerVR(validResponses=['right_trigger'])
sed -i "47s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventEndCurrentTrial.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventEndCurrentTrial.py, at line 55
# I propose to replace validResponses by valid_responses in :             my_input_next_trial = input.ControllerVR(validResponses=['left_trigger'])
sed -i "55s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventEndCurrentTrial.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventQuit.py, at line 37
# I propose to replace validResponses by valid_responses in :     my_input = input.Keyboard(validResponses=['space'])
sed -i "37s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventQuit.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventRestartCurrentScene.py, at line 37
# I propose to replace validResponses by valid_responses in :     my_input = input.Keyboard(validResponses=['space'])
sed -i "37s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventRestartCurrentScene.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventRestartCurrentTrial.py, at line 38
# I propose to replace validResponses by valid_responses in :     my_input = input.Keyboard(validResponses=['space'])
sed -i "38s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventRestartCurrentTrial.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSaveText.py, at line 48
# I propose to replace validResponses by valid_responses in :     my_input_true = input.Keyboard(validResponses=['y'],mode="press")
sed -i "48s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSaveText.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSaveText.py, at line 54
# I propose to replace validResponses by valid_responses in :     my_input_false = input.Keyboard(validResponses=['n'],mode="press")
sed -i "54s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSaveText.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSaveText.py, at line 58
# I propose to replace validResponses by valid_responses in :     my_input_true2 = input.Keyboard(validResponses=['space'],mode="press")
sed -i "58s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSaveText.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSaveText.py, at line 59
# I propose to replace validResponses by valid_responses in :     my_input_false2 = input.Keyboard(validResponses=['escape'],mode="press")
sed -i "59s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSaveText.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSuspendCurrentScene.py, at line 44
# I propose to replace validResponses by valid_responses in :     my_input_activate = input.Keyboard(validResponses=['space'],mode="press")
sed -i "44s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSuspendCurrentScene.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSuspendCurrentScene.py, at line 48
# I propose to replace validResponses by valid_responses in :     my_input_deactivate = input.Keyboard(validResponses=['space'],mode="release")
sed -i "48s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSuspendCurrentScene.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputControllerVR.py, at line 48
# I propose to replace validResponses by valid_responses in :     my_input_press = input.ControllerVR (validResponses = ['right_trigger'], mode="press")
sed -i "48s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputControllerVR.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputControllerVR.py, at line 49
# I propose to replace validResponses by valid_responses in :     my_input_release = input.ControllerVR (validResponses = ['right_trigger'], mode="release")
sed -i "49s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputControllerVR.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputKeyboard.py, at line 45
# I propose to replace validResponses by valid_responses in :     my_input_press = input.Keyboard(validResponses=['space'],mode="press")
sed -i "45s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputKeyboard.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputKeyboard.py, at line 46
# I propose to replace validResponses by valid_responses in :     my_input_release = input.Keyboard(validResponses=['space'],mode="release")
sed -i "46s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputKeyboard.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/mnread_vr_Clean.py, at line 143
# I propose to replace validResponses by valid_responses in :     myInputTest = input.Keyboard(validResponses=['y', 'n'])
sed -i "143s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/mnread_vr_Clean.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_Calibration_based_on_collision.py, at line 137
# I propose to replace validResponses by valid_responses in :         my_input_next_scene = input.ControllerVR(validResponses=['right_trigger'])
sed -i "137s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_Calibration_based_on_collision.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_Constant.py, at line 153
# I propose to replace validResponses by valid_responses in :         my_input_next_scene = input.ControllerVR(validResponses=['right_trigger'])
sed -i "153s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_Constant.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_Verification_Head_Gaze.py, at line 150
# I propose to replace validResponses by valid_responses in :     my_input_next_scene = input.ControllerVR(validResponses=['right_trigger'])
sed -i "150s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_Verification_Head_Gaze.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_with_PointedAt_target_visible_invisible.py, at line 154
# I propose to replace validResponses by valid_responses in :         my_input_next = input.ControllerVR(validResponses=['right_trigger'])
sed -i "154s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_with_PointedAt_target_visible_invisible.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Selection_Pointing_Cursor.py, at line 80
# I propose to replace validResponses by valid_responses in :         my_input_zoom_in = input.ControllerVR(validResponses=['right_trigger'])
sed -i "80s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Selection_Pointing_Cursor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Selection_Pointing_Cursor.py, at line 81
# I propose to replace validResponses by valid_responses in :         my_input_zoom_out = input.ControllerVR(validResponses=['left_trigger'])
sed -i "81s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Selection_Pointing_Cursor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/position_calculation.py, at line 42
# I propose to replace validResponses by valid_responses in :     visScene  = PTVR.Stimuli.World.VisualScene (display=input.UserOptionDisplay(events=input.Keyboard (validResponses=['y','n'])) )       
sed -i "42s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/position_calculation.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_NextTrial_Solution3.py, at line 32
# I propose to replace validResponses by valid_responses in :     myInputContinu = input.Keyboard(validResponses=['space'])
sed -i "32s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_NextTrial_Solution3.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_NextTrial_Solution3.py, at line 33
# I propose to replace validResponses by valid_responses in :     myInputCancel =  input.Keyboard(validResponses=['escape'])
sed -i "33s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_NextTrial_Solution3.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_NextTrial_Solution3.py, at line 45
# I propose to replace validResponses by valid_responses in :     myInputContinu = input.ControllerVR(validResponses=['right_trigger'])
sed -i "45s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_NextTrial_Solution3.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_NextTrial_Solution3.py, at line 46
# I propose to replace validResponses by valid_responses in :     myInputCancel =  input.ControllerVR(validResponses=['left_trigger'])
sed -i "46s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_NextTrial_Solution3.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_RestartSameTrial_Solution1.py, at line 36
# I propose to replace validResponses by valid_responses in :     myInputContinu = input.Keyboard(validResponses=['space'])
sed -i "36s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_RestartSameTrial_Solution1.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_RestartSameTrial_Solution1.py, at line 37
# I propose to replace validResponses by valid_responses in :     myInputCancel =  input.Keyboard(validResponses=['escape'])
sed -i "37s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_RestartSameTrial_Solution1.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_RestartSameTrial_Solution1.py, at line 49
# I propose to replace validResponses by valid_responses in :     myInputContinu = input.ControllerVR(validResponses=['right_trigger'])
sed -i "49s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_RestartSameTrial_Solution1.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_RestartSameTrial_Solution1.py, at line 50
# I propose to replace validResponses by valid_responses in :     myInputCancel =  input.ControllerVR(validResponses=['left_trigger'])
sed -i "50s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/CancelTrial_Logique_RestartSameTrial_Solution1.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/PrintResultCustom.py, at line 75
# I propose to replace validResponses by valid_responses in :     myInputTest = input.Keyboard(validResponses=['space'])
sed -i "75s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/PrintResultCustom.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/TrialDemo.py, at line 26
# I propose to replace validResponses by valid_responses in :             myInputSpace = input.Keyboard(validResponses=['space'])
sed -i "26s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/TrialDemo.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/special_point_static_POV.py, at line 42
# I propose to replace validResponses by valid_responses in :     myInput = input.Keyboard(validResponses=['space'], mode = "press", nameInput="titi")
sed -i "42s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/special_point_static_POV.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/special_point_static_POV.py, at line 93
# I propose to replace validResponses by valid_responses in :     myScene2  = PTVR.Stimuli.World.VisualScene (display=input.UserOptionDisplay(events=input.KeyboardEvent (validResponses=['space'])) )  
sed -i "93s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/special_point_static_POV.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py, at line 60
# I propose to replace validResponses by valid_responses in :     # fixscene = PTVR.Stimuli.World.FixationScene(text= "Have you calibrated the eyetracker? \n if yes, please orient your head to the cross\n and press trigger to launch trial",background_color=bgColor, display=input.UserOptionDisplay(events=input.HTCViveEvent(validResponses=["right_trigger", "left_trigger"])))
sed -i "60s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py, at line 66
# I propose to replace validResponses by valid_responses in :     #         #display=input.UserOptionDisplay(events=input.HTCViveEvent (validResponses=["left_trigger", "right_trigger"])), 
sed -i "66s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py, at line 67
# I propose to replace validResponses by valid_responses in :     #         display=input.UserOptionDisplay(events=input.KeyboardEvent (validResponses=['y','n'])),                              
sed -i "67s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py, at line 85
# I propose to replace validResponses by valid_responses in :     #         #display=input.UserOptionDisplay(events=input.HTCViveEvent (validResponses=["left_trigger", "right_trigger"])), 
sed -i "85s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py, at line 86
# I propose to replace validResponses by valid_responses in :     #         display=input.UserOptionDisplay(events=input.KeyboardEvent (validResponses=['y','n'])),                              
sed -i "86s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/visual_angle_of_on_screen_objects.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Test.py, at line 23
# I propose to replace validResponses by valid_responses in :     myInput= input.Keyboard(validResponses=['space'])
sed -i "23s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Test.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/text_showing_angle.py, at line 48
# I propose to replace validResponses by valid_responses in :             #display=input.UserOptionDisplay(events=input.HTCViveEvent (validResponses=["left_trigger", "right_trigger"])), 
sed -i "48s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/text_showing_angle.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/text_showing_angle.py, at line 49
# I propose to replace validResponses by valid_responses in :             display=input.UserOptionDisplay(events=input.KeyboardEvent (validResponses=['y','n'])),                              
sed -i "49s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/text_showing_angle.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_angle_check.py, at line 48
# I propose to replace validResponses by valid_responses in :     VisScene  = PTVR.Stimuli.World.VisualScene(id=1, display=input.UserOptionDisplay(events=input.KeyboardEvent (validResponses=['space'])) )
sed -i "48s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_angle_check.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center.py, at line 55
# I propose to replace validResponses by valid_responses in :     myInput = PTVR.Data.Input.Keyboard(validResponses=['space'])
sed -i "55s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center_animTemps.py, at line 56
# I propose to replace validResponses by valid_responses in :     #myInput = PTVR.Data.Input.Keyboard(validResponses=['space'])
sed -i "56s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center_animTemps.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_8deg_eccentricity_10deg.py, at line 59
# I propose to replace validResponses by valid_responses in :     myInput = PTVR.Data.Input.Keyboard(validResponses=['space'])
sed -i "59s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_8deg_eccentricity_10deg.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 181
# I propose to replace validResponses by valid_responses in :     myInputPressL = input.ControllerVR(validResponses=['left_trigger'],mode="press")
sed -i "181s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 182
# I propose to replace validResponses by valid_responses in :     myInputPressR = input.ControllerVR(validResponses=['right_trigger'],mode="press")
sed -i "182s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 183
# I propose to replace validResponses by valid_responses in :     endPausePress = input.ControllerVR(validResponses=['left_trigger','right_trigger'])
sed -i "183s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 186
# I propose to replace validResponses by valid_responses in :     myInputPressL = input.Keyboard(validResponses=['left'],mode="press")
sed -i "186s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 187
# I propose to replace validResponses by valid_responses in :     myInputPressR = input.Keyboard(validResponses=['right'],mode="press")
sed -i "187s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 188
# I propose to replace validResponses by valid_responses in :     endPausePress = input.Keyboard(validResponses=['space'],mode="press")
sed -i "188s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 914
# I propose to replace validResponses by valid_responses in :     #isplay=input.UserOptionDisplay(events=input.HTCViveEvent(validResponses=["left_trigger", "right_trigger"]))
sed -i "914s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 917
# I propose to replace validResponses by valid_responses in :     controllerVREndScene = input.ControllerVR(validResponses=['right_trigger', 'left_trigger'], mode="press")
sed -i "917s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 969
# I propose to replace validResponses by valid_responses in :     #events=input.HTCViveEvent (validResponses=["left_trigger", "right_trigger"]))
sed -i "969s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 972
# I propose to replace validResponses by valid_responses in :     controllerVREndScene = input.ControllerVR(validResponses=['right_trigger', 'left_trigger'], mode="press")
sed -i "972s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/adaptative.py, at line 42
# I propose to replace validResponses by valid_responses in :                 myInputPress = input.Keyboard(validResponses=['space'],mode="press")
sed -i "42s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/adaptative.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/adaptative.py, at line 50
# I propose to replace validResponses by valid_responses in :                 myInputPress = input.Keyboard(validResponses=['space'],mode="press")
sed -i "50s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/adaptative.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/adaptative.py, at line 61
# I propose to replace validResponses by valid_responses in :                 myInputPress = input.Keyboard(validResponses=['space'],mode="press")
sed -i "61s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/adaptative.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py, at line 60
# I propose to replace validResponses by valid_responses in :    # fixscene = PTVR.Stimuli.World.FixationScene(text= "Have you calibrated the eyetracker? \n if yes, please orient your head to the cross\n and press trigger to launch trial",background_color=bgColor, display=input.UserOptionDisplay(events=input.HTCViveEvent(validResponses=["right_trigger", "left_trigger"])))
sed -i "60s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py, at line 67
# I propose to replace validResponses by valid_responses in :    #                                (validResponses=["left_trigger", "right_trigger"])), controllers_visibility=False)
sed -i "67s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py, at line 108
# I propose to replace validResponses by valid_responses in :                                display=input.UserOptionDisplay(events=input.KeyboardEvent(validResponses=['y'])), controllers_visibility=False)
sed -i "108s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py, at line 131
# I propose to replace validResponses by valid_responses in :                                    display = input.UserOptionDisplay(events=input.KeyboardEvent(validResponses=['y'])), controllers_visibility=False)
sed -i "131s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/miminal_test_for_ec.py, at line 48
# I propose to replace validResponses by valid_responses in : #     global_fixation_scene_col.append(PTVR.Stimuli.World.FixationScene(text=aText,background_color=bgColor, display=input.UserOptionDisplay(events=input.HTCViveEvent(validResponses=["right_trigger", "left_trigger"]))))
sed -i "48s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/miminal_test_for_ec.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/miminal_test_for_ec.py, at line 64
# I propose to replace validResponses by valid_responses in :                                    (validResponses=["left_trigger", "right_trigger"])), controllers_visibility=False)
sed -i "64s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/miminal_test_for_ec.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/simple_demo.py, at line 67
# I propose to replace validResponses by valid_responses in :         myInput = input.ControllerVR(validResponses=["left_trigger", "right_trigger"])
sed -i "67s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/simple_demo.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/tangent_screen_hides_too_small_point.py, at line 69
# I propose to replace validResponses by valid_responses in :             #display=input.UserOptionDisplay(events=input.HTCViveEvent (validResponses=["left_trigger", "right_trigger"])), 
sed -i "69s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/tangent_screen_hides_too_small_point.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/tangent_screen_hides_too_small_point.py, at line 70
# I propose to replace validResponses by valid_responses in :             display=input.UserOptionDisplay(events=input.KeyboardEvent (validResponses=['y','n'])),                              
sed -i "70s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/tangent_screen_hides_too_small_point.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/TestCameraEvent.py, at line 39
# I propose to replace validResponses by valid_responses in :                                         (validResponses=["left_touch_press", "right_touch_press"])))
sed -i "39s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/TestCameraEvent.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/TestVisualObject.py, at line 39
# I propose to replace validResponses by valid_responses in :                                         (validResponses=["left_touch_press", "right_touch_press"])))
sed -i "39s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/TestVisualObject.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Test_Input.py, at line 43
# I propose to replace validResponses by valid_responses in :     visualScene1 = PTVR.Stimuli.World.VisualScene(id=1, display=input.UserOptionDisplay(events=input.KeyboardEvent(validResponses=['y', 'n'])))
sed -i "43s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/Test_Input.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Test_Input.py, at line 48
# I propose to replace validResponses by valid_responses in :     visualScene2 = PTVR.Stimuli.World.VisualScene(id=2, display=input.UserOptionDisplay(events=input.HTCViveEvent(validResponses=["left_touch_press", "right_touch_press"])))
sed -i "48s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/Test_Input.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Test_Warning_XYangularSize.py, at line 28
# I propose to replace validResponses by valid_responses in :             #display=input.UserOptionDisplay(events=input.HTCViveEvent (validResponses=["left_trigger", "right_trigger"])), 
sed -i "28s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/Test_Warning_XYangularSize.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Test_Warning_XYangularSize.py, at line 29
# I propose to replace validResponses by valid_responses in :             display=input.UserOptionDisplay(events=input.KeyboardEvent (validResponses=['y','n'])),                              
sed -i "29s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/Test_Warning_XYangularSize.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/mnreadformat_only_if_SetXheightAngularSize.py, at line 52
# I propose to replace validResponses by valid_responses in :     my_input = PTVR.Data.Input.Keyboard(validResponses=['space'])
sed -i "52s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/Text/mnreadformat_only_if_SetXheightAngularSize.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_alignment.py, at line 96
# I propose to replace validResponses by valid_responses in :     my_input = PTVR.Data.Input.Keyboard(validResponses=['space'])
sed -i "96s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_alignment.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_problems_with_coordinates.py, at line 57
# I propose to replace validResponses by valid_responses in :                                         (validResponses=['y','n']))
sed -i "57s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_problems_with_coordinates.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_problems_with_coordinates.py, at line 58
# I propose to replace validResponses by valid_responses in :         #display= input.HTCViveEvent (validResponses=["right_trigger","left_trigger"])        
sed -i "58s/validResponses/valid_responses/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_problems_with_coordinates.py
