#!/bin/bash

# RENAMING COMMANDS
# GOAL: imageFile -----> image_file
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 554
# I propose to replace imageFile by image_file in :     cue = PTVR.Stimuli.World.Sprite(size_in_meters=cueSize,imageFile=cueImage, 
sed -i "554s/imageFile/image_file/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 557
# I propose to replace imageFile by image_file in :     stim = PTVR.Stimuli.World.Sprite(size_in_meters=stimSize,imageFile=stimImage, 
sed -i "557s/imageFile/image_file/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 564
# I propose to replace imageFile by image_file in :         mask = PTVR.Stimuli.World.Sprite(size_in_meters = maskSize, imageFile = maskImage, 
sed -i "564s/imageFile/image_file/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 745
# I propose to replace imageFile by image_file in :         cue = PTVR.Stimuli.World.Sprite(size_in_meters=cueSize,imageFile=cueImage, position=cuePos, 
sed -i "745s/imageFile/image_file/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 748
# I propose to replace imageFile by image_file in :         stim = PTVR.Stimuli.World.Sprite(size_in_meters=stimSize,imageFile=stimImage, 
sed -i "748s/imageFile/image_file/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 758
# I propose to replace imageFile by image_file in :         mask = PTVR.Stimuli.World.Sprite(size_in_meters = maskSize, imageFile = maskImage, 
sed -i "758s/imageFile/image_file/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 417
# I propose to replace imageFile by image_file in :                                           imageFile = world_frame_image_H, 
sed -i "417s/imageFile/image_file/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 421
# I propose to replace imageFile by image_file in :                                           imageFile = world_frame_image_V, 
sed -i "421s/imageFile/image_file/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/TestVisualObject.py, at line 74
# I propose to replace imageFile by image_file in :   #  sprite_test = PTVR.Stimuli.World.Sprite (scale = 1,imageFile = "Scotome.png",position = (2.0,1.0,-1.0))
sed -i "74s/imageFile/image_file/" ../PTVR_Researchers/Python_Scripts/Tests/TestVisualObject.py
