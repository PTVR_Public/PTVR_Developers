#!/bin/bash

# RENAMING COMMANDS
# GOAL: isEventPausable -----> is_event_pausable
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 220
# I propose to replace isEventPausable by is_event_pausable in : endEventUnPausable = PTVR.Data.Event.EndCurrentScene(isEventPausable=False)
sed -i "220s/isEventPausable/is_event_pausable/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py
