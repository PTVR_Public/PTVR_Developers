#!/bin/bash

# RENAMING COMMANDS
# GOAL: isKeepPerimetricAngles -----> is_keep_perimetric_angles
# DATE: 04/28/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/objects_placed_on_tangent_screens.py, at line 88
# I propose to replace isKeepPerimetricAngles by is_keep_perimetric_angles in :     myTangentScreen.SetObjectOnTangentScreen (myObject_1, eccentricity = 0, halfMeridian= 0,isKeepPerimetricAngles=(True))
sed -i "88s/isKeepPerimetricAngles/is_keep_perimetric_angles/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/objects_placed_on_tangent_screens.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/objects_placed_on_tangent_screens.py, at line 90
# I propose to replace isKeepPerimetricAngles by is_keep_perimetric_angles in :     myTangentScreen.SetObjectOnTangentScreen (myObject_2, eccentricity = 8, halfMeridian= 45 ,isKeepPerimetricAngles=(True)) # 45
sed -i "90s/isKeepPerimetricAngles/is_keep_perimetric_angles/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/objects_placed_on_tangent_screens.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Calibration_Test_With_Flat_Screens.py, at line 61
# I propose to replace isKeepPerimetricAngles by is_keep_perimetric_angles in :     flatScreen.SetObjectOnScreen(rightbottom, eccentricity = 18, halfmeridian=315, isKeepPerimetricAngles = True)
sed -i "61s/isKeepPerimetricAngles/is_keep_perimetric_angles/" ../PTVR_Researchers/Python_Scripts/Tests/Calibration_Test_With_Flat_Screens.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/flatScreenPlacement.py, at line 80
# I propose to replace isKeepPerimetricAngles by is_keep_perimetric_angles in :     flatScreen.SetObjectOnScreen(rightbottom, eccentricity = 8, halfmeridian=315, isKeepPerimetricAngles = True)
sed -i "80s/isKeepPerimetricAngles/is_keep_perimetric_angles/" ../PTVR_Researchers/Python_Scripts/Tests/flatScreenPlacement.py
