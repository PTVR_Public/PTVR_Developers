#!/bin/bash

# RENAMING COMMANDS
# GOAL: idNextScene -----> id_next_scene
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Center_object.py, at line 37
# I propose to replace idNextScene by id_next_scene in :     myEvent2.SetNextScene(idNextScene=0)
sed -i "37s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Demos/Center_object.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventEndCurrentScene.py, at line 53
# I propose to replace idNextScene by id_next_scene in :         my_event.SetNextScene(idNextScene = exp.id_scene[0]) # go the the first scene of the loop 
sed -i "53s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventEndCurrentScene.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventEndCurrentTrial.py, at line 53
# I propose to replace idNextScene by id_next_scene in :                     my_event_end_current_scene.SetNextScene(idNextScene = exp.id_scene[0]) # go the the first scene of the loop 
sed -i "53s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventEndCurrentTrial.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputTimed.py, at line 57
# I propose to replace idNextScene by id_next_scene in :        my_event.SetNextScene(idNextScene=exp.id_scene[0])    
sed -i "57s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputTimed.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_Calibration_based_on_collision.py, at line 132
# I propose to replace idNextScene by id_next_scene in :             my_event_next_scene.SetNextScene(idNextScene=exp.id_scene[0])
sed -i "132s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_Calibration_based_on_collision.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Scene/cyclic_simplified_timing_scene.py, at line 39
# I propose to replace idNextScene by id_next_scene in :        my_scene.SetNextScene ( idNextScene = exp.id_scene[0] )
sed -i "39s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Demos/Scene/cyclic_simplified_timing_scene.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Scene/SimplifiedTimedScene.py, at line 37
# I propose to replace idNextScene by id_next_scene in :        my_scene.SetNextScene(idNextScene= exp.id_scene[0])
sed -i "37s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Demos/Scene/SimplifiedTimedScene.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center.py, at line 57
# I propose to replace idNextScene by id_next_scene in :     myEvent = PTVR.Data.Event.EndScene(idNextScene=myScene.id+1)
sed -i "57s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center.py, at line 59
# I propose to replace idNextScene by id_next_scene in :         myEvent = PTVR.Data.Event.EndScene(idNextScene=0)
sed -i "59s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center_animTemps.py, at line 58
# I propose to replace idNextScene by id_next_scene in :     myEvent = PTVR.Data.Event.EndScene(idNextScene=myScene.id+1)
sed -i "58s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center_animTemps.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center_animTemps.py, at line 60
# I propose to replace idNextScene by id_next_scene in :         myEvent = PTVR.Data.Event.EndScene(idNextScene=0)
sed -i "60s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/visual_circular_cone_2m_center_animTemps.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/simple_demo.py, at line 70
# I propose to replace idNextScene by id_next_scene in :             myEvent2.SetNextScene(idNextScene=exp.id_scene[0])
sed -i "70s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Tests/simple_demo.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/mnreadformat_only_if_SetXheightAngularSize.py, at line 53
# I propose to replace idNextScene by id_next_scene in :     my_event = PTVR.Data.Event.EndScene(idNextScene=(visScene.id+1))
sed -i "53s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Tests/Text/mnreadformat_only_if_SetXheightAngularSize.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_alignment.py, at line 98
# I propose to replace idNextScene by id_next_scene in :     #my_event = event.EndScene(idNextScene=(start_scene.id+1))
sed -i "98s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_alignment.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_alignment.py, at line 114
# I propose to replace idNextScene by id_next_scene in :                 #my_event = event.EndScene(idNextScene=(scene.id+1))
sed -i "114s/idNextScene/id_next_scene/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_alignment.py
