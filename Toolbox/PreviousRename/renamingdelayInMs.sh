#!/bin/bash

# RENAMING COMMANDS
# GOAL: delayInMs -----> delay_in_ms
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventEndCurrentScene.py, at line 47
# I propose to replace delayInMs by delay_in_ms in :     my_input_press = input.Timed (delayInMs = duration_scene)
sed -i "47s/delayInMs/delay_in_ms/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventEndCurrentScene.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputTimed.py, at line 52
# I propose to replace delayInMs by delay_in_ms in :    my_input_timed = input.Timed (delayInMs=scene_duration_in_ms)
sed -i "52s/delayInMs/delay_in_ms/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputTimed.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/infinite_circular_cone_16deg_eccentricity_10deg.py, at line 141
# I propose to replace delayInMs by delay_in_ms in :     myInput= PTVR.Data.Input.Timed (delayInMs = 100)
sed -i "141s/delayInMs/delay_in_ms/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/infinite_circular_cone_16deg_eccentricity_10deg.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 173
# I propose to replace delayInMs by delay_in_ms in : delayInMsBeforeOut= 100000
sed -i "173s/delayInMs/delay_in_ms/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 767
# I propose to replace delayInMs by delay_in_ms in :         timedInput.delayInMs = timePoint
sed -i "767s/delayInMs/delay_in_ms/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 799
# I propose to replace delayInMs by delay_in_ms in :         timedInput.delayInMs = timeCue
sed -i "799s/delayInMs/delay_in_ms/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 814
# I propose to replace delayInMs by delay_in_ms in :         timedInput.delayInMs = timeStim
sed -i "814s/delayInMs/delay_in_ms/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 834
# I propose to replace delayInMs by delay_in_ms in :         timedInput.delayInMs = timeMask
sed -i "834s/delayInMs/delay_in_ms/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 854
# I propose to replace delayInMs by delay_in_ms in :         timedInput.delayInMs = delayInMsBeforeOut
sed -i "854s/delayInMs/delay_in_ms/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py
