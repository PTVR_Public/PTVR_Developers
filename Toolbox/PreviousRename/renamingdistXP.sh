#!/bin/bash

# RENAMING COMMANDS
# GOAL: distXP -----> distance_xp
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 142
# I propose to replace distXP by distance_xp in : distXP=500 #experiment distance, 500m is the maximum, giving minimum parallaxe effect.
sed -i "142s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 147
# I propose to replace distXP by distance_xp in : # 1.5 in meters. Not important for distXP>>0, a priori ne pas toucher
sed -i "147s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 195
# I propose to replace distXP by distance_xp in : fixationPointPos = np.array([ 0,  0, distXP]) # position RELATIVE par rapport
sed -i "195s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 201
# I propose to replace distXP by distance_xp in : myTarget = PTVR.Stimuli.World.Sphere(is_visible = False,size_in_meters=distXP/2)
sed -i "201s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 203
# I propose to replace distXP by distance_xp in : myTarget.SetSphericalCoordinates(radialDistance = distXP,eccentricity =abs(elevation_theta), halfMeridian = np.sign(elevation_theta)*90)
sed -i "203s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 204
# I propose to replace distXP by distance_xp in : pointedAtInputPress = input.PointedAt(radius_deg=commonRadius,depth = distXP, targetId=myTarget.id, mode="press",)
sed -i "204s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 205
# I propose to replace distXP by distance_xp in : pointedAtInputRelease = input.PointedAt(radius_deg=commonRadius,depth =distXP , targetId=myTarget.id, mode="release")
sed -i "205s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 270
# I propose to replace distXP by distance_xp in : textPos = fixationPointPos + np.array([0,angToHeight(text_elevation_theta, distXP),0])
sed -i "270s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 406
# I propose to replace distXP by distance_xp in : pointSize = angToHeight (pointSizeDegree, distXP) # convert degrees of visual angle (dva) to a viewing distance (from head to object)
sed -i "406s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 418
# I propose to replace distXP by distance_xp in : cueSize = angToHeight (cueSizeDegree, distXP) #""
sed -i "418s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 431
# I propose to replace distXP by distance_xp in : stimSize = angToHeight (stimSizeDegree, distXP) #""
sed -i "431s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 539
# I propose to replace distXP by distance_xp in :     myTangentScreen = PTVR.Stimuli.World.FlatScreen(is_visible=TangentScreenIsVisible,size_in_meters=distXP/2)
sed -i "539s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 542
# I propose to replace distXP by distance_xp in :     myTangentScreen.SetSphericalCoordinates(radialDistance = distXP,eccentricity =abs(elevation_theta), halfMeridian = np.sign(elevation_theta)*90)
sed -i "542s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 627
# I propose to replace distXP by distance_xp in :             distXP*np.sin(elevation_theta/180*np.pi), 
sed -i "627s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 628
# I propose to replace distXP by distance_xp in :             distXP*np.cos(elevation_theta/180*np.pi)]) 
sed -i "628s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 645
# I propose to replace distXP by distance_xp in :     #textNormalization = 256/50*distXP 
sed -i "645s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 654
# I propose to replace distXP by distance_xp in :     angle = angToHeight(text_elevation_theta, distXP)
sed -i "654s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 699
# I propose to replace distXP by distance_xp in :         cuePos  = np.array([0, angToHeight(cueAngle, distXP), distXP])  
sed -i "699s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 701
# I propose to replace distXP by distance_xp in :         stimPos = np.array([0, angToHeight(stimAngle,distXP), distXP]) 
sed -i "701s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 111
# I propose to replace distXP by distance_xp in : # rule of thumb
sed -i "111s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 164
# I propose to replace distXP by distance_xp in : # 1.5 in meters. Not important for distXP>>0, a priori ne pas toucher
sed -i "164s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 166
# I propose to replace distXP by distance_xp in : distXP = 500
sed -i "166s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 169
# I propose to replace distXP by distance_xp in : #fixationPointPos = np.array([ 0,  0, distXP]) # position RELATIVE par rapport à la tête
sed -i "169s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 172
# I propose to replace distXP by distance_xp in : camEvtPos_wrt_OWoCS = np.array([0, 0, distXP]) + headPos  
sed -i "172s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 240
# I propose to replace distXP by distance_xp in :     viewing_distance_in_m = distXP)
sed -i "240s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 409
# I propose to replace distXP by distance_xp in :     viewing_distance_in_m = distXP)
sed -i "409s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 557
# I propose to replace distXP by distance_xp in : projScreen = PTVR.Stimuli.World.FlatScreen(position = np.array([0,0,distXP]), 
sed -i "557s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 607
# I propose to replace distXP by distance_xp in : #     #perpDist = angToDist (eccentricity/pi*180, distXP) 
sed -i "607s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 610
# I propose to replace distXP by distance_xp in : #         viewing_distance_in_m   = distXP)
sed -i "610s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 644
# I propose to replace distXP by distance_xp in : #         viewing_distance_in_m   = distXP)
sed -i "644s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 664
# I propose to replace distXP by distance_xp in : #         viewing_distance_in_m   = distXP)
sed -i "664s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 693
# I propose to replace distXP by distance_xp in :                             distXP)
sed -i "693s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 713
# I propose to replace distXP by distance_xp in :                             distXP)            
sed -i "713s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 825
# I propose to replace distXP by distance_xp in :     # note 
sed -i "825s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 912
# I propose to replace distXP by distance_xp in :     #block_text = PTVR.Stimuli.World.TextBox( block_text_str, font_size=256, position = np.array([0,0,distXP/10]), rotation = [0,0,0], font_color=color.RGBColor(r=1, g=1, b=1, a=1))
sed -i "912s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 913
# I propose to replace distXP by distance_xp in :     block_text = PTVR.Stimuli.World.Text(text= block_text_str, position = np.array([0,0,distXP/10]), color=color.RGBColor(r=1, g=1, b=1, a=1))
sed -i "913s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 960
# I propose to replace distXP by distance_xp in :     #block_text = PTVR.Stimuli.World.TextBox( end_block_text_str, font_size=256, position = np.array([0,0,distXP/10]), rotation = [0,0,0], font_color=color.RGBColor(r=1, g=1, b=1, a=1))
sed -i "960s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 961
# I propose to replace distXP by distance_xp in :     block_text = PTVR.Stimuli.World.Text(text= end_block_text_str, position = np.array([0,0,distXP/10]), rotation = [0,0,0], color=color.RGBColor(r=1, g=1, b=1, a=1))
sed -i "961s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/TestAngle.py, at line 36
# I propose to replace distXP by distance_xp in : distXP = 500
sed -i "36s/distXP/distance_xp/" ../PTVR_Researchers/Python_Scripts/Tests/TestAngle.py
