#!/bin/bash

# RENAMING COMMANDS
# GOAL: isFacingOrigin -----> is_facing_origin
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/shift_and_rotate_coordinate_systems.py, at line 65
# I propose to replace isFacingOrigin by is_facing_origin in :     myText_lookBehind.SetOrientation(isFacingOrigin=True) # This line is helpful to avoid calculating the orientation
sed -i "65s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Coordinate_Systems/shift_and_rotate_coordinate_systems.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/new_typography_comparison.py, at line 42
# I propose to replace isFacingOrigin by is_facing_origin in :     legend.SetOrientation(isFacingHeadPOV = False, isFacingOrigin = True)
sed -i "42s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/new_typography_comparison.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_tangent.py, at line 3
# I propose to replace isFacingOrigin by is_facing_origin in : Goal 
sed -i "3s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_tangent.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_tangent.py, at line 63
# I propose to replace isFacingOrigin by is_facing_origin in :     text2.isFacingOrigin=True # The text is facing the origin
sed -i "63s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_tangent.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 133
# I propose to replace isFacingOrigin by is_facing_origin in :     myNearText.isFacingOrigin = True
sed -i "133s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 141
# I propose to replace isFacingOrigin by is_facing_origin in :     myNearCube.isFacingOrigin = True
sed -i "141s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 149
# I propose to replace isFacingOrigin by is_facing_origin in :     myFarText.isFacingOrigin = True
sed -i "149s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 159
# I propose to replace isFacingOrigin by is_facing_origin in :     myFarCube.isFacingOrigin = True
sed -i "159s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 167
# I propose to replace isFacingOrigin by is_facing_origin in :     myLegendToTheLeft.isFacingOrigin=True
sed -i "167s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 183
# I propose to replace isFacingOrigin by is_facing_origin in :     myNearText.isFacingOrigin = True
sed -i "183s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 197
# I propose to replace isFacingOrigin by is_facing_origin in :     myNearCube.isFacingOrigin = True
sed -i "197s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 204
# I propose to replace isFacingOrigin by is_facing_origin in :     myFarText.isFacingOrigin = True  
sed -i "204s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 211
# I propose to replace isFacingOrigin by is_facing_origin in :     myFarCube.isFacingOrigin = True
sed -i "211s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py, at line 218
# I propose to replace isFacingOrigin by is_facing_origin in :     myLegendToTheRight.isFacingOrigin=True
sed -i "218s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Demos/Text/text_height_at_different_radial_distances.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_orientation.py, at line 42
# I propose to replace isFacingOrigin by is_facing_origin in :     text1.SetOrientation(isFacingHeadPOV = False, isFacingOrigin = True)
sed -i "42s/isFacingOrigin/is_facing_origin/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_orientation.py
