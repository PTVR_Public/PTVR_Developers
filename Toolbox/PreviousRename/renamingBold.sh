#!/bin/bash

# RENAMING COMMANDS
# GOAL: bold -----> is_bold
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectText.py, at line 37
# I propose to replace bold by is_bold in : text_activate_is_bold= True
sed -i "37s/bold/is_bold/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectText.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectText.py, at line 53
# I propose to replace bold by is_bold in :     my_event_activate = event.ChangeObjectText(text_object_id=my_text.id,text=text_activate,is_bold=text_activate_is_bold, is_italic=text_activate_is_italic, effect = "activate")
sed -i "53s/bold/is_bold/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectText.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_Constant.py, at line 135
# I propose to replace bold by is_bold in :                                                    bold=True,
sed -i "135s/bold/is_bold/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_Constant.py
