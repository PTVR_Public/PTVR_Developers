#!/bin/bash

# RENAMING COMMANDS
# GOAL: isVisible -----> is_visible
# DATE: 04/28/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Flat_Screens/objects_placed_on_flat_screens.py, at line 74
# I propose to replace isVisible by is_visible in :     myFlatScreen = PTVR.Stimuli.World.FlatScreen(position = np.array([0,0, radialDistance_m]), isVisible = True, 
sed -i "74s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Demos/Flat_Screens/objects_placed_on_flat_screens.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_placed_on_tangent_screens.py, at line 66
# I propose to replace isVisible by is_visible in :     myTangentScreen = PTVR.Stimuli.World.FlatScreen ( isVisible = True, 
sed -i "66s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Demos/Tangent_Screens/text_placed_on_tangent_screens.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/base_infinite_cone_demo.py, at line 58
# I propose to replace isVisible by is_visible in :                                                 isVisible=True,
sed -i "58s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/base_infinite_cone_demo.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/infinite_circular_cone_16deg_eccentricity_10deg.py, at line 42
# I propose to replace isVisible by is_visible in :                                                isVisible=True,
sed -i "42s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/infinite_circular_cone_16deg_eccentricity_10deg.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/infinite_circular_cone_ellipse_on_screen.py, at line 54
# I propose to replace isVisible by is_visible in :                                                isVisible=True,
sed -i "54s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Demos/Visual_Angles/infinite_circular_cone_ellipse_on_screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/vestibular.py, at line 31
# I propose to replace isVisible by is_visible in : myTangentScreen = PTVR.Stimuli.World.FlatScreen(position = np.array([0,0, 505]), isVisible = True, 
sed -i "31s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Experiments/vestibular.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Calibration_Test_With_Flat_Screens.py, at line 44
# I propose to replace isVisible by is_visible in :     flatScreen = PTVR.Stimuli.World.FlatScreen(position = np.array([0, HeadHeight,1.0]), isVisible = True, scale=np.array([1.3,1.3]), color = color.RGBColor(r=0.8, g=0.8, b=0.8, a=1))
sed -i "44s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Tests/Calibration_Test_With_Flat_Screens.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Calibration_Test_With_Flat_Screens.py, at line 68
# I propose to replace isVisible by is_visible in :         position = np.array([0, HeadHeight, 1.0]), isVisible = True, scale=np.array([1.3,1.3]), color = color.RGBColor(r=0.8, g=0.8, b=0.8, a=1))
sed -i "68s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Tests/Calibration_Test_With_Flat_Screens.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/flatScreenPlacement.py, at line 48
# I propose to replace isVisible by is_visible in :     flatScreen = PTVR.Stimuli.World.FlatScreen(position = np.array([0,0,5.0]), isVisible = True, scale=np.array([2,2]), color = color.RGBColor(r=0.8, g=0.8, b=0.8, a=1), getData = True)
sed -i "48s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Tests/flatScreenPlacement.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/flatScreenPlacement.py, at line 49
# I propose to replace isVisible by is_visible in :     tangentScreen = PTVR.Stimuli.World.TangentScreen(position = np.array([5,5,5.0]), isVisible = True, scale=np.array([2,2]), color = color.RGBColor(r=0.8, g=0.8, b=0.8, a=1), getData = True)
sed -i "49s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Tests/flatScreenPlacement.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py, at line 69
# I propose to replace isVisible by is_visible in :    # tangentScreen = PTVR.Stimuli.World.TangentScreen(position = np.array([0,0, radialDistance_m]), isVisible = False, 
sed -i "69s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py, at line 105
# I propose to replace isVisible by is_visible in :     tangentScreen = PTVR.Stimuli.World.TangentScreen(position = np.array([0,0, radialDistance_m]), isVisible = True, 
sed -i "105s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Tests/gaze_Position_Accuracy_On_Tangent_Screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Tangent_Screens/place_object_on_tangent_screen.py, at line 40
# I propose to replace isVisible by is_visible in :         position = np.array([0,1, 1]), isVisible = True, 
sed -i "40s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Tests/Tangent_Screens/place_object_on_tangent_screen.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/tangent_screen_hides_too_small_point.py, at line 73
# I propose to replace isVisible by is_visible in :     myTangentScreen = PTVR.Stimuli.World.TangentScreen(position = np.array([0,0, radialDistance_m]), isVisible = True, 
sed -i "73s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Tests/tangent_screen_hides_too_small_point.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_problems_with_coordinates.py, at line 60
# I propose to replace isVisible by is_visible in :     myTangentScreen = PTVR.Stimuli.World.TangentScreen(position = np.array([0,0, radialDistance_m]), isVisible = True, 
sed -i "60s/isVisible/is_visible/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_problems_with_coordinates.py
