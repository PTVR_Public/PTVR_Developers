#!/bin/bash

# RENAMING COMMANDS
# GOAL: textboxWidth_m -----> textbox_width
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py, at line 65
# I propose to replace textboxWidth_m by textbox_width in :     text1.SetTextBoxParameters(textboxWidth_m = 1, textboxHeight_m=0.5, isWrapping= False)
sed -i "65s/textboxWidth_m/textbox_width/" ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py, at line 68
# I propose to replace textboxWidth_m by textbox_width in :     text2.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= False) 
sed -i "68s/textboxWidth_m/textbox_width/" ../PTVR_Researchers/Python_Scripts/Tests/Text/textbox_properties.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py, at line 49
# I propose to replace textboxWidth_m by textbox_width in :     text1.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
sed -i "49s/textboxWidth_m/textbox_width/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py, at line 55
# I propose to replace textboxWidth_m by textbox_width in :     text2.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
sed -i "55s/textboxWidth_m/textbox_width/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py

# In the file ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py, at line 60
# I propose to replace textboxWidth_m by textbox_width in :     text3.SetTextBoxParameters(textboxWidth_m=1, textboxHeight_m=0.5, isWrapping= True)
sed -i "60s/textboxWidth_m/textbox_width/" ../PTVR_Researchers/Python_Scripts/Tests/Text/text_position.py
