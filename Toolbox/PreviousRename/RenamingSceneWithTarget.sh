#!/bin/bash

# RENAMING COMMANDS
# GOAL: SceneWithTarget -----> scene_with_target
# DATE: 05/04/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursor_verification_head_gaze.py, at line 94
# I propose to replace SceneWithTarget by scene_with_target in : # SceneWithTarget = Scene with a target of given eccentricity and halfMeridian on TangentScreen#
sed -i "94s/SceneWithTarget/scene_with_target/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursor_verification_head_gaze.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursor_verification_head_gaze.py, at line 96
# I propose to replace SceneWithTarget by scene_with_target in : def SceneWithTarget (exp,ecc,hm)
sed -i "96s/SceneWithTarget/scene_with_target/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursor_verification_head_gaze.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursor_verification_head_gaze.py, at line 179
# I propose to replace SceneWithTarget by scene_with_target in :         SceneWithTarget(exp=exp, ecc=0, hm=0)
sed -i "179s/SceneWithTarget/scene_with_target/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursor_verification_head_gaze.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursor_verification_head_gaze.py, at line 182
# I propose to replace SceneWithTarget by scene_with_target in :         SceneWithTarget(exp=exp, ecc=eccentricity, hm=hm_step*count)
sed -i "182s/SceneWithTarget/scene_with_target/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursor_verification_head_gaze.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursor_verification_head_gaze.py, at line 184
# I propose to replace SceneWithTarget by scene_with_target in :     SceneWithTarget(exp=exp, ecc=0, hm=0)
sed -i "184s/SceneWithTarget/scene_with_target/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/pointing_cursor_verification_head_gaze.py
