#!/bin/bash

# RENAMING COMMANDS
# GOAL: isActive -----> is_active
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectColor.py, at line 42
# I propose to replace isActive by is_active in :     my_object = PTVR.Stimuli.World.Cube(isActive= True,color=color.RGBColor(1.0, 0.0, 0.0, 1),position=np.array([0.0,3.0,10.0]))
sed -i "42s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectColor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectEnable.py, at line 43
# I propose to replace isActive by is_active in :     my_object = PTVR.Stimuli.World.Cube(isActive= True,color=color.RGBColor(1.0, 0.0, 0.0, 1),position=np.array([0.0,0.0,10.0]))
sed -i "43s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectEnable.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectVisibility.py, at line 43
# I propose to replace isActive by is_active in :     my_object = PTVR.Stimuli.World.Cube(isActive= True,color=color.RGBColor(1.0, 0.0, 0.0, 1),position=np.array([0.0,1.0,10.0]))
sed -i "43s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectVisibility.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_false_words.py, at line 116
# I propose to replace isActive by is_active in :             UI_BUTTON_WRONG = input.ButtonUI(position=virtualPoint1, color = color.RGBColor(r=1.0,g=0.0,b=0.0,a=1.0), isActive = False)
sed -i "116s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_Related/UI_false_words.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py, at line 483
# I propose to replace isActive by is_active in :     quit_button = interface.quit_button(isActive=True)
sed -i "483s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interactions.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 166
# I propose to replace isActive by is_active in :                                        isActive = True,
sed -i "166s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 193
# I propose to replace isActive by is_active in :                                        isActive = False,
sed -i "193s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 244
# I propose to replace isActive by is_active in :                                                ,isActive=False)
sed -i "244s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 253
# I propose to replace isActive by is_active in :                                                         isActive=False)
sed -i "253s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 266
# I propose to replace isActive by is_active in : def quit_button(isActive=False)
sed -i "266s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 271
# I propose to replace isActive by is_active in :                                                isActive = isActive
sed -i "271s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 294
# I propose to replace isActive by is_active in :                                                isActive = False
sed -i "294s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 319
# I propose to replace isActive by is_active in :                                                isActive=False
sed -i "319s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 332
# I propose to replace isActive by is_active in :                                                isActive=False
sed -i "332s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 355
# I propose to replace isActive by is_active in :                                                isActive=False
sed -i "355s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 365
# I propose to replace isActive by is_active in :                                                     isActive = False,
sed -i "365s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 372
# I propose to replace isActive by is_active in :                                                     isActive = False,
sed -i "372s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py, at line 407
# I propose to replace isActive by is_active in :                                                isActive = False
sed -i "407s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/MNRead/MNREAD_VR_Experiment/mnread_vr_interface.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_Verification_Head_Gaze.py, at line 109
# I propose to replace isActive by is_active in :                                        isActive = True,
sed -i "109s/isActive/is_active/" ../PTVR_Researchers/Python_Scripts/Demos/PointingCursor/Pointing_Cursor_Verification_Head_Gaze.py
