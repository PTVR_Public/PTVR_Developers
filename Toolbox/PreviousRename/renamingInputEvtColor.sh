#!/bin/bash

# RENAMING COMMANDS
# GOAL: evtColor -----> evt_color
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeBackgroundColor.py, at line 43
# I propose to replace evtColor by evt_color in :     my_event_activate = event.ChangeBackgroundColor(evtColor=color.RGBColor(r=0.7,g= 0.0,b= 0.0,a= 1.0),effect = "activate")
sed -i "43s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeBackgroundColor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectColor.py, at line 47
# I propose to replace evtColor by evt_color in :     my_event_activate = event.ChangeObjectColor(evtColor=color.RGBColor(0.0, 1.0, 0.0, 1),objectId= my_object.id,effect = "activate")
sed -i "47s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventChangeObjectColor.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSaveText.py, at line 47
# I propose to replace evtColor by evt_color in :     myEventActivate = event.ChangeBackgroundColor(evtColor=color.RGBColor(1.0, 0.0, 0.0, 1),effect = "activate")
sed -i "47s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSaveText.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSaveText.py, at line 53
# I propose to replace evtColor by evt_color in :     my_event_deactivate = event.ChangeBackgroundColor(evtColor=color.RGBColor(1.0, 0.0, 0.0, 1),effect = "deactivate")
sed -i "53s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSaveText.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSuspendCurrentScene.py, at line 39
# I propose to replace evtColor by evt_color in :     my_event_change_background_color_activate = event.ChangeBackgroundColor(evtColor=color.RGBColor(1.0, 0.0, 0.0, 1),effect = "activate")
sed -i "39s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Event/EventSuspendCurrentScene.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputButtonUI.py, at line 56
# I propose to replace evtColor by evt_color in :     my_event_press = event.ChangeBackgroundColor(evtColor=color.RGBColor(r=1.0),effect = "activate")
sed -i "56s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputButtonUI.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputButtonUI.py, at line 57
# I propose to replace evtColor by evt_color in :     my_event_release = event.ChangeBackgroundColor(evtColor=color.RGBColor(r=1.0),effect = "deactivate")
sed -i "57s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputButtonUI.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputButtonUI.py, at line 63
# I propose to replace evtColor by evt_color in :     my_event_change_object_color_activate = event.ChangeObjectColor(evtColor=color.RGBColor(g=1.0), objectId=my_button.id, effect="activate")
sed -i "63s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputButtonUI.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputButtonUI.py, at line 64
# I propose to replace evtColor by evt_color in :     my_event_change_object_color_deactivate  = event.ChangeObjectColor(evtColor=color.RGBColor(g=1.0), objectId=my_button.id, effect="deactivate")
sed -i "64s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputButtonUI.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputControllerVR.py, at line 52
# I propose to replace evtColor by evt_color in :     my_event_change_background_color_activate = event.ChangeBackgroundColor(evtColor=color.RGBColor(r = 0.7), effect="activate")
sed -i "52s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputControllerVR.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Input/InputKeyboard.py, at line 49
# I propose to replace evtColor by evt_color in :     my_event_change_background_color_activate = event.ChangeBackgroundColor(evtColor=color.RGBColor(r = 0.7), effect="activate")
sed -i "49s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Input/InputKeyboard.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Objects/buttonUI.py, at line 61
# I propose to replace evtColor by evt_color in :     myEventPress = event.ChangeBackgroundColor(evtColor=color.RGBColor(r=1.0),effect = "activate")
sed -i "61s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Objects/buttonUI.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Objects/buttonUI.py, at line 62
# I propose to replace evtColor by evt_color in :     myEventRelease = event.ChangeBackgroundColor(evtColor=color.RGBColor(r=1.0),effect = "deactivate")
sed -i "62s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Objects/buttonUI.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Objects/buttonUI.py, at line 71
# I propose to replace evtColor by evt_color in :     myEventChangeObjectColorActivate = event.ChangeObjectColor(evtColor=color.RGBColor(g=1.0), objectId=my_button_press.id, effect="activate")
sed -i "71s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Objects/buttonUI.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Objects/buttonUI.py, at line 72
# I propose to replace evtColor by evt_color in :     myEventChangeObjectColorDeactivate  = event.ChangeObjectColor(evtColor=color.RGBColor(g=1.0), objectId=my_button_release.id, effect="deactivate")
sed -i "72s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Objects/buttonUI.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/PrintResultCustom.py, at line 56
# I propose to replace evtColor by evt_color in :     myEventPress = event.ChangeBackgroundColor(evtColor=color.RGBColor(r=1.0),effect = "activate")
sed -i "56s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/PrintResultCustom.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/PrintResultCustom.py, at line 57
# I propose to replace evtColor by evt_color in :     myEventRelease = event.ChangeBackgroundColor(evtColor=color.RGBColor(r=1.0),effect = "deactivate")
sed -i "57s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/PrintResultCustom.py

# In the file ../PTVR_Researchers/Python_Scripts/Demos/Result_File/PrintResultCustom.py, at line 74
# I propose to replace evtColor by evt_color in :     myEventTest= event.ChangeBackgroundColor(evtColor=color.RGBColor(r=1.0),effect = "activate")
sed -i "74s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Demos/Result_File/PrintResultCustom.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 209
# I propose to replace evtColor by evt_color in : ChangeBackgroundColorActivate = event.ChangeBackgroundColor(evtColor=color.RGBColor(1, 0.6, 0.6, 1), 
sed -i "209s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/posner.py, at line 211
# I propose to replace evtColor by evt_color in : ChangeBackgroundColorDeactivate = event.ChangeBackgroundColor(evtColor=bgColor,
sed -i "211s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Experiments/posner.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 780
# I propose to replace evtColor by evt_color in :     ChangeObjectColorEventDeactivate = PTVR.Data.Event.ChangeObjectColor(evtColor=dynColor, objectId=theSphere.id, effect="deactivate")
sed -i "780s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 790
# I propose to replace evtColor by evt_color in :     ChangeObjectColorEventActivate = PTVR.Data.Event.ChangeObjectColor(evtColor=dynColor, objectId=theSphere.id, effect="activate")
sed -i "790s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 921
# I propose to replace evtColor by evt_color in :     ChangeBackgroundEventActivate = PTVR.Data.Event.ChangeBackgroundColor(evtColor=color.RGBColor(0.5, 0.3, 0.3, 1),effect="activate")
sed -i "921s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py

# In the file ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py, at line 922
# I propose to replace evtColor by evt_color in :     ChangeBackgroundEventDeactivate = PTVR.Data.Event.ChangeBackgroundColor(evtColor=color.RGBColor(0.5, 0.3, 0.3, 1),effect="deactivate")
sed -i "922s/evtColor/evt_color/" ../PTVR_Researchers/Python_Scripts/Experiments/saroi.py
