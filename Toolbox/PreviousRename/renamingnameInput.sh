#!/bin/bash

# RENAMING COMMANDS
# GOAL: nameInput -----> name_input
# DATE: 04/27/2022

# In the file ../PTVR_Researchers/Python_Scripts/Demos/special_point_static_POV.py, at line 42
# I propose to replace nameInput by name_input in :     myInput = input.Keyboard(valid_responses=['space'], mode = "press", nameInput="titi")
sed -i "42s/nameInput/name_input/" ../PTVR_Researchers/Python_Scripts/Demos/special_point_static_POV.py
