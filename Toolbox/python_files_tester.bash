#!/bin/bash

list_of_python_files=$(find . -name *.py)
export PYTHONPATH=$PYTHONPATH:/home/ice/dev/PTVRproject/PTVR/PTVR_Researchers


for i in $list_of_python_files
do
    echo ${i%/*}
    mkdir -p /tmp/ptvr_temp/${i%/*}
   
    sed "s|PTVR.SystemUtils.LaunchExperiment|#PTVR.SystemUtils.LaunchExperiment|g;s|(experiment=exp)|()|g" $i > /tmp/ptvr_temp/$i
    python3 /tmp/ptvr_temp/$i

    if [[ $? != 0 ]]
    then
	list_fail="$list_fail:$i"
    fi

done

rm *json

echo $list_fail
